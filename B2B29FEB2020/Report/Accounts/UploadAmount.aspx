﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="UploadAmount.aspx.cs" Inherits="SprReports_Accounts_UploadAmount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <style>
       

        table {
            /*width: 750px;*/
            border-collapse: collapse;
            /*margin:50px auto;*/
        }

        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #0952a4;
            color: white;
            /*font-weight: bold;*/
        }

        td, th {
            padding: 2px;
            border: 1px solid #ccc;
            text-align: left;
            /*font-size: 18px;*/
        }


        @media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px) {

            table {
                width: 100%;
            }


            table, thead, tbody, th, td, tr {
                display: block;
                padding: inherit;
            }


                thead tr {
                    position: absolute;
                    top: -9999px;
                    left: -9999px;
                }

            tr {
                border: 1px solid #ccc;
            }

            td {
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
            }

                td:before {
                    position: absolute;
                    top: 6px;
                    left: 6px;
                    width: 45%;
                    padding-right: 10px;
                    white-space: nowrap;
                    content: attr(data-column);
                    color: #000;
                    font-weight: bold;
                }
        }
    </style>

  

    
  

    
    <div class="card-main">

      <div class="card-header">
                <h3  class="main-heading">Wallet Top-Up By Payment Gateway</h3>
          </div>
      
        <div class="inner-box wallet">
            <div class="col-md-12">
              
                    <div class="row">
                        <div class="col-md-4">
                            <label>Enter Amount</label>
                            <asp:TextBox ID="TxtAmout" runat="server" class="form-control" onkeypress="return keyRestrict(event,'0123456789');" MaxLength="11" Text="0" AutoCompleteType="Disabled" style="color:#000;"></asp:TextBox>
                         </div>

                         <div class="col-md-4">
                        <label>Payment Mode</label>
                   
                         <asp:DropDownList ID="droplist" class="form-control" runat="server"></asp:DropDownList>
                    </div>

                       
                     </div>

                <div class="row">
                     <div class="col-md-4 mt-3">
                          <b>Transaction Charges:</b> &nbsp; &nbsp;<asp:Label ID="lblTransCharges" runat="server"></asp:Label>
                        </div>   
                        <div class="col-md-4 mt-3">
                            <b>Total  Amount:</b>&nbsp; &nbsp;<asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                        </div>
                </div>
                </div>

             <div class="col-md-12">
                                                    
                                            <div class="btn-upload">
                                                 <asp:Button ID="BtnUpload" runat="server" CssClass="btn cmn-btn" Text="Upload Amount" OnClick="BtnUpload_Click"/>
                                            </div>
                                        </div>
            
            </div>

          
        </div>


              
              

                    
                    <%-- <div class="col-md-4  text-center ">
                </div>--%>
             
    <br />
    <br />
    <br />
    <br />



        <input type="hidden" id="TransCharges" name="TransCharges" />
        <input type="hidden" id="TotalAmount" name="TotalAmount" />
 
    

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

    <%-- <script type="text/javascript">        
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_TxtAmout").keypress(function () {
                GetPgTransCharge();
            });
        });
</script>--%>

    <script type="text/javascript">
        //function preventBack() { window.history.forward(); }
        //setTimeout("preventBack()", 0);
        //window.onunload = function () { null };
        //window.onload = function () {
        //    document.getElementById("ctl00_ContentPlaceHolder1_TxtAmout").name = "txt" + Math.random();
        //}
    </script>

    <script type="text/javascript">
        function ValidateAmount() {
            if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() == "") {
                alert("Plese enter upload amount.")
                return false;
            }
            if (parseFloat($("#ctl00_ContentPlaceHolder1_TxtAmout").val()) < 1) {
                alert("Plese enter amount greater than zero.")
                return false;
            }
            GetPgTransCharge();

            var bConfirm = confirm('Payment gateway transaction charges Rs. ' + $('#TransCharges').val() + ' and  Total Amount Rs. ' + $('#TotalAmount').val() + ' debit from your bank a/c, Are you sure upload amount?');
            if (bConfirm == true) {
                $("#ctl00_ContentPlaceHolder1_BtnUpload").hide();
                return true;
            }
            else {
                $("#ctl00_ContentPlaceHolder1_BtnUpload").show();
                return false;
            }
        }

        $("#ctl00_ContentPlaceHolder1_TxtAmout").keyup(function () {
            GetPgTransCharge();
            //if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() == "" && $("#ctl00_ContentPlaceHolder1_TxtAmout").val() == null) {
            //    var str = 0;
            //    $("#ctl00_ContentPlaceHolder1_TxtAmout").val(str);
            //}            
        })

        $("#ctl00_ContentPlaceHolder1_rblPaymentMode").click(function () {
            GetPgTransCharge();
        });
        $(function () {
            $("#ctl00_ContentPlaceHolder1_droplist").change(function () {
                GetPgTransCharge();
            });
        });
        function GetPgTransCharge() {
            debugger;
            //var checked_radio = $("[id*=ctl00_ContentPlaceHolder1_rblPayMode] input:checked");
            //var PaymentMode = checked_radio.val();
            var checked_dropdown = $("[id*=ctl00_ContentPlaceHolder1_droplist]");
            var PaymentMode = checked_dropdown.val();
            var TotalPgCharges = 0;
            var TotalAmount = 0;
            var Amount = 0;
            if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() != "" && $("#ctl00_ContentPlaceHolder1_TxtAmout").val() != null) {
                Amount = $("#ctl00_ContentPlaceHolder1_TxtAmout").val();
            }
            else {
                Amount = 0
            }
            $.ajax({
                type: "POST",
                //url: "UploadAmount.aspx/GetPgChargeByMode",
                url: UrlBase + 'FltSearch1.asmx/GetPgChargeByMode',
                data: '{paymode: "' + PaymentMode + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d != "") {
                        if (data.d.indexOf("~") > 0) {
                            //var res = data.d.split('~');
                            var PgCharge = data.d.split('~')[0]
                            var chargeType = data.d.split('~')[1]
                            if (chargeType == "F") {
                                //calculate fixed pg charge 
                                TotalPgCharges = (parseFloat(PgCharge)).toFixed(2);
                                TotalAmount = (parseFloat(Amount) + parseFloat(TotalPgCharges)).toFixed(2);

                                $('#TransCharges').val(TotalPgCharges);
                                $('#TotalAmount').val(TotalAmount);
                                $('#<%=lblTransCharges.ClientID%>').html(TotalPgCharges);
                                $('#<%=lblTotalAmount.ClientID%>').html(TotalAmount);
                            }
                            else {
                                //calculate percentage pg charge                                     
                                TotalPgCharges = ((parseFloat(Amount) * parseFloat(PgCharge)) / 100).toFixed(2);
                                TotalAmount = (parseFloat(Amount) + parseFloat(TotalPgCharges)).toFixed(2);

                                $('#TransCharges').val(TotalPgCharges);
                                $('#TotalAmount').val(TotalAmount);
                                $('#<%=lblTransCharges.ClientID%>').html(TotalPgCharges);
                                     $('#<%=lblTotalAmount.ClientID%>').html(TotalAmount);
                            }
                        }
                    }
                    else {
                        alert("try again");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            debugger;
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>

</asp:Content>

