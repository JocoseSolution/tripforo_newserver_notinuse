﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMN_SHARED.Payment
{
    public class PaymentReqShrd
    {

        public string UserId { get; set; }
        public string ClientId { get; set; }
        public string OrderId { get; set; }
        public decimal Amount { get; set; }
        public PaymentMode PGMode { get; set; }
        public PaymentAction PaymentAction { get; set; }

    }


    public class PaymentResShrd
    {
        public string UserId { get; set; }
        public string ClientId { get; set; }
        public string OrderID { get; set; }
        public decimal Amount { get; set; }
        public string PaymentId { get; set;}

    }

    public enum PaymentMode { Wallet, PG }
    public enum PaymentAction { StatusCheck,Debit,Credit }


}


