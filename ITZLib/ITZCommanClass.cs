﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace ITZLib
{
    class ITZCommanClass
    {
    }
    //cltr+D+K
    public class _validateLogin
    {
        public string strModeType { get; set; }
        public string strUserName { get; set; }
        public string strPassword { get; set; }
    }

    public class _ForgetPassword
    {
        public string strDOB { get; set; }
        public string strEmailID { get; set; }
        public string strUserName { get; set; }
    }

    public class _ChangePassword
    {
        public string strConfrimPWD { get; set; }
        public string strDOB { get; set; }
        public string strNewPWD { get; set; }
        public string strOldPWD { get; set; }
        public string strUserID { get; set; }
    }

    public class _validateLoginResponse
    {
        public string _return { get; set; }
    }

    public class _GetBalance
    {
        public string _USERNAME { get; set; }
        public string _DCODE { get; set; }
        public string _MERCHANT_KEY { get; set; }
        public string _PASSWORD { get; set; }
    }

    public class _GetBalanceResponse
    {
        public string RP_USERNAME { get; set; }
        public string RP_DCODE { get; set; }
        public string RP_MERCHANT_KEY { get; set; }
        public string RP_ERROR_COD { get; set; }
        public string RP_MESSAGE { get; set; }

        public string RP_VAL_ACCOUNT_TYPE_NAME { get; set; }
        public double RP_VAL_ACCOUNT_BALANCE { get; set; }
    }

    public class _GetBalanceRequest
    {
        public _GetBalance GBRequest { get; set; }
    }

    public class _CrOrDb
    {
        public string _AMOUNT { get; set; }
        public string _MERCHANT_KEY { get; set; }
        public string _MODE { get; set; }
        public string _ORDERID { get; set; }
        public string _PASSWORD { get; set; }
        public string _DECODE { get; set; }
        public string _CHECKSUM { get; set; }
        public string _DESCRIPTION { get; set; }
        public string _SERVICE_TYPE { get; set; }
        public string _REFUNDTYPE { get; set; }
        public string _REFUNDORDERID { get; set; }
    }

    public class _DebitRequest
    {
        public _CrOrDb DebitReq { get; set; }
    }

    public class _DebitResponse
    {
        public string DR_USERNAME { get; set; }
        public string DR_MERCHANT_KEY { get; set; }
        public string DR_AMOUNT { get; set; }
        public string DR_ORDERID { get; set; }
        public string DR_SERVICETAX { get; set; }
        public string DR_CONVENIENCEFEE { get; set; }
        public string DR_TDS { get; set; }
        public string DR_COMMISSION { get; set; }
        public string DR_TOTALAMOUNT { get; set; }
        public string DR_EASY_ORDER_ID { get; set; }
        public string DR_EASY_TRAN_CODE { get; set; }
        public string DR_ERROR_CODE { get; set; }
        public string DR_MESSAGE { get; set; }
    }

    public class _CreditRequest
    {
        _CrOrDb CreditReq { get; set; }
    }

    public class _CreditResponse
    {
        public string CR_AMOUNT { get; set; }
        public string CR_MERCHANT_KEY { get; set; }
        public string CR_MODE { get; set; }
        public string CR_ORDERID { get; set; }
        public string CR_PASSWORD { get; set; }
        public string CR_DECODE { get; set; }
        public string CR_CHECKSUM { get; set; }
        public string CR_DESCRIPTION { get; set; }
        public string CR_SERVICE_TYPE { get; set; }
    }

    public class _RefundRequest
    {
        _CrOrDb RefundReq { get; set; }
    }

    public class _RefundResponse
    {
        public string RF_ORDER_ID { get; set; }
        public string RF_EASY_ORDER_ID { get; set; }
        public string RF_EASY_TRAN_CODE { get; set; }
        public string RF_REFUND_TYPE { get; set; }
        public string RF_ERROR_CODE { get; set; }
        public string RF_MESSAGE { get; set; }
    }

    public class AdvLoginPar
    {
        public string strModeType { get; set; }
        public string strUserName { get; set; }
        public string strPassword { get; set; }
    }

}
