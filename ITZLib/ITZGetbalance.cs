﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ITZLib
{
    //for Retrive the Balances info
    public class ITZGetbalance
    {
        ITZLib.ErrorLogs.ErrorLog errLog = new ITZLib.ErrorLogs.ErrorLog();
        public string ToXML(GetBalanceResponse objDeb)
        {
            System.IO.StringWriter stringwriter = new System.IO.StringWriter();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(objDeb.GetType());
            serializer.Serialize(stringwriter, objDeb);
            return stringwriter.ToString();
        }

        public string ToXML(GetBalance objDeb)
        {
            System.IO.StringWriter stringwriter = new System.IO.StringWriter();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(objDeb.GetType());
            serializer.Serialize(stringwriter, objDeb);
            return stringwriter.ToString();
        }

        public string ToXMLReq(GetBalanceRequest objDeb)
        {
            System.IO.StringWriter stringwriter = new System.IO.StringWriter();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(objDeb.GetType());
            serializer.Serialize(stringwriter, objDeb);
            return stringwriter.ToString();
        }

        public GetBalanceResponse GetBalanceCustomer(_GetBalance GBParam)
        {
            GetBalanceRequest objGBReq = new GetBalanceRequest();
            GetBalance objGetBalance = new GetBalance();
            GetBalanceResponse objRS = new GetBalanceResponse();
            GetBalanceService objGBSer = new GetBalanceService();
            try
            {
                //// ITZComman Class _GetBalance//
                ////objGetBalance.USERNAME = GBParam._USERNAME;
                objGetBalance.USERNAME = "";
                objGetBalance.DCODE = GBParam._DCODE;
                objGetBalance.MERCHANT_KEY = GBParam._MERCHANT_KEY;
                ////objGetBalance.PASSWORD = GBParam._PASSWORD;
                objGetBalance.PASSWORD = "";
                objGBReq.GetBalance = objGetBalance;                
                ////string balreq = ToXML(objGBReq.GetBalance);                
                objRS = objGBSer.GetBalance(objGBReq);
                string balresp = ToXML(objRS);
                string reqxml = ToXMLReq(objGBReq);
                string rq = "dcode:" + objGetBalance.DCODE + "mrchntkey:" + objGetBalance.MERCHANT_KEY + " UserName:" + objGetBalance.USERNAME+" pass:"+objGetBalance.PASSWORD;

                errLog.WriteErrorLog("req:  " + reqxml + "    res" + balresp.ToString());
            }
            catch (Exception ex)
            {
                ////throw ex;
                try
                {
                    errLog.WriteErrorLog(ex.Message.ToString());
                }
                catch (Exception err) { }
            }
            return objRS;
        }
    }
}

//     public string GetXMLFromObject(object o)
//     {
//         StringWriter sw = new StringWriter();
//         XmlTextWriter tw = new XmlTextWriter(sw);

//         try
//         {
//             XmlSerializer serializer = new XmlSerializer(o.GetType());
//             serializer.Serialize(tw, o);
//             return sw.ToString();
//         }
//         catch (Exception ex)
//         {
//             //throw ex;
//         }
//         finally
//         {
//             //sw.close();
//             // tw.close();
//         }
//         return sw.ToString();
//     }
//    }
//}
