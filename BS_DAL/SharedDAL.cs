﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.ComponentModel;
namespace BS_DAL
{
    public class SharedDAL
    {
       
        
        SqlCommand cmd; SqlConnection con; SqlDataAdapter adap; SqlConnection conn;
        int cnt = 0; SqlDataReader reader; string msg = ""; EXCEPTION_LOG.ErrorLog erlog;
        public SharedDAL()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        }
        #region[Insert source list into DB]
        public void InsertSrcList(List<BS_SHARED.SHARED> list, string provider)
        {
            try
            {
                con.Open();
                if (list != null)
                {
                    for (int i = 0; i <= list.Count - 1; i++)
                    {
                        cmd = new SqlCommand("SP_BUS_MODIFYSRCDEST", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@srcId", list[i].srcID);
                        cmd.Parameters.AddWithValue("@srcname", list[i].src);
                        cmd.Parameters.AddWithValue("@operation", "Insert");
                        cmd.Parameters.AddWithValue("@provider", provider.Trim());
                        cnt = cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    cmd = new SqlCommand("SP_BUS_MODIFYSRCDEST", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operation", "delete");
                    cmd.Parameters.AddWithValue("@provider", provider.Trim());
                    cnt = cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
        }
        #endregion
        #region[insert destination list into DB]
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.Name == "srcID" || prop.Name == "destID" || prop.Name == "dest" || prop.Name == "src" || prop.Name == "provider_name")
                {

                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    if (prop.Name == "srcID" || prop.Name == "destID" || prop.Name == "dest" || prop.Name == "src" || prop.Name == "provider_name")
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        public void InsertDestList(List<BS_SHARED.SHARED> list, string srcId, string provider)
        {
            try
            {
                DataTable dt = ConvertToDataTable(list);
                con.Open();
                cmd = new SqlCommand("SP_BUS_INSERTDESTINATION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@dest", dt);
                cmd.Parameters.AddWithValue("@sourceid", srcId);
                cmd.Parameters.AddWithValue("@sourceName", list[0].src);
                cmd.Parameters.AddWithValue("@provider", provider);
                cnt = cmd.ExecuteNonQuery();
                //////////// Update Source id ////////////
                UpdateSource(srcId.Trim());
                /////////////// End ////////////////
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
        }
        public void InsertDestListGS(List<BS_SHARED.cityListService> list)
        {
            try
            {              
                con.Open();
                for (int i = 0; i < list.Count; i++)
                {
                    cmd = new SqlCommand("sp_insert_Dest", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SourceID", list[i].SourceID);
                    cmd.Parameters.AddWithValue("@Source", list[i].Source);
                    cmd.Parameters.AddWithValue("@SrcPlaceCode", list[i].SrcPlaceCode);
                    cmd.Parameters.AddWithValue("@SrcStateCode", list[i].SrcStateCode);
                    cmd.Parameters.AddWithValue("@SrcStateName", list[i].SrcStateName);               
                    cmd.Parameters.AddWithValue("@PROVIDER_NAME", list[i].PROVIDER_NAME);
                    cnt = cmd.ExecuteNonQuery();
                }
                /////////////// End ////////////////
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        #region[get source list]
        public List<BS_SHARED.SHARED> getSourceList(string srcPrefix)
        {
            List<BS_SHARED.SHARED> srcList = new List<BS_SHARED.SHARED>();
            try
            {
                con.Open();
                cmd = new SqlCommand("SP_BUS_CITYAUTOSEARCH", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@cityprefix", srcPrefix);
                cmd.Parameters.AddWithValue("@tableref", "SourceTBL");
                cmd.Parameters.AddWithValue("@selectedSource", "");
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    srcList.Add(new BS_SHARED.SHARED { src = reader["City_Name"].ToString().Trim(), srcID = reader["City_ID"].ToString().Trim() });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return srcList;
        }
        #endregion
        #region[get destination list]
        public List<BS_SHARED.SHARED> getDestList(string destPrefix,string source)
        {
            List<BS_SHARED.SHARED> destList = new List<BS_SHARED.SHARED>();
            try
            {
                con.Open();
                cmd = new SqlCommand("SP_BUS_CITYAUTOSEARCH", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@cityprefix", destPrefix);
                cmd.Parameters.AddWithValue("@selectedSource", source);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    destList.Add(new BS_SHARED.SHARED { dest = reader["Destination"].ToString().Trim(), destID = reader["DestinationID"].ToString().Trim() });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return destList;
        }
        #endregion
        #region[update source id in destination table]
        private void UpdateSource(string srcid)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_UPDATESOURCEID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@cityid", srcid);
                cnt = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
        }
        #endregion
        #region[GET Auth Credentials]
        public DataTable getOauthCred()
        {
            DataTable dt = new DataTable();
            try
            {
                adap = new SqlDataAdapter("SP_BUS_GET_OAUTH_CREDENTIALS", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return dt;
        }
        #endregion
        #region[GET sourceID and destinationID]
        public DataTable getSRCDEST_ID(BS_SHARED.SHARED shared)
        {
            DataTable dt = new DataTable();
            try
            {
                adap = new SqlDataAdapter("SP_BUS_GET_SRC_ID", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@srcname", shared.src.Trim());
                if (shared.dest != null)
                {
                    adap.SelectCommand.Parameters.AddWithValue("@destname", shared.dest.Trim());
                }
                adap.Fill(dt);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return dt;
        }
        #endregion
        #region[Get Service Enable/Disable]
        public bool getBusProvider_Enable(string provider)
        {
            bool flag = false;
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_GET_BUS_SERVICEENABLE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@provider_name", provider);
                flag = (bool)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return flag;
        }
        #endregion
        #region[Get Random Number]
        public string getRandom_Num()
        {
            string rand = "";
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_GETRANDOMNUM", con);
                cmd.CommandType = CommandType.StoredProcedure;
                rand = (string)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
            return rand;
        }
        public string getGSRandom_TicketNum()
        {
            string rand = "";
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_GSBUS_GETRANDOMNUMTKT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                rand = (string)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
            return "1_"+rand.ToString().Trim();
        }
        #endregion
        #region[Calculate Bus Commission]
        public List<BS_SHARED.SHARED> getComList(BS_SHARED.SHARED shared)
        {
            DataTable dtcom = new DataTable(); DataTable dtmrkp = new DataTable();
            decimal com = 0; string[] farearray;
            decimal tds = 0; decimal taNetTotal = 0; decimal totalfare = 0; decimal AdmMrk = 0; decimal AgmMrk = 0;
            decimal totmrkp = 0; decimal[] mrkup;
            List<BS_SHARED.SHARED> comlist = new List<BS_SHARED.SHARED>();
            try
            {
                
                dtcom = getComm(shared.agentID.Trim(), shared.provider_name.Trim(),shared.traveler);
                dtmrkp = getMarkup(shared.agentID.Trim());
                if (shared.fare != "")
                {
                    farearray=shared.fare.Trim().Split(',');
                    for (int f = 0; f < farearray.Length; f++)
                    {
                        if (shared.fare.Trim().Split(',')[f] != "")
                        {
                            mrkup = getServiceChrg(dtmrkp, farearray[f].Trim());
                            totmrkp = mrkup[0] + mrkup[1];
                            AdmMrk = mrkup[0];
                            AgmMrk = mrkup[1];

                            decimal incomm = 0; 
                            if (dtcom.Rows[0]["COMM_TYPE"].ToString().ToUpper() == "P")
                            {
                                incomm = ((Convert.ToDecimal(farearray[f]) + AdmMrk) * Convert.ToDecimal(dtcom.Rows[0]["COMMISSION"].ToString()) / 100);
                                com = Math.Round(incomm);
                            }
                            else
                            {
                               // com = Math.Round(Convert.ToDecimal(farearray[f]) + Convert.ToDecimal(dtcom.Rows[0]["COMMISSION"].ToString()));
                                incomm =Convert.ToDecimal(dtcom.Rows[0]["COMMISSION"].ToString());
                                com = Math.Round(incomm);
                            }
                            tds = Math.Ceiling(incomm * Convert.ToDecimal(dtcom.Rows[0]["TDS"].ToString()) / 100);
                          
                            totalfare = Math.Round(Convert.ToDecimal(farearray[f]) + totmrkp);
                            taNetTotal = Math.Round(((totalfare - mrkup[1]) - com) + tds);
                            comlist.Add(new BS_SHARED.SHARED { adcomm = com, taTds = tds, admrkp = AdmMrk, agmrkp = AgmMrk, serviceChrg = totmrkp, taNetFare = taNetTotal, taTotFare = totalfare, totFare = totalfare });
                        }
                    }
                }            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return comlist;
        }
        #endregion
        #region[CalCulate Markup]
        private decimal[] getServiceChrg(DataTable dt, string fare)
        {
            decimal[] srvchrg = new decimal[2];
            try
            {
                switch (dt.Rows[0]["AdMrkupType"].ToString().ToUpper())
                {
                    case "P":
                        srvchrg[0] = Math.Round((Convert.ToDecimal(fare) * Convert.ToDecimal(dt.Rows[0]["AdMrkupVal"].ToString())) / 100);
                        break;
                    case "F":
                        srvchrg[0] = Convert.ToDecimal(dt.Rows[0]["AdMrkupVal"].ToString());
                        break;
                    default:
                        break;
                }
                switch (dt.Rows[0]["AgMrkupType"].ToString().ToUpper())
                {
                    case "P":
                        srvchrg[1] = Math.Round((Convert.ToDecimal(fare) * Convert.ToDecimal(dt.Rows[0]["AgMrkupVal"].ToString())) / 100);
                        break;
                    case "F":
                        srvchrg[1] = Convert.ToDecimal(dt.Rows[0]["AgMrkupVal"].ToString());
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return srvchrg;
        }
        #endregion
        #region[Get Commission]
        private DataTable getComm(string agentid, string provider, string traveler)
        {
           
            DataTable dt = new DataTable();
            try
            {
                adap = new SqlDataAdapter("SP_BUS_GET_COMMISSION", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@agentd", agentid.Trim());
                adap.SelectCommand.Parameters.AddWithValue("@provider", provider.Trim());
                adap.SelectCommand.Parameters.AddWithValue("@Operator", traveler);
                adap.Fill(dt);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return dt;
        }
        #endregion
        #region[Get markup]
        public DataTable getMarkup(string agentid)
        {
            DataTable dt = new DataTable();
            try
            {
                adap = new SqlDataAdapter("SP_BUS_GETMARKUP", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@userid", agentid.Trim());
                adap.Fill(dt);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return dt;
        }
        #endregion
        #region[insert selected seat details]
        public void insertselected_seatDetrails_Dal(BS_SHARED.SHARED shared)
        {
            DataTable dt = new DataTable();
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                dt = getSRCDEST_ID(shared);
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["Provider_Name"].ToString() == shared.provider_name.ToString())
                    {
                        cmd = new SqlCommand("SP_BUS_SELECTED_SEATDETAILS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@orderid", shared.orderID.Trim());
                        cmd.Parameters.AddWithValue("@tripid", shared.serviceID.Trim());
                        cmd.Parameters.AddWithValue("@src", shared.src.Trim() + "(" + dr["sourceid"].ToString() + ")");
                        cmd.Parameters.AddWithValue("@dest", shared.dest.Trim() + "(" + dr["destinationid"].ToString() + ")");
                        cmd.Parameters.AddWithValue("@jrdate", shared.journeyDate.Trim());
                        cmd.Parameters.AddWithValue("@busoperator", shared.traveler.Trim());
                        cmd.Parameters.AddWithValue("@seatno", shared.seat.Trim());
                        cmd.Parameters.AddWithValue("@noofpax", Convert.ToInt32(shared.NoOfPax.Trim()));
                        cmd.Parameters.AddWithValue("@bustype", shared.busType.Trim());
                        cmd.Parameters.AddWithValue("@ladiesseat", shared.ladiesSeat.Trim());
                        cmd.Parameters.AddWithValue("@fare", shared.fare.Trim());
                        cmd.Parameters.AddWithValue("@originalfare", shared.originalfare.Trim());
                        cmd.Parameters.AddWithValue("@bdpoint", shared.boardpoint.Trim());
                        cmd.Parameters.AddWithValue("@dppoint", shared.droppoint.Trim());
                        cmd.Parameters.AddWithValue("@agentid", shared.agentID.Trim());
                        cmd.Parameters.AddWithValue("@status", "Request");
                        cmd.Parameters.AddWithValue("@admrkup", shared.admrkp);
                        cmd.Parameters.AddWithValue("@agmrkup", shared.agmrkp);
                        cmd.Parameters.AddWithValue("@adcomm", shared.adcomm);
                        cmd.Parameters.AddWithValue("@taTds", shared.taTds);
                        cmd.Parameters.AddWithValue("@totfare", shared.totFare);
                        cmd.Parameters.AddWithValue("@taTotfare", shared.taTotFare);
                        cmd.Parameters.AddWithValue("@taNetfare", shared.taNetFare);
                        cmd.Parameters.AddWithValue("@operation", "Insert");
                        cmd.Parameters.AddWithValue("@tableref", "SEAT DETAILS");
                        cmd.Parameters.AddWithValue("@idproof", shared.idproofReq.Trim());
                        cmd.Parameters.AddWithValue("@providername", shared.provider_name.Trim());
                        cmd.Parameters.AddWithValue("@canpolicy", shared.canPolicy.Trim());
                        cmd.Parameters.AddWithValue("@partialcan", shared.partialCanAllowed.Trim());
                        cmd.Parameters.AddWithValue("@Arr_Time", Convert.ToString(shared.arrTime.Trim()));
                        cmd.Parameters.AddWithValue("@Dept_Time", Convert.ToString(shared.departTime.Trim()));
                        cmd.Parameters.AddWithValue("@Dur_Time", Convert.ToString(shared.Dur_Time.Trim()));
                        cmd.Parameters.AddWithValue("@SEAT_TYPE", Convert.ToString(shared.SEAT_TYPE.Trim()));
                        cmd.Parameters.AddWithValue("@AC_NONAC", Convert.ToString(shared.AC_NONAC.Trim()));                                                                   
                        cmd.Parameters.AddWithValue("@totalFareWithTaxes", Convert.ToString(shared.totWithTaxes.Trim()));               
                        cmd.Parameters.AddWithValue("@serviceTaxAmount", Convert.ToString(shared.WithTaxes.Trim()));                      
                        int i = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        #region[Get selected seat Details]
        public List<BS_SHARED.SHARED> getSelectedSeatList(BS_SHARED.SHARED shared)
        {
            
            string orderid = shared.orderID.Trim() + "O";
            List<BS_SHARED.SHARED> getList = new List<BS_SHARED.SHARED>();
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_SELECTED_SEATDETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@orderid", orderid);
                cmd.Parameters.AddWithValue("@operation", "Select");
                cmd.Parameters.AddWithValue("@tableref", "Seat Details");
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    getList.Add(new BS_SHARED.SHARED { agentID = reader["AGENTID"].ToString(), orderID = reader["ORDERID"].ToString(), serviceID = reader["TRIPID"].ToString(), src = reader["SOURCE"].ToString(), dest = reader["DESTINATION"].ToString(), journeyDate = reader["JOURNEYDATE"].ToString(), traveler = reader["BUSOPERATOR"].ToString(), seat = reader["SEATNO"].ToString(), NoOfPax = reader["NOOF_PAX"].ToString(), ladiesSeat = reader["LADIESSEAT"].ToString(), fare = reader["FARE"].ToString(), originalfare = reader["ORIGINAL_FARE"].ToString(), boardpoint = reader["BOARDINGPOINT"].ToString(), droppoint = reader["DROPINGPOINT"].ToString(), agmrkp = Convert.ToDecimal(reader["AGENT_MARKUP"].ToString()), admrkp = Convert.ToDecimal(reader["ADMIN_MARKUP"].ToString()), adcomm = Convert.ToDecimal(reader["ADMIN_COMM"].ToString()), taTds = Convert.ToDecimal(reader["TA_TDS"].ToString()), totFare = Convert.ToDecimal(reader["TOTAL_FARE"].ToString()), taTotFare = Convert.ToDecimal(reader["TA_TOT_FARE"].ToString()), taNetFare = Convert.ToDecimal(reader["TA_NET_FARE"].ToString()), idproofReq = reader["ISIDPROOF_REQUIRED"].ToString(), provider_name = reader["PROVIDER_NAME"].ToString(), AgencyName = reader["AGENCYNAME"].ToString().Trim(), usertype = reader["AGTYPE"].ToString().Trim(), canPolicy = reader["CANCEL_POLICY"].ToString().Trim(), partialCanAllowed = reader["PARTIAL_CANCEL"].ToString().Trim(), AC_NONAC = reader["AC_NONAC"].ToString(), SEAT_TYPE = reader["SEAT_TYPE"].ToString(), WithTaxes = reader["serviceTaxAmount"].ToString(), totWithTaxes = reader["totalFareWithTaxes"].ToString(), serviceType = reader["BUS_TYPE"].ToString() });
                  //  getList.Add(new BS_SHARED.SHARED { agentID = reader["AGENTID"].ToString(), orderID = reader["ORDERID"].ToString(), serviceID = reader["TRIPID"].ToString(), src = reader["SOURCE"].ToString(), dest = reader["DESTINATION"].ToString(), journeyDate = reader["JOURNEYDATE"].ToString(), traveler = reader["BUSOPERATOR"].ToString(), seat = reader["SEATNO"].ToString(), NoOfPax = reader["NOOF_PAX"].ToString(), ladiesSeat = reader["LADIESSEAT"].ToString(), fare = reader["FARE"].ToString(), originalfare = reader["ORIGINAL_FARE"].ToString(), boardpoint = reader["BOARDINGPOINT"].ToString(), droppoint = reader["DROPINGPOINT"].ToString(), agmrkp = Convert.ToDecimal(reader["AGENT_MARKUP"].ToString()), admrkp = Convert.ToDecimal(reader["ADMIN_MARKUP"].ToString()), adcomm = Convert.ToDecimal(reader["ADMIN_COMM"].ToString()), taTds = Convert.ToDecimal(reader["TA_TDS"].ToString()), totFare = Convert.ToDecimal(reader["TOTAL_FARE"].ToString()), taTotFare = Convert.ToDecimal(reader["TA_TOT_FARE"].ToString()), taNetFare = Convert.ToDecimal(reader["TA_NET_FARE"].ToString()), idproofReq = reader["ISIDPROOF_REQUIRED"].ToString(), provider_name = reader["PROVIDER_NAME"].ToString(), AgencyName = reader["AGENCYNAME"].ToString().Trim(), usertype = reader["AGTYPE"].ToString().Trim(), canPolicy = reader["CANCEL_POLICY"].ToString().Trim(), partialCanAllowed = reader["PARTIAL_CANCEL"].ToString().Trim() });
               
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return getList;
        }
        #endregion
        #region[Insert selected seat details for booking]
        public void insertSelectedSeatForBooking(BS_SHARED.SHARED shared, List<BS_SHARED.SHARED> list)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                for (int i = 0; i <= shared.paxseat.Count - 1; i++)
                {
                    cmd = new SqlCommand("SP_BUS_SEATBOOKINGDETAILS", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@orderid", shared.orderID);
                    cmd.Parameters.AddWithValue("@tripid", shared.serviceID);
                    cmd.Parameters.AddWithValue("@src", shared.src);
                    cmd.Parameters.AddWithValue("@dest", shared.dest);
                    cmd.Parameters.AddWithValue("@busoperator", shared.traveler);
                    cmd.Parameters.AddWithValue("@seatno", shared.paxseat[i].ToString());
                    cmd.Parameters.AddWithValue("@ladiesseat", shared.ladiesSeat);
                    if (shared.provider_name == "GS")
                    {
                        cmd.Parameters.AddWithValue("@fare", list[i].fare.ToString());
                        cmd.Parameters.AddWithValue("@bookstatus", "Hold");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@fare", shared.perFare[i].ToString());
                        cmd.Parameters.AddWithValue("@bookstatus", "Request");
                    }
                    cmd.Parameters.AddWithValue("@originalfare", shared.perOriginalFare[i].ToString());
                    cmd.Parameters.AddWithValue("@bdpoint", shared.boardpoint);
                    cmd.Parameters.AddWithValue("@dppoint", shared.droppoint);
                    cmd.Parameters.AddWithValue("@jrdate", shared.journeyDate);
                    cmd.Parameters.AddWithValue("@paxname", shared.paxname[i].ToString());
                    if (i == 0)
                    {
                        if (shared.paxname[i].ToString().IndexOf(",") >= 0)
                        {
                            string[] name = shared.paxname[i].Trim().Split(',');
                            cmd.Parameters.AddWithValue("@ppaxname", name[0].ToString());
                            cmd.Parameters.AddWithValue("@isprimary", name[1].ToString());
                        }
                    }
                    cmd.Parameters.AddWithValue("@gender", shared.gender[i].ToString());
                    if (shared.idproofReq == "true")
                    {
                        cmd.Parameters.AddWithValue("@idtype", shared.idtype);
                        cmd.Parameters.AddWithValue("@idnum", shared.idnumber);
                    }
                    cmd.Parameters.AddWithValue("@isidproof", shared.idproofReq);
                    cmd.Parameters.AddWithValue("@paxmob", shared.paxmob);
                    cmd.Parameters.AddWithValue("@paxemail", shared.paxemail);
                    cmd.Parameters.AddWithValue("@agentid", shared.agentID);                   
                    cmd.Parameters.AddWithValue("@adminmrkp", Convert.ToDecimal(list[i].admrkp));
                    cmd.Parameters.AddWithValue("@agentmrkp", Convert.ToDecimal(list[i].agmrkp));
                    cmd.Parameters.AddWithValue("@admincomm", Convert.ToDecimal(list[i].adcomm));
                    cmd.Parameters.AddWithValue("@tatds", Convert.ToDecimal(list[i].taTds));
                    cmd.Parameters.AddWithValue("@totfare", Convert.ToDecimal(list[i].totFare));
                    cmd.Parameters.AddWithValue("@tatotfare", Convert.ToDecimal(list[i].taTotFare));
                    cmd.Parameters.AddWithValue("@tanetfare", Convert.ToDecimal(list[i].taNetFare));
                    cmd.Parameters.AddWithValue("@operation", "Insert");
                    cmd.Parameters.AddWithValue("@paxaddress", shared.paxaddress);
                    cmd.Parameters.AddWithValue("@partialcancel", shared.partialCanAllowed);
                    cmd.Parameters.AddWithValue("@providername", shared.provider_name.Trim());
                    cmd.Parameters.AddWithValue("@canpolicy", shared.canPolicy.Trim());
                    cmd.Parameters.AddWithValue("@paxAge", shared.paxage[i].Trim());
                    if (shared.gender[i].ToString() == "MALE")
                    cmd.Parameters.AddWithValue("@Paxtitle", "MR");
                    else
                        cmd.Parameters.AddWithValue("@Paxtitle", "MISS");
                    cmd.Parameters.AddWithValue("@userID", shared.agentID.Trim());
                    int ii = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        #region[Insert Booking Request and Response]
        public void insertBooking_Request_Response(BS_SHARED.SHARED shared)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_SELECTED_SEATDETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@orderid", shared.orderID);
                cmd.Parameters.AddWithValue("@agentid", shared.agentID);
                cmd.Parameters.AddWithValue("@bookreq", shared.bookreq);
                cmd.Parameters.AddWithValue("@bookres", shared.bookres);
                cmd.Parameters.AddWithValue("@blockkey", shared.blockKey.Trim());
                cmd.Parameters.AddWithValue("@tinno", shared.tin);
                cmd.Parameters.AddWithValue("@providername",shared.provider_name);
                cmd.Parameters.AddWithValue("@operation", "Insert");
                cmd.Parameters.AddWithValue("@tableref", "BOOKREQUEST");
                int i = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        #region[deduct fare amount from agent account]
        public double deductAndaddfareAmt(BS_SHARED.SHARED shared, string func)
        {
            double avlBal = 0;
            try
            {
                conn.Open();
                if (func.ToUpper() == "SUBTRACT")
                {
                    cmd = new SqlCommand("SubtractFromLimit", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", shared.agentID);
                    cmd.Parameters.AddWithValue("@Amount", shared.subAmt);
                    avlBal = double.Parse(cmd.ExecuteScalar().ToString());
                }
                else if (func.ToUpper() == "ADD")
                {
                    cmd = new SqlCommand("AddToLimit", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", shared.agentID);
                    cmd.Parameters.AddWithValue("@Amount", shared.addAmt);
                    avlBal = double.Parse(cmd.ExecuteScalar().ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return avlBal;
        }
        #endregion
        #region[update ticket no]
        public void updateTicketno(BS_SHARED.SHARED shared)
        {
            try
            {
                con.Open();
                if (shared.tin != "" && shared.tin != null)
                {
                    cmd = new SqlCommand("SP_BUS_SEATBOOKINGDETAILS", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@orderid", shared.orderID);
                    cmd.Parameters.AddWithValue("@agentid", shared.agentID);
                    cmd.Parameters.AddWithValue("@bookstatus", "Booked");
                    cmd.Parameters.AddWithValue("@ticketno", shared.tin);
                    cmd.Parameters.AddWithValue("@pnr", shared.pnr);
                    cmd.Parameters.AddWithValue("@operation", "Update");
                    cmd.Parameters.AddWithValue("@date", DateTime.Now);
                    cmd.Parameters.AddWithValue("@partialcancel", shared.partialCancel);

                    int i = cmd.ExecuteNonQuery();

                }
                else
                {
                    cmd = new SqlCommand("SP_BUS_SEATBOOKINGDETAILS", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@orderid", shared.orderID);
                    cmd.Parameters.AddWithValue("@agentid", shared.agentID);
                    cmd.Parameters.AddWithValue("@bookstatus", "Hold");
                    cmd.Parameters.AddWithValue("@operation", "Update");
                    int i = cmd.ExecuteNonQuery();
                }
                con.Close();
            }

            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }

        public void UPDATE_GS_TBL_RB_SeatDetails(string ORDERID, string AGENTID, string S_FARE, decimal S_AD_MRK, decimal S_AD_COM, decimal S_AG_MRK, decimal S_TA_TDS, decimal S_TOTEL_FARE, decimal S_TA_TOT_FARE, decimal S_TA_NET_FARE)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("SP_UPDATE_GS_TBL_RB_SeatDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", ORDERID);
                cmd.Parameters.AddWithValue("@AGENTID", AGENTID);
                cmd.Parameters.AddWithValue("@S_FARE", S_FARE);
                cmd.Parameters.AddWithValue("@S_AD_MRK", S_AD_MRK);
                cmd.Parameters.AddWithValue("@S_AD_COM", S_AD_COM);
                cmd.Parameters.AddWithValue("@S_AG_MRK", S_AG_MRK);
                cmd.Parameters.AddWithValue("@S_TA_TDS", S_TA_TDS);
                cmd.Parameters.AddWithValue("@S_TOTEL_FARE", S_TOTEL_FARE);
                cmd.Parameters.AddWithValue("@S_TA_TOT_FARE", S_TA_TOT_FARE);
                cmd.Parameters.AddWithValue("@S_TA_NET_FARE", S_TA_NET_FARE);
                int i = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }            

        #endregion
        #region[Insert ledger details]
        public void insertLedgerDetails(BS_SHARED.SHARED shared, string func)
        {
            try
            {
                conn.Open();
                cmd = new SqlCommand("InsLedgerDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AgentID", shared.agentID);
                cmd.Parameters.AddWithValue("@AgencyName", shared.AgencyName);
                cmd.Parameters.AddWithValue("@InvoiceNo", shared.orderID);
                cmd.Parameters.AddWithValue("@PnrNo", "");
                cmd.Parameters.AddWithValue("@TicketNo", "");
                cmd.Parameters.AddWithValue("@TicketingCarrier", "");
                cmd.Parameters.AddWithValue("@YatraAccountID", "");
                cmd.Parameters.AddWithValue("@AccountID", "");
                cmd.Parameters.AddWithValue("@ExecutiveID", "");
                cmd.Parameters.AddWithValue("@IPAddress", HttpContext.Current.Request.UserHostAddress);
                if (func.ToUpper() == "SUBTRACT")
                {
                    cmd.Parameters.AddWithValue("@Debit", shared.subAmt);
                    cmd.Parameters.AddWithValue("@BookingType", "BUSBKG");
                    cmd.Parameters.AddWithValue("@Remark", "Bus Booking");
                    cmd.Parameters.AddWithValue("@Credit", "0");
                }
                if (func.ToUpper() == "ADD")
                {
                    cmd.Parameters.AddWithValue("@Credit", shared.addAmt);
                    cmd.Parameters.AddWithValue("@BookingType", "BUSREF");
                    cmd.Parameters.AddWithValue("@Remark", "Bus booking refund with orderid '" + shared.orderID + "' ");
                    cmd.Parameters.AddWithValue("@Debit", "0");
                }
                cmd.Parameters.AddWithValue("@Aval_Balance", shared.avalBal);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                conn.Close();
            }
        }
        #endregion
        public void UPDATEBOOKING_STATUS(string orderId,string bookingstatus,string ticketNo)
        {

            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_RB_UPDATEBOOKING_STATUS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", orderId);
                cmd.Parameters.AddWithValue("@TKTNO", ticketNo);
                cmd.Parameters.AddWithValue("@BOOKSTATUS", bookingstatus);
                int i = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                conn.Close();
            }
        }
        #region[update book response]
        public void updateBookResponse(BS_SHARED.SHARED shared)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_RB_UpdateBookResponse", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@orderid", shared.orderID);
                cmd.Parameters.AddWithValue("@agentid", shared.agentID);
                cmd.Parameters.AddWithValue("@blockkey", shared.blockKey);
                cmd.Parameters.AddWithValue("@tin", shared.tin);
                cmd.Parameters.AddWithValue("@bookreq", shared.bookreq);
                cmd.Parameters.AddWithValue("@bookres", shared.bookres);
                cmd.Parameters.AddWithValue("@TicketNo", shared.TicketNoRes);
                //cmd.Parameters.AddWithValue("@TaNetFare", Convert.ToDecimal(shared.originalfare));
                //cmd.Parameters.AddWithValue("@markup", Convert.ToDecimal(shared.mrkpFare));
                //cmd.Parameters.AddWithValue("@comm", Convert.ToDecimal(shared.commission));
                int i = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        #region[update ticketno and pnr]
        public void updateTicketandPnr(BS_SHARED.SHARED shared)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_RB_updateTicketPnr", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pnr", shared.pnr);
                cmd.Parameters.AddWithValue("@ticketno", shared.tin);
                cmd.Parameters.AddWithValue("orderid", shared.orderID);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        #region[Get Ticket Copy]
        public List<BS_SHARED.SHARED> getticketcopyDetails(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> ticketrsp = new List<BS_SHARED.SHARED>();
            
            try
            {
                con.Open();
                cmd = new SqlCommand("SP_BUS_SELECTED_SEATDETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@orderid", shared.orderID.Replace("*", string.Empty));
                cmd.Parameters.AddWithValue("@tinno", shared.tin.Replace("*",string.Empty));
                cmd.Parameters.AddWithValue("@operation", "Select");
                cmd.Parameters.AddWithValue("@tableref", "BOOKREQUEST");
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                        if (reader["PROVIDER_NAME"].ToString().Trim() == "GS")
                            ticketrsp.Add(new BS_SHARED.SHARED { bookres = reader["BOOK_RESPONSE"].ToString(), src = reader["source"].ToString(), dest = reader["destination"].ToString(), journeyDate = reader["journeydate"].ToString(), AgencyName = reader["Agency_Name"].ToString(), AgencyAddress = reader["Address"].ToString(), State = reader["State"].ToString(), City = reader["City"].ToString(), Country = reader["Country"].ToString(), Phone = reader["Phone"].ToString(), Mobile = reader["Mobile"].ToString(), agentID = reader["AGENTID"].ToString(), fare = reader["TA_TOT_FARE"].ToString().Trim(), provider_name = reader["PROVIDER_NAME"].ToString().Trim(), traveler = reader["BUSOPERATOR"].ToString().Trim(), pnr = reader["PNR"].ToString().Trim(), tin = reader["TICKETNO"].ToString().Trim(), Passengername = reader["PAXNAME"].ToString().Trim(), paxmob = reader["PRIMARY_PAX_PAXMOB"].ToString().Trim(), canPolicy = reader["CANCEL_POLICY"].ToString().Trim(), Isprimary = reader["ISPRIMARY"].ToString().Trim(), seat = reader["SEATNO"].ToString(), busType = reader["BUSTYPE"].ToString().Trim(), boardpoint = reader["BOARDINGPOINT"].ToString().Trim() });
                        else
                            ticketrsp.Add(new BS_SHARED.SHARED { bookres = reader["BOOK_RESPONSE"].ToString(), src = reader["source"].ToString(), dest = reader["destination"].ToString(), journeyDate = reader["journeydate"].ToString(), AgencyName = reader["Agency_Name"].ToString(), AgencyAddress = reader["Address"].ToString(), State = reader["State"].ToString(), City = reader["City"].ToString(), Country = reader["Country"].ToString(), Phone = reader["Phone"].ToString(), Mobile = reader["Mobile"].ToString(), agentID = reader["AGENTID"].ToString(), fare = reader["FARE"].ToString().Trim(), provider_name = reader["PROVIDER_NAME"].ToString().Trim(), traveler = reader["BUSOPERATOR"].ToString().Trim(), pnr = reader["PNR"].ToString().Trim(), tin = reader["TICKETNO"].ToString().Trim(), Passengername = reader["PAXNAME"].ToString().Trim(), paxmob = reader["PRIMARY_PAX_PAXMOB"].ToString().Trim(), canPolicy = reader["CANCEL_POLICY"].ToString().Trim(), Isprimary = reader["ISPRIMARY"].ToString().Trim(), seat = reader["SEATNO"].ToString(), busType = reader["BUSTYPE"].ToString().Trim(), boardpoint = reader["BOARDINGPOINT"].ToString().Trim() });
                }
                con.Close();

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return ticketrsp;
        }
        #endregion
        #region[Get Credit Limit]
        public decimal getCreditLimit(string agentid)
        {
            decimal limit = 0;
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_GETCREDITLIMIT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@agentid", agentid.Trim());
                limit = (decimal)cmd.ExecuteScalar();
                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
            }
            return limit;
        }
        #endregion
        #region[insert and update cancel request]
        public void InsertUpdateCanREQ(BS_SHARED.SHARED shared, string operation)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_CANCEL_RES_REQ", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@orderid", shared.orderID);
                cmd.Parameters.AddWithValue("@canreq", shared.canrequest);
                cmd.Parameters.AddWithValue("@canres", shared.canresponse);
                cmd.Parameters.AddWithValue("@agentid", shared.agentID);
                cmd.Parameters.AddWithValue("@operation", operation);
                cmd.Parameters.AddWithValue("@provider", shared.provider_name);
                int i = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        #region[get partial cancel]
        public List<BS_SHARED.SHARED> getpartialcancellist(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> getlist = new List<BS_SHARED.SHARED>();           
            try
            {
               
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_SEATBOOKINGDETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@orderid", shared.orderID);
                cmd.Parameters.AddWithValue("@ticketno", shared.tin);
                cmd.Parameters.AddWithValue("@operation", "Select");
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    //getlist.Add(new BS_SHARED.SHARED { adcomm = Convert.ToDecimal(reader["ADMIN_COMM"].ToString()), admrkp = Convert.ToDecimal(reader["ADMIN_MARKUP"].ToString()), taTotFare = Convert.ToDecimal(reader["TA_TOT_FARE"].ToString()), agmrkp = Convert.ToDecimal(reader["AGENT_MARKUP"].ToString()), TA_tds = Convert.ToDecimal(reader["TA_TDS"].ToString()), agentID = reader["AGENTID"].ToString().Trim(), AgencyName = reader["AGENCY_NAME"].ToString().Trim(), orderID = reader["OrderId"].ToString(), bookres = reader["BOOK_RESPONSE"].ToString(), pnr = reader["PNR"].ToString(), tin = reader["TICKETNO"].ToString(), src = reader["SOURCE"].ToString(), dest = reader["DESTINATION"].ToString(), busoperator = reader["BUSOPERATOR"].ToString(), journeyDate = reader["JOURNEYDATE"].ToString(), Passengername = reader["PAXNAME"].ToString(), taNetFare = Convert.ToDecimal(reader["TA_NET_FARE"].ToString()), originalfare = reader["ORIGINAL_FARE"].ToString().Trim(), fare = reader["FARE"].ToString(), partialCanAllowed = reader["PARTIALCANCEL"].ToString(), seat = reader["SEATNO"].ToString(), canTime = reader["CANTIME"].ToString(), primaryPax = reader["ISPRIMARY"].ToString(), idproofReq = reader["PRIMARY_PAX_IDPROOF_REQUIRED"].ToString(), provider_name = reader["PROVIDER_NAME"].ToString().Trim(), canPolicy = reader["CANCEL_POLICY"].ToString().Trim(), serviceID = Convert.ToString(reader["TRIPID"]), canrequest = Convert.ToString(reader["BOOK_REQUEST"]) });
                    getlist.Add(new BS_SHARED.SHARED { adcomm = Convert.ToDecimal(reader["ADMIN_COMM"].ToString()), admrkp = Convert.ToDecimal(reader["ADMIN_MARKUP"].ToString()), taTotFare = Convert.ToDecimal(reader["TA_TOT_FARE"].ToString()), agmrkp = Convert.ToDecimal(reader["AGENT_MARKUP"].ToString()), TA_tds = Convert.ToDecimal(reader["TA_TDS"].ToString()), agentID = reader["AGENTID"].ToString().Trim(), AgencyName = reader["AGENCY_NAME"].ToString().Trim(), orderID = reader["OrderId"].ToString(), bookres = reader["BOOK_RESPONSE"].ToString(), pnr = reader["PNR"].ToString(), tin = reader["TICKETNO"].ToString(), src = reader["SOURCE"].ToString(), dest = reader["DESTINATION"].ToString(), busoperator = reader["BUSOPERATOR"].ToString(), journeyDate = reader["JOURNEYDATE"].ToString(), Passengername = reader["PAXNAME"].ToString(), taNetFare = Convert.ToDecimal(reader["TA_NET_FARE"].ToString()), originalfare = reader["ORIGINAL_FARE"].ToString().Trim(), fare = reader["FARE"].ToString(), partialCanAllowed = reader["PARTIALCANCEL"].ToString(), seat = reader["SEATNO"].ToString(), canTime = reader["CANTIME"].ToString(), primaryPax = reader["ISPRIMARY"].ToString(), idproofReq = reader["PRIMARY_PAX_IDPROOF_REQUIRED"].ToString(), provider_name = reader["PROVIDER_NAME"].ToString().Trim(), canPolicy = reader["CANCEL_POLICY"].ToString().Trim(), serviceID = reader["TRIPID"].ToString().Trim(), canrequest = reader["BOOK_REQUEST"].ToString().Trim(), tripId = reader["TRIPID"].ToString(), paymentmode = Convert.ToString(reader["Paymentmode"]), status = Convert.ToString(reader["Status"]) });
                   //// getlist.Add(new BS_SHARED.SHARED { adcomm = Convert.ToDecimal(reader["ADMIN_COMM"].ToString()), admrkp = Convert.ToDecimal(reader["ADMIN_MARKUP"].ToString()), taTotFare = Convert.ToDecimal(reader["TA_TOT_FARE"].ToString()), agmrkp = Convert.ToDecimal(reader["AGENT_MARKUP"].ToString()), TA_tds = Convert.ToDecimal(reader["TA_TDS"].ToString()), agentID = reader["AGENTID"].ToString().Trim(), AgencyName = reader["AGENCY_NAME"].ToString().Trim(), orderID = reader["OrderId"].ToString(), bookres = reader["BOOK_RESPONSE"].ToString(), pnr = reader["PNR"].ToString(), tin = reader["TICKETNO"].ToString(), src = reader["SOURCE"].ToString(), dest = reader["DESTINATION"].ToString(), busoperator = reader["BUSOPERATOR"].ToString(), journeyDate = reader["JOURNEYDATE"].ToString(), Passengername = reader["PAXNAME"].ToString(), taNetFare = Convert.ToDecimal(reader["TA_NET_FARE"].ToString()), originalfare = reader["ORIGINAL_FARE"].ToString().Trim(), fare = reader["FARE"].ToString(), partialCanAllowed = reader["PARTIALCANCEL"].ToString(), seat = reader["SEATNO"].ToString(), canTime = reader["CANTIME"].ToString(), primaryPax = reader["ISPRIMARY"].ToString(), idproofReq = reader["PRIMARY_PAX_IDPROOF_REQUIRED"].ToString(), provider_name = reader["PROVIDER_NAME"].ToString().Trim(), canPolicy = reader["CANCEL_POLICY"].ToString().Trim(), serviceID = reader["TRIPID"].ToString().Trim(), canrequest = reader["BOOK_REQUEST"].ToString().Trim(), paymentmode = Convert.ToString(reader["Paymentmode"]), tripId = reader["TRIPID"].ToString() });
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
            con.Close();
            return getlist;
        }


        public DataSet GetBusServiceChrg()
        {         
         DataSet ds = new DataSet();
            try
            {
                cmd = new SqlCommand("SP_BUS_GETSERVICE_CHARGE", con);
                cmd.CommandType = CommandType.StoredProcedure;             
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");            
            }
            return ds;
        }
        #endregion

        #region[update cancellation charge and refund charge]
        public void updateCanchrgandRefund(BS_SHARED.SHARED shared, string rfndSrvCharge = "", string ApiCancelStatus = "", string RefundStatus = "", string easyRefundID = "", string cancelID = "", string easyTransCode="")
        {
            try
            {
                con.Open();
                if (shared.seat.IndexOf(",") >= 0)
                {
                    string[] s = shared.seat.Split(',');
                    for (int i = 0; i <= s.Length - 1; i++)
                    {

                        string[] canchrg = shared.cancelRecharge.Remove(shared.cancelRecharge.LastIndexOf(",")).Split(',');
                        string[] refund = shared.refundAmt.Remove(shared.refundAmt.LastIndexOf(",")).Split(',');
                        string[] refundSrvChargeA = rfndSrvCharge.Remove(rfndSrvCharge.LastIndexOf(",")).Split(',');
                        cmd = new SqlCommand("SP_BUS_UPDATE_REFUNDAMT", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@tinno", shared.tin);
                        cmd.Parameters.AddWithValue("@canchrg", canchrg[i]);
                        cmd.Parameters.AddWithValue("@refund", refund[i]);
                        cmd.Parameters.AddWithValue("@seatno", s[i]);
                        cmd.Parameters.AddWithValue("@rfndSrvCharge", refundSrvChargeA[i]);
                        cmd.Parameters.AddWithValue("@Status", RefundStatus);
                        cmd.Parameters.AddWithValue("@ApiSatus", ApiCancelStatus);
                        cmd.Parameters.AddWithValue("@EasyRefundID", easyRefundID);
                        cmd.Parameters.AddWithValue("@CANCELLATIONID", cancelID);
                        cmd.Parameters.AddWithValue("@easyTransCode", easyTransCode);
                        
                                                
                        int cnt = cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    cmd = new SqlCommand("SP_BUS_UPDATE_REFUNDAMT", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tinno", shared.tin);
                    cmd.Parameters.AddWithValue("@canchrg", shared.cancelRecharge.Replace(",",""));
                    cmd.Parameters.AddWithValue("@refund", shared.refundAmt.Replace(",", ""));
                    cmd.Parameters.AddWithValue("@seatno", shared.seat.Replace(",", ""));
                    cmd.Parameters.AddWithValue("@rfndSrvCharge", rfndSrvCharge.Replace(",", ""));
                    cmd.Parameters.AddWithValue("@Status", RefundStatus);
                    cmd.Parameters.AddWithValue("@ApiSatus", ApiCancelStatus);
                    cmd.Parameters.AddWithValue("@EasyRefundID", easyRefundID);
                    cmd.Parameters.AddWithValue("@CANCELLATIONID", cancelID);
                    cmd.Parameters.AddWithValue("@easyTransCode", easyTransCode);
                    int cnt = cmd.ExecuteNonQuery();
                }

                con.Close();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");
                con.Close();
            }
        }
        #endregion
        public DataSet getSelectedSeatPaxDetails(string BookingRef, string OrderId)
        {
            DataSet ds = new DataSet();
            try
            {

                //adapter.SelectCommand = new SqlCommand("SP_TBL_RB_SEATBOOKINGDETAILS_PAX", con);
                cmd = new SqlCommand("SP_TBL_RB_SEATBOOKINGDETAILS_PAX", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BOOKINGrFiD", BookingRef);
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_DAL");            
            }
            return ds;
        }

        //public List<BS_SHARED.SHARED> getDeatlistitem()
        //{
        //    List<BS_SHARED.SHARED> srcList = new List<BS_SHARED.SHARED>();
        //    try
        //    {
        //        con.Open();
        //        cmd = new SqlCommand("SP_BUS_CITYAUTOSEARCH", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@cityprefix", "");
        //        cmd.Parameters.AddWithValue("@tableref", "getdest");
        //        cmd.Parameters.AddWithValue("@selectedSource", "");
        //        reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            srcList.Add(new BS_SHARED.SHARED { destID = reader["DestinationID"].ToString().Trim(), dest = reader["Destination"].ToString().Trim() });
        //        }
        //        con.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        con.Close();
        //        erlog = new EXCEPTION_LOG.ErrorLog();
        //        erlog.writeErrorLog(ex, "SHARED_DAL");
        //    }
        //    return srcList;
        //}
    }

}
