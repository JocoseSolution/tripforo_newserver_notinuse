﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NimbusSms
{
    public static class nimbus
    {
        private static string EntityID = "1701162791219259103";
        private static string UserID = "tripforo";
        private static string Password = "tripforo@123";
        private static string SenderID = "TRIPFO";

        public static bool ConfirmBooking(string mobileno, string message)
        {
            string response = SendSms(mobileno, message, "1707162807323818039");
            if (!string.IsNullOrEmpty(response))
            {
                PostResponse postRespo = JsonConvert.DeserializeObject<PostResponse>(response);
                if (postRespo.Status.ToLower() == "ok")
                {
                    return true;
                }
            }
            return false;
        }
        public static bool FlightRevised(string mobileno, string message)
        {
            string response = SendSms(mobileno, message, "1707162807355065764");
            if (!string.IsNullOrEmpty(response))
            {
                PostResponse postRespo = JsonConvert.DeserializeObject<PostResponse>(response);
                if (postRespo.Status.ToLower() == "ok")
                {
                    return true;
                }
            }
            return false;
        }
        public static bool FlightCancelled(string mobileno, string message)
        {
            string response = SendSms(mobileno, message, "1707162807382658213");
            if (!string.IsNullOrEmpty(response))
            {
                PostResponse postRespo = JsonConvert.DeserializeObject<PostResponse>(response);
                if (postRespo.Status.ToLower() == "ok")
                {
                    return true;
                }
            }
            return false;
        }

        //internal static string SendSms(string UserID, string Password, string SenderID, string PhoneNo, string Msg, string EntityID, string TemplateID)
        internal static string SendSms(string PhoneNo, string Msg, string TemplateID)
        {
            string result = "";
            Msg = HttpUtility.UrlEncode(Msg);
            //For single number
            string testUrl = "http://nimbusit.biz/api/SmsApi/SendSingleApi?UserID=" + UserID + "&Password=" + Password + "&SenderID=" + SenderID + "&Phno=" + PhoneNo + "&Msg=" + Msg + "&EntityID=" + EntityID + "&TemplateID=" + TemplateID;
            WebRequest reqObj = WebRequest.Create(testUrl);
            reqObj.Method = "POST";
            reqObj.ContentLength = 0;
            HttpWebResponse resObj = null;
            resObj = (HttpWebResponse)reqObj.GetResponse();
            using (Stream st = resObj.GetResponseStream())
            {
                StreamReader str = new StreamReader(st);
                result = str.ReadToEnd();//we can return result
                str.Close();
            }
            return result;
        }
    }
}

//Confirm Booking Template
//---------------Hello {#var#}, we are happy to confirm your booking on tripforo.com. Your booking details : {#var#}, {#var#} Thanks & Regards tripforo.com
//=====================================
//FlightRevised Template
//Dear Customer (PNR: {#var#}): Due to operational reason, your {#var#}flight to Kolkata has been revised. Here are the updated details: {#var#} {#var#}, {#var#}-{#var#}, {#var#} Thanks & Regards tripforo.com
//=======================================
//FlightCancelled Template
//FLIGHT CHANGED: We regret to inform you that for your PNR {#var#} the following flight(s) has been cancelled due to Operational Reasons: {#var#} on {#var#} Thanks & Regards tripforo.com