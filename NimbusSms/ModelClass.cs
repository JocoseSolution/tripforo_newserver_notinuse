﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NimbusSms
{
    public class ResponseText
    {
        public string Message { get; set; }
    }

    public class PostResponse
    {
        public ResponseText Response { get; set; }
        public string Status { get; set; }
    }
}
