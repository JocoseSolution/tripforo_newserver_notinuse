﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using HotelShared;
using HotelDAL;
using System.Data;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Configuration;
namespace HotelBAL
{
   public class RoomXMLHotelResponse
    {
        RoomXMLRequest HtlReq = new RoomXMLRequest();
        public HotelComposite RoomXMLHotel(HotelSearch SearchDetails)
        {
            HotelComposite obgHotelsCombo = new HotelComposite(); 
            try
            {
                if (SearchDetails.RegionId != "" && SearchDetails.RoomXMLURL != null && (SearchDetails.RoomXMLTrip == SearchDetails.HtlType || SearchDetails.RoomXMLTrip == "ALL"))
                {
                    SearchDetails = HtlReq.HotelSearchAvailabilityRequest(SearchDetails);
                    SearchDetails.RoomXML_HotelSearchRes = RoomXMLPostXml(SearchDetails.RoomXMLURL, SearchDetails.RoomXML_HotelSearchReq);
                    obgHotelsCombo.Hotelresults = GetRoomXMLHotelsPrice(SearchDetails.RoomXML_HotelSearchRes, SearchDetails);


                    //XDocument document1;
                    //string XMLFile = "D:\\HotelPriject\\B2BHotel_25_10_2014\\Home\\TGHotelSearch.xml";
                    //document1 = XDocument.Load(XMLFile);
                    //SearchDetails.RoomXML_HotelSearchRes = document1.ToString();
                    //obgHotelsCombo.Hotelresults = GetRoomXMLHotelsPrice(SearchDetails.RoomXML_HotelSearchRes, SearchDetails); 
                
                }
                else
                {
                    SearchDetails.RoomXML_HotelSearchRes = "Hotel Not available for " + SearchDetails.SearchCity + ", " + SearchDetails.Country;
                    SearchDetails.RoomXML_HotelSearchReq = "not allow";
                    List<HotelResult> objHotellist = new List<HotelResult>();
                    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = SearchDetails.RoomXML_HotelSearchRes });
                    obgHotelsCombo.Hotelresults = objHotellist;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "RoomXMLHotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RoomXML_HotelSearchReq, SearchDetails.RoomXML_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                SearchDetails.RoomXML_HotelSearchRes = ex.Message;
            }
            obgHotelsCombo.HotelSearchDetail = SearchDetails;
            return obgHotelsCombo;
        }
        protected string RoomXMLPostXml(string url, string xml)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    Http.Timeout = 400000;
                   
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelDA objhtlDa = new HotelDA();
                HotelDA.InsertHotelErrorLog(WebEx, "RoomXMLPostXml");
                WebResponse response = WebEx.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "RoomXML", "HotelInsert");
                }
                else
                {
                    int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, WebEx.Message, "RoomXML", "HotelInsert");
                    sbResult.Append(WebEx.Message + "<Error>");
                }
            }
            catch (Exception ex)
            {
                sbResult.Append(ex.Message + "<Error>");
                HotelDA.InsertHotelErrorLog(ex, "RoomXMLPostXml");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, ex.Message, "RoomXML", "HotelInsert");
            }
            return sbResult.ToString();
        }
        protected List<HotelResult> GetRoomXMLHotelsPrice(string XmlRes, HotelSearch SearchDetails)
        {
            HotelDA objhtlDa = new HotelDA(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            List<HotelResult> objHotellist = new List<HotelResult>();
            try
            {
                if (XmlRes.Contains("<Error>"))
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RoomXML_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");
                    
                    XDocument document = XDocument.Parse(XmlRes);
                    var Hotels_temps = (from Hotel in document.Descendants("Error") select new { Errormsg = Hotel.Element("Description"), ErrorID = Hotel.Element("Code") }).ToList();
                    objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].ErrorID.Value + " : " + Hotels_temps[0].Errormsg.Value });
                }
                else
                {
                    XDocument document = XDocument.Parse(XmlRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    var Hotels_temps = (from Hotel in document.Descendants("AvailabilitySearchResult").Descendants("HotelAvailability")
                        select new { HotelRooms = Hotel}).ToList();
                    if (Hotels_temps.Count > 0)
                    {
                        int k = 1100;
                        foreach (var Hotels in Hotels_temps)
                        {
                            try
                            {
                                HotelInformation HotelInfo = new HotelInformation();
                                HotelInfo = GetRoomXMLHotelInfo(SearchDetails, Hotels.HotelRooms.Element("Hotel").Attribute("id").Value.Trim(), SearchDetails.RegionId);
                               // k++;
                                int c = 0; decimal orgrates = 0; string RatePlanCode = "", location = "";
                                foreach (var hotelPrice in Hotels.HotelRooms.Descendants("Result"))
                                {
                                    decimal RoomPrice = 0;
                                    foreach (var htlPrice in hotelPrice.Descendants("Room"))
                                    {
                                        RoomPrice += Convert.ToDecimal(htlPrice.Element("Price").Attribute("amt").Value);
                                    }
                                    if (c == 0)
                                    {
                                        orgrates = RoomPrice; RatePlanCode = hotelPrice.Attribute("id").Value; c = 1;
                                    }
                                    if (RoomPrice < orgrates)
                                    {
                                        orgrates = RoomPrice; RatePlanCode = hotelPrice.Attribute("id").Value;
                                    }
                                }
                                orgrates = orgrates / (SearchDetails.NoofNight * SearchDetails.NoofRoom);
                                MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelInfo.StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, orgrates, SearchDetails.ROOMXML_servicetax);
                                if (HotelInfo.Address.Split(',')[1].Length > 4 && HotelInfo.Address.Split(',')[1].Length < 16)
                                    location = HotelInfo.Address.Split(',')[1];
                                else if (HotelInfo.Address.Split(',')[1].Length > 15)
                                    location = HotelInfo.Address.Split(',')[1].Substring(0, 15);
                                
                                //Add Hotels Detaisl in List start
                                objHotellist.Add(new HotelResult
                                {
                                    HotelCityCode = SearchDetails.RegionId,
                                    HotelCity = SearchDetails.SearchCity,
                                    HotelCode = Hotels.HotelRooms.Element("Hotel").Attribute("id").Value.Trim(),
                                    HotelName = Hotels.HotelRooms.Element("Hotel").Attribute("name").Value.Trim().Replace("'", ""),
                                    StarRating = HotelInfo.StarRating,
                                    Location = location,
                                    hotelPrice = MarkList.TotelAmt,
                                    AgtMrk = MarkList.AgentMrkAmt,
                                    DiscountMsg = "",
                                    hotelDiscoutAmt = 0,
                                    HotelAddress = HotelInfo.Address,
                                    HotelThumbnailImg = HotelInfo.ThumbImg,
                                    HotelServices = HotelInfo.HotelServices,
                                    HotelDescription = System.Web.HttpUtility.HtmlDecode(HotelInfo.HotelDiscription),
                                    Lati_Longi = HotelInfo.Latitude + "##" + HotelInfo.Longitude,
                                    inclusions = "",Provider = "RoomXML",ReviewRating = "",
                                    RatePlanCode = RatePlanCode,
                                    PopulerId = k++
                                });
                                //Add Hotels Detaisl in List end
                            }
                            catch (Exception ex)
                            {
                                objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                                HotelDA.InsertHotelErrorLog(ex, "InnerGetRoomXMLHotelsPrice " + SearchDetails.SearchCity);
                            }
                        }
                    }
                    else
                    {
                        objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search." });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.RoomXML_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");
                    }
                }
            }
            catch (Exception ex)
            {
                objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                HotelDA.InsertHotelErrorLog(ex, "GetRoomXMLHotelsPrice " + SearchDetails.SearchCity);
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RoomXML_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");
            }
            return objHotellist;
        }

        protected HotelInformation GetRoomXMLHotelInfo(HotelSearch SearchDetails, string HotelCode, string CityCode)
        {
            HotelInformation HotelInfo = new HotelInformation();
            XDocument document;
            try
            {
                string XMLFile = ConfigurationManager.AppSettings["RoomXmlHotelDetail"] + "\\" + HotelCode + ".xml";
                if (File.Exists(XMLFile))
                    document = XDocument.Load(XMLFile);
                else
                {
                    string RoomXMLResponce = RoomXMLHotelInformation(SearchDetails, HotelCode);
                    document = XDocument.Parse(RoomXMLResponce);
                    document.Save(XMLFile);
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", SearchDetails.GTAItemInformationReq, RoomXMLResponce, "RoomXML", "HotelInsert");
                }

                var Hotelinfos = (from Hotel in document.Descendants("HotelElement") select new { HotelDetails = Hotel }).ToList();
                 if (Hotelinfos.Count == 0)
                     Hotelinfos = (from Hotel in document.Elements("AvailabilitySearchResult").Elements("HotelAvailability").Descendants("Hotel") select new { HotelDetails = Hotel }).ToList();
                if (Hotelinfos.Count > 0)
                {
                    foreach (var Htl in Hotelinfos)
                    {
                        string Addresss = " ,Address Not Found ";
                        try
                        {
                            Addresss = Htl.HotelDetails.Element("Address").Element("Address1").Value;
                            if (Htl.HotelDetails.Element("Address").Element("Address2") != null)
                                Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Address2").Value;
                            if (Htl.HotelDetails.Element("Address").Element("City") != null)
                                Addresss += ", " + Htl.HotelDetails.Element("Address").Element("City").Value;
                            if (Htl.HotelDetails.Element("Address").Element("Country") != null)
                                Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Country").Value;
                        }
                        catch (Exception ex)
                        {
                            HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-Addresss");
                            Addresss = " ," +SearchDetails.SearchCity;
                        }
                        HotelInfo.Address = Addresss;

                        HotelInfo.HotelDiscription = "";
                        if (Htl.HotelDetails.Element("Description") != null)
                        {
                            foreach (var discrepsion in Htl.HotelDetails.Descendants("Description"))
                            {
                                if (discrepsion.Element("Type") != null)
                                {
                                    if (discrepsion.Element("Type").Value == "General")
                                    {
                                       // HotelInfo.HotelDiscription = "<strong>Description</strong>: " + System.Web.HttpUtility.HtmlDecode(discrepsion.Element("Text").Value).Replace("&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt;&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt;&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt;&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt; &lt;br/&gt;", " ").Replace("&lt;br/&gt;", " ").Replace("&lt;br/&gt;", "").Replace("&lt;br /&gt;", " ").Replace("&lt;br /&gt;", " ").Replace("&lt;br /&gt; &lt;br/&gt;", " ").Replace("&lt;br /&gt;", " ").Replace("&lt;br /&gt;", " ");
                                        HotelInfo.HotelDiscription = "<strong>Description</strong>: " + System.Web.HttpUtility.HtmlDecode(discrepsion.Element("Text").Value);
                                        break;
                                    }
                                }
                            }
                        }
                        HotelInfo.HotelServices = "";
                        if (Htl.HotelDetails.Element("Amenity") != null)
                            foreach (var flt in Htl.HotelDetails.Descendants("Amenity"))
                            {
                                HotelInfo.HotelServices += flt.Element("Text").Value.Replace(" ", "") + "#";
                            }
                       
                        HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg"; HotelInfo.Latitude = ""; HotelInfo.Longitude = "";

                        if (Htl.HotelDetails.Element("Photo") != null)
                            if (Htl.HotelDetails.Element("Photo").Value.Contains("http:"))
                                HotelInfo.ThumbImg = Htl.HotelDetails.Element("Photo").Element("ThumbnailUrl").Value;
                            else
                                HotelInfo.ThumbImg = "http://www.roomsxml.com" + Htl.HotelDetails.Element("Photo").Element("ThumbnailUrl").Value;
                        if (Htl.HotelDetails.Element("GeneralInfo") != null)
                            if (Htl.HotelDetails.Element("GeneralInfo").Element("Latitude") != null)
                            {
                              HotelInfo.Latitude = Htl.HotelDetails.Element("GeneralInfo").Element("Latitude").Value;
                              HotelInfo.Longitude = Htl.HotelDetails.Element("GeneralInfo").Element("Longitude").Value;
                            }
                        if (Htl.HotelDetails.Element("Stars")!=null)
                            HotelInfo.StarRating = Htl.HotelDetails.Element("Stars").Value;
                        else
                            HotelInfo.StarRating = Htl.HotelDetails.Attribute("stars").Value;
                    }
                }
                else
                {
                    HotelInfo.StarRating = "0";
                    HotelInfo.Address = " ,Address Not Found ";
                    HotelInfo.HotelServices = "";
                    HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg";
                    HotelInfo.Latitude = ""; HotelInfo.Longitude = "";
                    HotelInfo.HotelDiscription = "";
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", CityCode + "_" + HotelCode, document.ToString(), "", "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                HotelInfo.Address = " ,Address Not Found ";
                HotelInfo.HotelServices = "";
                HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg";
                HotelInfo.Latitude = ""; HotelInfo.Longitude = "";
                HotelInfo.HotelDiscription = "";
                HotelInfo.StarRating = "0";
                HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode);
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", CityCode + "_" + HotelCode, "GetGTAHotelInformation", "", "HotelInsert");
            }
            return HotelInfo;
        }
        private string RoomXMLHotelInformation(HotelSearch SearchDetails, string ItemCode)
        {
            string RoomXMLResponce = "";
            try
            {
                SearchDetails = HtlReq.SearchItemInformationRequest(SearchDetails, ItemCode);
                RoomXMLResponce = RoomXMLPostXml(SearchDetails.RoomXMLURL, SearchDetails.GTAItemInformationReq);
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, ItemCode);
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTAItemInformationReq, RoomXMLResponce, "RoomXML", "HotelInsert");

            }
            return RoomXMLResponce;
        }
      
        public RoomComposite RoomXMLRoomDetails(HotelSearch SearchDetails)
        {
            RoomComposite objRoomDetals = new RoomComposite();
            List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
            HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            try
            {
                XDocument document = XDocument.Parse(SearchDetails.RoomXML_HotelSearchRes);
                var RoomsDetails = (from Hotel in document.Element("AvailabilitySearchResult").Descendants("HotelAvailability").Descendants("Result")
                                    where Hotel.Parent.Element("Hotel").Attribute("id").Value.Trim() == SearchDetails.HtlCode
                                    select new{HotelRooms = Hotel}).ToList();

                if (RoomsDetails.Count > 0)
                {
                    #region Hotel Details
                    HotelDetail = GetRoomXMLRoomHotelInformation(SearchDetails.HtlCode);
                    #endregion
                    #region Room Details
                    foreach (var Hotels in RoomsDetails)
                    {
                        try
                        {
                            decimal Rate_Org = 0,  MrktotalRate = 0, AdmMkr = 0, Agtmrk = 0, VenderTax = 0, Agttax = 0;
                            string RoomName = "",  Inclusion = "", Org_RoomRateStr = "", MrkRoomrateStr = "";
                            foreach (var hotelPrice in Hotels.HotelRooms.Descendants("Room"))
                            {
                                RoomName += hotelPrice.Element("RoomType").Attribute("text").Value ;
                                if (SearchDetails.NoofRoom > 1)
                                    RoomName += "<div class='clear1'></div>";
                                decimal OrgRate = Convert.ToDecimal(hotelPrice.Element("Price").Attribute("amt").Value);
                                MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, OrgRate, SearchDetails.ROOMXML_servicetax);

                                Rate_Org += OrgRate;
                                MrktotalRate += MarkList.TotelAmt;
                                AdmMkr += MarkList.AdminMrkAmt;
                                Agtmrk += MarkList.AgentMrkAmt;
                                Agttax += MarkList.AgentServiceTaxAmt;
                                VenderTax += MarkList.VenderServiceTaxAmt;

                                Org_RoomRateStr += hotelPrice.Element("RoomType").Attribute("code").Value + "_" + hotelPrice.Element("Price").Attribute("amt").Value + "/";
                                MrkRoomrateStr = hotelPrice.Element("RoomType").Attribute("code").Value + "_" + MarkList.TotelAmt.ToString() + "/"; ;
                            }
                           
                            if (Hotels.HotelRooms.Element("Room").Element("MealType") != null)
                                Inclusion = Hotels.HotelRooms.Element("Room").Element("MealType").Attribute("text").Value;
                            //Add Hotels Detaisl in List start
                            objRoomList.Add(new RoomList
                            {
                                HotelCode = SearchDetails.HtlCode,
                                RatePlanCode = Hotels.HotelRooms.Attribute("id").Value,
                                RoomTypeCode = Hotels.HotelRooms.Element("Room").Element("RoomType").Attribute("code").Value,
                                RoomName = RoomName,
                                discountMsg = "",DiscountAMT = 0,
                                Total_Org_Roomrate = Rate_Org,
                                TotalRoomrate = MrktotalRate,
                                AdminMarkupPer = MarkList.AdminMrkPercent,
                                AdminMarkupAmt = AdmMkr,
                                AdminMarkupType = MarkList.AdminMrkType,
                                AgentMarkupPer = MarkList.AgentMrkPercent,
                                AgentMarkupAmt = Agtmrk,
                                AgentMarkupType = MarkList.AgentMrkType,
                                AgentServiseTaxAmt = Agttax,
                                V_ServiseTaxAmt = VenderTax,
                                AmountBeforeTax = 0,  Taxes = 0, MrkTaxes = 0, ExtraGuest_Charge =0, Smoking = "",
                                inclusions = Inclusion,
                                CancelationPolicy ="", //RoomXmlCancellationpolicy(SearchDetails, Hotels.HotelRooms.Attribute("id").Value, HotelDetail.StarRating),
                                OrgRateBreakups = Org_RoomRateStr,
                                MrkRateBreakups = MrkRoomrateStr,
                                DiscRoomrateBreakups="",  EssentialInformation = "", RoomDescription = "",
                                Provider = "RoomXML",
                                RoomImage = ""
                            });
                        }
                        catch (Exception ex)
                        {
                          objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                          HotelDA.InsertHotelErrorLog(ex, "Roomsadding");
                        }
                    }
                    #endregion
                    objRoomDetals.RoomDetails = objRoomList;
                    objRoomDetals.SelectedHotelDetail = HotelDetail;
                }
                else
                {
                    objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.RoomXML_HotelSearchReq, SearchDetails.RoomXML_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RoomXML_HotelSearchReq, SearchDetails.RoomXML_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                HotelDA.InsertHotelErrorLog(ex, "RoomXML RoomDetals");
            }
            return objRoomDetals;
        }
        public SelectedHotel GetRoomXMLRoomHotelInformation(string HotelCode)
        {
            SelectedHotel HotelInfo = new SelectedHotel();
            try
            {
                XDocument document = XDocument.Load(ConfigurationManager.AppSettings["RoomXmlHotelDetail"] + "\\" + HotelCode + ".xml");
 
                var Hoteldtl = (from Hotel in document.Descendants("HotelElement") select new { HotelDetails = Hotel }).ToList();
                if (Hoteldtl.Count == 0)
                    Hoteldtl = (from Hotel in document.Elements("AvailabilitySearchResult").Elements("HotelAvailability").Descendants("Hotel") select new { HotelDetails = Hotel }).ToList();
                if (Hoteldtl.Count > 0)
                {
                    foreach (var Htl in Hoteldtl)
                    {
                        string Addresss = "Address Not Found",Telephones = "";
                        try
                        {
                            Addresss = Htl.HotelDetails.Element("Address").Element("Address1").Value;
                            if (Htl.HotelDetails.Element("Address").Element("Address2") != null)
                                Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Address2").Value;
                            if (Htl.HotelDetails.Element("Address").Element("City") != null)
                                Addresss += ", " + Htl.HotelDetails.Element("Address").Element("City").Value;
                            if (Htl.HotelDetails.Element("Address").Element("Country") != null)
                                Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Country").Value;

                            if (Htl.HotelDetails.Element("Address").Element("Tel") != null)
                                Telephones = Htl.HotelDetails.Element("Address").Element("Tel").Value;
                            HotelInfo.HotelContactNo = Telephones;
                        }
                        catch (Exception ex)
                        {
                            HotelDA.InsertHotelErrorLog(ex,  HotelCode + "-Addresss");
                        }
                        HotelInfo.HotelAddress = Addresss;

                        string hoteldiscription = "", facliStr = "";
                        HotelInfo.Attraction = ""; HotelInfo.RoomAmenities = "";
                        HotelInfo.HotelDescription = "<strong>Description</strong>: ";
                        if (Htl.HotelDetails.Element("Description") != null)
                        {
                            foreach (var discrepsion in Htl.HotelDetails.Descendants("Description"))
                            {
                                if (discrepsion.Element("Type") != null)
                                {
                                    hoteldiscription += "<div style='padding: 4px 0;'><span style='text-transform:uppercase; font-size:13px;font-weight: bold;color:#B21E55;'>" + discrepsion.Element("Type").Value + ": </span> " + discrepsion.Element("Text").Value + "</div><hr />";
                                }
                            }
                        }
                        HotelInfo.HotelDescription = hoteldiscription;
                        //Hotel facility Facilities
                        try
                        {
                            if (Htl.HotelDetails.Element("Facilities") != null)
                                foreach (var flt in Htl.HotelDetails.Element("Facilities").Descendants("Facility"))
                                {
                                    facliStr += "<div class='check1'>" + flt.Value.Trim() + "</div>";
                                }
                            if (Htl.HotelDetails.Element("Amenity") != null)
                                foreach (var flt in Htl.HotelDetails.Descendants("Amenity"))
                                {
                                    facliStr += "<div class='check1'>" + flt.Element("Text").Value.Trim() + "</div>"; ;
                                }
                        }
                        catch (Exception ex)
                        {
                            HotelDA.InsertHotelErrorLog(ex, HotelCode + "-Facilities");
                        }
                        HotelInfo.HotelAmenities = facliStr;
                        string imgdiv = "";
                        //Images Links ImageLinks
                        ArrayList Images = new ArrayList();
                        try
                        {
                            int im = 0;
                            if (Htl.HotelDetails.Element("Photo") != null)
                            {
                                foreach (var img in Htl.HotelDetails.Descendants("Photo"))
                                {
                                    im++;
                                    if (Htl.HotelDetails.Element("Photo").Value.Contains("http:"))
                                    {
                                        imgdiv += "<img id='img" + im + "' src='" + img.Element("Url").Value + "' onmouseover='return ShowHtlImg(this);' title='" + img.Element("PhotoType").Value + "' alt='' class='imageHtlDetailsshow' />";
                                        Images.Add(img.Element("Url").Value);
                                    }
                                    else
                                    {
                                        imgdiv += "<img id='img" + im + "' src='http://www.roomsxml.com" + img.Element("Url").Value + "' onmouseover='return ShowHtlImg(this);' title='" + img.Element("PhotoType").Value + "' alt='' class='imageHtlDetailsshow' />";
                                        Images.Add("http://www.roomsxml.com" + img.Element("Url").Value);
                                    }
                                }
                            }
                            else Images.Add("Images/Hotel/NoImage.jpg");
                            HotelInfo.ThumbnailUrl = Images[0].ToString();
                            HotelInfo.HotelImage = imgdiv;
                     }
                     catch (Exception ex)
                     {
                         HotelDA.InsertHotelErrorLog(ex, HotelCode + "-ImageLinks");
                     }
                        HotelInfo.Lati_Longi = "";
                        if (Htl.HotelDetails.Element("GeneralInfo") != null)
                            if (Htl.HotelDetails.Element("GeneralInfo").Element("Latitude") != null)
                            {
                                HotelInfo.Lati_Longi = Htl.HotelDetails.Element("GeneralInfo").Element("Latitude").Value + "," + Htl.HotelDetails.Element("GeneralInfo").Element("Longitude").Value;
                            }
                        HotelInfo.StarRating = "0";
                        if (Htl.HotelDetails.Element("Stars") != null)
                            HotelInfo.StarRating = Htl.HotelDetails.Element("Stars").Value;
                        else
                            HotelInfo.StarRating = Htl.HotelDetails.Attribute("stars").Value;
                    }
                }
                else
                {
                    HotelInfo.StarRating = "0";
                    HotelInfo.HotelAddress = "";
                    HotelInfo.HotelContactNo = "";
                    HotelInfo.Attraction = "";
                    HotelInfo.HotelDescription = "";
                    HotelInfo.RoomAmenities = "";
                    HotelInfo.HotelAmenities = "";
                    HotelInfo.Lati_Longi = "";
                    HotelInfo.HotelImage = "<li><a href='Images/NoImage.jpg'><img src='Images/NoImage.jpg' width='40px' height='40px' title='Image not found' /></a></li>";
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", HotelCode, document.ToString(), "", "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, HotelCode + "-GetRoomXMLRoomHotelInformation");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", "", "", HotelCode, "HotelInsert");
            }
            return HotelInfo;
        }
       
       public HotelBooking RoomXMLHotelsPreBooking(HotelBooking HotelDetail)
        {
            try
            {
                if (HotelDetail.RoomXMLURL !=null)
                {
                    HotelDetail.BookingType = "prepare";
                    HotelDetail.ProBookingReq = HtlReq.BookingRoomXmlRequest(HotelDetail);
                  
                    HotelDetail.ProBookingRes = RoomXMLPostXml(HotelDetail.RoomXMLURL, HotelDetail.ProBookingReq);

                    //XDocument document1;
                    //string XMLFile = "D:\\HotelPriject\\B2BHotel_25_10_2014\\HotelRoomXML\\BookingPrepare_Response.xml";
                    //document1 = XDocument.Load(XMLFile);
                    //HotelDetail.ProBookingRes = document1.ToString();

                    if (!HotelDetail.ProBookingRes.Contains("<Error>"))
                    {
                        string resp=HotelDetail.ProBookingRes.Replace(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", String.Empty).Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", String.Empty);
                        XDocument document = XDocument.Parse(resp.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        var Hotels_temps = (from Hotel in document.Elements("BookingCreateResult").Elements("Booking").Descendants("HotelBooking")
                                            where Hotel.Element("HotelId").Value == HotelDetail.HtlCode 
                                            select new { ItemPrice = Hotel.Element("Room").Element("TotalSellingPrice").Attribute("amt"), Confirmation = Hotel.Element("Status") }).ToList();
                        decimal orgrateNow = 0;
                        foreach (var Htl in Hotels_temps)
                        {
                            orgrateNow +=Convert.ToDecimal(Htl.ItemPrice.Value);
                        }
                        //if (HotelDetail.Total_Org_Roomrate <= orgrateNow)
                        if (HotelDetail.Total_Org_Roomrate >= orgrateNow)
                            HotelDetail.ProBookingID = "true";
                        else
                        {
                            HotelDetail.BookingID = "Rate Not matching"; HotelDetail.ProBookingID = "false";
                        }
                    }
                    else
                    {
                        HotelDetail.BookingID = "Exception"; HotelDetail.ProBookingID = "false";
                    }
                }
                else
                {
                    HotelDetail.BookingID = "BookingNotAlowed"; HotelDetail.ProBookingID = "false";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "RoomXMLHotelsPreBooking");
                HotelDetail.BookingID = ex.Message; HotelDetail.ProBookingID = "false";
            }
            return HotelDetail;
        }

       public HotelBooking RoomXMLHotelsBooking(HotelBooking HotelDetail)
       {
           try
           {
               HotelDetail.BookingConfRes = "";
               HotelDetail.BookingType = "confirm";
               HotelDetail.BookingConfReq = HtlReq.BookingRoomXmlRequest(HotelDetail);
               HotelDetail.BookingConfRes = RoomXMLPostXml(HotelDetail.RoomXMLURL, HotelDetail.BookingConfReq);
               if (!HotelDetail.BookingConfRes.Contains("<Error>"))
               {
                   XDocument document = XDocument.Parse(HotelDetail.BookingConfRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                   var HtlBooking = (from book in document.Elements("BookingCreateResult").Descendants("Booking")
                                     select new
                                     {
                                         bookConfirmation = book.Element("Id"),
                                         BookingStatus = book.Element("HotelBooking").Element("Status"),
                                         BookingReferences = book.Element("HotelBooking").Element("VoucherInfo").Element("VoucherRef")
                                     }).ToList();

                   foreach (var Htlbook in HtlBooking)
                   {
                       HotelDetail.BookingID = Htlbook.bookConfirmation.Value;
                       HotelDetail.Status = Htlbook.BookingStatus.Value;
                       HotelDetail.ProBookingID = Htlbook.BookingReferences.Value;
                   }
               }
               else
               {
                   HotelDetail.ProBookingID = "Exception";
                   HotelDetail.BookingID = "";
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "GTAHotelsBooking");
               HotelDetail.ProBookingID = ex.Message; HotelDetail.BookingID = "";
           }
           return HotelDetail;
       }

       public HotelCancellation RoomXMLCancelBooking(HotelCancellation HotelDetail)
       {
           HotelDetail.CancellationCharge = 0; HotelDetail.CancellationID = "";
           try
           {
               HotelDetail.BookingCancelReq = HtlReq.HotelCancellationRequest(HotelDetail, "confirm");
               HotelDetail.BookingCancelRes = RoomXMLPostXml(HotelDetail.HotelUrl, HotelDetail.BookingCancelReq);
              
               if (!HotelDetail.BookingCancelRes.Contains("<Error>"))
               {
                   XDocument document = XDocument.Parse(HotelDetail.BookingCancelRes);
                   var HtlBooking = (from book in document.Elements("BookingCancelResult").Descendants("Booking")
                                     select new{ Cancelltion = book.Element("HotelBooking"), CancelAmt = book.Element("Charge")}).ToList();

                   foreach (var Htlbook in HtlBooking)
                   {
                       HotelDetail.CancellationID = Htlbook.Cancelltion.Element("Id").Value;
                       HotelDetail.CancelStatus = Htlbook.Cancelltion.Element("Status").Value;
                       if (Htlbook.CancelAmt != null)
                            HotelDetail.CancellationCharge = Convert.ToDecimal(Htlbook.CancelAmt.Element("TotalSellingPrice").Attribute("amt").Value);
                   }
               }

               if (HotelDetail.CancelStatus == "cancelled")
                   HotelDetail.CancelStatus = "Cancelled";
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "RoomXMLPreCancelBooking");
           }

           return HotelDetail;
       }

       public string RoomXmlCancellationpolicy(HotelSearch HotelDetail, string Quadid, string StarRating)
       {
           string Cancellationpolcy = "", SupplierNotes = "";
           try
           {
               int i = 0;
               HotelDetail.HtlRoomCode = Quadid;
               string canpolyResponse = RoomXMLPostXml(HotelDetail.RoomXMLURL, HtlReq.CancelationPolicyRoomXmlRequest(HotelDetail));
               if (!canpolyResponse.Contains("<Error>"))
               {
                   HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
                   MarkupDS = objhtlDa.GetHtlMarkup("", HotelDetail.AgentID, HotelDetail.Country, HotelDetail.SearchCity, "", HotelDetail.HtlType, "", "", 0, "SEL");

                   XDocument document = XDocument.Parse(canpolyResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                   var Hotels_temps = (from Hotel in document.Elements("BookingCreateResult").Elements("Booking").Descendants("HotelBooking")
                                       select new { CanxFees = Hotel.Element("Room") }).ToList();

                   foreach (var Htl in Hotels_temps)
                   {
                       if (Htl.CanxFees.Element("Messages") != null)
                       {
                           foreach (var notes in Htl.CanxFees.Element("Messages").Descendants("Message"))
                           {
                               if (notes.Element("Text").Value != SupplierNotes && notes.Element("Text").Value != "")
                                   SupplierNotes += notes.Element("Text").Value;
                           }
                       }
                       if (i > 0)
                           Cancellationpolcy += "<div class='clear1'></div>";
                       if (Hotels_temps.Count > 1)
                           Cancellationpolcy += Htl.CanxFees.Element("RoomType").Attribute("text").Value + " policy<div class='clear1'></div>";
                       foreach (var htlcanc in Htl.CanxFees.Descendants("CanxFees"))
                       {
                           foreach (var htlPoliy in htlcanc.Descendants("Fee"))
                           {
                               if (htlPoliy.Attribute("from") == null || htlPoliy.Element("Amount") == null)
                               {
                                   if (htlPoliy.Element("Amount") != null && htlPoliy.Element("from") == null)
                                   {
                                     //  decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                       Cancellationpolcy += "<li>The cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), 2).ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                   }
                                   else if (htlPoliy.Element("from") == null && htlPoliy.Element("Amount") == null)
                                       Cancellationpolcy += "<li>No refund if you cancel this booking. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                  // break;
                               }
                               if (htlPoliy.Attribute("from") != null && htlPoliy.Element("Amount") != null)
                               {
                                  // decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                   Cancellationpolcy += "<li>Cancellation done from " + htlPoliy.Attribute("from").Value.Replace("T00:00:00", string.Empty) + " the cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value),2).ToString() + "/-.</li>";
                               }
                           }
                       }
                       i++;
                   }
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "RoomXmlCancellationpolicy");
               Cancellationpolcy += Cancellationpolcy + SupplierNotes + ex.Message;
           }
           //return "<span>" + Cancellationpolcy + "<li>" + SupplierNotes.Replace("lt;bgt;", "").Replace("lt;bgt;", "").Replace("lt;/brgt;", "").Replace("lt;/brgt;", "") + "</li></span>";
           if (SupplierNotes !="")
               return System.Web.HttpUtility.HtmlDecode("<span>" + Cancellationpolcy + "<li>" + SupplierNotes + "</li></span>");
           else
               return System.Web.HttpUtility.HtmlDecode("<span>" + Cancellationpolcy +  SupplierNotes + "</span>");
       }

       public string RoomXmlCancellationPolicy_ForRoom(HotelSearch HotelDetail, string Quadid)
       {
           string Cancellationpolcy = "", SupplierNotes = "";
           try
           {
               int i = 0;
               HotelDetail.HtlRoomCode = Quadid;
               string canpolyResponse = RoomXMLPostXml(HotelDetail.RoomXMLURL, HtlReq.CancelationPolicyRoomXmlRequest(HotelDetail));
               if (!canpolyResponse.Contains("<Error>"))
               {
                  // HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
                   //MarkupDS = objhtlDa.GetHtlMarkup("", HotelDetail.AgentID, HotelDetail.Country, HotelDetail.SearchCity, "", HotelDetail.HtlType, "", "", 0, "SEL");
                  
                   XDocument document = XDocument.Parse(canpolyResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                   var Hotels_temps = (from Hotel in document.Elements("BookingCreateResult").Elements("Booking").Descendants("HotelBooking")
                                       select new { CanxFees = Hotel.Element("Room") }).ToList();

                   foreach (var Htl in Hotels_temps)
                   {
                       if (Htl.CanxFees.Element("Messages") != null)
                       {
                           foreach (var notes in Htl.CanxFees.Element("Messages").Descendants("Message"))
                           {
                               if (notes.Element("Text").Value != SupplierNotes && notes.Element("Text").Value != "")
                                   SupplierNotes += notes.Element("Text").Value;
                           }
                       }
                       if (i > 0)
                           Cancellationpolcy += "<div class='clear1'></div>";
                       if (Hotels_temps.Count > 1)
                           Cancellationpolcy += Htl.CanxFees.Element("RoomType").Attribute("text").Value + " policy<div class='clear1'></div>";
                       foreach (var htlcanc in Htl.CanxFees.Descendants("CanxFees"))
                       {
                           foreach (var htlPoliy in htlcanc.Descendants("Fee"))
                           {
                               if (htlPoliy.Attribute("from") == null || htlPoliy.Element("Amount") == null)
                               {
                                   if (htlPoliy.Element("Amount") != null && htlPoliy.Element("from") == null)
                                   {
                                      // decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, HotelDetail.StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                       Cancellationpolcy += "<li>The cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value),2).ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                   }
                                   else if (htlPoliy.Element("from") == null && htlPoliy.Element("Amount") == null)
                                       Cancellationpolcy += "<li>No refund if you cancel this booking. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                   //break;
                               }
                               if (htlPoliy.Attribute("from") != null && htlPoliy.Element("Amount") != null)
                               {
                                  // decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, HotelDetail.StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                   Cancellationpolcy += "<li>Cancellation done from " + htlPoliy.Attribute("from").Value.Replace("T00:00:00", string.Empty) + " the cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), 2).ToString() + "/-.</li>";
                               }
                           }
                       }
                       i++;
                   }
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "RoomXmlCancellationPolicy_ForRoom");
               Cancellationpolcy += Cancellationpolcy + SupplierNotes + ex.Message;
           }
          // return "<span>" + Cancellationpolcy + "<li>" + SupplierNotes.Replace("lt;bgt;", "").Replace("lt;bgt;", "").Replace("lt;/brgt;", "").Replace("lt;/brgt;", "") + "</li></span>";
           if (SupplierNotes !="")
                return System.Web.HttpUtility.HtmlDecode("<span>" + Cancellationpolcy + "<li>" + SupplierNotes + "</li></span>");
           else
               return System.Web.HttpUtility.HtmlDecode("<span>" + Cancellationpolcy  + "</span>");
       }

       public string RoomXmlCancellationPolicy_ForRoom_withMrk(HotelSearch HotelDetail, string Quadid)
       {
           string Cancellationpolcy = "", SupplierNotes = "";
           try
           {
               int i = 0;
               HotelDetail.HtlRoomCode = Quadid;
               string canpolyResponse = RoomXMLPostXml(HotelDetail.RoomXMLURL, HtlReq.CancelationPolicyRoomXmlRequest(HotelDetail));
               if (!canpolyResponse.Contains("<Error>"))
               {
                   HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
                   MarkupDS = objhtlDa.GetHtlMarkup("", HotelDetail.AgentID, HotelDetail.Country, HotelDetail.SearchCity, "", HotelDetail.HtlType, "", "", 0, "SEL");

                   XDocument document = XDocument.Parse(canpolyResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                   var Hotels_temps = (from Hotel in document.Elements("BookingCreateResult").Elements("Booking").Descendants("HotelBooking")
                                       select new { CanxFees = Hotel.Element("Room") }).ToList();

                   foreach (var Htl in Hotels_temps)
                   {
                       if (Htl.CanxFees.Element("Messages") != null)
                       {
                           foreach (var notes in Htl.CanxFees.Element("Messages").Descendants("Message"))
                           {
                               if (notes.Element("Text").Value != SupplierNotes && notes.Element("Text").Value != "")
                                   SupplierNotes += notes.Element("Text").Value;
                           }
                       }
                       if (i > 0)
                           Cancellationpolcy += "<div class='clear1'></div>";
                       if (Hotels_temps.Count > 1)
                           Cancellationpolcy += Htl.CanxFees.Element("RoomType").Attribute("text").Value + " policy<div class='clear1'></div>";
                       foreach (var htlcanc in Htl.CanxFees.Descendants("CanxFees"))
                       {
                           foreach (var htlPoliy in htlcanc.Descendants("Fee"))
                           {
                               if (htlPoliy.Attribute("from") == null || htlPoliy.Element("Amount") == null)
                               {
                                   if (htlPoliy.Element("Amount") != null && htlPoliy.Element("from") == null)
                                   {
                                       decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, HotelDetail.StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                       Cancellationpolcy += "<li>The cancellation charge will be Rs. " + cancelAmt.ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                   }
                                   else if (htlPoliy.Element("from") == null && htlPoliy.Element("Amount") == null)
                                       Cancellationpolcy += "<li>No refund if you cancel this booking. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                   break;
                               }
                               if (htlPoliy.Attribute("from") != null && htlPoliy.Element("Amount") != null)
                               {
                                   decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, HotelDetail.StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                   Cancellationpolcy += "<li>Cancellation done from " + htlPoliy.Attribute("from").Value.Replace("T00:00:00", string.Empty) + " the cancellation charge will be Rs. " + cancelAmt.ToString() + "/-.</li>";
                               }
                           }
                       }
                       i++;
                   }
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "RoomXmlCancellationPolicy_ForRoom");
               Cancellationpolcy += Cancellationpolcy + SupplierNotes + ex.Message;
           }
           return "<span>" + Cancellationpolcy + "<li>" + SupplierNotes + "</li></span>";
       }
       public string RoomXmlCancellationpolicy_withMrk(HotelSearch HotelDetail, string Quadid, string StarRating)
       {
           string Cancellationpolcy = "", SupplierNotes = "";
           try
           {
               int i = 0;
               HotelDetail.HtlRoomCode = Quadid;
               string canpolyResponse = RoomXMLPostXml(HotelDetail.RoomXMLURL, HtlReq.CancelationPolicyRoomXmlRequest(HotelDetail));
               if (!canpolyResponse.Contains("<Error>"))
               {
                   HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
                   MarkupDS = objhtlDa.GetHtlMarkup("", HotelDetail.AgentID, HotelDetail.Country, HotelDetail.SearchCity, "", HotelDetail.HtlType, "", "", 0, "SEL");

                   XDocument document = XDocument.Parse(canpolyResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                   var Hotels_temps = (from Hotel in document.Elements("BookingCreateResult").Elements("Booking").Descendants("HotelBooking")
                                       select new { CanxFees = Hotel.Element("Room") }).ToList();

                   foreach (var Htl in Hotels_temps)
                   {
                       if (Htl.CanxFees.Element("Messages") != null)
                       {
                           foreach (var notes in Htl.CanxFees.Element("Messages").Descendants("Message"))
                           {
                               if (notes.Element("Text").Value != SupplierNotes && notes.Element("Text").Value != "")
                                   SupplierNotes += notes.Element("Text").Value;
                           }
                       }
                       if (i > 0)
                           Cancellationpolcy += "<div class='clear1'></div>";
                       if (Hotels_temps.Count > 1)
                           Cancellationpolcy += Htl.CanxFees.Element("RoomType").Attribute("text").Value + " policy<div class='clear1'></div>";
                       foreach (var htlcanc in Htl.CanxFees.Descendants("CanxFees"))
                       {
                           foreach (var htlPoliy in htlcanc.Descendants("Fee"))
                           {
                               if (htlPoliy.Attribute("from") == null || htlPoliy.Element("Amount") == null)
                               {
                                   if (htlPoliy.Element("Amount") != null && htlPoliy.Element("from") == null)
                                   {
                                       decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                       Cancellationpolcy += "<li>The cancellation charge will be Rs. " + cancelAmt.ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                   }
                                   else if (htlPoliy.Element("from") == null && htlPoliy.Element("Amount") == null)
                                       Cancellationpolcy += "<li>No refund if you cancel this booking. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                   break;
                               }
                               if (htlPoliy.Attribute("from") != null && htlPoliy.Element("Amount") != null)
                               {
                                   decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                   Cancellationpolcy += "<li>Cancellation done from " + htlPoliy.Attribute("from").Value.Replace("T00:00:00", string.Empty) + " the cancellation charge will be Rs. " + cancelAmt.ToString() + "/-.</li>";
                               }

                               //if (htlPoliy.Attribute("from") != null && htlPoliy.Element("Amount") != null)
                               //{
                               //    decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                               //    Cancellationpolcy += "<li>Cancellation done from " + htlPoliy.Attribute("from").Value + " the cancellation charge will be Rs. " + cancelAmt.ToString() + "/-.</li>";
                               //}
                               //else
                               //{
                               //    if (htlPoliy.Element("Amount") != null)
                               //    {
                               //        decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                               //        Cancellationpolcy += "<li>The cancellation charge will be Rs. " + cancelAmt.ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                               //    }
                               //    else
                               //        Cancellationpolcy += "<li>No refund if you cancel this booking. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                               //}Type Notes Supplier
                           }
                       }
                       i++;
                   }
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "RoomXmlCancellationpolicy");
               Cancellationpolcy += Cancellationpolcy + SupplierNotes + ex.Message;
           }
           return "<span>" + Cancellationpolcy + "<li>" + SupplierNotes + "</li></span>";
       }
   }
}
