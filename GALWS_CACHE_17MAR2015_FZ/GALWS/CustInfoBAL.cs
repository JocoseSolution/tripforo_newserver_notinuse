﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.DAL;
using STD.Shared;
using System.Data;

namespace STD.BAL
{
    public class CustInfoBAL
    {
        //COMMENTED AS METHOD IS PRESENT IN FLIGHT COMMONDAL

        //public int Insert_Flt_PaxDetail_BAL(string ORDERID, string TITLE, string FNAME, string MNAME, string LNAME, string PAXTYPE, string TICKETNO, string DOB, string FFNUMBER, string FFAIRLINE, string MEALTYPE, string SEATTYPE, bool ISPRIMARY, string ASSOCIATEDPAXNAME)
        //{
        //    int res = 0;
        //    FltPaxDetails objFpaxShared = new FltPaxDetails();
        //    FlightCommonDAL objFltComDal = new FlightCommonDAL();
        //    objFpaxShared.OrderID = ORDERID;
        //    objFpaxShared.Title = TITLE;
        //    objFpaxShared.FName = FNAME;
        //    objFpaxShared.MName = MNAME;
        //    objFpaxShared.LName = LNAME;
        //    objFpaxShared.PaxType = PAXTYPE;
        //    objFpaxShared.TicketNo = TICKETNO;
        //    objFpaxShared.DOB = DOB;
        //    objFpaxShared.FFNumber = FFNUMBER;
        //    objFpaxShared.FFAirline = FFAIRLINE;
        //    objFpaxShared.MealType = MEALTYPE;
        //    objFpaxShared.SeatType = SEATTYPE;
        //    objFpaxShared.IsPrimary = ISPRIMARY;
        //    objFpaxShared.InfAssociatePaxName = ASSOCIATEDPAXNAME;
        //    res = objFltComDal.InsertFltPxDetails(objFpaxShared);
        //    return res;
        //}

        public DataTable Fetch_Customer_Info(string tid)
        {
            CustInfoDAL objCsutInfoDal = new CustInfoDAL();
            return objCsutInfoDal.Fetch_Cust_Info(tid);
        }
    }
}
