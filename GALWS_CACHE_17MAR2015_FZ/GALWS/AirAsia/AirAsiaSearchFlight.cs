﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.XPath;
using STD.Shared;
using STD.BAL;
using ITZERRORLOG;
using System.Xml.Linq;
using System.Collections;
using System.Web;

namespace GALWS.AirAsia
{
   public class AirAsiaSearchFlight
    {
        public AirAsiaSearchFlight()
        {
          //  AirAsiaUserid = userid; LoginUrl = url; passw = pass; SearchURLs = SearchURL;
        }
        public List<FlightSearchResults> GetAirAsiaAvailibility(FlightSearch objFlt, bool RTFS, string fareType, bool isLCC, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, List<FlightCityList> CityList, CredentialList CrdList, HttpContext contx, List<MISCCharges> MiscList)
        {
            HttpContext.Current = contx;
            List<FlightSearchResults> fsrList = new List<FlightSearchResults>(); List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            try
            {
                List<GALWS.AirAsia.CurrencyRate> currancyinfo = new List<GALWS.AirAsia.CurrencyRate>();
                currancyinfo=  AirAsiaUtility.CurrancyExchangeRate(objFlt.HidTxtDepCity.Split(',')[1], constr, currancyinfo);
                if (currancyinfo.Count > 0)
                {
                    string PromoCode = "";
                    AirAsiaLogon objlogindtl = new AirAsiaLogon();
                    string sessionids = objlogindtl.GetSessionID(CrdList.UserID, CrdList.Password, CrdList.LoginID);
                    string AvailibilityRequest = AirAsiaAvailibilityRequest(objFlt, fareType, sessionids, currancyinfo[0].CurrancyCode, constr, CrdList.UserID, ref PromoCode);
                    string AvailibilityResponse = AirAsiaUtility.AirAsiaPostXML(CrdList.LoginPWD, "http://tempuri.org/IFareService/GetAvailabilityWithTaxes", AvailibilityRequest);
                    if (objFlt.Trip != Trip.I)
                    {
                        AirAsiaUtility.SaveFile(AvailibilityRequest, "SearchReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_" + fareType + RTFS.ToString());
                        AirAsiaUtility.SaveFile(AvailibilityResponse, "SearchRes_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_" + fareType + RTFS.ToString());
                    }
                    else
                    {
                        AirAsiaUtility.SaveFile(AvailibilityRequest, "SearchReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_I");
                        AirAsiaUtility.SaveFile(AvailibilityResponse, "SearchRes_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_I");
                    }
                    try
                    {
                        if (!AvailibilityResponse.Contains("<Errors>"))
                        {
                            XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; //XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
                            XDocument AirAsiaDocument = XDocument.Parse(AvailibilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                            XElement AvailabilityResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetAvailabilityWithTaxesResponse").Element("GetAvailabilityWithTaxesResult");
                            if (AvailabilityResult != null)
                            {
                                if (AvailabilityResult.Element("Schedule") != null)
                                {
                                    if (AvailabilityResult.Element("Schedule").Element("JourneyDateMarket") != null)
                                    {
                                        IEnumerable<XElement> JourneyDateMarket = AvailabilityResult.Element("Schedule").Elements("JourneyDateMarket");
                                        if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.D && RTFS == true)
                                        {
                                            if (JourneyDateMarket.Count() < 2)
                                                goto NikalLe;
                                        }
                                        else if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                        {
                                            if (JourneyDateMarket.Count() < 2)
                                                goto NikalLe;
                                        }
                                        int flight = 1;
                                        foreach (var JourneyDate in JourneyDateMarket)
                                        {
                                            IEnumerable<XElement> Journey = JourneyDate.Elements("Journeys").Elements("Journey");
                                            int lineNum = 1;
                                            foreach (var Journeyss in Journey)
                                            {
                                                int legno = 1; bool linenuberTrueFalse = true;
                                                int seg = 0; DateTime SegSTA = DateTime.Now, SegSTD = DateTime.Now;
                                                IEnumerable<XElement> Segment = Journeyss.Element("Segments").Elements("Segment");
                                                #region Fare calculation
                                                decimal Adtbasefares = 0, AdtTaxs = 0, AdtFSur = 0, AdtWO = 0, AdtOT = 0, Chdbasefares = 0, ChdTaxs = 0, ChdFSur = 0, ChdWO = 0, ChdOT = 0, AdtDisc = 0, ChdDisc = 0;
                                                string Promofares = "";
                                                foreach (var fltAllDetail in Segment)
                                                {
                                                    if (fltAllDetail.Element("Fares").Element("Fare") != null)
                                                    {
                                                        IEnumerable<XElement> Farefilter = fltAllDetail.Element("Fares").Elements("Fare");
                                                        if (objFlt.Cabin != "")
                                                        {
                                                            switch (objFlt.Cabin)
                                                            {
                                                                case "C":
                                                                    Farefilter = Farefilter.Where(x => x.Element("ProductClass").Value == "PM");
                                                                    break;
                                                                case "Y":
                                                                    Farefilter = Farefilter.Where(x => x.Element("ProductClass").Value == "EC");
                                                                    break;
                                                                case "W":
                                                                    Farefilter = Farefilter.Where(x => x.Element("ProductClass").Value == "HF");
                                                                    break;
                                                            }
                                                        }
                                                        foreach (var Fares in Farefilter)
                                                        {
                                                            IEnumerable<XElement> objAdt = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "ADT");
                                                            IEnumerable<XElement> objChd = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "CHD");
                                                            #region Adult
                                                            decimal ABfare = 0, ATax = 0, AOT = 0, ADisc = 0;

                                                            foreach (var adt in objAdt)
                                                            {
                                                                IEnumerable<XElement> ServiceCharges = adt.Element("ServiceCharges").Elements("BookingServiceCharge");
                                                                decimal basefare = 0, Tax = 0, othrcharge = 0, Discount = 0;//
                                                                foreach (var ChargeType in ServiceCharges)
                                                                {
                                                                    if (ChargeType.Element("ChargeType").Value.Trim() == "FarePrice")
                                                                        basefare += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    else if (ChargeType.Element("ChargeType").Value.Trim() == "Discount" || ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                                                        Discount += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    else if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                                                        Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    else
                                                                        othrcharge += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    if (ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                                                        Promofares = PromoCode;
                                                                }
                                                                ABfare += basefare;
                                                                ATax += Tax;
                                                                AOT += othrcharge;
                                                                ADisc += Discount;
                                                            }
                                                            Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;

                                                            #endregion
                                                            #region Child
                                                            decimal CBfare = 0, CTax = 0, COT = 0, CDisc = 0;//
                                                            foreach (var chd in objChd)
                                                            {
                                                                IEnumerable<XElement> ServiceCharges = chd.Element("ServiceCharges").Elements("BookingServiceCharge");
                                                                decimal basefare = 0, Tax = 0, othrcharge = 0, Discount = 0;//
                                                                foreach (var ChargeType in ServiceCharges)
                                                                {
                                                                    if (ChargeType.Element("ChargeType").Value.Trim() == "FarePrice")
                                                                        basefare += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    else if (ChargeType.Element("ChargeType").Value.Trim() == "Discount" || ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                                                        Discount += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    else if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                                                        Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    else
                                                                        othrcharge += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                    if (ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                                                        Promofares = PromoCode;
                                                                }
                                                                CBfare += basefare;
                                                                CTax += Tax;
                                                                COT += othrcharge;
                                                                CDisc += Discount;
                                                            }
                                                            Chdbasefares += CBfare; ChdTaxs += CTax; ChdOT += COT; ChdDisc += CDisc;
                                                            #endregion
                                                        }
                                                    }
                                                    //seg++;
                                                    //if (seg == 1)
                                                    //    SegSTD = Convert.ToDateTime(fltAllDetail.Element("STD").Value.Trim());
                                                    //else if (seg == Segment.Count())
                                                    //    SegSTA = Convert.ToDateTime(fltAllDetail.Element("STA").Value.Trim());
                                                }
                                                //TimeSpan SegtsTimeSpan = SegSTA.Subtract(SegSTD);
                                               // string totalDuration = GetTimeInHrsAndMin(Convert.ToInt32(SegtsTimeSpan.TotalMinutes));
                                                #endregion

                                                if ( Segment.Count() > 0)
                                                {
                                                    DateTime STA = Convert.ToDateTime(Segment.ElementAt(Segment.Count() - 1).Element("STA").Value.Trim());
                                                    TimeSpan tsTimeSpan = STA.Subtract(Convert.ToDateTime(Segment.ElementAt(0).Element("STD").Value.Trim()));
                                                  
                                                    foreach (var fltAllDetail in Segment)
                                                    {
                                                        #region Filtering Fare class in case of a particular class search
                                                        if (fltAllDetail.Element("Fares").Element("Fare") != null)
                                                        {
                                                            DataTable CommDt = new DataTable();
                                                            Hashtable STTFTDS = new Hashtable();

                                                            IEnumerable<XElement> Farefilter = fltAllDetail.Element("Fares").Elements("Fare");
                                                            if (objFlt.Cabin != "")
                                                            {
                                                                switch (objFlt.Cabin)
                                                                {
                                                                    case "C":
                                                                        Farefilter = Farefilter.Where(x => x.Element("ProductClass").Value == "PM");
                                                                        break;
                                                                    case "Y":
                                                                        Farefilter = Farefilter.Where(x => x.Element("ProductClass").Value == "EC");
                                                                        break;
                                                                    case "W":
                                                                        Farefilter = Farefilter.Where(x => x.Element("ProductClass").Value == "HF");
                                                                        break;
                                                                }
                                                            }
                                                        #endregion
                                                            foreach (var Fares in Farefilter)
                                                            {
                                                                foreach (var fltdtl in fltAllDetail.Element("Legs").Elements("Leg"))
                                                                {
                                                                    FlightSearchResults objfsr = new FlightSearchResults();
                                                                    float srvChargeAdt = 0, srvChargeChd = 0;
                                                                    objfsr.Stops = (fltAllDetail.Element("Legs").Elements("Leg").Count() + Segment.Count() - 2).ToString() + "-Stop";
                                                                    objfsr.LineNumber = lineNum; //Segment.Count()
                                                                    objfsr.Flight = flight.ToString();
                                                                    objfsr.Leg = legno;
                                                                    legno++;
                                                                    objfsr.Adult = objFlt.Adult;
                                                                    objfsr.Child = objFlt.Child;
                                                                    objfsr.Infant = objFlt.Infant;

                                                                    objfsr.depdatelcc = fltdtl.Element("STD").Value.Trim();//Convert.ToDateTime(fltdtl.Element("STD").Value.Trim()).ToString();
                                                                    objfsr.arrdatelcc = fltdtl.Element("STA").Value.Trim(); //Convert.ToDateTime(fltdtl.Element("STA").Value.Trim()).ToString();
                                                                    objfsr.Departure_Date = Convert.ToDateTime(fltdtl.Element("STD").Value.Trim()).ToString("dd MMM");
                                                                    objfsr.Arrival_Date = Convert.ToDateTime(fltdtl.Element("STA").Value.Trim()).ToString("dd MMM");
                                                                    objfsr.DepartureDate = Convert.ToDateTime(fltdtl.Element("STD").Value.Trim()).ToString("ddMMyy");
                                                                    objfsr.ArrivalDate = Convert.ToDateTime(fltdtl.Element("STA").Value.Trim()).ToString("ddMMyy");

                                                                    objfsr.FlightIdentification = fltdtl.Element("FlightDesignator").Element("FlightNumber").Value;
                                                                    objfsr.AirLineName = "Air Asia";
                                                                    // objfsr.AirLineName = fltdtl.Element("FlightDesignator").Element("CarrierCode").Value.Trim();
                                                                    //me = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Destination").Value.Trim());
                                                                    objfsr.ValiDatingCarrier = "AK";//fltdtl.Element("FlightDesignator").Element("CarrierCode").Value.Trim();
                                                                    objfsr.OperatingCarrier = fltdtl.Element("FlightDesignator").Element("CarrierCode").Value.Trim();
                                                                    //fltdtl.Element("LegInfo").Element("OperatingCarrier") != null ? fltdtl.Element("LegInfo").Element("OperatingCarrier").Value.Trim() : "";

                                                                    objfsr.MarketingCarrier = fltdtl.Element("LegInfo").Element("OperatingCarrier") != null ? fltdtl.Element("LegInfo").Element("OperatingCarrier").Value.Trim() : "";

                                                                    if (objfsr.MarketingCarrier.Trim() == "")
                                                                        objfsr.MarketingCarrier = objfsr.OperatingCarrier;

                                                                    objfsr.DepartureLocation = fltdtl.Element("DepartureStation").Value.Trim();
                                                                    objfsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, fltdtl.Element("DepartureStation").Value.Trim());
                                                                    objfsr.DepartureTime = Convert.ToDateTime(fltdtl.Element("STD").Value.Trim()).ToString("HHmm");

                                                                    if (fltdtl.Element("LegInfo").Element("DepartureStationAirportName") != null)
                                                                        objfsr.DepartureAirportName = fltdtl.Element("LegInfo").Element("DepartureStationAirportName").Value;
                                                                    else
                                                                        objfsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.DepartureLocation);
                                                                    objfsr.DepartureTerminal = "Terminal " + fltdtl.Element("LegInfo").Element("DepartureTerminal").Value;
                                                                    objfsr.DepAirportCode = objfsr.DepartureLocation;

                                                                    objfsr.ArrivalLocation = fltdtl.Element("ArrivalStation").Value.Trim();
                                                                    objfsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, fltdtl.Element("ArrivalStation").Value.Trim());
                                                                    objfsr.ArrivalTime = Convert.ToDateTime(fltdtl.Element("STA").Value.Trim()).ToString("HHmm");

                                                                    if (fltdtl.Element("LegInfo").Element("ArrivalStationAirportName") != null)
                                                                        objfsr.ArrivalAirportName = fltdtl.Element("LegInfo").Element("ArrivalStationAirportName").Value;
                                                                    else
                                                                        objfsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.ArrivalLocation);
                                                                    objfsr.ArrivalTerminal = "Terminal " + fltdtl.Element("LegInfo").Element("ArrivalTerminal").Value;
                                                                    objfsr.ArrAirportCode = objfsr.ArrivalLocation;
                                                                    objfsr.Trip = objFlt.Trip.ToString();
                                                                    DateTime STANew = Convert.ToDateTime(fltdtl.Element("STA").Value.Trim());
                                                                    TimeSpan tsTimeSpanNew = STANew.Subtract(Convert.ToDateTime(fltdtl.Element("STD").Value.Trim()));

                                                                    objfsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpan.TotalMinutes));
                                                                    objfsr.TripCnt = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpanNew.TotalMinutes));
                                                                    objfsr.TotalTripDur = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpanNew.TotalMinutes));
                                                                    //int ArrvLTV = Convert.ToInt32(fltdtl.Element("LegInfo").Element("ArrvLTV").Value.Trim());
                                                                    //int DeptLTV = Convert.ToInt32(fltdtl.Element("LegInfo").Element("DeptLTV").Value.Trim());

                                                                    #region Adding Fare
                                                                    objfsr.fareBasis = Fares.Element("FareBasisCode").Value;
                                                                    objfsr.EQ = fltdtl.Element("LegInfo").Element("EquipmentType").Value;
                                                                    objfsr.AvailableSeats = Fares.Element("AvailableCount").Value;
                                                                    string ft = Fares.Element("ClassOfService").Value;
                                                                    if (ft == "Z" || ft == "I" || ft == "A" || ft == "V" || ft == "P" || ft == "L" || ft == "U" || ft == "T" || ft == "Q" || ft == "M" || ft == "Y" || ft == "E" || ft == "O" || ft == "X" || ft == "W" || ft == "K")
                                                                        objfsr.BagInfo = "Hand Baggage: 7 KG included.";
                                                                    else if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                                                                        objfsr.BagInfo = "Baggage: 20KG Hand Baggage: 7 KG included.";
                                                                    else if (ft == "D" || ft == "C" || ft == "J")
                                                                        objfsr.BagInfo = "Baggage: 40KG Hand Baggage: 7 KG included.";
                                                                    if (fltdtl.Element("FlightDesignator").Element("CarrierCode").Value.Trim() == "I5" && objFlt.Trip == STD.Shared.Trip.D)
                                                                        objfsr.BagInfo = "Check-In Baggage: 15KG and " + objfsr.BagInfo;
                                                                    //objfsr.FareRule = Fares.ElementAt(0).Element("RuleNumber").Value;

                                                                    #region Adult
                                                                    objfsr.AdtBfare = (float)Math.Ceiling((Adtbasefares - AdtDisc) * currancyinfo[0].ExchangeRate);
                                                                    objfsr.AdtFSur = (float)Math.Ceiling(AdtFSur * currancyinfo[0].ExchangeRate);
                                                                    #region Get MISC Markup Charges Date 26-12-2018
                                                                    try
                                                                    {
                                                                        srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, CrdList.CrdType, Convert.ToString(objfsr.AdtBfare), Convert.ToString(objfsr.AdtFSur));
                                                                    }
                                                                    catch { srvChargeAdt = 0; }
                                                                    #endregion
                                                                    objfsr.AdtWO = (float)Math.Ceiling(AdtWO * currancyinfo[0].ExchangeRate);//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                                    objfsr.AdtTax = (float) Math.Ceiling((float)((AdtTaxs + AdtOT) * currancyinfo[0].ExchangeRate) + objfsr.AdtWO + srvChargeAdt);
                                                                    objfsr.AdtOT = objfsr.AdtTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                                    objfsr.AdtCabin = fltAllDetail.Element("CabinOfService").Value.Trim() == "" ? "Y" : fltAllDetail.Element("CabinOfService").Value.Trim();
                                                                    objfsr.AdtRbd = Fares.Element("FareClassOfService").Value;
                                                                    objfsr.AdtFarebasis = objfsr.fareBasis;
                                                                    if (ft == "E" || ft == "O" || ft == "X" || ft == "W")
                                                                        objfsr.AdtFareType = "Promo Fare";
                                                                    else if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                                                                        objfsr.AdtFareType = "Premium Flex Fare";
                                                                    else if (ft == "D" || ft == "C" || ft == "J")
                                                                        objfsr.AdtFareType = "Business Class";
                                                                    else
                                                                        objfsr.AdtFareType = "";
                                                                    //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                                    objfsr.AdtFare = objfsr.AdtTax + objfsr.AdtBfare + objfsr.AdtWO + objfsr.AdtFSur; //  + SegmentSellKey

                                                                    objfsr.fareBasis = objfsr.AdtFarebasis;
                                                                    objfsr.FBPaxType = "ADT";
                                                                    objfsr.ElectronicTicketing = "";
                                                                    objfsr.AdtFar = "NRM"; 
                                                                    // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;
                                                                    objfsr.ADTAdminMrk = 0;
                                                                    // objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());
                                                                    objfsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());
                                                                    if ((objFlt.Trip.ToString() == JourneyType.I.ToString()) && (objFlt.IsCorp == true))
                                                                    {
                                                                        objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                                        objfsr.AdtBfare = objfsr.AdtBfare + objfsr.ADTAdminMrk;
                                                                        objfsr.AdtFare = objfsr.AdtFare + objfsr.ADTAdminMrk;
                                                                    }
                                                                    CommDt.Clear();
                                                                    STTFTDS.Clear();
                                                                    try
                                                                    {
                                                                        if (objFlt.Trip.ToString().ToUpper() == "D")
                                                                        {
                                                                            //CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                                            //objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                                            //objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                                            //STTFTDS.Clear();
                                                                            //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, objFlt.TDS);
                                                                            //objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                                            //objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                                                            //objfsr.AdtEduCess = 0;
                                                                            //objfsr.AdtHighEduCess = 0;
                                                                            //objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                                            //objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                                            #region Show hide Net Fare and Commission Calculation-Adult Pax
                                                                            if (objfsr.Leg == 1)
                                                                            {
                                                                                CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Segment.Count().ToString(),contx,"");
                                                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                                                {
                                                                                    objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                                                    objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                                                    STTFTDS.Clear();
                                                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, objFlt.TDS);
                                                                                    objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                                                    objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                                                                    objfsr.AdtEduCess = 0;
                                                                                    objfsr.AdtHighEduCess = 0;
                                                                                    objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                                                    objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                                                }
                                                                                else
                                                                                {
                                                                                    objfsr.AdtDiscount1 = 0;
                                                                                    objfsr.AdtCB = 0;

                                                                                    objfsr.AdtSrvTax1 = 0;
                                                                                    objfsr.AdtDiscount = 0;
                                                                                    objfsr.AdtEduCess = 0;
                                                                                    objfsr.AdtHighEduCess = 0;
                                                                                    objfsr.AdtTF = 0;
                                                                                    objfsr.AdtTds = 0;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                objfsr.AdtDiscount1 = 0;
                                                                                objfsr.AdtCB = 0;

                                                                                objfsr.AdtSrvTax1 = 0;
                                                                                objfsr.AdtDiscount = 0;
                                                                                objfsr.AdtEduCess = 0;
                                                                                objfsr.AdtHighEduCess = 0;
                                                                                objfsr.AdtTF = 0;
                                                                                objfsr.AdtTds = 0;
                                                                            }
                                                                            #endregion                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            objfsr.AdtDiscount1 = 0;
                                                                            objfsr.AdtCB = 0;

                                                                            objfsr.AdtSrvTax1 = 0;
                                                                            objfsr.AdtDiscount = 0;
                                                                            objfsr.AdtEduCess = 0;
                                                                            objfsr.AdtHighEduCess = 0;
                                                                            objfsr.AdtTF = 0;
                                                                            objfsr.AdtTds = 0;
                                                                        }
                                                                    }
                                                                    catch (Exception es)
                                                                    {

                                                                        // CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                                        objfsr.AdtDiscount1 = 0;
                                                                        objfsr.AdtCB = 0;

                                                                        objfsr.AdtSrvTax1 = 0;
                                                                        objfsr.AdtDiscount = 0;
                                                                        objfsr.AdtEduCess = 0;
                                                                        objfsr.AdtHighEduCess = 0;
                                                                        objfsr.AdtTF = 0;
                                                                        objfsr.AdtTds = 0;
                                                                    }
                                                                    if (objFlt.IsCorp == true)
                                                                    {
                                                                        try
                                                                        {
                                                                            DataTable MGDT = new DataTable();
                                                                            MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.AdtFare.ToString()));
                                                                            objfsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                                            objfsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                                        }
                                                                        catch { }
                                                                    }

                                                                    #endregion
                                                                    #region Child

                                                                    objfsr.ChdBFare = (float)Math.Ceiling((Chdbasefares - ChdDisc) * currancyinfo[0].ExchangeRate);
                                                                    objfsr.ChdFSur = (float)Math.Ceiling(ChdFSur * currancyinfo[0].ExchangeRate);
                                                                    if (objfsr.Child > 0)
                                                                    {
                                                                        #region Get MISC Markup Charges Date 21-12-2018
                                                                        try
                                                                        {
                                                                            srvChargeChd = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, CrdList.CrdType, Convert.ToString(objfsr.ChdBFare), Convert.ToString(objfsr.ChdFSur));
                                                                        }
                                                                        catch { srvChargeChd = 0; }
                                                                        #endregion
                                                                    }

                                                                    objfsr.ChdWO = (float)Math.Ceiling(ChdWO * currancyinfo[0].ExchangeRate);///objchd.tcf != null ? float.Parse(objchd.tcf.amount.ToString()) : 0;
                                                                    objfsr.ChdTax = (float)Math.Ceiling((float)((ChdTaxs + ChdOT) * currancyinfo[0].ExchangeRate) + objfsr.ChdWO + srvChargeChd);
                                                                    objfsr.ChdOT = objfsr.ChdTax;//(float)Math.Round(ChdOT * currancyinfo[0].ExchangeRate, 4); //(objfsr.ChdTax - objfsr.ChdFSur) + // + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                                    objfsr.ChdCabin = fltAllDetail.Element("CabinOfService").Value.Trim() == "" ? "Y" : fltAllDetail.Element("CabinOfService").Value.Trim();
                                                                    objfsr.ChdRbd = Fares.Element("FareClassOfService").Value;
                                                                    objfsr.ChdFarebasis = objfsr.ChdFarebasis;
                                                                    // objfsr.ChdDiscount = (float)Math.Round(ChdDisc * currancyinfo[0].ExchangeRate, 4);
                                                                    objfsr.ChdFare = objfsr.ChdTax + objfsr.ChdWO + objfsr.ChdFSur + objfsr.ChdBFare;
                                                                    // objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objfsr.Trip.ToString());
                                                                    // objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                                    objfsr.CHDAdminMrk = 0;
                                                                    objfsr.CHDAgentMrk = 0;
                                                                    if (objfsr.Child > 0)
                                                                    {
                                                                        objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                                        if ((objFlt.Trip.ToString() == JourneyType.I.ToString()) && (objFlt.IsCorp == true))
                                                                        {
                                                                            objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                                            objfsr.ChdBFare = objfsr.ChdBFare + objfsr.CHDAdminMrk;
                                                                            objfsr.ChdFare = objfsr.ChdFare + objfsr.CHDAdminMrk;
                                                                        }
                                                                    }
                                                                    CommDt.Clear();
                                                                    STTFTDS.Clear();
                                                                    try
                                                                    {
                                                                        if (objFlt.Trip.ToString().ToUpper() == "D")
                                                                        {
                                                                            //CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                                            //objfsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                                            //objfsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                                            //STTFTDS.Clear();
                                                                            //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, objFlt.TDS);
                                                                            //objfsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                                            //objfsr.ChdDiscount = objfsr.ChdDiscount1 - objfsr.ChdSrvTax1;
                                                                            //objfsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                                            //objfsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                                            #region Show hide Net Fare and Commission Calculation-Child Pax
                                                                            if (objfsr.Leg == 1)
                                                                            {
                                                                                CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Segment.Count().ToString(), contx, "");
                                                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                                                {
                                                                                    objfsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                                                    objfsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                                                    STTFTDS.Clear();
                                                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, objFlt.TDS);
                                                                                    objfsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                                                    objfsr.ChdDiscount = objfsr.ChdDiscount1 - objfsr.ChdSrvTax1;
                                                                                    objfsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                                                    objfsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                                                }
                                                                                else
                                                                                {
                                                                                    objfsr.ChdEduCess = 0;
                                                                                    objfsr.ChdHighEduCess = 0;
                                                                                    objfsr.ChdDiscount1 = 0;
                                                                                    objfsr.ChdCB = 0;
                                                                                    objfsr.ChdSrvTax1 = 0;
                                                                                    objfsr.ChdDiscount = 0;
                                                                                    objfsr.ChdTF = 0;
                                                                                    objfsr.ChdTds = 0;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                objfsr.ChdEduCess = 0;
                                                                                objfsr.ChdHighEduCess = 0;
                                                                                objfsr.ChdDiscount1 = 0;
                                                                                objfsr.ChdCB = 0;
                                                                                objfsr.ChdSrvTax1 = 0;
                                                                                objfsr.ChdDiscount = 0;
                                                                                objfsr.ChdTF = 0;
                                                                                objfsr.ChdTds = 0;
                                                                            }
                                                                            #endregion                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            objfsr.ChdEduCess = 0;
                                                                            objfsr.ChdHighEduCess = 0;
                                                                            objfsr.ChdDiscount1 = 0;
                                                                            objfsr.ChdCB = 0;
                                                                            objfsr.ChdSrvTax1 = 0;
                                                                            objfsr.ChdDiscount = 0;
                                                                            objfsr.ChdTF = 0;
                                                                            objfsr.ChdTds = 0;
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        objfsr.ChdEduCess = 0;
                                                                        objfsr.ChdHighEduCess = 0;
                                                                        objfsr.ChdDiscount1 = 0;
                                                                        objfsr.ChdCB = 0;
                                                                        objfsr.ChdSrvTax1 = 0;
                                                                        objfsr.ChdDiscount = 0;
                                                                        objfsr.ChdTF = 0;
                                                                        objfsr.ChdTds = 0;
                                                                    }
                                                                    objfsr.ChdFar = "NRM";
                                                                    if (objFlt.IsCorp == true)
                                                                    {
                                                                        try
                                                                        {
                                                                            DataTable MGDT = new DataTable();
                                                                            MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.ChdFare.ToString()));
                                                                            objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                                            objfsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                                        }
                                                                        catch { }
                                                                    }

                                                                    #endregion
                                                                    #region Infant
                                                                    if (objfsr.Infant > 0)
                                                                    {
                                                                        objfsr.InfBfare = (float)CrdList.INFBasic;
                                                                        objfsr.InfCabin = "";
                                                                        objfsr.InfFSur = 0;
                                                                        objfsr.InfTax = (float)CrdList.INFTax;
                                                                        objfsr.InfOT = objfsr.InfTax - objfsr.InfFSur;//+ paxFare.AdditionalTxnFeePub);
                                                                        objfsr.InfRbd = objfsr.AdtRbd;
                                                                        objfsr.InfFarebasis = objfsr.AdtFarebasis;//; fareInfo.FBCode.Trim();
                                                                        objfsr.InfFare = objfsr.InfTax + objfsr.InfBfare;
                                                                        objfsr.InfFar = "NRM";
                                                                        if (objFlt.IsCorp == true)
                                                                        {
                                                                            try
                                                                            {
                                                                                DataTable MGDT = new DataTable();
                                                                                MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.InfBfare.ToString()), decimal.Parse(objfsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.InfFare.ToString()));
                                                                                objfsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                                                objfsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                                            }
                                                                            catch { }
                                                                        }

                                                                    }
                                                                    #endregion
                                                                    objfsr.FareRule = Fares.Element("RuleNumber").Value;
                                                                    objfsr.sno = Fares.Element("RuleNumber").Value + ":" + Fares.Element("CarrierCode").Value + ":" + Fares.Element("ClassOfService").Value + ":" + Fares.Element("FareApplicationType").Value + ":" + Fares.Element("FareSequence").Value + ":" + Fares.Element("ProductClass").Value + ":" + Segment.Count().ToString() + ":" + currancyinfo[0].CurrancyCode + ":" + currancyinfo[0].ExchangeRate.ToString() + ":" + Promofares;//flt.ResultIndex + ":" + items.Response.TraceId + ":" + flt.IsLCC + ":" + flt.Source;
                                                                    objfsr.Searchvalue = sessionids;
                                                                   // objfsr.OriginalTF = (objfsr.AdtFare * objfsr.Adult) + (objfsr.ChdFare * objfsr.Child) + (objfsr.InfFare * objfsr.Infant);
                                                                    decimal AdtOrgfare = Math.Round( ((Adtbasefares - AdtDisc) * currancyinfo[0].ExchangeRate) + (AdtFSur * currancyinfo[0].ExchangeRate) + (AdtWO * currancyinfo[0].ExchangeRate) + ((AdtTaxs + AdtOT) * currancyinfo[0].ExchangeRate) ,4);
                                                                    decimal ChdOrgfare = Math.Round(((Chdbasefares - ChdDisc) * currancyinfo[0].ExchangeRate) + (ChdFSur * currancyinfo[0].ExchangeRate) + (ChdWO * currancyinfo[0].ExchangeRate) + ((ChdTaxs + ChdOT) * currancyinfo[0].ExchangeRate), 4);
                                                                    objfsr.OriginalTF = (float)(AdtOrgfare * objfsr.Adult) + (float)(ChdOrgfare * objfsr.Child) + (objfsr.InfFare * objfsr.Infant);
                                                                   // objfsr.OriginalTF = ((objfsr.AdtFare - srvChargeAdt) * objfsr.Adult) + ((objfsr.ChdFare - srvChargeChd) * objfsr.Child) + (objfsr.InfFare * objfsr.Infant);
                                                                    objfsr.OriginalTT = (srvChargeAdt * objfsr.Adult) + (srvChargeChd * objfsr.Child);
                                                                    objfsr.Provider = "AK";
                                                                    //Rounding the fare
                                                                    objfsr.AdtFare = (float)Math.Ceiling(objfsr.AdtFare);
                                                                    objfsr.ChdFare = (float)Math.Ceiling(objfsr.ChdFare);
                                                                    objfsr.InfFare = (float)Math.Ceiling(objfsr.InfFare);

                                                                    objfsr.RBD = objfsr.AdtRbd + ":" + objfsr.ChdRbd + ":" + objfsr.InfRbd;

                                                                    objfsr.STax = (objfsr.AdtSrvTax * objfsr.Adult) + (objfsr.ChdSrvTax * objfsr.Child) + (objfsr.InfSrvTax * objfsr.Infant);
                                                                    objfsr.TFee = (objfsr.AdtTF * objfsr.Adult) + (objfsr.ChdTF * objfsr.Child) + (objfsr.InfTF * objfsr.Infant);
                                                                    objfsr.TotDis = (objfsr.AdtDiscount * objfsr.Adult) + (objfsr.ChdDiscount * objfsr.Child);
                                                                    objfsr.TotCB = (objfsr.AdtCB * objfsr.Adult) + (objfsr.ChdCB * objfsr.Child);
                                                                    objfsr.TotTds = (objfsr.AdtTds * objfsr.Adult) + (objfsr.ChdTds * objfsr.Child) + objfsr.InfTds;
                                                                    objfsr.TotMgtFee = (objfsr.AdtMgtFee * objfsr.Adult) + (objfsr.ChdMgtFee * objfsr.Child) + (objfsr.InfMgtFee * objfsr.Infant);

                                                                    //objfsr.AvailableSeats = objfsr.AdtAvlStatus + ":" + objfsr.ChdAvlStatus + ":" + objfsr.InfAvlStatus;
                                                                    objfsr.TotalFare = (objfsr.Adult * objfsr.AdtFare) + (objfsr.Child * objfsr.ChdFare) + (objfsr.Infant * objfsr.InfFare);
                                                                    objfsr.TotalFuelSur = (objfsr.Adult * objfsr.AdtFSur) + (objfsr.Child * objfsr.ChdFSur) + (objfsr.Infant * objfsr.InfFSur);
                                                                    objfsr.TotalTax = (objfsr.Adult * objfsr.AdtTax) + (objfsr.Child * objfsr.ChdTax) + (objfsr.Infant * objfsr.InfTax);
                                                                    objfsr.TotBfare = (objfsr.Adult * objfsr.AdtBfare) + (objfsr.Child * objfsr.ChdBFare) + (objfsr.Infant * objfsr.InfBfare);
                                                                    // objfsr.AvailableSeats = objfsr.AdtAvlStatus + ":" + objfsr.ChdAvlStatus + ":" + objfsr.InfAvlStatus;
                                                                    objfsr.TotMrkUp = (objfsr.ADTAdminMrk * objfsr.Adult) + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAdminMrk * objfsr.Child) + (objfsr.CHDAgentMrk * objfsr.Child);
                                                                    objfsr.TotalFare = objfsr.TotalFare + objfsr.TotMrkUp + objfsr.STax + objfsr.TFee + objfsr.TotMgtFee;
                                                                    objfsr.NetFare = (objfsr.TotalFare + objfsr.TotTds) - (objfsr.TotDis + objfsr.TotCB + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAgentMrk * objfsr.Child));
                                                                    #endregion

                                                                    if (objFlt.RTF == true || objFlt.GDSRTF == true || RTFS == true)
                                                                    {
                                                                        objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                                        objfsr.FType = "RTF";
                                                                    }
                                                                    else
                                                                    {
                                                                        if (fareType == "IB")
                                                                            objfsr.Sector = objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                                        else
                                                                            objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                                                    }
                                                                    if (fareType == "IB")
                                                                    {
                                                                        objfsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                                                        objfsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                                                    }
                                                                    else
                                                                    {
                                                                        objfsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                                                        objfsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                                                    }
                                                                    if (JourneyDateMarket.Count() == 1 && flight == 1)
                                                                        objfsr.TripType = "O";
                                                                    else if (JourneyDateMarket.Count() == 2 && flight == 1)
                                                                        objfsr.TripType = "O";
                                                                    else if (JourneyDateMarket.Count() == 2 && flight == 2)
                                                                        objfsr.TripType = "R";
                                                                    else objfsr.TripType = "M";

                                                                    objfsr.Trip = objFlt.Trip.ToString();//From Search Input

                                                                    if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.D && RTFS == true)
                                                                    {
                                                                        if (Convert.ToInt32(objfsr.Flight) == 1)
                                                                            fsrList.Add(objfsr);
                                                                        else
                                                                            RfsrList.Add(objfsr);
                                                                    }
                                                                    else if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                                                    {
                                                                        if (Convert.ToInt32(objfsr.Flight) == 1)
                                                                            fsrList.Add(objfsr);
                                                                        else
                                                                            RfsrList.Add(objfsr);
                                                                    }
                                                                    else
                                                                        fsrList.Add(objfsr);

                                                                }
                                                                if (fltAllDetail.Element("Fares").Elements("Fare").Count() > 1)
                                                                {
                                                                    lineNum++;
                                                                    linenuberTrueFalse = false;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (linenuberTrueFalse)
                                                    lineNum++;
                                            }
                                            flight++;
                                        }
                                    NikalLe: string ks = "";
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { ExecptionLogger.FileHandling("onwardJourneysAvailibility", "Error_001", ex, "AirAsiaFlight"); }

                    if (RTFS == true && objFlt.Trip == Trip.D && fsrList.Count > 0 && RfsrList.Count > 0)  //RTF True
                        FinalList = RoundTripFare(fsrList, RfsrList);
                    else if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == Trip.I && fsrList.Count > 0 && RfsrList.Count > 0)  //RTF True
                        FinalList = RoundTripFare(fsrList, RfsrList);
                    else
                    {
                        FinalList = fsrList;
                        if (RfsrList.Count != 0)
                            FinalList = RfsrList;
                    }
                }
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("GetAirAsiaAvailibility", "Error_001", ex, "AirAsiaFlight"); }
            return objFltComm.AddFlightKey(FinalList, RTFS);
           // return FinalList;
        }
        public List<FlightSearchResults> AirAsiaItineraryPrice(DataSet Fltdtlds, DataSet Credencilas, DataSet MarkupDs, List<FltSrvChargeList> SrvChargeList, string agentType, string TDS, string ConStr, List<FlightSearchResults> objlist, string RTFS, List<MISCCharges> MiscList)
        {
            try
            {
                List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
                List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
                List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
               // FlightCommonBAL objFltComm = new FlightCommonBAL(ConStr);
                if (Fltdtlds != null)
                {
                    DataTable fltdt = Fltdtlds.Tables[0]; DataTable Credencialdt = Credencilas.Tables[0];
                    DataView dv1 = Credencialdt.DefaultView;
                    dv1.RowFilter = "TripType = '" + fltdt.Rows[0]["Trip"].ToString().Trim() + "'";
                    Credencialdt = dv1.ToTable();
                    if (fltdt.Rows.Count > 0 && Credencialdt.Rows.Count > 0)
                    {
                        List<GALWS.AirAsia.CurrencyRate> currancyinfo = new List<GALWS.AirAsia.CurrencyRate>();
                        currancyinfo = AirAsiaUtility.CurrancyExchangeRate(fltdt.Rows[0]["AdtRbd"].ToString(), ConStr, currancyinfo);
                        
                        XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
                        IEnumerable<XElement> Journeys = null; IEnumerable<XElement> Passengers = null;

                        string ft = fltdt.Rows[0]["AdtRbd"].ToString();
                        string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                        decimal ExchangeRates = Convert.ToDecimal( sno[8]);
                        int segmentcount = Convert.ToInt32(sno[6]);
                        AirAsiaLogon objlogindtl = new AirAsiaLogon();
                        string  sessionids = objlogindtl.GetSessionID(Credencialdt.Rows[0]["UserID"].ToString(), Credencialdt.Rows[0]["Password"].ToString(), Credencialdt.Rows[0]["LoginID"].ToString());

                        AirAsiaUtility objupdatedb = new AirAsiaUtility();
                        objupdatedb.UpdateAirAsiaSessionId(sessionids, fltdt.Rows[0]["Track_id"].ToString(), ConStr);

                        string ItineraryPriceRequest2 = "";
                        if (segmentcount == 1)
                            ItineraryPriceRequest2 = AirAsiaItineraryPriceRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt, RTFS, sno[7], ConStr, Credencialdt.Rows[0]["UserID"].ToString());
                        else if (segmentcount > 1)
                            ItineraryPriceRequest2 = MultiSegmentPriceRequest(sessionids, fltdt, RTFS, sno[7], ConStr, Credencialdt.Rows[0]["UserID"].ToString());

                        AirAsiaUtility.SaveFile(ItineraryPriceRequest2, "ItineraryPriceRequest_" + fltdt.Rows[0]["Track_id"].ToString());
                        string ItineraryPriceResponse2 = AirAsiaUtility.AirAsiaPostXML(Credencialdt.Rows[0]["LoginPwd"].ToString(), "http://tempuri.org/IFareService/GetItineraryPrice", ItineraryPriceRequest2);
                        AirAsiaUtility.SaveFile(ItineraryPriceResponse2, "ItineraryPriceResponse_" + fltdt.Rows[0]["Track_id"].ToString());

                        XDocument AirAsiaDocument2 = XDocument.Parse(ItineraryPriceResponse2.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        XElement GetItineraryPriceResult = AirAsiaDocument2.Element(s + "Envelope").Element(s + "Body").Element("GetItineraryPriceResponse").Element("GetItineraryPriceResult");
                        Journeys = GetItineraryPriceResult.Element("Booking").Element("Journeys").Elements("Journey");
                        Passengers = GetItineraryPriceResult.Element("Booking").Element("Passengers").Elements("Passenger");
                        if (Journeys != null && Passengers!=null)
                        {
                            Decimal BookingSum = 0;
                            if (GetItineraryPriceResult.Element("Booking").Element("BookingSum") != null)
                                BookingSum = Convert.ToDecimal(GetItineraryPriceResult.Element("Booking").Element("BookingSum").Element("TotalCost").Value);
                            if (Fltdtlds.Tables[0].Rows[Fltdtlds.Tables[0].Rows.Count - 1]["TripType"].ToString().Trim() == "R" && RTFS == "RTF")
                            {
                                objlist = AirAsiaItineraryPriceSepcialRoundtrip(Journeys, Passengers, fltdt, MarkupDs, SrvChargeList,  agentType, TDS, ConStr, objlist, RTFS, Math.Round(BookingSum * ExchangeRates, 4), ExchangeRates, sessionids, sno, MiscList, Credencialdt.Rows[0]["CrdType"].ToString());
                            }
                            else
                            {
                                objlist = AirAsiaItineraryPriceSepcialOneWay(Journeys, Passengers, fltdt, MarkupDs, SrvChargeList, agentType, TDS, ConStr, objlist, RTFS, Math.Round(BookingSum * ExchangeRates, 4), ExchangeRates, sessionids, sno, MiscList, Credencialdt.Rows[0]["CrdType"].ToString());
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ExecptionLogger.FileHandling("AirAsiaItineraryPrice", "Error_001", ex, "AirAsiaFlight");
            }
            return objlist;
        }
        public List<FlightSearchResults> AirAsiaItineraryPriceSepcialRoundtrip(IEnumerable<XElement> Journeys, IEnumerable<XElement> Passengers, DataTable fltdt, DataSet MarkupDs, List<FltSrvChargeList> SrvChargeList, string agentType, string TDS, string ConStr, List<FlightSearchResults> objlist, string RTFS, decimal BookingSum, decimal ExchangeRates, string sessionids, string[] sno, List<MISCCharges> MiscList, string CrdType)
        {
            try
            {
                HttpContext contx=HttpContext.Current;
                FlightCommonBAL objFltComm = new FlightCommonBAL(ConStr);
               // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities";
                decimal TotAdtbasefares = 0, TotAdtTaxs = 0, TotAdtDisc = 0, TotAdtOT = 0, TotChdbasefares = 0, TotChdTaxs = 0, TotChdDisc = 0, TotChdOT = 0;
                #region only Fare calculation
                foreach (var Journeyss in Journeys)
                {
                    IEnumerable<XElement> Segment = Journeyss.Element("Segments").Elements("Segment");
                    decimal Adtbasefares = 0, AdtTaxs = 0, AdtDisc = 0, AdtOT = 0, Chdbasefares = 0, ChdTaxs = 0, ChdDisc = 0, ChdOT = 0;
                    foreach (var fltAllDetail in Segment)
                    {
                        if (fltAllDetail.Element("Fares").Element("Fare") != null)
                        {
                            foreach (var Fares in fltAllDetail.Element("Fares").Elements("Fare"))
                            {
                                IEnumerable<XElement> objAdt = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "ADT");
                                IEnumerable<XElement> objChd = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "CHD");
                                #region Adult
                                decimal ABfare = 0, ATax = 0, AOT = 0, ADiscount = 0;

                                foreach (var adt in objAdt)
                                {
                                    IEnumerable<XElement> ServiceCharges = adt.Element("ServiceCharges").Elements("BookingServiceCharge");
                                    decimal basefare = 0, Tax = 0, othrcharge = 0, Discount = 0;//
                                    foreach (var ChargeType in ServiceCharges)
                                    {
                                        if (ChargeType.Element("ChargeType").Value.Trim() == "FarePrice")
                                            basefare += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Discount" || ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                            Discount += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                            Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else
                                            othrcharge += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                    }
                                    ABfare += basefare;
                                    ATax += Tax;
                                    AOT += othrcharge;
                                    ADiscount += Discount;
                                }
                                Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADiscount;
                                #endregion
                                #region Child
                                decimal CBfare = 0, CTax = 0, COT = 0, CDiscount = 0;//
                                foreach (var chd in objChd)
                                {
                                    IEnumerable<XElement> ServiceCharges = chd.Element("ServiceCharges").Elements("BookingServiceCharge");
                                    decimal basefare = 0, Tax = 0, othrcharge = 0, Discount = 0;//
                                    foreach (var ChargeType in ServiceCharges)
                                    {
                                        if (ChargeType.Element("ChargeType").Value.Trim() == "FarePrice")
                                            basefare += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Discount" || ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                            Discount += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                            Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else
                                            othrcharge += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                    }
                                    CBfare += basefare;
                                    CTax += Tax;
                                    COT += othrcharge;
                                    CDiscount += Discount;
                                }
                                Chdbasefares += CBfare; ChdTaxs += CTax; ChdOT += COT; ChdDisc += CDiscount;
                                #endregion
                            }

                        }

                    }
                    TotAdtbasefares += Adtbasefares; TotAdtTaxs += AdtTaxs; TotAdtOT += AdtOT; TotAdtDisc += AdtDisc;
                    TotChdbasefares += Chdbasefares; TotChdTaxs += ChdTaxs; TotChdOT += ChdOT; TotChdDisc += ChdDisc;
                }
                #endregion

                foreach (var Journeyss in Journeys)
                {
                    IEnumerable<XElement> Segment = Journeyss.Element("Segments").Elements("Segment");
                    foreach (var fltAllDetail in Segment)
                    {
                        if (fltAllDetail.Element("Fares").Element("Fare") != null)
                        {
                            DataTable CommDt = new DataTable(); Hashtable STTFTDS = new Hashtable();
                            foreach (var Fares in fltAllDetail.Element("Fares").Elements("Fare"))
                            {
                                foreach (var fltdtl in fltAllDetail.Element("Legs").Elements("Leg"))
                                {
                                    string snoKey = Fares.Element("RuleNumber").Value + ":" + Fares.Element("CarrierCode").Value + ":" + Fares.Element("ClassOfService").Value + ":" + Fares.Element("FareApplicationType").Value + ":" + Fares.Element("FareSequence").Value + ":" + Fares.Element("ProductClass").Value + ":" + Segment.Count().ToString() + ":" + sno[7] + ":" + ExchangeRates.ToString() + ":" + sno[9];
                                    foreach (FlightSearchResults fsr in objlist.Where(x => x.sno == snoKey && x.fareBasis.Trim() == Fares.Element("FareBasisCode").Value.Trim() && x.FlightIdentification.Trim() == fltdtl.Element("FlightDesignator").Element("FlightNumber").Value.Trim() && x.OperatingCarrier.Trim() == fltdtl.Element("FlightDesignator").Element("CarrierCode").Value.Trim() && x.DepartureLocation.Trim() == fltdtl.Element("DepartureStation").Value.Trim() && x.ArrivalLocation.Trim() == fltdtl.Element("ArrivalStation").Value.Trim()))
                                    {
                                        IEnumerable<XElement> objAdt = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "ADT");
                                        IEnumerable<XElement> objChd = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "CHD");
                                        float srvChargeAdt = 0, srvChargeChd = 0;
                                        // fsr.fareBasis=Fares.Element("FareBasisCode").Value;
                                        #region Adult
                                        fsr.AdtBfare = (float)Math.Ceiling((TotAdtbasefares - TotAdtDisc) * ExchangeRates);
                                        // fsr.AdtCabin = fltAllDetail.Element("CabinOfService").Value.Trim() == "" ? "Y" : fltAllDetail.Element("CabinOfService").Value.Trim();
                                        fsr.AdtFSur = 0;
                                        fsr.AdtWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                        #region Get MISC Markup Charges Date 26-12-2018
                                        try
                                        {
                                            srvChargeAdt = objFltComm.MISCServiceFee(MiscList, fsr.ValiDatingCarrier, CrdType, Convert.ToString(fsr.AdtBfare), Convert.ToString(fsr.AdtFSur));
                                        }
                                        catch { srvChargeAdt = 0; }
                                        #endregion
                                        fsr.AdtTax = (float)Math.Ceiling((float)((TotAdtTaxs + TotAdtOT) * ExchangeRates) + fsr.AdtWO + srvChargeAdt);
                                        fsr.AdtOT = fsr.AdtTax;//(float)Math.Round(AdtOT * ExchangeRates, 4); //fsr.AdtTax - fsr.AdtFSur + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                        // fsr.AdtRbd = Fares.Element("FareClassOfService").Value;
                                        //fsr.AdtFarebasis = fsr.fareBasis;//; fareInfo.FBCode.Trim();
                                        fsr.AdtFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                        // fsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                        fsr.AdtFare = fsr.AdtTax + fsr.AdtWO + fsr.AdtFSur + fsr.AdtBfare;//fsr.AdtTax + fsr.AdtBfare; //  + SegmentSellKey
                                        //fsr.sno = Fares.Element("RuleNumber").Value + ":" + Fares.Element("CarrierCode").Value + ":" + Fares.Element("ClassOfService").Value + ":" + Fares.Element("FareApplicationType").Value + ":" + Fares.Element("FareSequence").Value + ":" + Fares.Element("ProductClass").Value + ":" + Segment.Count().ToString();//flt.ResultIndex + ":" + items.Response.TraceId + ":" + flt.IsLCC + ":" + flt.Source;
                                        //fsr.fareBasis = fsr.AdtFarebasis;
                                        fsr.FBPaxType = "ADT";
                                        fsr.ElectronicTicketing = "";
                                        fsr.AdtFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                        // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;

                                       // fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                        fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                        fsr.ADTAdminMrk = 0;
                                        if (fsr.Trip.ToString() == JourneyType.I.ToString() && fsr.IsCorp == true)
                                        {
                                            fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                            fsr.AdtBfare = fsr.AdtBfare + fsr.ADTAdminMrk;
                                            fsr.AdtFare = fsr.AdtFare + fsr.ADTAdminMrk;
                                        }
                                        CommDt.Clear();
                                        //CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", "");
                                        try
                                        {
                                            if (fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                                            {
                                                //CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                //fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                //fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                //STTFTDS.Clear();
                                                //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, TDS);
                                                //fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                //fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                                //fsr.AdtEduCess = 0;
                                                //fsr.AdtHighEduCess = 0;
                                                //fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                //fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                #region Show hide Net Fare and Commission Calculation-Adult Pax

                                                if (fsr.Leg == 1)
                                                {
                                                    CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString(), contx, "");
                                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                                    {
                                                        fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                        fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                        STTFTDS.Clear();
                                                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, TDS);
                                                        fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                        fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                                        fsr.AdtEduCess = 0;
                                                        fsr.AdtHighEduCess = 0;
                                                        fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                        fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    }
                                                    else
                                                    {
                                                        fsr.AdtDiscount1 = 0;
                                                        fsr.AdtCB = 0;
                                                        fsr.AdtSrvTax1 = 0;
                                                        fsr.AdtDiscount = 0;
                                                        fsr.AdtEduCess = 0;
                                                        fsr.AdtHighEduCess = 0;
                                                        fsr.AdtTF = 0;
                                                        fsr.AdtTds = 0;
                                                    }
                                                }
                                                else
                                                {
                                                    fsr.AdtDiscount1 = 0;
                                                    fsr.AdtCB = 0;
                                                    fsr.AdtSrvTax1 = 0;
                                                    fsr.AdtDiscount = 0;
                                                    fsr.AdtEduCess = 0;
                                                    fsr.AdtHighEduCess = 0;
                                                    fsr.AdtTF = 0;
                                                    fsr.AdtTds = 0;
                                                }
                                                #endregion                                             
                                            }
                                            else
                                            {
                                                fsr.AdtDiscount1 = 0;
                                                fsr.AdtCB = 0;
                                                fsr.AdtSrvTax1 = 0;
                                                fsr.AdtDiscount = 0;
                                                fsr.AdtEduCess = 0;
                                                fsr.AdtHighEduCess = 0;
                                                fsr.AdtTF = 0;
                                                fsr.AdtTds = 0;
                                            }
                                        }
                                        catch (Exception sd)
                                        {

                                            fsr.AdtDiscount1 = 0;
                                            fsr.AdtCB = 0;
                                            fsr.AdtSrvTax1 = 0;
                                            fsr.AdtDiscount = 0;
                                            fsr.AdtEduCess = 0;
                                            fsr.AdtHighEduCess = 0;
                                            fsr.AdtTF = 0;
                                            fsr.AdtTds = 0;
                                        }
                                        if (fsr.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), fsr.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                                fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }

                                        #endregion
                                        #region Child

                                        fsr.ChdBFare = (float)Math.Ceiling((TotChdbasefares - TotChdDisc) * ExchangeRates);
                                        //   fsr.ChdCabin = fltAllDetail.Element("CabinOfService").Value.Trim() == "" ? "Y" : fltAllDetail.Element("CabinOfService").Value.Trim();
                                        fsr.ChdFSur = 0;
                                        if (fsr.Child > 0)
                                        {
                                            #region Get MISC Markup Charges Date 26-12-2018
                                            try
                                            {
                                                srvChargeChd = objFltComm.MISCServiceFee(MiscList, fsr.ValiDatingCarrier, CrdType, Convert.ToString(fsr.ChdBFare), Convert.ToString(fsr.ChdFSur));
                                            }
                                            catch { srvChargeChd = 0; }
                                            #endregion
                                        }
                                        
                                        fsr.ChdWO = 0;///objchd.tcf != null ? float.Parse(objchd.tcf.amount.ToString()) : 0;
                                        fsr.ChdTax = (float)Math.Ceiling((float)((TotChdTaxs + TotChdOT) * ExchangeRates) + fsr.ChdWO +  srvChargeChd);
                                        fsr.ChdOT = fsr.ChdTax;//(float)Math.Round(ChdOT * ExchangeRates, 4);  //(float)fsr.ChdTax - fsr.ChdFSur ;// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                        //  fsr.ChdRbd = Fares.Element("FareClassOfService").Value;
                                        //  fsr.ChdFarebasis = fsr.fareBasis;
                                        //fsr.ChdDiscount = (float)Math.Round(ChdDisc * currancyinfo[0].ExchangeRate, 4);
                                        fsr.ChdFare = fsr.ChdTax + fsr.ChdWO + fsr.ChdFSur + fsr.ChdBFare;//fsr.ChdTax + fsr.ChdBFare;
                                       // fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                        fsr.CHDAdminMrk = 0;
                                        fsr.CHDAgentMrk = 0;
                                        if (fsr.Child > 0)
                                        {
                                            fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip);
                                            if (fsr.Trip.ToString() == JourneyType.I.ToString() && fsr.IsCorp == true)
                                            {
                                                fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                                fsr.ChdBFare = fsr.ChdBFare + fsr.CHDAdminMrk;
                                                fsr.ChdFare = fsr.ChdFare + fsr.CHDAdminMrk;
                                            }
                                        }
                                        
                                        CommDt.Clear();
                                        try
                                        {
                                            if (fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                                            {
                                                //CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                //fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                //fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                //STTFTDS.Clear();
                                                //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, TDS);
                                                //fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                //fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                                //fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                //fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                //fsr.ChdEduCess = 0;
                                                //fsr.ChdHighEduCess = 0;
                                                #region Show hide Net Fare and Commission Calculation-Child Pax
                                                if (fsr.Leg == 1)
                                                {
                                                    CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString(),contx,"");
                                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                                    {
                                                        //CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                        fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                        fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                        STTFTDS.Clear();
                                                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, TDS);
                                                        fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                        fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                                        fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                        fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                        fsr.ChdEduCess = 0;
                                                        fsr.ChdHighEduCess = 0;
                                                    }
                                                    else
                                                    {
                                                        fsr.ChdDiscount1 = 0;
                                                        fsr.ChdCB = 0;
                                                        fsr.ChdSrvTax1 = 0;
                                                        fsr.ChdDiscount = 0;
                                                        fsr.ChdTF = 0;
                                                        fsr.ChdTds = 0;
                                                        fsr.ChdEduCess = 0;
                                                        fsr.ChdHighEduCess = 0;
                                                    }

                                                }
                                                else
                                                {
                                                    fsr.ChdDiscount1 = 0;
                                                    fsr.ChdCB = 0;
                                                    fsr.ChdSrvTax1 = 0;
                                                    fsr.ChdDiscount = 0;
                                                    fsr.ChdTF = 0;
                                                    fsr.ChdTds = 0;
                                                    fsr.ChdEduCess = 0;
                                                    fsr.ChdHighEduCess = 0;
                                                }
                                                #endregion                                                
                                            }
                                            else
                                            {
                                                fsr.ChdDiscount1 = 0;
                                                fsr.ChdCB = 0;
                                                fsr.ChdSrvTax1 = 0;
                                                fsr.ChdDiscount = 0;
                                                fsr.ChdTF = 0;
                                                fsr.ChdTds = 0;
                                                fsr.ChdEduCess = 0;
                                                fsr.ChdHighEduCess = 0;
                                            }
                                        }
                                        catch (Exception ed)
                                        {
                                            fsr.ChdDiscount1 = 0;
                                            fsr.ChdCB = 0;
                                            fsr.ChdSrvTax1 = 0;
                                            fsr.ChdDiscount = 0;
                                            fsr.ChdTF = 0;
                                            fsr.ChdTds = 0;
                                            fsr.ChdEduCess = 0;
                                            fsr.ChdHighEduCess = 0;
                                        }

                                        fsr.ChdFar = "NRM";

                                        #endregion
                                        #region Infant

                                        if (fsr.Infant > 0)
                                        {
                                            decimal InfBasefare = 0, InfTax = 0;
                                            foreach (var Passenger in Passengers)
                                            {
                                                if (Passenger.Element("PassengerFees") != null)
                                                {
                                                    if (Passenger.Element("PassengerFees").Element("PassengerFee") != null)
                                                    {
                                                        foreach (var PassengerFee in Passenger.Element("PassengerFees").Elements("PassengerFee"))
                                                        {
                                                            IEnumerable<XElement> ServiceCharges = PassengerFee.Element("ServiceCharges").Elements("BookingServiceCharge");
                                                            decimal basefare = 0, Tax = 0;//
                                                            foreach (var ChargeType in ServiceCharges)
                                                            {
                                                                if (ChargeType.Element("ChargeType").Value.Trim() == "ServiceCharge")
                                                                {
                                                                    basefare = Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                }
                                                                else
                                                                    Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                //if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                                                //{
                                                                //    Tax = Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                //}
                                                            }
                                                            InfBasefare += basefare;
                                                            InfTax += Tax;

                                                        }
                                                    }
                                                }
                                            }
                                            fsr.InfBfare = (float)Math.Ceiling(InfBasefare * ExchangeRates);
                                            fsr.InfCabin = "";
                                            fsr.InfFSur = 0;
                                            fsr.InfTax = (float)Math.Ceiling(InfTax * ExchangeRates);
                                            fsr.InfOT = fsr.InfTax - fsr.InfFSur;//+ paxFare.AdditionalTxnFeePub);
                                            //fsr.InfRbd = seg.Airline.FareClass.ToString();
                                            fsr.InfFarebasis = fsr.fareBasis;
                                            fsr.InfFare = fsr.InfTax + fsr.InfBfare;
                                            fsr.InfFar = "NRM";
                                        }
                                        #endregion

                                        fsr.Searchvalue = sessionids;
                                        //fsr.OriginalTF = (fsr.AdtFare * fsr.Adult) + (fsr.ChdFare * fsr.Child) + fsr.InfFare ;
                                        fsr.OriginalTF = (float)BookingSum;
                                        fsr.OriginalTT =  (srvChargeAdt * fsr.Adult) + (srvChargeChd * fsr.Child);
                                        fsr.Provider = "AK";
                                        //Rounding The fare
                                        fsr.AdtFare = (float)Math.Ceiling(fsr.AdtFare);
                                        fsr.ChdFare = (float)Math.Ceiling(fsr.ChdFare);
                                        fsr.InfFare = (float)Math.Ceiling(fsr.InfFare);
                                        //Math.Ceiling(
                                        fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + fsr.InfSrvTax;
                                        fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child) + fsr.InfTF;
                                        fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                                        fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                                        fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child) + fsr.InfTds;
                                        fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + fsr.InfMgtFee;

                                        fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + fsr.InfFare;
                                        fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + fsr.InfFSur;
                                        fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + fsr.InfTax;
                                        fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + fsr.InfBfare;
                                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                        fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                                        fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                        fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                                        fsr.Track_id = fltdt.Rows[0]["Track_id"].ToString();
                                        //if (fltdt.Rows[fltdt.Rows.Count - 1]["TripType"].ToString().Trim() == "R" && fltdt.Rows[0]["Trip"].ToString() == "D" && RTFS == "RTF" && TripCount > 0)
                                        //{
                                        //    double Roudtripcharge = (fsr.TotMrkUp + fsr.TotMgtFee) /2;
                                        //    fsr.NetFare = fsr.NetFare - (float) Math.Ceiling(Roudtripcharge);
                                        //    fsr.TotalFare = fsr.TotalFare - (float)Math.Ceiling(Roudtripcharge);
                                        //}
                                        //TotalFare += fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                        //NetFare += (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child)); ;

                                    }
                                }
                            }

                        }
                    }
                    // flight++;
                }
                //if (fltdt.Rows[fltdt.Rows.Count - 1]["TripType"].ToString().Trim() == "R" && fltdt.Rows[0]["Trip"].ToString() == "D" && RTFS == "RTF")
                //{
                //    float NetFare = 0, TotalFare = 0; 
                    //foreach (FlightSearchResults fsr in objlist)
                    //{
                    //    if (ifd == 0)
                    //    {
                    //        NetFare += fsr.NetFare;
                    //        TotalFare += fsr.TotalFare;
                    //    }
                    //    else
                    //    {
                    //        NetFare += fsr.NetFare - fsr.TotMrkUp - fsr.TotMgtFee;
                    //        TotalFare += fsr.TotalFare - fsr.TotMrkUp - fsr.TotMgtFee;
                    //    }
                    //    ifd++;
                    //}

                    //foreach (FlightSearchResults fsr in objlist)
                    //{
                    //    fsr.NetFare = NetFare;
                    //    fsr.TotalFare = TotalFare;
                    //}
               // }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("AirAsiaItineraryPriceSepcialRoundtrip", "Error_001", ex, "AirAsiaFlight");
            }
            return objlist;
        }
        public List<FlightSearchResults> AirAsiaItineraryPriceSepcialOneWay(IEnumerable<XElement> Journeys, IEnumerable<XElement> Passengers, DataTable fltdt, DataSet MarkupDs, List<FltSrvChargeList> SrvChargeList, string agentType, string TDS, string ConStr, List<FlightSearchResults> objlist, string RTFS, decimal BookingSum, decimal ExchangeRates, string sessionids, string[] sno, List<MISCCharges> MiscList, string CrdType)
        {
            try
            {
                HttpContext contx = HttpContext.Current;
                    FlightCommonBAL objFltComm = new FlightCommonBAL(ConStr);
               // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities";
                foreach (var Journeyss in Journeys)
                {
                    IEnumerable<XElement> Segment = Journeyss.Element("Segments").Elements("Segment");
                    #region Fare                 
                    decimal Adtbasefares = 0, AdtTaxs = 0, AdtDisc = 0, AdtOT = 0, Chdbasefares = 0, ChdTaxs = 0, ChdDisc = 0, ChdOT = 0;
                    foreach (var fltAllDetail in Segment)
                    {
                        if (fltAllDetail.Element("Fares").Element("Fare") != null)
                        {
                            foreach (var Fares in fltAllDetail.Element("Fares").Elements("Fare"))
                            {
                                IEnumerable<XElement> objAdt = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "ADT");
                                IEnumerable<XElement> objChd = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "CHD");
                                #region Adult
                                decimal ABfare = 0, ATax = 0, AOT = 0, ADiscount = 0;

                                foreach (var adt in objAdt)
                                {
                                    IEnumerable<XElement> ServiceCharges = adt.Element("ServiceCharges").Elements("BookingServiceCharge");
                                    decimal basefare = 0, Tax = 0, othrcharge = 0, Discount = 0;//
                                    foreach (var ChargeType in ServiceCharges)
                                    {
                                        if (ChargeType.Element("ChargeType").Value.Trim() == "FarePrice")
                                            basefare += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Discount" || ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                            Discount += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                            Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else
                                            othrcharge += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                    }
                                    ABfare += basefare;
                                    ATax += Tax;
                                    AOT += othrcharge;
                                    ADiscount += Discount;
                                }
                                Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADiscount;
                                #endregion
                                #region Child
                                decimal CBfare = 0, CTax = 0, COT = 0, CDiscount = 0;//
                                foreach (var chd in objChd)
                                {
                                    IEnumerable<XElement> ServiceCharges = chd.Element("ServiceCharges").Elements("BookingServiceCharge");
                                    decimal basefare = 0, Tax = 0, othrcharge = 0, Discount = 0;//
                                    foreach (var ChargeType in ServiceCharges)
                                    {
                                        if (ChargeType.Element("ChargeType").Value.Trim() == "FarePrice")
                                            basefare += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Discount" || ChargeType.Element("ChargeType").Value.Trim() == "PromotionDiscount")
                                            Discount += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                            Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                        else
                                            othrcharge += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                    }
                                    CBfare += basefare;
                                    CTax += Tax;
                                    COT += othrcharge;
                                    CDiscount += Discount;
                                }
                                Chdbasefares += CBfare; ChdTaxs += CTax; ChdOT += COT; ChdDisc += CDiscount;
                                #endregion
                            }
                        }
                    }
                    #endregion

                    foreach (var fltAllDetail in Segment)
                    {

                        if (fltAllDetail.Element("Fares").Element("Fare") != null)
                        {
                            DataTable CommDt = new DataTable(); Hashtable STTFTDS = new Hashtable();
                            foreach (var Fares in fltAllDetail.Element("Fares").Elements("Fare"))
                            {
                                foreach (var fltdtl in fltAllDetail.Element("Legs").Elements("Leg"))
                                {
                                    string snoKey = Fares.Element("RuleNumber").Value + ":" + Fares.Element("CarrierCode").Value + ":" + Fares.Element("ClassOfService").Value + ":" + Fares.Element("FareApplicationType").Value + ":" + Fares.Element("FareSequence").Value + ":" + Fares.Element("ProductClass").Value + ":" + Segment.Count().ToString() + ":" + sno[7] + ":" + ExchangeRates.ToString() + ":" + sno[9];
                                    foreach (FlightSearchResults fsr in objlist.Where(x => x.sno == snoKey && x.fareBasis.Trim() == Fares.Element("FareBasisCode").Value.Trim() && x.FlightIdentification.Trim() == fltdtl.Element("FlightDesignator").Element("FlightNumber").Value.Trim() && x.OperatingCarrier.Trim() == fltdtl.Element("FlightDesignator").Element("CarrierCode").Value.Trim() && x.DepartureLocation.Trim() == fltdtl.Element("DepartureStation").Value.Trim() && x.ArrivalLocation.Trim() == fltdtl.Element("ArrivalStation").Value.Trim()))
                                    {
                                        IEnumerable<XElement> objAdt = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "ADT");
                                        IEnumerable<XElement> objChd = Fares.Element("PaxFares").Elements("PaxFare").Where(x => x.Element("PaxType").Value.Trim() == "CHD");
                                        float srvChargeAdt = 0, srvChargeChd = 0;
                                        // fsr.fareBasis=Fares.Element("FareBasisCode").Value;
                                        #region Adult
                                        fsr.AdtBfare = (float)Math.Ceiling((Adtbasefares - AdtDisc) * ExchangeRates);
                                        // fsr.AdtCabin = fltAllDetail.Element("CabinOfService").Value.Trim() == "" ? "Y" : fltAllDetail.Element("CabinOfService").Value.Trim();
                                        fsr.AdtFSur = 0;
                                        fsr.AdtWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                        #region Get MISC Markup Charges Date 26-12-2018
                                        try
                                        {
                                            srvChargeAdt = objFltComm.MISCServiceFee(MiscList, fsr.ValiDatingCarrier, CrdType, Convert.ToString(fsr.AdtBfare), Convert.ToString(fsr.AdtFSur));
                                        }
                                        catch { srvChargeAdt = 0; }
                                        #endregion
                                        fsr.AdtTax = (float)Math.Ceiling((float)((AdtTaxs + AdtOT) * ExchangeRates) + fsr.AdtWO + srvChargeAdt);
                                        fsr.AdtOT = fsr.AdtTax;//(float)Math.Round(AdtOT * ExchangeRates, 4); //fsr.AdtTax - fsr.AdtFSur + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                        // fsr.AdtRbd = Fares.Element("FareClassOfService").Value;
                                        //fsr.AdtFarebasis = fsr.fareBasis;//; fareInfo.FBCode.Trim();
                                        fsr.AdtFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                        // fsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                        fsr.AdtFare = fsr.AdtTax + fsr.AdtWO + fsr.AdtFSur + fsr.AdtBfare;//fsr.AdtTax + fsr.AdtBfare; //  + SegmentSellKey
                                        //fsr.sno = Fares.Element("RuleNumber").Value + ":" + Fares.Element("CarrierCode").Value + ":" + Fares.Element("ClassOfService").Value + ":" + Fares.Element("FareApplicationType").Value + ":" + Fares.Element("FareSequence").Value + ":" + Fares.Element("ProductClass").Value + ":" + Segment.Count().ToString();//flt.ResultIndex + ":" + items.Response.TraceId + ":" + flt.IsLCC + ":" + flt.Source;
                                        //fsr.fareBasis = fsr.AdtFarebasis;
                                        fsr.FBPaxType = "ADT";
                                        fsr.ElectronicTicketing = "";
                                        fsr.AdtFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                        // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;
                                        fsr.ADTAdminMrk = 0;
                                        //fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                        fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                        if (fsr.Trip.ToString() == JourneyType.I.ToString() && fsr.IsCorp == true)
                                        {
                                            fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                            fsr.AdtBfare = fsr.AdtBfare + fsr.ADTAdminMrk;
                                            fsr.AdtFare = fsr.AdtFare + fsr.ADTAdminMrk;
                                        }
                                        CommDt.Clear();
                                        try
                                        {
                                            if (fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                                            {
                                                //CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                //fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                //fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                //STTFTDS.Clear();
                                                //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, TDS);
                                                //fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                //fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                                //fsr.AdtEduCess = 0;
                                                //fsr.AdtHighEduCess = 0;
                                                //fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                //fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                #region Show hide Net Fare and Commission Calculation-Adult Pax
                                                if (fsr.Leg == 1)
                                                {
                                                    CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString(), contx,"");
                                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                                    {
                                                        // CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                        fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                        fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                        STTFTDS.Clear();
                                                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, TDS);
                                                        fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                        fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                                        fsr.AdtEduCess = 0;
                                                        fsr.AdtHighEduCess = 0;
                                                        fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                        fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    }
                                                    else
                                                    {
                                                        fsr.AdtDiscount1 = 0;
                                                        fsr.AdtCB = 0;
                                                        fsr.AdtSrvTax1 = 0;
                                                        fsr.AdtDiscount = 0;
                                                        fsr.AdtEduCess = 0;
                                                        fsr.AdtHighEduCess = 0;
                                                        fsr.AdtTF = 0;
                                                        fsr.AdtTds = 0;
                                                    }

                                                }
                                                else
                                                {
                                                    fsr.AdtDiscount1 = 0;
                                                    fsr.AdtCB = 0;
                                                    fsr.AdtSrvTax1 = 0;
                                                    fsr.AdtDiscount = 0;
                                                    fsr.AdtEduCess = 0;
                                                    fsr.AdtHighEduCess = 0;
                                                    fsr.AdtTF = 0;
                                                    fsr.AdtTds = 0;
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                fsr.AdtDiscount1 = 0;
                                                fsr.AdtCB = 0;
                                                fsr.AdtSrvTax1 = 0;
                                                fsr.AdtDiscount = 0;
                                                fsr.AdtEduCess = 0;
                                                fsr.AdtHighEduCess = 0;
                                                fsr.AdtTF = 0;
                                                fsr.AdtTds = 0;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            fsr.AdtDiscount1 = 0;
                                            fsr.AdtCB = 0;
                                            fsr.AdtSrvTax1 = 0;
                                            fsr.AdtDiscount = 0;
                                            fsr.AdtEduCess = 0;
                                            fsr.AdtHighEduCess = 0;
                                            fsr.AdtTF = 0;
                                            fsr.AdtTds = 0;
                                        }
                                        if (fsr.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), fsr.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                                fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }

                                        #endregion
                                        #region Child

                                        fsr.ChdBFare = (float)Math.Ceiling((Chdbasefares - ChdDisc) * ExchangeRates);
                                        //   fsr.ChdCabin = fltAllDetail.Element("CabinOfService").Value.Trim() == "" ? "Y" : fltAllDetail.Element("CabinOfService").Value.Trim();
                                        fsr.ChdFSur = 0;
                                        fsr.ChdWO = 0;///objchd.tcf != null ? float.Parse(objchd.tcf.amount.ToString()) : 0;
                                         if (fsr.Child > 0)
                                        {
                                            #region Get MISC Markup Charges Date 26-12-2018
                                            try
                                            {
                                                srvChargeChd = objFltComm.MISCServiceFee(MiscList, fsr.ValiDatingCarrier, CrdType, Convert.ToString(fsr.ChdBFare), Convert.ToString(fsr.ChdFSur));
                                            }
                                            catch { srvChargeChd = 0; }
                                            #endregion
                                        }
                                         fsr.ChdTax = (float)Math.Ceiling( (float)((ChdTaxs + ChdOT) * ExchangeRates) + fsr.ChdWO + srvChargeChd);
                                        fsr.ChdOT = fsr.ChdTax;//(float)Math.Round(ChdOT * ExchangeRates, 4);  //(float)fsr.ChdTax - fsr.ChdFSur ;// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                        //  fsr.ChdRbd = Fares.Element("FareClassOfService").Value;
                                        //  fsr.ChdFarebasis = fsr.fareBasis;
                                        //fsr.ChdDiscount = (float)Math.Round(ChdDisc * currancyinfo[0].ExchangeRate, 4);
                                        fsr.ChdFare = fsr.ChdTax + fsr.ChdWO + fsr.ChdFSur + fsr.ChdBFare;//fsr.ChdTax + fsr.ChdBFare;
                                        // fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                        // fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                        fsr.CHDAdminMrk = 0;
                                        fsr.CHDAgentMrk = 0;
                                        if (fsr.Child > 0)
                                        {
                                            fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip);
                                            if (fsr.Trip.ToString() == JourneyType.I.ToString() && fsr.IsCorp == true)
                                            {
                                                fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                                fsr.ChdBFare = fsr.ChdBFare + fsr.CHDAdminMrk;
                                                fsr.ChdFare = fsr.ChdFare + fsr.CHDAdminMrk;
                                            }
                                        }
                                        CommDt.Clear();
                                        try
                                        {
                                            if (fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                                            {
                                                //CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                //fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                //fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                //STTFTDS.Clear();
                                                //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, TDS);
                                                //fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                //fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                                //fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                //fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                //fsr.ChdEduCess = 0;
                                                //fsr.ChdHighEduCess = 0;
                                                #region Show hide Net Fare and Commission Calculation-Child Pax
                                                if (fsr.Leg == 1)
                                                {
                                                    CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString(),contx,"");
                                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                                    {
                                                        //CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                                        fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                        fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                        STTFTDS.Clear();
                                                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, TDS);
                                                        fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                        fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                                        fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                        fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                        fsr.ChdEduCess = 0;
                                                        fsr.ChdHighEduCess = 0;
                                                    }
                                                    else
                                                    {
                                                        fsr.ChdDiscount1 = 0;
                                                        fsr.ChdCB = 0;
                                                        fsr.ChdSrvTax1 = 0;
                                                        fsr.ChdDiscount = 0;
                                                        fsr.ChdTF = 0;
                                                        fsr.ChdTds = 0;
                                                        fsr.ChdEduCess = 0;
                                                        fsr.ChdHighEduCess = 0;
                                                    }

                                                }
                                                else
                                                {
                                                    fsr.ChdDiscount1 = 0;
                                                    fsr.ChdCB = 0;
                                                    fsr.ChdSrvTax1 = 0;
                                                    fsr.ChdDiscount = 0;
                                                    fsr.ChdTF = 0;
                                                    fsr.ChdTds = 0;
                                                    fsr.ChdEduCess = 0;
                                                    fsr.ChdHighEduCess = 0;
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                fsr.ChdDiscount1 = 0;
                                                fsr.ChdCB = 0;
                                                fsr.ChdSrvTax1 = 0;
                                                fsr.ChdDiscount = 0;
                                                fsr.ChdTF = 0;
                                                fsr.ChdTds = 0;
                                                fsr.ChdEduCess = 0;
                                                fsr.ChdHighEduCess = 0;
                                            }
                                        }
                                        catch (Exception ass)
                                        {
                                            fsr.ChdDiscount1 = 0;
                                            fsr.ChdCB = 0;
                                            fsr.ChdSrvTax1 = 0;
                                            fsr.ChdDiscount = 0;
                                            fsr.ChdTF = 0;
                                            fsr.ChdTds = 0;
                                            fsr.ChdEduCess = 0;
                                            fsr.ChdHighEduCess = 0;

                                        }


                                        fsr.ChdFar = "NRM";

                                        #endregion
                                        #region Infant

                                        if (fsr.Infant > 0)
                                        {
                                            decimal InfBasefare = 0, InfTax = 0;
                                            foreach (var Passenger in Passengers)
                                            {
                                                if (Passenger.Element("PassengerFees") != null)
                                                {
                                                    if (Passenger.Element("PassengerFees").Element("PassengerFee") != null)
                                                    {
                                                        foreach (var PassengerFee in Passenger.Element("PassengerFees").Elements("PassengerFee"))
                                                        {
                                                            IEnumerable<XElement> ServiceCharges = PassengerFee.Element("ServiceCharges").Elements("BookingServiceCharge");
                                                            decimal basefare = 0, Tax = 0;//
                                                            foreach (var ChargeType in ServiceCharges)
                                                            {
                                                                if (ChargeType.Element("ChargeType").Value.Trim() == "ServiceCharge")
                                                                {
                                                                    basefare = Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                }
                                                                else
                                                                    Tax += Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                //if (ChargeType.Element("ChargeType").Value.Trim() == "Tax")
                                                                //{
                                                                //    Tax = Convert.ToDecimal(ChargeType.Element("Amount").Value);
                                                                //}
                                                            }
                                                            InfBasefare += basefare;
                                                            InfTax += Tax;
                                                        }
                                                    }
                                                }
                                            }
                                            fsr.InfBfare = (float)Math.Ceiling(InfBasefare * ExchangeRates);
                                            fsr.InfCabin = "";
                                            fsr.InfFSur = 0;
                                            fsr.InfTax = (float)Math.Ceiling(InfTax * ExchangeRates);
                                            fsr.InfOT = fsr.InfTax - fsr.InfFSur;//+ paxFare.AdditionalTxnFeePub);
                                            //fsr.InfRbd = seg.Airline.FareClass.ToString();
                                            fsr.InfFarebasis = fsr.fareBasis;
                                            fsr.InfFare = fsr.InfTax + fsr.InfBfare;
                                            fsr.InfFar = "NRM";
                                        }
                                        #endregion

                                        fsr.Searchvalue = sessionids;
                                        //fsr.OriginalTF = (fsr.AdtFare * fsr.Adult) + (fsr.ChdFare * fsr.Child) + fsr.InfFare ;
                                        fsr.OriginalTF = (float)BookingSum;
                                        fsr.OriginalTT = (srvChargeAdt * fsr.Adult) + (srvChargeChd * fsr.Child);
                                        fsr.Provider = "AK";
                                        //Rounding The fare
                                        fsr.AdtFare = (float)Math.Ceiling(fsr.AdtFare);
                                        fsr.ChdFare = (float)Math.Ceiling(fsr.ChdFare);
                                        fsr.InfFare = (float)Math.Ceiling(fsr.InfFare);
                                        //Math.Ceiling(
                                        fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + fsr.InfSrvTax;
                                        fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child) + fsr.InfTF;
                                        fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                                        fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                                        fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child) + fsr.InfTds;
                                        fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + fsr.InfMgtFee;

                                        fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + fsr.InfFare;
                                        fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + fsr.InfFSur;
                                        fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + fsr.InfTax;
                                        fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + fsr.InfBfare;
                                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                        fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                                        fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                        fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                                        fsr.Track_id = fltdt.Rows[0]["Track_id"].ToString();
                                        //OriginalTF += (float)BookingSum;
                                        //TotalFare += fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                        //NetFare += (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child)); ;

                                        //if (Fltdtlds.Tables[0].Rows[Fltdtlds.Tables[0].Rows.Count - 1]["TripType"].ToString().Trim() == "R" && Fltdtlds.Tables[0].Rows[0]["Trip"].ToString() == "D" && RTFS == "RTF")
                                        //{
                                        //    if (Convert.ToInt32(flight) == 1)
                                        //        fsrList.Add(fsr);
                                        //    else
                                        //        RfsrList.Add(fsr);
                                        //}
                                        //else
                                        //    fsrList.Add(fsr);
                                    }
                                }
                            }

                        }
                    }
                    // flight++;
                }
                if ( fltdt.Rows[fltdt.Rows.Count - 1]["TripType"].ToString().Trim() == "R" && fltdt.Rows[0]["Trip"].ToString() == "D" && RTFS == "RTF")
                {
                    float NetFare = 0, TotalFare = 0; int ifd = 0;
                    foreach (FlightSearchResults fsr in objlist)
                    {
                        if (ifd == 0)
                        {
                            NetFare += fsr.NetFare;
                            TotalFare += fsr.TotalFare;
                        }
                        else
                        {
                            NetFare += fsr.NetFare - fsr.TotMrkUp - fsr.TotMgtFee;
                            TotalFare += fsr.TotalFare - fsr.TotMrkUp - fsr.TotMgtFee;
                        }
                        ifd++;
                    }

                    foreach (FlightSearchResults fsr in objlist)
                    {
                        fsr.NetFare = NetFare;
                        fsr.TotalFare = TotalFare;
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("AirAsiaItineraryPriceSepcialOneWay", "Error_001", ex, "AirAsiaFlight");
            }
            return objlist;
        }

        public string AirAsiaAvailibilityRequest(FlightSearch objFlt, string fareType, string Sessionids, string CurrancyCode, string constr, string UserID, ref string PromoCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetAvailabilityWithTaxes>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objTripAvailabilityRequest><ace:AvailabilityRequests><ace:AvailabilityRequest>");
                if (fareType == "IB")
                {
                    string BeganDate=Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) ;
                    PromoCode = AirAsiaUtility.HaveAnyPromoCodeAplicable(objFlt.HidTxtArrCity.Split(',')[0].Trim(), objFlt.HidTxtDepCity.Split(',')[0].Trim(), BeganDate + "T00:00:00", constr, "I5");

                    objreqXml.Append("<ace:DepartureStation>" + objFlt.HidTxtArrCity.Split(',')[0].Trim() + "</ace:DepartureStation>");
                    objreqXml.Append("<ace:ArrivalStation>" + objFlt.HidTxtDepCity.Split(',')[0].Trim() + "</ace:ArrivalStation>");
                    objreqXml.Append("<ace:BeginDate>" + BeganDate + "T00:00:00</ace:BeginDate>");
                    objreqXml.Append("<ace:EndDate>" + BeganDate + "T23:59:00</ace:EndDate>");
                }
                else if (fareType == "OB")
                {
                     string BeganDate=Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2) ;
                     PromoCode = AirAsiaUtility.HaveAnyPromoCodeAplicable(objFlt.HidTxtDepCity.Split(',')[0].Trim(), objFlt.HidTxtArrCity.Split(',')[0].Trim(), BeganDate + "T00:00:00", constr, "I5");

                    objreqXml.Append("<ace:DepartureStation>" + objFlt.HidTxtDepCity.Split(',')[0].Trim() + "</ace:DepartureStation>");
                    objreqXml.Append("<ace:ArrivalStation>" + objFlt.HidTxtArrCity.Split(',')[0].Trim() + "</ace:ArrivalStation>");
                    objreqXml.Append("<ace:BeginDate>" + Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2) + "T00:00:00</ace:BeginDate>");
                    objreqXml.Append("<ace:EndDate>" + Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2) + "T23:59:00</ace:EndDate>");
                }
                objreqXml.Append("<ace:FlightType>All</ace:FlightType>");
                objreqXml.Append("<ace:PaxCount>" + (objFlt.Adult + objFlt.Child).ToString() + "</ace:PaxCount>");
                objreqXml.Append("<ace:Dow>Daily</ace:Dow>");
                objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode>");
                if (PromoCode !="")
                {
                    objreqXml.Append("<ace:PromotionCode>" + PromoCode + "</ace:PromotionCode>");
                    objreqXml.Append("<ace:AvailabilityType>Default</ace:AvailabilityType>");
                    objreqXml.Append("<ace:SourceOrganization>" + UserID + "</ace:SourceOrganization>");
                }
                else
                    objreqXml.Append("<ace:AvailabilityType>Default</ace:AvailabilityType>");
                // <ace:PromotionCode>240918ID</ace:PromotionCode><ace:AvailabilityType>Default</ace:AvailabilityType> <ace:SourceOrganization>APIINSEVSE</ace:SourceOrganization>
               
                objreqXml.Append("<ace:MaximumConnectingFlights>10</ace:MaximumConnectingFlights>");
                objreqXml.Append("<ace:AvailabilityFilter>ExcludeUnavailable</ace:AvailabilityFilter>");
                if (objFlt.Cabin != "")
                {
                    switch (objFlt.Cabin)
                    {
                        case "C":
                            objreqXml.Append("<ace:ProductClass>PM</ace:ProductClass><ace:FareClassControl>Default</ace:FareClassControl>");
                            break;
                        case "Y":
                            objreqXml.Append("<ace:ProductClass>EC</ace:ProductClass><ace:FareClassControl>Default</ace:FareClassControl>");
                            break;
                        case "F":
                            objreqXml.Append("");
                            break;
                        case "W":
                            objreqXml.Append("<ace:ProductClass>HF</ace:ProductClass><ace:FareClassControl>Default</ace:FareClassControl>");
                            break;
                    }
                }
                else
                    objreqXml.Append("<ace:FareClassControl>LowestFareClass</ace:FareClassControl>");
                objreqXml.Append("<ace:MinimumFarePrice>0.0</ace:MinimumFarePrice><ace:MaximumFarePrice>0.0</ace:MaximumFarePrice>");
                //objreqXml.Append("<ace:SSRCollectionsMode>All</ace:SSRCollectionsMode>");
                objreqXml.Append("<ace:InboundOutbound>Both</ace:InboundOutbound>");
                objreqXml.Append("<ace:NightsStay>0</ace:NightsStay>");
                objreqXml.Append("<ace:IncludeAllotments>true</ace:IncludeAllotments>");
                
                if (fareType == "IB")
                {
                    objreqXml.Append("<ace:DepartureStations><arr:string>" + objFlt.HidTxtArrCity.Split(',')[0] + "</arr:string></ace:DepartureStations>");
                    objreqXml.Append("<ace:ArrivalStations><arr:string>" + objFlt.HidTxtDepCity.Split(',')[0] + "</arr:string></ace:ArrivalStations>");
                }
                else if (fareType == "OB")
                {
                    objreqXml.Append("<ace:DepartureStations><arr:string>" + objFlt.HidTxtDepCity.Split(',')[0] + "</arr:string></ace:DepartureStations>");
                    objreqXml.Append("<ace:ArrivalStations><arr:string>" + objFlt.HidTxtArrCity.Split(',')[0] + "</arr:string></ace:ArrivalStations>");
                }
               // objreqXml.Append("<ace:FareTypes><arr:string>R</arr:string></ace:FareTypes>");
                objreqXml.Append("<ace:PaxPriceTypes>");
                for (int ad = 0; ad < objFlt.Adult; ad++)
                {
                    objreqXml.Append("<ace:PaxPriceType><ace:PaxType>ADT</ace:PaxType></ace:PaxPriceType>");
                }
                for (int ad = 0; ad < objFlt.Child; ad++)
                {
                    objreqXml.Append("<ace:PaxPriceType><ace:PaxType>CHD</ace:PaxType></ace:PaxPriceType>");
                }
                objreqXml.Append("</ace:PaxPriceTypes>");
                objreqXml.Append("<ace:JourneySortKeys><ace:JourneySortKey>LowestFare</ace:JourneySortKey></ace:JourneySortKeys>");
                objreqXml.Append("<ace:IncludeTaxesAndFees>true</ace:IncludeTaxesAndFees> <FareRuleFilter>Default</FareRuleFilter><LoyaltyFilter>MonetaryOnly</LoyaltyFilter>");
                objreqXml.Append("</ace:AvailabilityRequest>");
                #region Rountrip request in case of international
                if ((objFlt.TripType.ToString() == "RoundTrip" && objFlt.RTF==true && objFlt.Trip == Trip.D) || (objFlt.TripType.ToString() == "RoundTrip" && objFlt.Trip.ToString() == "I"))
                {
                    string BeganDate = Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2);
                    PromoCode = AirAsiaUtility.HaveAnyPromoCodeAplicable(objFlt.HidTxtArrCity.Split(',')[0].Trim(), objFlt.HidTxtDepCity.Split(',')[0].Trim(), BeganDate + "T00:00:00", constr, "I5");

                    objreqXml.Append("<ace:AvailabilityRequest>");
                    objreqXml.Append("<ace:DepartureStation>" + objFlt.HidTxtArrCity.Split(',')[0] + "</ace:DepartureStation>");
                    objreqXml.Append("<ace:ArrivalStation>" + objFlt.HidTxtDepCity.Split(',')[0] + "</ace:ArrivalStation>");
                    objreqXml.Append("<ace:BeginDate>" + Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) + "T00:00:00</ace:BeginDate>");
                    objreqXml.Append("<ace:EndDate>" + Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) + "T23:59:00</ace:EndDate>");
                    
                    objreqXml.Append("<ace:FlightType>All</ace:FlightType>");
                    objreqXml.Append("<ace:PaxCount>" + (objFlt.Adult + objFlt.Child).ToString() + "</ace:PaxCount>");
                    objreqXml.Append("<ace:Dow>Daily</ace:Dow>");
                    objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode>");
                    objreqXml.Append("<ace:AvailabilityType>Default</ace:AvailabilityType>");
                    objreqXml.Append("<ace:MaximumConnectingFlights>10</ace:MaximumConnectingFlights>");
                    objreqXml.Append("<ace:AvailabilityFilter>ExcludeUnavailable</ace:AvailabilityFilter>");
                   
                    if (objFlt.Cabin != "")
                    {
                        switch (objFlt.Cabin)
                        {
                            case "C":
                                objreqXml.Append("<ace:ProductClass>PM</ace:ProductClass><ace:FareClassControl>Default</ace:FareClassControl>");
                                break;
                            case "Y":
                                objreqXml.Append("<ace:ProductClass>EC</ace:ProductClass><ace:FareClassControl>Default</ace:FareClassControl>");
                                break;
                            case "F":
                                objreqXml.Append("");
                                break;
                            case "W":
                                objreqXml.Append("<ace:ProductClass>HF</ace:ProductClass><ace:FareClassControl>Default</ace:FareClassControl>");
                                break;
                        }
                    }
                    else
                        objreqXml.Append("<ace:FareClassControl>LowestFareClass</ace:FareClassControl>");

                    objreqXml.Append("<ace:MinimumFarePrice>0.0</ace:MinimumFarePrice><ace:MaximumFarePrice>0.0</ace:MaximumFarePrice>");
                    //objreqXml.Append("<ace:SSRCollectionsMode>All</ace:SSRCollectionsMode>");
                    objreqXml.Append("<ace:InboundOutbound>Both</ace:InboundOutbound>");
                    objreqXml.Append("<ace:NightsStay>0</ace:NightsStay>");
                    objreqXml.Append("<ace:IncludeAllotments>true</ace:IncludeAllotments>");
                    objreqXml.Append("<ace:DepartureStations><arr:string>" + objFlt.HidTxtArrCity.Split(',')[0] + "</arr:string></ace:DepartureStations>");
                    objreqXml.Append("<ace:ArrivalStations><arr:string>" + objFlt.HidTxtDepCity.Split(',')[0] + "</arr:string></ace:ArrivalStations>");
                    objreqXml.Append("<ace:PaxPriceTypes>");
                    for (int ad = 0; ad < objFlt.Adult; ad++)
                    {
                        objreqXml.Append("<ace:PaxPriceType><ace:PaxType>ADT</ace:PaxType></ace:PaxPriceType>");
                    }
                    for (int ad = 0; ad < objFlt.Child; ad++)
                    {
                        objreqXml.Append("<ace:PaxPriceType><ace:PaxType>CHD</ace:PaxType></ace:PaxPriceType>");
                    }
                    objreqXml.Append("</ace:PaxPriceTypes>");
                    objreqXml.Append("<ace:JourneySortKeys><ace:JourneySortKey>LowestFare</ace:JourneySortKey></ace:JourneySortKeys>");
                    objreqXml.Append("<ace:IncludeTaxesAndFees>true</ace:IncludeTaxesAndFees> <FareRuleFilter>Default</FareRuleFilter><LoyaltyFilter>MonetaryOnly</LoyaltyFilter>");
                    objreqXml.Append("</ace:AvailabilityRequest>");
                }
                #endregion
                objreqXml.Append("</ace:AvailabilityRequests>");
                objreqXml.Append("</tem:objTripAvailabilityRequest></tem:GetAvailabilityWithTaxes>");
                objreqXml.Append("</soapenv:Body></soapenv:Envelope>");
               
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("AirAsiaAvailibilityRequest", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }
        //public string AirAsiaItineraryPriceRequest(string Sessionids, string FlightCarrierCode, string FlightNumber, string ArrivalStation, string DepartureStation, string STA, string STD, string ClassOfService, string FareCarrierCode, string RuleNumber, string FareBasisCode, string FareSequence, string FareApplicationType, int adt, int chd, int Infant)
        public string AirAsiaItineraryPriceRequest(string Sessionids, DataTable fltdt, string RTFS, string CurrancyCode, string constr, string UserID)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                DataRow[] newFltDsIN = fltdt.Select("TripType='R'");
                DataRow[] newFltDsOut = fltdt.Select("TripType='O'");

                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetItineraryPrice>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objItineraryPriceRequest><ace:PriceItineraryBy>JourneyWithLegs</ace:PriceItineraryBy>");

                //string BeganDate = Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2);
               // string PromoCode = AirAsiaUtility.HaveAnyPromoCodeAplicable(fltdt.Rows[0]["DepartureLocation"].ToString().Trim(), newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString().Trim(), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-dd") + "T00:00:00", constr, "AK");
                string PromoCode = newFltDsOut[0]["sno"].ToString().Split(':')[9];
                if (PromoCode != "")
                    objreqXml.Append("<ace:TypeOfSale><ace:State>New</ace:State><ace:PromotionCode>" + PromoCode + "</ace:PromotionCode></ace:TypeOfSale>");
              //  <ace:TypeOfSale><ace:State>New</ace:State><ace:PromotionCode>240XXXXX1S</ace:PromotionCode></ace:TypeOfSale
                #region SSR Request
                if (Convert.ToInt32(fltdt.Rows[0]["Infant"]) > 0)
                {
                    objreqXml.Append("<ace:SSRRequest>");
                    objreqXml.Append("<ace:SegmentSSRRequests>");
                    
                    if (RTFS == "")
                    {
                            objreqXml.Append("<ace:SegmentSSRRequest>");
                            objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                            objreqXml.Append("<ace:STD>" + Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                            objreqXml.Append("<ace:DepartureStation>" + fltdt.Rows[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + fltdt.Rows[fltdt.Rows.Count - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                            objreqXml.Append("<ace:PaxSSRs>");
                            for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Infant"]); ad++)
                            {
                                for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                                {
                                    objreqXml.Append("<ace:PaxSSR>");
                                    objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                    objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                    objreqXml.Append("</ace:PaxSSR>");
                                }
                            }
                            objreqXml.Append("</ace:PaxSSRs>");
                            objreqXml.Append("</ace:SegmentSSRRequest>");                      
                    }
                    else if (RTFS == "RTF")
                    {
                        #region Onword
                         objreqXml.Append("<ace:SegmentSSRRequest>");
                         objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsOut[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsOut[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                         objreqXml.Append("<ace:STD>" + Convert.ToDateTime(newFltDsOut[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                         objreqXml.Append("<ace:DepartureStation>" + newFltDsOut[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                            objreqXml.Append("<ace:PaxSSRs>");
                            for (int ad = 0; ad < Convert.ToInt32(newFltDsOut[0]["Infant"]); ad++)
                            {
                                for (int lg = 0; lg < newFltDsOut.Length; lg++)
                                {
                                    objreqXml.Append("<ace:PaxSSR>");
                                    objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                    objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                    objreqXml.Append("</ace:PaxSSR>");
                                }
                            }
                            objreqXml.Append("</ace:PaxSSRs>");
                            objreqXml.Append("</ace:SegmentSSRRequest>");  
                        #endregion
                        #region Inbond
                        objreqXml.Append("<ace:SegmentSSRRequest>");
                        objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsIN[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsIN[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                        objreqXml.Append("<ace:STD>" + Convert.ToDateTime(newFltDsIN[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                        objreqXml.Append("<ace:DepartureStation>" + newFltDsIN[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + newFltDsIN[newFltDsIN.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                        objreqXml.Append("<ace:PaxSSRs>");
                        for (int ad = 0; ad < Convert.ToInt32(newFltDsIN[0]["Infant"]); ad++)
                        {
                            for (int lg = 0; lg < newFltDsIN.Length; lg++)
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("</ace:PaxSSR>");
                            }
                        }
                        objreqXml.Append("</ace:PaxSSRs>");
                        objreqXml.Append("</ace:SegmentSSRRequest>");
#endregion
                    }
                    objreqXml.Append("</ace:SegmentSSRRequests>");
                    objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode><ace:CancelFirstSSR>false</ace:CancelFirstSSR><ace:SSRFeeForceWaiveOnSell>false</ace:SSRFeeForceWaiveOnSell>");
                    objreqXml.Append("</ace:SSRRequest>");
                }
                        #endregion

                #region Pricing
                objreqXml.Append("<ace:PriceJourneyWithLegsRequest><ace:PriceJourneys>");
                if (RTFS =="")
                 {
                     #region Onway Pricing
                     string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                     objreqXml.Append("<ace:PriceJourney><ace:Segments>");
                    objreqXml.Append("<ace:PriceSegment>");
                    objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[fltdt.Rows.Count - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                    objreqXml.Append("<ace:STA>" + Convert.ToDateTime(fltdt.Rows[fltdt.Rows.Count - 1]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                    objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
               
                    objreqXml.Append("<ace:Fare>");
                    objreqXml.Append("<ace:ClassOfService>" + sno[2] + "</ace:ClassOfService><ace:CarrierCode>" + sno[1] + "</ace:CarrierCode>");
                    objreqXml.Append("<ace:RuleNumber>" + sno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + fltdt.Rows[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + sno[4] + "</ace:FareSequence>");
                    objreqXml.Append("<ace:FareClassOfService>" + sno[2] + "</ace:FareClassOfService><ace:FareApplicationType>" + sno[3] + "</ace:FareApplicationType>");
                    objreqXml.Append("</ace:Fare>");
                    objreqXml.Append("</ace:PriceSegment>");
                    objreqXml.Append("</ace:Segments></ace:PriceJourney>");
                     #endregion
                 }
                else if (RTFS == "RTF")
                 {
                     #region Round trip Pricing
                     string[] sno = newFltDsOut[0]["sno"].ToString().Split(':');
  
                    objreqXml.Append("<ace:PriceJourney><ace:Segments>");
                    objreqXml.Append("<ace:PriceSegment>");
                    objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                    objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsOut[newFltDsOut.Length - 1]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsOut[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                    objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsOut[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsOut[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");

                    objreqXml.Append("<ace:Fare>");
                    objreqXml.Append("<ace:ClassOfService>" + sno[2] + "</ace:ClassOfService><ace:CarrierCode>" + sno[1] + "</ace:CarrierCode>");
                    objreqXml.Append("<ace:RuleNumber>" + sno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsOut[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + sno[4] + "</ace:FareSequence>");
                    objreqXml.Append("<ace:FareClassOfService>" + sno[2] + "</ace:FareClassOfService><ace:FareApplicationType>" + sno[3] + "</ace:FareApplicationType>");
                    objreqXml.Append("</ace:Fare>");
                    objreqXml.Append("</ace:PriceSegment>");
                    objreqXml.Append("</ace:Segments></ace:PriceJourney>");

                    if (newFltDsIN[0]["TripType"].ToString().Trim() == "R")
                    {
                        string[] snoIN = newFltDsIN[0]["sno"].ToString().Split(':');
                        objreqXml.Append("<ace:PriceJourney><ace:Segments>");
                        objreqXml.Append("<ace:PriceSegment>");
                        objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[newFltDsIN.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                        objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsIN[newFltDsIN.Length - 1]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsIN[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                        objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsIN[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsIN[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");

                        objreqXml.Append("<ace:Fare>");
                        objreqXml.Append("<ace:ClassOfService>" + snoIN[2] + "</ace:ClassOfService><ace:CarrierCode>" + snoIN[1] + "</ace:CarrierCode>");
                        objreqXml.Append("<ace:RuleNumber>" + snoIN[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsIN[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + snoIN[4] + "</ace:FareSequence>");
                        objreqXml.Append("<ace:FareClassOfService>" + snoIN[2] + "</ace:FareClassOfService><ace:FareApplicationType>" + snoIN[3] + "</ace:FareApplicationType>");
                        objreqXml.Append("</ace:Fare>");
                        objreqXml.Append("</ace:PriceSegment>");
                        objreqXml.Append("</ace:Segments></ace:PriceJourney>");
                    }
                     #endregion
                 }
                objreqXml.Append("</ace:PriceJourneys>");
                #endregion
                
                objreqXml.Append("<ace:PaxCount>" + (Convert.ToInt32(fltdt.Rows[0]["Adult"]) + Convert.ToInt32(fltdt.Rows[0]["Child"]) - 1).ToString() + "</ace:PaxCount><ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode>");
                if (PromoCode != "")
                    objreqXml.Append("<ace:SourcePOS><ace:State>New</ace:State><ace:OrganizationCode>" + UserID + "</ace:OrganizationCode></ace:SourcePOS>");
                // <ace:SourcePOS><ace:State>New</ace:State><ace:OrganizationCode>XXXXXXX</ace:OrganizationCode></ace:SourcePOS>
                #region Pax Info
                objreqXml.Append("<ace:Passengers>");
                for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Adult"]); ad++)
                {
                    objreqXml.Append("<ace:Passenger><ace:PassengerNumber>" + ad.ToString() + "</ace:PassengerNumber><ace:PassengerTypeInfo><ace:PaxType>ADT</ace:PaxType></ace:PassengerTypeInfo></ace:Passenger>");
                }
                for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Child"]); ad++)
                {
                    objreqXml.Append("<ace:Passenger><ace:PassengerNumber>" + ad.ToString() + "</ace:PassengerNumber><ace:PassengerTypeInfo><ace:PaxType>CHD</ace:PaxType></ace:PassengerTypeInfo></ace:Passenger>");
                }
                objreqXml.Append("</ace:Passengers>");
                #endregion
                objreqXml.Append("</ace:PriceJourneyWithLegsRequest></tem:objItineraryPriceRequest></tem:GetItineraryPrice>");
                 objreqXml.Append("</soapenv:Body></soapenv:Envelope>");
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("AirAsiaItineraryPriceRequest", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }

        public string MultiSegmentPriceRequest(string Sessionids, DataTable fltdt, string RTFS, string CurrancyCode, string constr, string UserID)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetItineraryPrice>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objItineraryPriceRequest><ace:PriceItineraryBy>JourneyWithLegs</ace:PriceItineraryBy>");

                DataRow[] newFltDsOut = fltdt.Select("TripType='O'");
                //string PromoCode = AirAsiaUtility.HaveAnyPromoCodeAplicable(fltdt.Rows[0]["DepartureLocation"].ToString().Trim(), newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString().Trim(), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-dd") + "T00:00:00", constr, "AK");
                string PromoCode = newFltDsOut[0]["sno"].ToString().Split(':')[9];
                if (PromoCode != "")
                    objreqXml.Append("<ace:TypeOfSale><ace:State>New</ace:State><ace:PromotionCode>" + PromoCode + "</ace:PromotionCode></ace:TypeOfSale>");

                if (Convert.ToInt32(fltdt.Rows[0]["Infant"]) > 0)
                {
                    #region SSR Request
                    objreqXml.Append("<ace:SSRRequest>");
                    objreqXml.Append("<ace:SegmentSSRRequests>");

                    for (int sg = 0; sg < fltdt.Rows.Count; sg++)
                    {
                        objreqXml.Append("<ace:SegmentSSRRequest>");
                        objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[sg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[sg]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                        objreqXml.Append("<ace:STD>" + Convert.ToDateTime(fltdt.Rows[sg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                        objreqXml.Append("<ace:DepartureStation>" + fltdt.Rows[sg]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + fltdt.Rows[sg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                        objreqXml.Append("<ace:PaxSSRs>");
                        for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Infant"]); ad++)
                        {
                            objreqXml.Append("<ace:PaxSSR>");
                            objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[sg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[sg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                            objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                            objreqXml.Append("</ace:PaxSSR>");
                        }
                        objreqXml.Append("</ace:PaxSSRs>");
                        objreqXml.Append("</ace:SegmentSSRRequest>");
                    }
                    objreqXml.Append("</ace:SegmentSSRRequests>");
                    objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode><ace:CancelFirstSSR>false</ace:CancelFirstSSR><ace:SSRFeeForceWaiveOnSell>false</ace:SSRFeeForceWaiveOnSell>");
                    objreqXml.Append("</ace:SSRRequest>");
                    #endregion
                }
                //AirAsiaItineraryPriceRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), 
                //Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), sno[2], sno[1], sno[0], fltdt.Rows[0]["FareBasis"].ToString(), sno[4], sno[3], Convert.ToInt32(fltdt.Rows[0]["Adult"]), Convert.ToInt32(fltdt.Rows[0]["Child"]), Convert.ToInt32(fltdt.Rows[0]["Infant"]));
                #region Pricing
                objreqXml.Append("<ace:PriceJourneyWithLegsRequest><ace:PriceJourneys>");
                objreqXml.Append("<ace:PriceJourney><ace:Segments>");
                for (int sg = 0; sg < fltdt.Rows.Count; sg++)
                {
                    string[] sno = fltdt.Rows[sg]["sno"].ToString().Split(':');
                    objreqXml.Append("<ace:PriceSegment>");
                    objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[sg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[sg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                    objreqXml.Append("<ace:STA>" + Convert.ToDateTime(fltdt.Rows[sg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(fltdt.Rows[sg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                    objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[sg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[sg]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");

                    objreqXml.Append("<ace:Fare>");
                    objreqXml.Append("<ace:ClassOfService>" + sno[2] + "</ace:ClassOfService><ace:CarrierCode>" + sno[1] + "</ace:CarrierCode>");
                    objreqXml.Append("<ace:RuleNumber>" + sno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + fltdt.Rows[sg]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + sno[4] + "</ace:FareSequence>");
                    objreqXml.Append("<ace:FareClassOfService>" + sno[2] + "</ace:FareClassOfService><ace:FareApplicationType>" + sno[3] + "</ace:FareApplicationType>");
                    objreqXml.Append("</ace:Fare>");
                    objreqXml.Append("</ace:PriceSegment>");
                }
                objreqXml.Append("</ace:Segments></ace:PriceJourney>");
                objreqXml.Append("</ace:PriceJourneys>");
                objreqXml.Append("<ace:PaxCount>" + (Convert.ToInt32(fltdt.Rows[0]["Adult"]) + Convert.ToInt32(fltdt.Rows[0]["Child"]) - 1).ToString() + "</ace:PaxCount><ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode>");

                if (PromoCode != "")
                    objreqXml.Append("<ace:SourcePOS><ace:State>New</ace:State><ace:OrganizationCode>" + UserID + "</ace:OrganizationCode></ace:SourcePOS>");

                objreqXml.Append("<ace:Passengers>");
                for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Adult"]); ad++)
                {
                    objreqXml.Append("<ace:Passenger><ace:PassengerNumber>" + ad.ToString() + "</ace:PassengerNumber><ace:PassengerTypeInfo><ace:PaxType>ADT</ace:PaxType></ace:PassengerTypeInfo></ace:Passenger>");
                }
                for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Child"]); ad++)
                {
                    objreqXml.Append("<ace:Passenger><ace:PassengerNumber>" + ad.ToString() + "</ace:PassengerNumber><ace:PassengerTypeInfo><ace:PaxType>CHD</ace:PaxType></ace:PassengerTypeInfo></ace:Passenger>");
                }
                objreqXml.Append("</ace:Passengers>");
                #endregion
                objreqXml.Append("</ace:PriceJourneyWithLegsRequest></tem:objItineraryPriceRequest></tem:GetItineraryPrice>");
                objreqXml.Append("</soapenv:Body></soapenv:Envelope>");

            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("MultiSegmentPriceRequest", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }
        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].City;
            }

            return name;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

       private string SetFareClass(string productClass)
        {
           string fareclass="";

           switch(productClass)
           {
               case "EC" :
                   fareclass = "Economy";
                   break;
               case "EP":
                   fareclass = "Economy Promo";
                   break;
               case "PM":
                   fareclass = "Premium";
                   break;
               case "HF":
                   fareclass = "High Flyer";
                   break;
               case "DT":
                   fareclass = "Duty Travel";
                   break;
               case "FS":
                   fareclass = "Free SSR";
                   break;
               case "PP":
                   fareclass = "Premium Promo";
                   break;
               default :
                   fareclass = "Economy";
                   break;

           }
           return fareclass;
        }
       private List<FlightSearchResults> RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR)
       {
           int ln = 1;//For Total Line No.         
           int LnOb = objO[objO.Count - 1].LineNumber;
           int LnIb = objR[objR.Count - 1].LineNumber;
     
           List<FlightSearchResults> Final = new List<FlightSearchResults>();
           for (int k = 1; k <= LnOb; k++)
           {
               var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

               for (int l = 1; l <= LnIb; l++)
               {
                   var IB = (from c in objR where c.LineNumber == l select c).ToList();
                   //List<FlightSearchResults> st = new List<FlightSearchResults>();

                   if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                   {
                       int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                       int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                       if ((obtmin + 120) <= ibtmin)
                       {
                           // st = Merge(OB, IB, ln);
                           Final.AddRange(Merge(OB, IB, ln));
                           ln++;///Increment Total Ln
                       }
                   }
                   else
                   {
                       Final.AddRange(Merge(OB, IB, ln));
                       ln++;
                   }

               }

           }
           return Final;
       }
       private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln)
       {
           List<FlightSearchResults> Final = new List<FlightSearchResults>();

           float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
           //ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
           float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
           //CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
           float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;//,
           //InfSrvTax = 0, InfTF = 0;
           float ADTAgentMrk = 0, CHDAgentMrk = 0;
           var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
           var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
           var item = (FlightSearchResults)OB[0].Clone();
           var itemib = (FlightSearchResults)IB[0].Clone();
           #region ADULT
           int Adult = item.Adult;
           AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
           AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
           AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
           AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
           AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
           AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
           AdtOT = AdtOT + item.AdtOT + itemib.AdtOT ;
           AdtFare = AdtFare + item.AdtFare + itemib.AdtFare ;
           AdtTax = AdtTax + item.AdtTax + itemib.AdtTax ;
           ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
           #endregion

           #region CHILD
           int Child = item.Child;
           ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
           ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
           ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
           ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
           ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
           ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
           ChdOT = ChdOT + item.ChdOT + itemib.ChdOT ;
           ChdFare = ChdFare + item.ChdFare + itemib.ChdFare ;
           ChdTax = ChdTax + item.ChdTax + itemib.ChdTax ;
           CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;

           #endregion

           #region INFANT
           int Infant = item.Infant;
           InfFare = InfFare + item.InfFare + itemib.InfFare;
           InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
           InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
           InfIN = InfIN + item.InfIN + itemib.InfIN;
           InfJN = InfJN + item.InfJN + itemib.InfJN;
           InfOT = InfOT + item.InfOT + itemib.InfOT;
           InfQ = InfQ + item.InfQ + itemib.InfQ;
           InfTax = InfTax + item.InfTax + itemib.InfTax;
           #endregion

           #region TOTAL
           float OriginalTF = item.OriginalTF + itemib.OriginalTF;
           float OriginalTT = item.OriginalTT + itemib.OriginalTT;// item.OriginalTT + itemib.OriginalTT;
           float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
           float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
           float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
           float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
           float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child);
           float NetFare = TotalFare - ((ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
           #endregion


           ObF.ForEach(y =>
           {
               #region Adult
               y.LineNumber = Ln;
               y.AdtFSur = AdtFSur;
               y.AdtIN = AdtIN;
               y.AdtJN = AdtJN;
               y.AdtYR = AdtYR;
               y.AdtBfare = AdtBfare;
               y.AdtOT = AdtOT;
               y.AdtFare = AdtFare;
               y.AdtTax = AdtTax;
               y.ADTAgentMrk = ADTAgentMrk;
               #endregion

               #region Child
               y.ChdFSur = ChdFSur;
               y.ChdWO = ChdWO;
               y.ChdIN = ChdIN;
               y.ChdJN = ChdJN;
               y.ChdYR = ChdYR;
               y.ChdBFare = ChdBFare;
               y.ChdOT = ChdOT;
               y.ChdFare = ChdFare;
               y.ChdTax = ChdTax;
               y.CHDAgentMrk = CHDAgentMrk;
               #endregion

               #region Infant
               y.InfFare = InfFare;
               y.InfBfare = InfBfare;
               y.InfFSur = InfFSur;
               y.InfIN = InfIN;
               y.InfJN = InfJN;
               y.InfOT = InfOT;
               y.InfQ = InfQ;
               y.InfTax = InfTax;
               #endregion

               #region Total
               y.TotBfare = TotBfare;
               y.TotalTax = TotalTax;
               y.TotalFuelSur = TotalFuelSur;
               y.TotalFare = TotalFare;
               y.OriginalTF = OriginalTF;
               y.OriginalTT = OriginalTT;
               y.NetFare = NetFare;
               #endregion
           });
           Final.AddRange(ObF);



           IbF.ForEach(y =>
           {
               #region Adult
               y.LineNumber = Ln;
               y.AdtFSur = AdtFSur;
               y.AdtIN = AdtIN;
               y.AdtJN = AdtJN;
               y.AdtYR = AdtYR;
               y.AdtBfare = AdtBfare;
               y.AdtOT = AdtOT;
               y.AdtFare = AdtFare;
               y.AdtTax = AdtTax;
               y.ADTAgentMrk = ADTAgentMrk;
               #endregion

               #region Child
               y.ChdFSur = ChdFSur;
               y.ChdWO = ChdWO;
               y.ChdIN = ChdIN;
               y.ChdJN = ChdJN;
               y.ChdYR = ChdYR;
               y.ChdBFare = ChdBFare;
               y.ChdOT = ChdOT;
               y.ChdFare = ChdFare;
               y.ChdTax = ChdTax;
               y.CHDAgentMrk = CHDAgentMrk;

               #endregion

               #region Infant
               y.InfFare = InfFare;
               y.InfBfare = InfBfare;
               y.InfFSur = InfFSur;
               y.InfIN = InfIN;
               y.InfJN = InfJN;
               y.InfOT = InfOT;
               y.InfQ = InfQ;
               y.InfTax = InfTax;
               #endregion

               #region Total
               y.TotBfare = TotBfare;
               y.TotalTax = TotalTax;
               y.TotalFuelSur = TotalFuelSur;
               y.TotalFare = TotalFare;
               y.OriginalTF = OriginalTF;
               y.OriginalTT = OriginalTT;
               y.NetFare = NetFare;
               #endregion
           });
           Final.AddRange(IbF);


           return Final;
       }

   }
}
