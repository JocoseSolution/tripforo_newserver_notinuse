﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.Shared;
using STD.DAL;
using System.Threading;
using System.Data;
using Microsoft.VisualBasic;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using LCCINTL;
using AirArabia;
using LCCRTFDISTR;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using STD.BAL.TBO;
using System.IO;
using STD.BAL.G8NAV;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Configuration;
using GALWS;
using STD.BAL;
using Newtonsoft.Json;
using STD.BAL._6ENAV420;
using STD.BAL.SGNAV420;

namespace GALWS
{
  public class LCCFareCrossCheck
    {
        private System.Data.SqlClient.SqlConnection Con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        //string ConnStr = "";
        public string connectionString { get; set; }
        public LCCFareCrossCheck(string ConStr)
        {
            //ConnStr = ConStr;
            connectionString = ConStr;
        }
       
        ICacheManager objCacheManager = CacheFactory.GetCacheManager("MyCacheManager");
        //string ConStr = Convert.ToString(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        private string BlockServiceList { get; set; }
        List<FareTypeSettings> FareTypeSettingsList;
        private List<CredentialList> CrdList;
        List<FltSrvChargeList> SrvChargeList;
        DataTable PFCodeDt;
        DataTable PFCodeDt_LCC;
        List<FlightCityList> CityList;
        List<AirlineList> Airlist;
        List<MISCCharges> MiscList = new List<MISCCharges>();
        DataSet MarkupDs = new DataSet();
       // private FlightSearch searchInputs;       
        public FlightSearch SetSearchValue(ArrayList AirArray, string Trip, string AirLine,string AgentType, string UID, string DistrId, string UserType, string TypeID, string[] JSK, string[] FSK)
        {

            STD.Shared.FlightSearch req = new STD.Shared.FlightSearch();
            Dictionary<string, object> a = new Dictionary<string, object>();
            a = (Dictionary<string, object>)AirArray[0];

            Dictionary<string, object> b = new Dictionary<string, object>();
            b = (Dictionary<string, object>)AirArray[AirArray.Count - 1];
            


            #region Set Search Value
            //string listProvider = "";
            //bool isLCC = false;
            //string callApi = "";
            req.Trip1 = Convert.ToString(a["Trip"]); //Convert.ToString(a["Trip1"]);
            req.Trip = req.Trip1 == "D" ? STD.Shared.Trip.D : STD.Shared.Trip.I;
            //req.TripType1 = Convert.ToString(a["TripType"]);
            string  TripType1 = Convert.ToString(a["TripType"]);

            if (TripType1 == "O")
            {
                req.TripType1 = "rdbOneWay";
            }
            else if (TripType1 == "R")
            {
                req.TripType1 = "rdbRoundTrip";
            }
            else
            {
                req.TripType1 = "rdbMultiCity";
            }


            if (req.TripType1 == "rdbMultiCity")
                {
                    req.TripType = STD.Shared.TripType.MultiCity;
                }
                else
                {
                    req.TripType = TripType1 == "O" ? STD.Shared.TripType.OneWay : STD.Shared.TripType.RoundTrip;                  
                }


            //req.TripType = req.TripType1 == "rdbOneWay" ? STD.Shared.TripType.OneWay : STD.Shared.TripType.RoundTrip;        
           // List<AirportList> ArrivalAirportList;
            //FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
            //List<AirportList> ArrivalAirportList = objfltBal.GetAirportList("");
            //List<AirportList> DepartureAirportList = objfltBal.GetAirportList("");

                req.Adult = Convert.ToInt16(a["Adult"]);
                req.Child = Convert.ToInt16(a["Child"]);
                req.Infant = Convert.ToInt16(a["Infant"].ToString());
                req.AgentType = AgentType; //FCSHARED[0].AgentType;
                req.AirLine = Convert.ToString(a["AirLineName"])+","+ Convert.ToString(a["ValiDatingCarrier"]);
            //req.ArrivalCity = ArrivalAirportList[0].AirportCity; //Convert.ToString(a["ArrivalCityName"])+"," +Convert.ToString(a["ArrivalAirportName"]) + "(" + Convert.ToString(a["ArrAirportCode"]) +")";
            //req.DepartureCity = DepartureAirportList[0].AirportCity; //Convert.ToString(a["DepartureCityName"]) + "," + Convert.ToString(a["DepartureAirportName"]) + "(" + Convert.ToString(a["DepAirportCode"]) + ")";//Convert.ToString(a["DepartureCity"]);
            
              req.DepartureCity = Convert.ToString(a["DepartureCityName"])+"," +Convert.ToString(a["DepartureAirportName"]) + "(" + Convert.ToString(a["DepAirportCode"]) +")";
              req.ArrivalCity = Convert.ToString(b["ArrivalCityName"]) + "," + Convert.ToString(b["ArrivalAirportName"]) + "(" + Convert.ToString(b["ArrAirportCode"]) + ")";//Convert.ToString(a["DepartureCity"]);
              //string DepDate= Convert.ToString(a["DepDate"]);            
             //DateTime dt = DateTime.ParseExact(DepDate, "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);               
             //string depdate = dt.ToString("dd/MM/yyyy");
            DateTime dt = DateTime.ParseExact(Convert.ToString(a["DepartureDate"]), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);               
                req.DepDate = dt.ToString("dd/MM/yyyy");//"28/12/2018";
                req.DISTRID = DistrId; //"SPRING";
                req.GDSRTF = false;// Convert.ToBoolean(a["GDSRTF"]);
                req.HidTxtAirLine = Convert.ToString(a["ValiDatingCarrier"]);
                req.HidTxtArrCity = Convert.ToString(b["ArrAirportCode"]) + ",IN";  //Get Value from Datbase
                req.HidTxtDepCity = Convert.ToString(a["DepAirportCode"]) + ",IN";                
                req.IsCorp = Convert.ToBoolean(a["IsCorp"]);
                req.NStop = false;//Convert.ToBoolean(a["NStop"]);
                req.OwnerId = UID;//Convert.ToString(a["OwnerId"]); //FCSHARED[0].UserId;
                req.Provider = "LCC";// Convert.ToString(a["Provider"]);
                string RetDate = Convert.ToString(a["DepartureDate"]);
                DateTime Rdt = DateTime.ParseExact(RetDate, "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);                
                req.RetDate = Rdt.ToString("dd/MM/yyyy"); //"28/12/2018";                
                req.RTF = false;//Convert.ToBoolean(a["RTF"]);
                req.TypeId = TypeID;//"TA1";//Convert.ToString(a["TypeId"]); //FCSHARED[0].TypeId;
                req.UserType = UserType;//"TA";// Convert.ToString(a["UserType"]);//[0].UserType;
                //string UserType, string TypeID
                // isCoupon = Convert.ToBoolean(a["isCoupon"]);
                req.TDS = STD.BAL.Data.Calc_TDS(Con.ConnectionString, req.OwnerId);
                req.UID = req.OwnerId;
                #region Set Cabin : 16-01-2019
                if (Convert.ToString(a["AdtCabin"]).ToUpper() == "BUSINESS")
                {
                    req.Cabin = "C"; //Convert.ToString(a["AdtCabin"]);
                }
                else
                {
                    req.Cabin = "";
                }
            //else if (Convert.ToString(a["AdtCabin"]).ToUpper() == "ECONOMY")
            //{
            //    req.Cabin = "Y";
            //}
            //else
            //{
            //    req.Cabin = "";
            //}
            #endregion

            //Add New  for MultiCity
            req.DepartureCity2 = "";// Convert.ToString(a["DepartureCity2"]);
            req.ArrivalCity2 = "";// Convert.ToString(a["ArrivalCity2"]);
            req.HidTxtArrCity2 = "";//Convert.ToString(a["HidTxtArrCity2"]);
            req.HidTxtDepCity2 = "";//Convert.ToString(a["HidTxtDepCity2"]);
            req.DepDate2 = "";//Convert.ToString(a["DepDate2"]);

            req.DepartureCity3 = "";// Convert.ToString(a["DepartureCity3"]);
            req.ArrivalCity3 = "";//Convert.ToString(a["ArrivalCity3"]);
            req.HidTxtArrCity3 = "";// Convert.ToString(a["HidTxtArrCity3"]);
            req.HidTxtDepCity3 = "";//Convert.ToString(a["HidTxtDepCity3"]);
            req.DepDate3 = "";// Convert.ToString(a["DepDate3"]);

            req.DepartureCity4 = "";// Convert.ToString(a["DepartureCity4"]);
            req.ArrivalCity4 = "";// Convert.ToString(a["ArrivalCity4"]);
            req.HidTxtArrCity4 = "";//Convert.ToString(a["HidTxtArrCity4"]);
            req.HidTxtDepCity4 = "";// Convert.ToString(a["HidTxtDepCity4"]);
            req.DepDate4 = "";//Convert.ToString(a["DepDate4"]);

            req.DepartureCity5 = "";//Convert.ToString(a["DepartureCity5"]);
            req.ArrivalCity5 = "";//Convert.ToString(a["ArrivalCity5"]);
            req.HidTxtArrCity5 = "";//Convert.ToString(a["HidTxtArrCity5"]);
            req.HidTxtDepCity5 = "";//Convert.ToString(a["HidTxtDepCity5"]);
            req.DepDate5 = "";//Convert.ToString(a["DepDate5"]);

            req.DepartureCity6 = "";//Convert.ToString(a["DepartureCity6"]);
            req.ArrivalCity6 = "";//Convert.ToString(a["ArrivalCity6"]);
            req.HidTxtArrCity6 = "";// Convert.ToString(a["HidTxtArrCity6"]);
            req.HidTxtDepCity6 = "";// Convert.ToString(a["HidTxtDepCity6"]);
            req.DepDate6 = "";// Convert.ToString(a["DepDate6"]);
            req.CheckReprice = true;
            //req.JSK[0] = Convert.ToString(a["sno"]); 
            //req.FSK[0] = Convert.ToString(a["Searchvalue"]);

            //try
            //{
                List<string> JSKList = new List<string>();               
                JSKList.Add(Convert.ToString(a["sno"]));

                List<string> FSKList = new List<string>();
                FSKList.Add(Convert.ToString(a["Searchvalue"]));

                req.JSK = JSKList;//Convert.ToString(a["sno"]);
                req.FSK = FSKList;//Convert.ToString(a["Searchvalue"]);

                //req.JSK.Add(Convert.ToString(a["sno"]));
                //req.FSK.Add(Convert.ToString(a["Searchvalue"]));
                //req.JSK[0] = Convert.ToString(a["sno"]);
                //req.FSK[0] = Convert.ToString(a["Searchvalue"]);
            //}
            //catch (Exception ex)
            //{

            //}
            

            //listProvider = Convert.ToString(a["ListProvider"]);
            //isLCC = Convert.ToBoolean(a["isLCC"]);
            //callApi = Convert.ToString(a["callApi"]);

            #endregion


            return req;


        }
        
        public ArrayList SearchResultAvilabilityLcc(ArrayList AirArray, string AirLine, string AgentType, string UID, string DistrId, string UserType, string TypeID, string[] JSK, string[] FSK, HttpContext ctx)
        {
            
            ArrayList Finallist = new ArrayList();
            ArrayList Finallist2 = new ArrayList();
            STD.Shared.FlightSearch searchInputs = new STD.Shared.FlightSearch();
            //if (Finallist == null || Finallist.Count < 1)
            //{

            //}
            searchInputs = SetSearchValue(AirArray,"", AirLine, AgentType, UID, DistrId, UserType, TypeID, JSK, FSK);
            try
            {

                //string[] arr1 = new string[] { "one", "two", "three" };
                //string[] JSK,string[] FSK,string[] JSK,string[] JSK,
                //  Dim JSK(cnt), FSK(cnt), CC(cnt), FNO(cnt), DD(cnt) As String
                STD.Shared.FlightSearch req = new STD.Shared.FlightSearch();
                Dictionary<string, object> a = new Dictionary<string, object>();
                a = (Dictionary<string, object>)AirArray[0];
                string IsBagFare =  Convert.ToString(a["IsBagFare"]);
                string IsSMEFare = Convert.ToString(a["IsSMEFare"]);
                string CrdType = Convert.ToString(a["AdtFar"]);

                //{[IsBagFare, False]} {[IsSMEFare, False]} {[AdtFar, NRM]}  CrdType

                // List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType == Convert.ToString(a["AdtFar"])) && x.Provider == AirLine && x.Status == true && x.Trip == Trip).ToList();
                ctx = HttpContext.Current;
                #region Citylist and Airlinelist
                Credentials objCrd = new Credentials(connectionString);
                FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
                FareTypeSettingsList = objfltBal.GetFareTypeSettings("", searchInputs.Trip.ToString(), "");
                ArrayList objFltResultList = new ArrayList();
                CrdList = objCrd.GetServiceCredentials("");
                BlockServiceList = Data.GetBlockServices(connectionString, searchInputs.UID, searchInputs.Trip.ToString());
                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), connectionString);// Get Data From DB or Cache
                FltDeal objDeal = new FltDeal(connectionString);
                PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                PFCodeDt_LCC = objDeal.GetCodeforPForDCFromDB("PC", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                //FareTypeSettingsList = objfltBal.GetFareTypeSettings("", searchInputs.Trip.ToString(), "");

                if (objCacheManager.Contains("CityList"))
                {
                    CityList = (List<FlightCityList>)objCacheManager.GetData("CityList");

                }
                else
                {
                    CityList = Data.GetCityList("I", connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("CityList", CityList, CacheItemPriority.Normal, null, _objFileDependency);
                }
                if (objCacheManager.Contains("Airlist"))
                {
                    Airlist = (List<AirlineList>)objCacheManager.GetData("Airlist");
                }
                else
                {
                    Airlist = Data.GetAirlineList(connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("Airlist", Airlist, CacheItemPriority.Normal, null, _objFileDependency);
                }
                #endregion
                #region Markup
                string Provider = "";
                Provider = searchInputs.Provider;
                //MarkupDs = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.UserType, searchInputs.Trip.ToString()); //"MML1","DI1","DI","DI1"
                DataTable dtAgentMarkup = new DataTable();
                DataTable dtAdminMarkup = new DataTable();
                //Calculation For AgentMarkUp
                dtAgentMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                //Calculation For AdminMarkUp'
                dtAdminMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "AD");
                dtAdminMarkup.TableName = "AdminMarkUp";
                MarkupDs.Tables.Add(dtAdminMarkup);
                #endregion
                #region Misc Charges
                FlightCommonBAL objFltComm = new FlightCommonBAL(connectionString);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch (Exception ex)
                { }
                #endregion
                #region Check Airline
                string AirCode = "";
                if (searchInputs.HidTxtAirLine.Length > 1) { AirCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
                // to add other not listed flight.
                
                #endregion
               
                DataRow[] PFRow = null;
                try
                {
                    PFRow = PFCodeDt.Select("AppliedOn <>'BOOK'");
                }
                catch { }

                #region Call LCC API
                try
                {
                    //CrdListCPN[0]
                   // List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType != "CRP") && x.Provider == AirCode && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                    List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType == Convert.ToString(a["AdtFar"])) && x.Provider == AirLine && x.Status == true && x.Trip == Convert.ToString(searchInputs.Trip)).ToList();
                    var t1 = (dynamic)null;
                    CredentialList crdNew = new CredentialList();
                    crdNew = CrdListCPN[0];
                    string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                    //CredentialList objSpiceCrd = new CredentialList();
                    //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                    //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                    //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                    bool SMEFare = false;
                    if (crdNew.CrdType.ToUpper() == "SME")
                    {
                        SMEFare = true;
                    }
                    #region Call LCC API G8
                    if (AirLine=="G8")
                    {
                        G8NAV4 objLcc = new G8NAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 411, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                        ArrayList O = new ArrayList();
                        if (searchInputs.Trip == Trip.D)
                        {
                            // Finallist =  objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, MiscList, "G8", crdNew.CrdType, FareTypeSettingsList, ctx);
                            t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, MiscList, "G8", crdNew.CrdType, FareTypeSettingsList, ctx, SMEFare);
                            Finallist.Add(t1[0]);
                        }
                        //else
                        //{
                        //    t1 = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, MiscList, "G8", crdNew.CrdType, FareTypeSettingsList, ctx);
                        //    Finallist.Add(t1[0]);
                        //}
                    }
                    
                    #endregion

                    #region Call LCC API 6E
                    if(AirLine=="6E")
                    {
                        //string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                        //string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                        #region 6E NORMAL AND CORPORATE
                        if (IsBagFare.ToLower() != "true"  && IsSMEFare.ToLower() != "true")
                        { 
                         _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode                      
                        if (searchInputs.Trip == Trip.D)
                        {
                            t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, false, ctx);
                            Finallist.Add(t1[0]);
                        }
                        //else
                        //{
                        //        if (searchInputs.Trip == Trip.I)
                        //        {
                        //            t1 = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, ctx);
                        //            Finallist.Add(t1[0]);
                        //        }                                                     
                        //}

                        }
                        #endregion NORMAL AND CORPORATE

                        #region 6E HAND BAGGAGES FARE

                        if (IsBagFare.ToLower() == "true")
                        {
                            _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode                         
                           if (searchInputs.Trip == Trip.D)
                            {                                
                                t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, true, false, ctx);
                                Finallist.Add(t1[0]);
                            }
                        }

                        #endregion 6E HAND BAGGAGES FARE

                        #region 6E SME FARE

                        if (IsSMEFare.ToLower() == "true")
                        {
                            _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                            if (searchInputs.Trip == Trip.D)
                            {                                
                                t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, true, ctx);
                                Finallist.Add(t1[0]);
                            }
                        }

                        #endregion 6E SME FARE

                        //{[IsBagFare, False]} {[IsSMEFare, False]} {[AdtFar, NRM]}  CrdType
                    }

                    #endregion

                    #region Call LCC API SG
                    if (AirLine == "SG")
                    {
                        #region SG NORMAL AND CORPORATE

                        if (IsBagFare.ToLower() != "true")
                        {
                            if (crdNew.ServerIP == "V4")
                            {
                                SGNAV4 objLcc = new SGNAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, crdNew.CrdType, PFCodeDt_LCC); //,promocode
                                if (searchInputs.Trip == Trip.D)
                                {
                                    t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, false, false, ctx);
                                    Finallist.Add(t1[0]);
                                }
                                //else
                                //{                                
                                //    t1 = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, ctx);
                                //    Finallist.Add(t1[0]);
                                //}
                            }
                            else
                            {
                                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, crdNew.CrdType, PFCodeDt_LCC); //,promocode
                                if (searchInputs.Trip == Trip.D)
                                {
                                    t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, false, false, ctx);
                                    Finallist.Add(t1[0]);
                                }
                                //else
                                //{                                
                                //    t1 = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, ctx);
                                //    Finallist.Add(t1[0]);
                                //}
                            }

                        }

                        #endregion SG NORMAL AND CORPORATE

                        #region SG HAND BAGGAGES FARE

                        if (IsBagFare.ToLower() == "true")
                        {
                            if (crdNew.ServerIP == "V4")
                            {
                                SGNAV4 objLcc = new SGNAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode                           
                                if (searchInputs.Trip == Trip.D)
                                {
                                    t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, true, false, ctx);
                                    Finallist.Add(t1[0]);
                                }
                            }
                            else
                            {
                                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode                           
                                if (searchInputs.Trip == Trip.D)
                                {
                                    t1 = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, true, false, ctx);
                                    Finallist.Add(t1[0]);
                                }
                            }
                        }

                        #endregion SG HAND BAGGAGES FARE
                    }
                    #endregion
                    #region Call LCC API IX
                    if (AirLine == "IX")
                    {
                        try
                        {
                            LCCResult objLccRes = new LCCResult(connectionString);
                            // CredentialList objGoairCrd = new CredentialList();
                            // for (int i = 0; i <= CrdList.Count - 1; i++) { if (CrdList[i].Provider == "G8") { objGoairCrd = CrdList[i]; break; } }
                            objLccRes.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
                            objLccRes.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
                            // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8");
                            ArrayList objG8ListO = new ArrayList();
                            objG8ListO = objLccRes.G8Avilability(searchInputs, crdNew, SrvChargeList, CityList, Airlist, MarkupDs, 0, false);
                            //objFltResultList_O.Add(objGoairFltResultList);

                            for (int i = 0; i <= objG8ListO.Count - 1; i++)
                            {
                                if (i == 0)
                                    Finallist.Add(objG8ListO[i]);
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("LccFareCrossCheck(SearchResultAvilabilityLcc-CallLCCAPI)", "Error_002", ex, "SearchResultAvilabilityLcc");
                    
                }
                #endregion

            }
            catch (Exception ex) {
                ITZERRORLOG.ExecptionLogger.FileHandling("LccFareCrossCheck(SearchResultAvilabilityLcc)", "Error_002", ex, "SearchResultAvilabilityLcc");
            }            
            return Finallist;
        }
        
    }
}
