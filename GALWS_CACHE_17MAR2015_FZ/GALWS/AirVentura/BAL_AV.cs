﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.DAL;
using System.Data;

namespace STD.BAL
{
    public class BAL_AV
    {
        string Conn = "";

        public BAL_AV(string ConnectionString)
        {
            Conn = ConnectionString;
        }
        public int InsertAVBookingLogs(string ORDERID, string PNR, string HOLDREQ, string HOLDENCRYPTEDRES, string HOLDRES, string BOOKINGREQ, string BOOKINGENCRYPTEDRES, string BOOKINGRES, string FARERULEONEWAY, string FARERULEROUNDTRIP, string OTHER, string PROVIDER)
        {
            DAL_AV DAL1A = new DAL_AV(Conn);
            return DAL1A.InsertAVBookingLogs(ORDERID, PNR, HOLDREQ, HOLDENCRYPTEDRES, HOLDRES, BOOKINGREQ, BOOKINGENCRYPTEDRES, BOOKINGRES, FARERULEONEWAY, FARERULEROUNDTRIP, OTHER, PROVIDER);
        }
        public bool GetVASearchStatus(string ORIGIN, string DEST, string PROVIDER)
        {
            DAL_AV DAL1A = new DAL_AV(Conn);
            return DAL1A.GetVASearchStatus(ORIGIN, DEST, PROVIDER);
        }
    }
}
