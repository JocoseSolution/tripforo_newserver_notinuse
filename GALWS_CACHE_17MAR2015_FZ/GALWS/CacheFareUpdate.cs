﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.Shared;
using System.Data;
using System.Collections;
using STD.DAL;
using System.IO;
using System.Web;

namespace STD.BAL
{
    public class CacheFareUpdate
    {
        HttpContext contx;
        string ConnStr = "";
        public CacheFareUpdate()
        { }
        public CacheFareUpdate(string ConnectionString)
        {
            //m
            ConnStr = ConnectionString;
        }
        //private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF)
        //{
        //    FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
        //    ArrayList CacheList = new ArrayList(); float srvCharge = 0;
        //    DataTable CommDt = new DataTable();
        //    Hashtable STTFTDS = new Hashtable();


        //    objFS.ToList().ForEach(Objs =>
        //    {
        //        try
        //        {
        //            srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
        //            CommDt.Clear();
        //            STTFTDS.Clear();
        //            CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1);
        //            Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
        //            Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
        //            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
        //            Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
        //            Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
        //            Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
        //            Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());

        //            if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
        //            {
        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvCharge * 2;
        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvCharge * 2;
        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvCharge * 2;
        //            }

        //            else
        //            {
        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvCharge;
        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvCharge;
        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvCharge;
        //            }

        //            if (Objs.Child > 0)
        //            {
        //                CommDt.Clear();
        //                STTFTDS.Clear();
        //                CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1);
        //                Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
        //                Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
        //                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
        //                Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
        //                Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
        //                Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
        //                Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
        //                if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
        //                {
        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvCharge * 2;
        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvCharge * 2;
        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvCharge * 2;
        //                }
        //                else
        //                {
        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvCharge;
        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvCharge;
        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvCharge;
        //                }
        //            }

        //            Objs.OriginalTT = srvCharge;

        //            //OTHER
        //            // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
        //            Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
        //            //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
        //            Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
        //            Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
        //            //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
        //            Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
        //            Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
        //            Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
        //            Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
        //            // objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
        //            //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
        //            // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
        //            // else
        //            Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
        //            Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
        //        }
        //        catch (Exception ex) { }

        //    });
        //    CacheList.Add(objFS);
        //    return CacheList;
        //}
        private string GetCPNValidatingCarrier(string sno)
        {
            string VC = "";
            try
            {
                if (sno.ToUpper().Contains("INDIGOCORP"))
                {
                    VC = "6ECORP";
                }
                else if (sno.ToUpper().Contains("INDIGOTBF"))
                {
                    VC = "6ETBF";
                }
                else if (sno.ToUpper().Contains("GOAIRCORP"))
                {
                    VC = "G8CORP";
                }
                else if (sno.ToUpper().Contains("AIRASIA"))
                {
                    VC = "AKCPN";
                }
                else if (sno.ToUpper().Contains("INDIGOSPECIAL"))
                {
                    VC = "6ECPN";
                }
                else if (sno.ToUpper().Contains("SPICEJETSPECIAL"))
                {
                    VC = "SGCPN";
                }
                else if (sno.ToUpper().Contains("GOAIRSPECIAL"))
                {
                    VC = "G8CPN";
                }
                else if (sno.ToUpper().Contains("AIINDIAEXPRESS"))
                {
                    VC = "IXCPN";
                }
            }
            catch (Exception ex) { VC = ""; }
            return VC;
        }
        private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF, FlightSearch searchInputs, string CrdType)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            ArrayList CacheList = new ArrayList(); float srvCharge = 0;
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            float srvChargeAdt = 0;
            float srvChargeChd = 0;
            
            //FlightCommonBAL obj = new FlightCommonBAL();
            string FareType = "";
            objFS.ToList().ForEach(Objs =>
            {
                try
                {
                    string VTCar = "";
                    VTCar = GetCPNValidatingCarrier(Objs.sno);
                    FareType = Objs.AdtFar;
                    #region Get TotalViaSector for Commission PPPSector and PPPSegment
                    int TotalViaSector = 1;
                    try
                    {
                        //TotalViaSector = t.Leg.Select(x => x["FlightNumber"]).Distinct().Count();
                        TotalViaSector = objFS.Where(x => x.ValiDatingCarrier == Objs.ValiDatingCarrier && x.LineNumber == Objs.LineNumber).Select(y => y.FlightIdentification).Distinct().Count();
                    }
                    catch (Exception ex)
                    {
                        TotalViaSector = 1;
                    }
                    #endregion


                    //if (VTCar == "")
                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
                    //else
                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, VTCar.Trim());

                    CommDt.Clear();
                    STTFTDS.Clear();

                    try
                    {

                        //CommDt = objFltComm.GetFltComm_WithouDB(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                        //Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                        //Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                        //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
                        //Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                        //Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
                        //Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                        //Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());

                        //just cmt mohit

                        if (Objs.Sector.Length > 8 && RTF == true)
                        {
                            Objs.AdtDiscount1 = 0;
                            Objs.AdtCB = 0;
                            Objs.AdtSrvTax1 = 0;
                            Objs.AdtDiscount = 0;
                            Objs.AdtTF = 0;
                            Objs.AdtTds = 0;
                            Objs.IATAComm = 0;
                        }
                        else {
                            #region Show hide Net Fare and Commission Calculation-Adult Pax
                            if (Objs.Leg == 1)//(Objs.Flight == "1" && Objs.Leg == 1 && Objs.LineNumber == 1)
                            {
                                CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, Objs.AdtRbd, Objs.AdtCabin, searchInputs.DepDate, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, FareType, Convert.ToString(TotalViaSector),contx,"");
                                //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, Objs.AdtRbd, Objs.AdtCabin, searchInputs.DepDate, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                if (CommDt != null && CommDt.Rows.Count > 0)
                                {
                                    Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    //FlightCommonBAL obj = new FlightCommonBAL();
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, searchInputs.TDS);
                                    //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, searchInputs.TDS);
                                    Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
                                    Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    Objs.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                }
                                else
                                {
                                    Objs.AdtDiscount1 = 0;
                                    Objs.AdtCB = 0;
                                    Objs.AdtSrvTax1 = 0;
                                    Objs.AdtDiscount = 0;
                                    Objs.AdtTF = 0;
                                    Objs.AdtTds = 0;
                                }
                            }
                            else
                            {

                                Objs.AdtDiscount1 = 0;
                                Objs.AdtCB = 0;
                                Objs.AdtSrvTax1 = 0;
                                Objs.AdtDiscount = 0;
                                Objs.AdtTF = 0;
                                Objs.AdtTds = 0;
                            }
                            #endregion                 
                        }

                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText("C:\\\\CPN_SP\\\\COMMCAL_ADULT_" + System.DateTime.Now.Date.ToString("ddMMyyyy") + ".txt", ex.StackTrace.ToString() + ex.Message);
                        Objs.AdtDiscount1 = 0;
                        Objs.AdtCB = 0;
                        Objs.AdtSrvTax1 = 0;
                        Objs.AdtDiscount = 0;
                        Objs.AdtTF = 0;
                        Objs.AdtTds = 0;
                    }

                    //if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
                    //{
                    //    //CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
                    //    //Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                    //    //Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                    //    //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
                    //    //Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                    //    //Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
                    //    //Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                    //    //Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                    //}
                    //else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
                    //{
                    //    //CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
                    //    //Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                    //    //Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                    //    //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
                    //    //Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                    //    //Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
                    //    //Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                    //    //Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                    //}

                    #region Get MISC Markup Charges Date 06-03-2018
                    try
                    {
                        srvChargeAdt = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier, FareType, Convert.ToString(Objs.AdtBfare), Convert.ToString(Objs.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                    }
                    catch { srvChargeAdt = 0; }
                    #endregion


                    if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "G8" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
                    {
                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvChargeAdt * 2;
                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvChargeAdt * 2;
                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvChargeAdt * 2;
                    }

                    else
                    {
                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvChargeAdt;
                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvChargeAdt;
                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvChargeAdt;
                    }


                    if (Objs.Child > 0)
                    {
                        CommDt.Clear();
                        STTFTDS.Clear();
                        try
                        {
                            //CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                            //Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            //Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
                            //Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            //Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
                            //Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                            //Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());

                            //just cmt mohit
                           // CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, Objs.ChdRbd, Objs.ChdCabin, searchInputs.DepDate, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));

                            if (Objs.Sector.Length > 8 && RTF == true)
                            {
                                Objs.ChdDiscount1 = 0;
                                Objs.ChdCB = 0;
                                Objs.ChdSrvTax1 = 0;
                                Objs.ChdDiscount = 0;
                                Objs.ChdTF = 0;
                                Objs.ChdTds = 0;
                            }
                            else
                            {
                                #region Show hide Net Fare and Commission Calculation-Child Pax
                                if (Objs.Leg == 1)//Objs.Flight == "1" && Objs.Leg == 1 && Objs.LineNumber == 1
                                {
                                    CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, Objs.ChdRbd, Objs.ChdCabin, searchInputs.DepDate, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, FareType, Convert.ToString(TotalViaSector),contx,"");
                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                    {
                                        Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                        Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, searchInputs.TDS);
                                        // STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, searchInputs.TDS);
                                        Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                        Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
                                        Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                        Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    }
                                    else
                                    {
                                        Objs.ChdDiscount1 = 0;
                                        Objs.ChdCB = 0;
                                        Objs.ChdSrvTax1 = 0;
                                        Objs.ChdDiscount = 0;
                                        Objs.ChdTF = 0;
                                        Objs.ChdTds = 0;
                                    }

                                }
                                else
                                {
                                    Objs.ChdDiscount1 = 0;
                                    Objs.ChdCB = 0;
                                    Objs.ChdSrvTax1 = 0;
                                    Objs.ChdDiscount = 0;
                                    Objs.ChdTF = 0;
                                    Objs.ChdTds = 0;
                                }
                                #endregion
                            }
                        }
                        catch (Exception ex)
                        {
                            File.AppendAllText("C:\\\\CPN_SP\\\\COMMCAL_CHILD_" + System.DateTime.Now.Date.ToString("ddMMyyyy") + ".txt", ex.StackTrace.ToString() + ex.Message);
                            Objs.ChdDiscount1 = 0;
                            Objs.ChdCB = 0;
                            Objs.ChdSrvTax1 = 0;
                            Objs.ChdDiscount = 0;
                            Objs.ChdTF = 0;
                            Objs.ChdTds = 0;
                        }

                        //if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
                        //{
                        //    //CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
                        //    //Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                        //    //Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                        //    //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
                        //    //Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                        //    //Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
                        //    //Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                        //    //Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                        //}
                        //else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
                        //{
                        //    //CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
                        //    //Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                        //    //Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                        //    //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
                        //    //Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                        //    //Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
                        //    //Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                        //    //Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                        //}

                        #region Get MISC Markup Charges Date 06-03-2018
                        try
                        {
                            srvChargeChd = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier, FareType, Convert.ToString(Objs.ChdBFare), Convert.ToString(Objs.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                        }
                        catch { srvChargeChd = 0; }
                        #endregion

                        if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "G8" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
                        {
                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvChargeChd * 2;
                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvChargeChd * 2;
                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvChargeChd * 2;
                        }
                        else
                        {
                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvChargeChd;
                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvChargeChd;
                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvChargeChd;
                        }
                    }

                    Objs.OriginalTT = srvChargeAdt + srvChargeChd;

                    //OTHER
                    // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                    Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
                    //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                    Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
                    Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
                    //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                    Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
                    Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
                    Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
                    Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
                    //Objs.TotMgtFee = (Objs.AdtMgtFee * Objs.Adult) + (Objs.ChdMgtFee * Objs.Child) + (Objs.InfMgtFee * Objs.Infant);
                    //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                    // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                    // else
                    //Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
                    //Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));

                    //byte mk
                    Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
                    Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
                    //objFS.OperatingCarrier = p.opreatingcarrier;
                }
                catch (Exception ex) {

                    File.AppendAllText("C:\\\\CPN_SP\\\\CacheAvilabilityList_" + System.DateTime.Now.Date.ToString("ddMMyyyy") + ".txt", ex.StackTrace.ToString() + ex.Message);
                }

            });
            CacheList.Add(objFS);
            return CacheList;
        }
        private ArrayList CacheAvilability(FlightSearch searchInputs, List<FlightSearchResults> FltList, string CrdType)
        {
            ArrayList ReturnList = new ArrayList();
            try
            {
                FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
                List<FltSrvChargeList> SrvChargeList; List<MISCCharges> MiscList = new List<MISCCharges>();
                DataTable dtAgentMarkup = new DataTable();
                DataSet MarkupDs = new DataSet();
                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), ConnStr);// Get Data From DB or Cache
                dtAgentMarkup = Data.GetMarkup(ConnStr, searchInputs.UID.ToString(), searchInputs.DISTRID.ToString(), searchInputs.Trip.ToString(), "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID.ToString(), searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch (Exception ex2)
                { }
                ReturnList = CacheAvilabilityList(FltList, SrvChargeList, MarkupDs, MiscList, searchInputs.AgentType, searchInputs.TDS, searchInputs.RTF, searchInputs, CrdType);
            }
            catch (Exception ex) { }
            return ReturnList;

        }
        public int InsertCache(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Airline, string Response, string Searchdate, string Cabin)
        {
            string Sector = ""; string Key = "";
            Searchdate = Utility.Right(Searchdate, 4) + "-" + Utility.Mid(Searchdate, 3, 2) + "-" + Utility.Left(Searchdate, 2);
            Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
            Key = SearchKey(Org, Dest, Depdate, RetDate, Adt, Chd, Inf, GdsRtf, LccRtf, Cabin);
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            int InsCnt = objFCDAL.InsertCache(Sector, Searchdate, Key, Airline, Response);
            return InsCnt;


        }
        private string SearchKey(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Cabin)
        {
            string SKey = "";
            if (GdsRtf == true || LccRtf == true)
            {

                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Utility.Left(Org.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
            }
            else
            {
                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
            }
            return SKey;
        }
        public ArrayList GetCacheList(FlightSearch searchInputs, string CrdType, string ac = "")
        {
            ArrayList CList = new ArrayList();
            try
            {
                if (string.IsNullOrEmpty(searchInputs.Cabin) || searchInputs.Cabin.Trim().ToUpper() == "Y")
                {
                DataSet Cacheds = new DataSet();
                FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
                string Org = searchInputs.HidTxtDepCity.Trim();
                string Dest = searchInputs.HidTxtArrCity.Trim();
                string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
                string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
                string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
                string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
                //if (searchInputs.HidTxtAirLine.Length > 1)
                //{
                //    AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2);
                //}
                //else
                //{
                AirlineCode = ac;
                //}

                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                {
                    UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
                    RoundTrip = true;
                }
                else
                {
                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
                }
                Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
                if (Cacheds.Tables.Count > 0)
                {
                    ArrayList OBJFS_O = new ArrayList();
                    ArrayList OBJFS_R = new ArrayList();

                    foreach (DataRow dr in Cacheds.Tables[0].Rows)
                    {
                        string CacheJson = "";
                        ArrayList ChacheList = new ArrayList();
                        List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
                        ArrayList objFltResultList = new ArrayList();
                        CacheFinalList CacheFinalList = new CacheFinalList();
                        List<CacheList> clsRetList = new List<CacheList>();
                        if (RoundTrip == true)
                        {
                            clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
                                          where row.Field<string>("Airline") == dr["Airline"].ToString()
                                          select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
                            if (clsRetList.Count > 0)
                            {


                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
                                OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
                                OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
                                objFltResultList.Add(OBJFS_O);
                                objFltResultList.Add(OBJFS_R);
                                ChacheList = objFCBAL.MergeResultList(objFltResultList);
                                //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObjet(ChacheList);
                                //CacheFinalList.Airline = dr["Airline"].ToString();
                                // CacheFinalList.Json = CacheJson;
                                // CList.Add(CacheFinalList);
                                CList = ChacheList;

                            }

                        }

                        else
                        {
                            // if (searchInputs.TripType.ToString() != "RoundTrip")
                            //{
                            OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
                            OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
                            //ChacheList.Add(OBJFS_O);
                            //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
                            //CacheFinalList.Airline = dr["Airline"].ToString();
                            //CacheFinalList.Json = CacheJson;
                            //CList.Add(CacheFinalList);
                            //CList.Add(OBJFS_O[0]);
                            CList = OBJFS_O;
                            //  } 
                        }

                    }
                }
            }
                //
            }

            catch (Exception ex) {
                ITZERRORLOG.ExecptionLogger.FileHandling("CacheFareUpdate(GetCacheList)", "Error_001", ex, "GetCacheList");
            }

            return CList;
        }
        public ArrayList GetCacheListNoth(FlightSearch searchInputs, string CrdType)
        {
            ArrayList CList = new ArrayList();
            try
            {

                DataSet Cacheds = new DataSet();
                FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
                string Org = searchInputs.HidTxtDepCity.Trim();
                string Dest = searchInputs.HidTxtArrCity.Trim();
                string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
                string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
                string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
                string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
                if (searchInputs.HidTxtAirLine.Length > 1)
                {
                    AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2);
                }


                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                {
                    UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
                    RoundTrip = true;
                }
                else
                {
                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
                }
                //Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
                if (Cacheds.Tables.Count > 0)
                {
                    ArrayList OBJFS_O = new ArrayList();
                    ArrayList OBJFS_R = new ArrayList();

                    foreach (DataRow dr in Cacheds.Tables[0].Rows)
                    {
                        string CacheJson = "";
                        ArrayList ChacheList = new ArrayList();
                        List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
                        ArrayList objFltResultList = new ArrayList();
                        CacheFinalList CacheFinalList = new CacheFinalList();
                        List<CacheList> clsRetList = new List<CacheList>();
                        if (RoundTrip == true)
                        {
                            clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
                                          where row.Field<string>("Airline") == dr["Airline"].ToString()
                                          select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
                            if (clsRetList.Count > 0)
                            {


                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
                                OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
                                OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
                                objFltResultList.Add(OBJFS_O);
                                objFltResultList.Add(OBJFS_R);
                                ChacheList = objFCBAL.MergeResultList(objFltResultList);
                                //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObjet(ChacheList);
                                //CacheFinalList.Airline = dr["Airline"].ToString();
                                // CacheFinalList.Json = CacheJson;
                                CList = ChacheList;

                            }

                        }

                        else
                        {
                            // if (searchInputs.TripType.ToString() != "RoundTrip")
                            //{
                            OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
                            OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
                            //ChacheList.Add(OBJFS_O);
                            //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
                            //CacheFinalList.Airline = dr["Airline"].ToString();
                            //CacheFinalList.Json = CacheJson;
                            //CList.Add(CacheFinalList);
                            //CList.Add(OBJFS_O[0]);
                            CList = OBJFS_O;
                            //  } 
                        }

                    }
                }
            }

            catch (Exception ex) { }

            return CList;
        }

        //public ArrayList GetCacheList(FlightSearch searchInputs, string CrdType)
        //{
        //    ArrayList CList = new ArrayList();
        //    try
        //    {
        //        //if (searchInputs.AirLine.ToUpper().Contains("6E"))    // MK
        //        // {
        //        DataSet Cacheds = new DataSet();
        //        FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
        //        string Org = searchInputs.HidTxtDepCity.Trim();
        //        string Dest = searchInputs.HidTxtArrCity.Trim();
        //        string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
        //        string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
        //        string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
        //        string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
        //        if (searchInputs.HidTxtAirLine.Length > 1) { AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
        //        if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
        //        {
        //            UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
        //            UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
        //            RoundTrip = true;
        //        }
        //        else
        //        {
        //            UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
        //        }
        //        Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
        //        if (Cacheds.Tables.Count > 0)
        //        {
        //            ArrayList OBJFS_O = new ArrayList();
        //            ArrayList OBJFS_R = new ArrayList();

        //            foreach (DataRow dr in Cacheds.Tables[0].Rows)
        //            {
        //                string CacheJson = "";
        //                ArrayList ChacheList = new ArrayList();
        //                List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
        //                ArrayList objFltResultList = new ArrayList();
        //                CacheFinalList CacheFinalList = new CacheFinalList();
        //                List<CacheList> clsRetList = new List<CacheList>();
        //                if (RoundTrip == true)
        //                {
        //                    clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
        //                                  where row.Field<string>("Airline") == dr["Airline"].ToString()
        //                                  select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
        //                    if (clsRetList.Count > 0)
        //                    {


        //                        OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
        //                        OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
        //                        OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
        //                        OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
        //                        objFltResultList.Add(OBJFS_O);
        //                        objFltResultList.Add(OBJFS_R);
        //                        ChacheList = objFCBAL.MergeResultList(objFltResultList);
        //                        //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObjet(ChacheList);
        //                        //CacheFinalList.Airline = dr["Airline"].ToString();
        //                        // CacheFinalList.Json = CacheJson;
        //                        // CList.Add(CacheFinalList);
        //                        CList = ChacheList;

        //                    }

        //                }

        //                else
        //                {
        //                    // if (searchInputs.TripType.ToString() != "RoundTrip")
        //                    //{
        //                    OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
        //                    OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
        //                    //ChacheList.Add(OBJFS_O);
        //                    //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
        //                    //CacheFinalList.Airline = dr["Airline"].ToString();
        //                    //CacheFinalList.Json = CacheJson;
        //                    //CList.Add(CacheFinalList);
        //                    //CList.Add(OBJFS_O[0]);
        //                    CList = OBJFS_O;
        //                    //  } 
        //                }

        //            }
        //        }
        //    }
        //    // }
        //    catch (Exception ex) { }

        //    return CList;
        //}

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;            
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                if (StNew.Count > 0)
                {
                    STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                    TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                    IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                }
                //STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                //TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                //IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                //STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
    }
}



//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using STD.Shared;
//using System.Data;
//using System.Collections;
//using STD.DAL;

//namespace STD.BAL
//{
//    public class CacheFareUpdate
//    {
//        string ConnStr = "";
//        public CacheFareUpdate()
//        { }
//        public CacheFareUpdate(string ConnectionString)
//        {
//            ConnStr = ConnectionString;
//        }
//        //private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF)
//        //{
//        //    FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
//        //    ArrayList CacheList = new ArrayList(); float srvCharge = 0;
//        //    DataTable CommDt = new DataTable();
//        //    Hashtable STTFTDS = new Hashtable();


//        //    objFS.ToList().ForEach(Objs =>
//        //    {
//        //        try
//        //        {
//        //            srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
//        //            CommDt.Clear();
//        //            STTFTDS.Clear();
//        //            CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1);
//        //            Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//        //            Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//        //            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
//        //            Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//        //            Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
//        //            Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
//        //            Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());

//        //            if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//        //            {
//        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
//        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvCharge * 2;
//        //            }

//        //            else
//        //            {
//        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
//        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvCharge;
//        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvCharge;
//        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvCharge;
//        //            }

//        //            if (Objs.Child > 0)
//        //            {
//        //                CommDt.Clear();
//        //                STTFTDS.Clear();
//        //                CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1);
//        //                Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//        //                Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//        //                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
//        //                Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//        //                Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
//        //                Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
//        //                Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
//        //                if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//        //                {
//        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
//        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                }
//        //                else
//        //                {
//        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
//        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvCharge;
//        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvCharge;
//        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvCharge;
//        //                }
//        //            }

//        //            Objs.OriginalTT = srvCharge;

//        //            //OTHER
//        //            // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
//        //            Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
//        //            //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
//        //            Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
//        //            Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
//        //            //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
//        //            Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
//        //            Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
//        //            Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
//        //            Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
//        //            // objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
//        //            //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
//        //            // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
//        //            // else
//        //            Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
//        //            Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
//        //        }
//        //        catch (Exception ex) { }

//        //    });
//        //    CacheList.Add(objFS);
//        //    return CacheList;
//        //}
//        private string GetCPNValidatingCarrier(string sno)
//        {
//            string VC = "";
//            try
//            {
//                if (sno.ToUpper().Contains("INDIGOCORP"))
//                {
//                    VC = "6ECORP";
//                }
//                else if (sno.ToUpper().Contains("INDIGOTBF"))
//                {
//                    VC = "6ETBF";
//                }
//                else if (sno.ToUpper().Contains("GOAIRCORP"))
//                {
//                    VC = "G8CORP";
//                }
//                else if (sno.ToUpper().Contains("AIRASIA"))
//                {
//                    VC = "AKCPN";
//                }
//                else if (sno.ToUpper().Contains("INDIGOSPECIAL"))
//                {
//                    VC = "6ECPN";
//                }
//                else if (sno.ToUpper().Contains("SPICEJETSPECIAL"))
//                {
//                    VC = "SGCPN";
//                }
//                else if (sno.ToUpper().Contains("GOAIRSPECIAL"))
//                {
//                    VC = "G8CPN";
//                }
//                else if (sno.ToUpper().Contains("AIINDIAEXPRESS"))
//                {
//                    VC = "IXCPN";
//                }
//            }
//            catch (Exception ex) { VC = ""; }
//            return VC;
//        }
//        private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF, FlightSearch searchInputs, string CrdType)
//        {
//            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
//            ArrayList CacheList = new ArrayList(); float srvCharge = 0;
//            DataTable CommDt = new DataTable();
//            Hashtable STTFTDS = new Hashtable();


//            objFS.ToList().ForEach(Objs =>
//            {
//                try
//                {
//                    string VTCar = "";
//                    VTCar = GetCPNValidatingCarrier(Objs.sno);
//                    //if (VTCar == "")
//                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
//                    //else
//                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, VTCar.Trim());

//                    CommDt.Clear();
//                    STTFTDS.Clear();
//                    if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
//                    {
//                        CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                        Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                        Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
//                        Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                        Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
//                        Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
//                        Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
//                    }
//                    else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
//                    {
//                        CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                        Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                        Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
//                        Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                        Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
//                        Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
//                        Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
//                    }
//                    if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//                    {
//                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
//                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvCharge * 2;
//                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvCharge * 2;
//                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvCharge * 2;
//                    }

//                    else
//                    {
//                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
//                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvCharge;
//                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvCharge;
//                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvCharge;
//                    }

//                    if (Objs.Child > 0)
//                    {
//                        CommDt.Clear();
//                        STTFTDS.Clear();
//                        if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
//                        {
//                            CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                            Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                            Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
//                            Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                            Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
//                            Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
//                            Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
//                        }
//                        else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
//                        {
//                            CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                            Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                            Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
//                            Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                            Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
//                            Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
//                            Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
//                        }
//                        if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//                        {
//                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
//                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvCharge * 2;
//                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvCharge * 2;
//                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvCharge * 2;
//                        }
//                        else
//                        {
//                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
//                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvCharge;
//                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvCharge;
//                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvCharge;
//                        }
//                    }

//                    Objs.OriginalTT = srvCharge;

//                    //OTHER
//                    // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
//                    Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
//                    //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
//                    Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
//                    Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
//                    //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
//                    Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
//                    Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
//                    Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
//                    Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
//                    // objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
//                    //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
//                    // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
//                    // else
//                    Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
//                    Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
//                }
//                catch (Exception ex) { }

//            });
//            CacheList.Add(objFS);
//            return CacheList;
//        }
//        private ArrayList CacheAvilability(FlightSearch searchInputs, List<FlightSearchResults> FltList,string CrdType)
//        {
//            ArrayList ReturnList = new ArrayList();
//            try
//            {
//                FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
//                List<FltSrvChargeList> SrvChargeList; List<MISCCharges> MiscList = new List<MISCCharges>();
//                DataTable dtAgentMarkup = new DataTable();
//                DataSet MarkupDs = new DataSet();
//                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), ConnStr);// Get Data From DB or Cache
//                dtAgentMarkup = Data.GetMarkup(ConnStr, searchInputs.UID.ToString(), searchInputs.DISTRID.ToString(), searchInputs.Trip.ToString(), "TA");
//                dtAgentMarkup.TableName = "AgentMarkUp";
//                MarkupDs.Tables.Add(dtAgentMarkup);
//                try
//                {
//                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID.ToString(), searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
//                }
//                catch (Exception ex2)
//                { }
//                ReturnList = CacheAvilabilityList(FltList, SrvChargeList, MarkupDs, MiscList, searchInputs.AgentType, searchInputs.TDS, searchInputs.RTF, searchInputs, CrdType);
//            }
//            catch (Exception ex) { }
//            return ReturnList;

//        }
//        public int InsertCache(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Airline, string Response, string Searchdate,string Cabin)
//        {
//            string Sector = ""; string Key = "";
//            Searchdate = Utility.Right(Searchdate, 4) + "-" + Utility.Mid(Searchdate, 3, 2) + "-" + Utility.Left(Searchdate, 2);
//            Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
//            Key = SearchKey(Org, Dest, Depdate, RetDate, Adt, Chd, Inf, GdsRtf, LccRtf,Cabin);
//            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
//            int InsCnt = objFCDAL.InsertCache(Sector, Searchdate, Key, Airline, Response);
//            return InsCnt;


//        }
//        private string SearchKey(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf,string Cabin)
//        {
//            string SKey = "";
//            if (GdsRtf == true || LccRtf == true)
//            {

//                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Utility.Left(Org.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
//            }
//            else
//            {
//                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
//            }
//            return SKey;
//        }
//        public ArrayList GetCacheList(FlightSearch searchInputs, string CrdType)
//        {
//            ArrayList CList = new ArrayList();
//            try
//            {
//                DataSet Cacheds = new DataSet();
//                FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
//                string Org = searchInputs.HidTxtDepCity.Trim();
//                string Dest = searchInputs.HidTxtArrCity.Trim();
//                string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
//                string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
//                string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
//                string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
//                if (searchInputs.HidTxtAirLine.Length > 1) { AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
//                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
//                {
//                    UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF,searchInputs.Cabin);
//                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                    RoundTrip = true;
//                }
//                else
//                {
//                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                }
//                Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
//                if (Cacheds.Tables.Count > 0)
//                {
//                    ArrayList OBJFS_O = new ArrayList();
//                    ArrayList OBJFS_R = new ArrayList();

//                    foreach (DataRow dr in Cacheds.Tables[0].Rows)
//                    {
//                        string CacheJson = "";
//                        ArrayList ChacheList = new ArrayList();
//                        List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
//                        ArrayList objFltResultList = new ArrayList();
//                        CacheFinalList CacheFinalList = new CacheFinalList();
//                        List<CacheList> clsRetList = new List<CacheList>();
//                        if (RoundTrip == true)
//                        {
//                            clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
//                                          where row.Field<string>("Airline") == dr["Airline"].ToString()
//                                          select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
//                            if (clsRetList.Count > 0)
//                            {
//                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//                                OBJFS_O = CacheAvilability(searchInputs, OBJFS,CrdType);
//                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
//                                OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
//                                objFltResultList.Add(OBJFS_O);
//                                objFltResultList.Add(OBJFS_R);
//                                ChacheList = objFCBAL.MergeResultList(objFltResultList);
//                                CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
//                                CacheFinalList.Airline = dr["Airline"].ToString();
//                                CacheFinalList.Json = CacheJson;
//                                CList.Add(CacheFinalList);
//                            }

//                        }

//                        else
//                        {
//                            OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//                            OBJFS_O = CacheAvilability(searchInputs, OBJFS,CrdType);
//                            ChacheList.Add(OBJFS_O);
//                            CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
//                            CacheFinalList.Airline = dr["Airline"].ToString();
//                            CacheFinalList.Json = CacheJson;
//                            CList.Add(CacheFinalList);
//                        }

//                    }
//                }
//            }
//            catch (Exception ex) { }

//            return CList;
//        }
//        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
//        {
//            decimal STaxP = 0;
//            decimal TFeeP = 0;
//            int IATAComm = 0;
//            decimal originalDis = 0;
//            Hashtable STHT = new Hashtable();
//            try
//            {
//                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
//                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
//                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
//                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
//                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
//                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
//                //STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
//                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
//                STHT.Add("IATAComm", IATAComm);
//            }
//            catch
//            {
//                STHT.Add("STax", 0);
//                STHT.Add("TFee", 0);
//                STHT.Add("Tds", 0);
//                STHT.Add("IATAComm", 0);
//            }
//            return STHT;
//        }
//        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
//        {
//            DataRow[] airMrkArray;
//            double mrkamt = 0;
//            try
//            {
//                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

//                if (!(airMrkArray != null && airMrkArray.Length > 0))
//                {
//                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

//                }


//                if (airMrkArray.Length > 0)
//                {
//                    if (Trip == "I")
//                    {
//                        if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
//                        {
//                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
//                        }
//                        else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
//                        {
//                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
//                        }
//                    }
//                    else
//                    {
//                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
//                    }
//                }
//                else
//                {
//                    mrkamt = 0;
//                }
//            }
//            catch (Exception ex)
//            {
//                mrkamt = 0;
//            }
//            return float.Parse(mrkamt.ToString());
//        }
//    }
//}










//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using STD.Shared;
//using System.Data;
//using System.Collections;
//using STD.DAL;

//namespace STD.BAL
//{
//    public class CacheFareUpdate
//    {
//        string ConnStr = "";
//        public CacheFareUpdate()
//        { }
//        public CacheFareUpdate(string ConnectionString)
//        {
//            ConnStr = ConnectionString;
//        }
//        //private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF)
//        //{
//        //    FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
//        //    ArrayList CacheList = new ArrayList(); float srvCharge = 0;
//        //    DataTable CommDt = new DataTable();
//        //    Hashtable STTFTDS = new Hashtable();


//        //    objFS.ToList().ForEach(Objs =>
//        //    {
//        //        try
//        //        {
//        //            srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
//        //            CommDt.Clear();
//        //            STTFTDS.Clear();
//        //            CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1);
//        //            Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//        //            Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//        //            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
//        //            Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//        //            Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
//        //            Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
//        //            Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());

//        //            if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//        //            {
//        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
//        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvCharge * 2;
//        //            }

//        //            else
//        //            {
//        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
//        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvCharge;
//        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvCharge;
//        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvCharge;
//        //            }

//        //            if (Objs.Child > 0)
//        //            {
//        //                CommDt.Clear();
//        //                STTFTDS.Clear();
//        //                CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1);
//        //                Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//        //                Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//        //                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
//        //                Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//        //                Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
//        //                Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
//        //                Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
//        //                if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//        //                {
//        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
//        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvCharge * 2;
//        //                }
//        //                else
//        //                {
//        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
//        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvCharge;
//        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvCharge;
//        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvCharge;
//        //                }
//        //            }

//        //            Objs.OriginalTT = srvCharge;

//        //            //OTHER
//        //            // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
//        //            Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
//        //            //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
//        //            Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
//        //            Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
//        //            //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
//        //            Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
//        //            Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
//        //            Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
//        //            Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
//        //            // objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
//        //            //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
//        //            // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
//        //            // else
//        //            Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
//        //            Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
//        //        }
//        //        catch (Exception ex) { }

//        //    });
//        //    CacheList.Add(objFS);
//        //    return CacheList;
//        //}
//        private string GetCPNValidatingCarrier(string sno)
//        {
//            string VC = "";
//            try
//            {
//                if (sno.ToUpper().Contains("INDIGOCORP"))
//                {
//                    VC = "6ECORP";
//                }
//                else if (sno.ToUpper().Contains("INDIGOTBF"))
//                {
//                    VC = "6ETBF";
//                }
//                else if (sno.ToUpper().Contains("GOAIRCORP"))
//                {
//                    VC = "G8CORP";
//                }
//                else if (sno.ToUpper().Contains("AIRASIA"))
//                {
//                    VC = "AKCPN";
//                }
//                else if (sno.ToUpper().Contains("INDIGOSPECIAL"))
//                {
//                    VC = "6ECPN";
//                }
//                else if (sno.ToUpper().Contains("SPICEJETSPECIAL"))
//                {
//                    VC = "SGCPN";
//                }
//                else if (sno.ToUpper().Contains("GOAIRSPECIAL"))
//                {
//                    VC = "G8CPN";
//                }
//                else if (sno.ToUpper().Contains("AIINDIAEXPRESS"))
//                {
//                    VC = "IXCPN";
//                }
//            }
//            catch (Exception ex) { VC = ""; }
//            return VC;
//        }
//        private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF, FlightSearch searchInputs, string CrdType)
//        {
//            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
//            ArrayList CacheList = new ArrayList(); float srvCharge = 0;
//            DataTable CommDt = new DataTable();
//            Hashtable STTFTDS = new Hashtable();
//            float srvChargeAdt = 0;
//            float srvChargeChd = 0;


//            objFS.ToList().ForEach(Objs =>
//            {
//                try
//                {
//                    string VTCar = "";
//                    VTCar = GetCPNValidatingCarrier(Objs.sno);


//                    //if (VTCar == "")
//                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
//                    //else
//                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, VTCar.Trim());

//                    CommDt.Clear();
//                    STTFTDS.Clear();
//                    if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
//                    {
//                        //CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                        //Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                        //Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                        //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
//                        //Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                        //Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
//                        //Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
//                        //Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
//                    }
//                    else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
//                    {
//                        //CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                        //Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                        //Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                        //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
//                        //Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                        //Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
//                        //Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
//                        //Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
//                    }

//                    #region Get MISC Markup Charges Date 06-03-2018
//                    try
//                    {
//                        srvChargeAdt = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier, CrdType, Convert.ToString(Objs.AdtBfare), Convert.ToString(Objs.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
//                    }
//                    catch { srvChargeAdt = 0; }
//                    #endregion


//                    if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "G8" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//                    {
//                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
//                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvChargeAdt * 2;
//                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvChargeAdt * 2;
//                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvChargeAdt * 2;
//                    }

//                    else
//                    {
//                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
//                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvChargeAdt;
//                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvChargeAdt;
//                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvChargeAdt;
//                    }


//                    if (Objs.Child > 0)
//                    {
//                        CommDt.Clear();
//                        STTFTDS.Clear();
//                        if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
//                        {
//                            //CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                            //Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                            //Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                            //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
//                            //Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                            //Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
//                            //Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
//                            //Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
//                        }
//                        else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
//                        {
//                            //CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
//                            //Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
//                            //Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
//                            //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
//                            //Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
//                            //Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
//                            //Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
//                            //Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
//                        }

//                        #region Get MISC Markup Charges Date 06-03-2018
//                        try
//                        {
//                            srvChargeChd = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier, CrdType, Convert.ToString(Objs.ChdBFare), Convert.ToString(Objs.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
//                        }
//                        catch { srvChargeChd = 0; }
//                        #endregion

//                        if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "G8" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
//                        {
//                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
//                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvChargeChd * 2;
//                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvChargeChd * 2;
//                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvChargeChd * 2;
//                        }
//                        else
//                        {
//                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
//                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvChargeChd;
//                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvChargeChd;
//                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvChargeChd;
//                        }
//                    }

//                    Objs.OriginalTT = srvChargeAdt + srvChargeChd;

//                    //OTHER
//                    // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
//                    Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
//                    //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
//                    Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
//                    Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
//                    //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
//                    Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
//                    Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
//                    Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
//                    Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
//                    //Objs.TotMgtFee = (Objs.AdtMgtFee * Objs.Adult) + (Objs.ChdMgtFee * Objs.Child) + (Objs.InfMgtFee * Objs.Infant);
//                    //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
//                    // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
//                    // else
//                    //Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
//                    //Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));

//                    //byte mk
//                    Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
//                    Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
//                    //objFS.OperatingCarrier = p.opreatingcarrier;
//                }
//                catch (Exception ex) { }

//            });
//            CacheList.Add(objFS);
//            return CacheList;
//        }
//        private ArrayList CacheAvilability(FlightSearch searchInputs, List<FlightSearchResults> FltList, string CrdType)
//        {
//            ArrayList ReturnList = new ArrayList();
//            try
//            {
//                FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
//                List<FltSrvChargeList> SrvChargeList; List<MISCCharges> MiscList = new List<MISCCharges>();
//                DataTable dtAgentMarkup = new DataTable();
//                DataSet MarkupDs = new DataSet();
//                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), ConnStr);// Get Data From DB or Cache
//                dtAgentMarkup = Data.GetMarkup(ConnStr, searchInputs.UID.ToString(), searchInputs.DISTRID.ToString(), searchInputs.Trip.ToString(), "TA");
//                dtAgentMarkup.TableName = "AgentMarkUp";
//                MarkupDs.Tables.Add(dtAgentMarkup);
//                try
//                {
//                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID.ToString(), searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
//                }
//                catch (Exception ex2)
//                { }
//                ReturnList = CacheAvilabilityList(FltList, SrvChargeList, MarkupDs, MiscList, searchInputs.AgentType, searchInputs.TDS, searchInputs.RTF, searchInputs, CrdType);
//            }
//            catch (Exception ex) { }
//            return ReturnList;

//        }
//        public int InsertCache(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Airline, string Response, string Searchdate, string Cabin)
//        {
//            string Sector = ""; string Key = "";
//            Searchdate = Utility.Right(Searchdate, 4) + "-" + Utility.Mid(Searchdate, 3, 2) + "-" + Utility.Left(Searchdate, 2);
//            Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
//            Key = SearchKey(Org, Dest, Depdate, RetDate, Adt, Chd, Inf, GdsRtf, LccRtf, Cabin);
//            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
//            int InsCnt = objFCDAL.InsertCache(Sector, Searchdate, Key, Airline, Response);
//            return InsCnt;


//        }
//        private string SearchKey(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Cabin)
//        {
//            string SKey = "";
//            if (GdsRtf == true || LccRtf == true)
//            {

//                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Utility.Left(Org.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
//            }
//            else
//            {
//                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
//            }
//            return SKey;
//        }
//        public ArrayList GetCacheList(FlightSearch searchInputs, string CrdType,string ac ="")
//        {
//            ArrayList CList = new ArrayList();
//            try
//            {

//                DataSet Cacheds = new DataSet();
//                FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
//                string Org = searchInputs.HidTxtDepCity.Trim();
//                string Dest = searchInputs.HidTxtArrCity.Trim();
//                string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
//                string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
//                string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
//                string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
//                //if (searchInputs.HidTxtAirLine.Length > 1)
//                //{
//                //    AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2);
//                //}
//                //else
//                //{
//                    AirlineCode = ac;
//                //}

//                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
//                {
//                    UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                    RoundTrip = true;
//                }
//                else
//                {
//                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                }
//                Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
//                if (Cacheds.Tables.Count > 0)
//                {
//                    ArrayList OBJFS_O = new ArrayList();
//                    ArrayList OBJFS_R = new ArrayList();

//                    foreach (DataRow dr in Cacheds.Tables[0].Rows)
//                    {
//                        string CacheJson = "";
//                        ArrayList ChacheList = new ArrayList();
//                        List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
//                        ArrayList objFltResultList = new ArrayList();
//                        CacheFinalList CacheFinalList = new CacheFinalList();
//                        List<CacheList> clsRetList = new List<CacheList>();
//                        if (RoundTrip == true)
//                        {
//                            clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
//                                          where row.Field<string>("Airline") == dr["Airline"].ToString()
//                                          select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
//                            if (clsRetList.Count > 0)
//                            {


//                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//                                OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
//                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
//                                OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
//                                objFltResultList.Add(OBJFS_O);
//                                objFltResultList.Add(OBJFS_R);
//                                ChacheList = objFCBAL.MergeResultList(objFltResultList);
//                                //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObjet(ChacheList);
//                                //CacheFinalList.Airline = dr["Airline"].ToString();
//                                // CacheFinalList.Json = CacheJson;
//                                // CList.Add(CacheFinalList);
//                                CList = ChacheList;

//                            }

//                        }

//                        else
//                        {
//                            // if (searchInputs.TripType.ToString() != "RoundTrip")
//                            //{
//                            OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//                            OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
//                            //ChacheList.Add(OBJFS_O);
//                            //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
//                            //CacheFinalList.Airline = dr["Airline"].ToString();
//                            //CacheFinalList.Json = CacheJson;
//                            //CList.Add(CacheFinalList);
//                            //CList.Add(OBJFS_O[0]);
//                            CList = OBJFS_O;
//                            //  } 
//                        }

//                    }
//                }
//            }

//            catch (Exception ex) { }

//            return CList;
//        }
//        public ArrayList GetCacheListNoth(FlightSearch searchInputs, string CrdType)
//        {
//            ArrayList CList = new ArrayList();
//            try
//            {

//                DataSet Cacheds = new DataSet();
//                FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
//                string Org = searchInputs.HidTxtDepCity.Trim();
//                string Dest = searchInputs.HidTxtArrCity.Trim();
//                string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
//                string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
//                string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
//                string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
//                if (searchInputs.HidTxtAirLine.Length > 1)
//                {
//                    AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2);
//                }


//                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
//                {
//                    UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                    RoundTrip = true;
//                }
//                else
//                {
//                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//                }
//                //Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
//                if (Cacheds.Tables.Count > 0)
//                {
//                    ArrayList OBJFS_O = new ArrayList();
//                    ArrayList OBJFS_R = new ArrayList();

//                    foreach (DataRow dr in Cacheds.Tables[0].Rows)
//                    {
//                        string CacheJson = "";
//                        ArrayList ChacheList = new ArrayList();
//                        List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
//                        ArrayList objFltResultList = new ArrayList();
//                        CacheFinalList CacheFinalList = new CacheFinalList();
//                        List<CacheList> clsRetList = new List<CacheList>();
//                        if (RoundTrip == true)
//                        {
//                            clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
//                                          where row.Field<string>("Airline") == dr["Airline"].ToString()
//                                          select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
//                            if (clsRetList.Count > 0)
//                            {


//                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//                                OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
//                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
//                                OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
//                                objFltResultList.Add(OBJFS_O);
//                                objFltResultList.Add(OBJFS_R);
//                                ChacheList = objFCBAL.MergeResultList(objFltResultList);
//                                //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObjet(ChacheList);
//                                //CacheFinalList.Airline = dr["Airline"].ToString();
//                                // CacheFinalList.Json = CacheJson;
//                                CList = ChacheList;

//                            }

//                        }

//                        else
//                        {
//                            // if (searchInputs.TripType.ToString() != "RoundTrip")
//                            //{
//                            OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//                            OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
//                            //ChacheList.Add(OBJFS_O);
//                            //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
//                            //CacheFinalList.Airline = dr["Airline"].ToString();
//                            //CacheFinalList.Json = CacheJson;
//                            //CList.Add(CacheFinalList);
//                            //CList.Add(OBJFS_O[0]);
//                            CList = OBJFS_O;
//                            //  } 
//                        }

//                    }
//                }
//            }

//            catch (Exception ex) { }

//            return CList;
//        }

//        //public ArrayList GetCacheList(FlightSearch searchInputs, string CrdType)
//        //{
//        //    ArrayList CList = new ArrayList();
//        //    try
//        //    {
//        //        //if (searchInputs.AirLine.ToUpper().Contains("6E"))    // MK
//        //        // {
//        //        DataSet Cacheds = new DataSet();
//        //        FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
//        //        string Org = searchInputs.HidTxtDepCity.Trim();
//        //        string Dest = searchInputs.HidTxtArrCity.Trim();
//        //        string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
//        //        string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
//        //        string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
//        //        string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
//        //        if (searchInputs.HidTxtAirLine.Length > 1) { AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
//        //        if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
//        //        {
//        //            UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//        //            UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//        //            RoundTrip = true;
//        //        }
//        //        else
//        //        {
//        //            UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
//        //        }
//        //        Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
//        //        if (Cacheds.Tables.Count > 0)
//        //        {
//        //            ArrayList OBJFS_O = new ArrayList();
//        //            ArrayList OBJFS_R = new ArrayList();

//        //            foreach (DataRow dr in Cacheds.Tables[0].Rows)
//        //            {
//        //                string CacheJson = "";
//        //                ArrayList ChacheList = new ArrayList();
//        //                List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
//        //                ArrayList objFltResultList = new ArrayList();
//        //                CacheFinalList CacheFinalList = new CacheFinalList();
//        //                List<CacheList> clsRetList = new List<CacheList>();
//        //                if (RoundTrip == true)
//        //                {
//        //                    clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
//        //                                  where row.Field<string>("Airline") == dr["Airline"].ToString()
//        //                                  select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
//        //                    if (clsRetList.Count > 0)
//        //                    {


//        //                        OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//        //                        OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
//        //                        OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
//        //                        OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
//        //                        objFltResultList.Add(OBJFS_O);
//        //                        objFltResultList.Add(OBJFS_R);
//        //                        ChacheList = objFCBAL.MergeResultList(objFltResultList);
//        //                        //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObjet(ChacheList);
//        //                        //CacheFinalList.Airline = dr["Airline"].ToString();
//        //                        // CacheFinalList.Json = CacheJson;
//        //                        // CList.Add(CacheFinalList);
//        //                        CList = ChacheList;

//        //                    }

//        //                }

//        //                else
//        //                {
//        //                    // if (searchInputs.TripType.ToString() != "RoundTrip")
//        //                    //{
//        //                    OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
//        //                    OBJFS_O = CacheAvilability(searchInputs, OBJFS, CrdType);
//        //                    //ChacheList.Add(OBJFS_O);
//        //                    //CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
//        //                    //CacheFinalList.Airline = dr["Airline"].ToString();
//        //                    //CacheFinalList.Json = CacheJson;
//        //                    //CList.Add(CacheFinalList);
//        //                    //CList.Add(OBJFS_O[0]);
//        //                    CList = OBJFS_O;
//        //                    //  } 
//        //                }

//        //            }
//        //        }
//        //    }
//        //    // }
//        //    catch (Exception ex) { }

//        //    return CList;
//        //}

//        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
//        {
//            decimal STaxP = 0;
//            decimal TFeeP = 0;
//            int IATAComm = 0;
//            decimal originalDis = 0;
//            Hashtable STHT = new Hashtable();
//            try
//            {
//                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
//                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
//                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
//                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
//                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
//                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
//                //STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
//                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
//                STHT.Add("IATAComm", IATAComm);
//            }
//            catch
//            {
//                STHT.Add("STax", 0);
//                STHT.Add("TFee", 0);
//                STHT.Add("Tds", 0);
//                STHT.Add("IATAComm", 0);
//            }
//            return STHT;
//        }
//        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
//        {
//            DataRow[] airMrkArray;
//            double mrkamt = 0;
//            try
//            {
//                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

//                if (!(airMrkArray != null && airMrkArray.Length > 0))
//                {
//                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

//                }

//                if (airMrkArray.Length > 0)
//                {

//                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
//                    {
//                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
//                    }
//                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
//                    {
//                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
//                    }

//                }
//                else
//                {
//                    mrkamt = 0;
//                }
//            }
//            catch (Exception ex)
//            {
//                mrkamt = 0;
//            }
//            return float.Parse(mrkamt.ToString());
//        }
//    }
//}



////using System;
////using System.Collections.Generic;
////using System.Linq;
////using System.Text;
////using STD.Shared;
////using System.Data;
////using System.Collections;
////using STD.DAL;

////namespace STD.BAL
////{
////    public class CacheFareUpdate
////    {
////        string ConnStr = "";
////        public CacheFareUpdate()
////        { }
////        public CacheFareUpdate(string ConnectionString)
////        {
////            ConnStr = ConnectionString;
////        }
////        //private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF)
////        //{
////        //    FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
////        //    ArrayList CacheList = new ArrayList(); float srvCharge = 0;
////        //    DataTable CommDt = new DataTable();
////        //    Hashtable STTFTDS = new Hashtable();


////        //    objFS.ToList().ForEach(Objs =>
////        //    {
////        //        try
////        //        {
////        //            srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
////        //            CommDt.Clear();
////        //            STTFTDS.Clear();
////        //            CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1);
////        //            Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
////        //            Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
////        //            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
////        //            Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
////        //            Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
////        //            Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
////        //            Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());

////        //            if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
////        //            {
////        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
////        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvCharge * 2;
////        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvCharge * 2;
////        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvCharge * 2;
////        //            }

////        //            else
////        //            {
////        //                Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
////        //                Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvCharge;
////        //                Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvCharge;
////        //                Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvCharge;
////        //            }

////        //            if (Objs.Child > 0)
////        //            {
////        //                CommDt.Clear();
////        //                STTFTDS.Clear();
////        //                CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1);
////        //                Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
////        //                Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
////        //                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
////        //                Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
////        //                Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
////        //                Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
////        //                Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
////        //                if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
////        //                {
////        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
////        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvCharge * 2;
////        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvCharge * 2;
////        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvCharge * 2;
////        //                }
////        //                else
////        //                {
////        //                    Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
////        //                    Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvCharge;
////        //                    Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvCharge;
////        //                    Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvCharge;
////        //                }
////        //            }

////        //            Objs.OriginalTT = srvCharge;

////        //            //OTHER
////        //            // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
////        //            Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
////        //            //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
////        //            Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
////        //            Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
////        //            //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
////        //            Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
////        //            Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
////        //            Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
////        //            Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
////        //            // objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
////        //            //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
////        //            // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
////        //            // else
////        //            Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
////        //            Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
////        //        }
////        //        catch (Exception ex) { }

////        //    });
////        //    CacheList.Add(objFS);
////        //    return CacheList;
////        //}
////        private string GetCPNValidatingCarrier(string sno)
////        {
////            string VC = "";
////            try
////            {
////                if (sno.ToUpper().Contains("INDIGOCORP"))
////                {
////                    VC = "6ECORP";
////                }
////                else if (sno.ToUpper().Contains("INDIGOTBF"))
////                {
////                    VC = "6ETBF";
////                }
////                else if (sno.ToUpper().Contains("GOAIRCORP"))
////                {
////                    VC = "G8CORP";
////                }
////                else if (sno.ToUpper().Contains("AIRASIA"))
////                {
////                    VC = "AKCPN";
////                }
////                else if (sno.ToUpper().Contains("INDIGOSPECIAL"))
////                {
////                    VC = "6ECPN";
////                }
////                else if (sno.ToUpper().Contains("SPICEJETSPECIAL"))
////                {
////                    VC = "SGCPN";
////                }
////                else if (sno.ToUpper().Contains("GOAIRSPECIAL"))
////                {
////                    VC = "G8CPN";
////                }
////                else if (sno.ToUpper().Contains("AIINDIAEXPRESS"))
////                {
////                    VC = "IXCPN";
////                }
////            }
////            catch (Exception ex) { VC = ""; }
////            return VC;
////        }
////        private ArrayList CacheAvilabilityList(List<FlightSearchResults> objFS, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string GType, string TDS, bool RTF, FlightSearch searchInputs, string CrdType)
////        {
////            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
////            ArrayList CacheList = new ArrayList(); float srvCharge = 0;
////            DataTable CommDt = new DataTable();
////            Hashtable STTFTDS = new Hashtable();


////            objFS.ToList().ForEach(Objs =>
////            {
////                try
////                {
////                    string VTCar = "";
////                    VTCar = GetCPNValidatingCarrier(Objs.sno);
////                    //if (VTCar == "")
////                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, Objs.ValiDatingCarrier);
////                    //else
////                    //    srvCharge = objFltComm.MISCServiceFee(MiscList, VTCar.Trim());

////                    CommDt.Clear();
////                    STTFTDS.Clear();
////                    if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
////                    {
////                        CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
////                        Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
////                        Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
////                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
////                        Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
////                        Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
////                        Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
////                        Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
////                    }
////                    else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
////                    {
////                        CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.AdtBfare.ToString()), decimal.Parse(Objs.AdtFSur.ToString()), 1, "", Objs.AdtCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
////                        Objs.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
////                        Objs.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
////                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.AdtDiscount1, Objs.AdtBfare, Objs.AdtFSur, TDS);
////                        Objs.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
////                        Objs.AdtDiscount = Objs.AdtDiscount1 - Objs.AdtSrvTax1;
////                        Objs.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
////                        Objs.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
////                    }
////                    if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
////                    {
////                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip) * 2;
////                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT * 2) + srvCharge * 2;
////                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT * 2) + srvCharge * 2;
////                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT * 2) + srvCharge * 2;
////                    }

////                    else
////                    {
////                        Objs.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.AdtFare, Objs.Trip);
////                        Objs.AdtOT = (Objs.AdtOT - Objs.OriginalTT) + srvCharge;
////                        Objs.AdtTax = (Objs.AdtTax - Objs.OriginalTT) + srvCharge;
////                        Objs.AdtFare = (Objs.AdtFare - Objs.OriginalTT) + srvCharge;
////                    }

////                    if (Objs.Child > 0)
////                    {
////                        CommDt.Clear();
////                        STTFTDS.Clear();
////                        if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == false)
////                        {
////                            CommDt = objFltComm.GetFltComm_Gal(GType, Objs.ValiDatingCarrier, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
////                            Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
////                            Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
////                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
////                            Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
////                            Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
////                            Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
////                            Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
////                        }
////                        else if (Objs.AdtFareType.ToUpper().Contains("SPECIAL FARE") == true)
////                        {
////                            CommDt = objFltComm.GetFltComm_Gal(GType, VTCar, decimal.Parse(Objs.ChdBFare.ToString()), decimal.Parse(Objs.ChdFSur.ToString()), 1, "", Objs.ChdCabin, Objs.depdatelcc, Objs.OrgDestFrom + "-" + Objs.OrgDestTo, searchInputs.RetDate, Objs.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), Objs.FlightIdentification, Objs.OperatingCarrier, Objs.MarketingCarrier, CrdType, "");
////                            Objs.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
////                            Objs.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
////                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, Objs.ValiDatingCarrier, Objs.ChdDiscount1, Objs.ChdBFare, Objs.ChdFSur, TDS);
////                            Objs.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
////                            Objs.ChdDiscount = Objs.ChdDiscount1 - Objs.ChdSrvTax1;
////                            Objs.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
////                            Objs.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
////                        }
////                        if ((Objs.ValiDatingCarrier.ToUpper() == "SG" || Objs.ValiDatingCarrier.ToUpper() == "6E" || Objs.ValiDatingCarrier.ToUpper() == "LB" || Objs.ValiDatingCarrier.ToUpper() == "VA") && RTF == true)
////                        {
////                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip) * 2;// searchInputs.Trip.ToString());
////                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT * 2) + srvCharge * 2;
////                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT * 2) + srvCharge * 2;
////                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT * 2) + srvCharge * 2;
////                        }
////                        else
////                        {
////                            Objs.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Objs.ValiDatingCarrier, Objs.ChdFare, Objs.Trip);// searchInputs.Trip.ToString());
////                            Objs.ChdOT = (Objs.ChdOT - Objs.OriginalTT) + srvCharge;
////                            Objs.ChdTax = (Objs.ChdTax - Objs.OriginalTT) + srvCharge;
////                            Objs.ChdFare = (Objs.ChdFare - Objs.OriginalTT) + srvCharge;
////                        }
////                    }

////                    Objs.OriginalTT = srvCharge;

////                    //OTHER
////                    // Objs.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
////                    Objs.TotalTax = (Objs.AdtTax * Objs.Adult) + (Objs.ChdTax * Objs.Child) + (Objs.InfTax * Objs.Infant);
////                    //objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
////                    Objs.TotalFare = (Objs.AdtFare * Objs.Adult) + (Objs.ChdFare * Objs.Child) + (Objs.InfFare * Objs.Infant);
////                    Objs.STax = (Objs.AdtSrvTax * Objs.Adult) + (Objs.ChdSrvTax * Objs.Child) + (Objs.InfSrvTax * Objs.Infant);
////                    //objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
////                    Objs.TotDis = (Objs.AdtDiscount * Objs.Adult) + (Objs.ChdDiscount * Objs.Child);
////                    Objs.TotCB = (Objs.AdtCB * Objs.Adult) + (Objs.ChdCB * Objs.Child);
////                    Objs.TotTds = (Objs.AdtTds * Objs.Adult) + (Objs.ChdTds * Objs.Child);// +objFS.InfTds;
////                    Objs.TotMrkUp = (Objs.ADTAdminMrk * Objs.Adult) + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAdminMrk * Objs.Child) + (Objs.CHDAgentMrk * Objs.Child);
////                    // objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
////                    //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
////                    // objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
////                    // else
////                    Objs.TotalFare = Objs.TotalFare + Objs.TotMrkUp + Objs.STax + Objs.TFee + Objs.TotMgtFee;
////                    Objs.NetFare = (Objs.TotalFare + Objs.TotTds) - (Objs.TotDis + Objs.TotCB + (Objs.ADTAgentMrk * Objs.Adult) + (Objs.CHDAgentMrk * Objs.Child));
////                }
////                catch (Exception ex) { }

////            });
////            CacheList.Add(objFS);
////            return CacheList;
////        }
////        private ArrayList CacheAvilability(FlightSearch searchInputs, List<FlightSearchResults> FltList,string CrdType)
////        {
////            ArrayList ReturnList = new ArrayList();
////            try
////            {
////                FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
////                List<FltSrvChargeList> SrvChargeList; List<MISCCharges> MiscList = new List<MISCCharges>();
////                DataTable dtAgentMarkup = new DataTable();
////                DataSet MarkupDs = new DataSet();
////                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), ConnStr);// Get Data From DB or Cache
////                dtAgentMarkup = Data.GetMarkup(ConnStr, searchInputs.UID.ToString(), searchInputs.DISTRID.ToString(), searchInputs.Trip.ToString(), "TA");
////                dtAgentMarkup.TableName = "AgentMarkUp";
////                MarkupDs.Tables.Add(dtAgentMarkup);
////                try
////                {
////                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID.ToString(), searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
////                }
////                catch (Exception ex2)
////                { }
////                ReturnList = CacheAvilabilityList(FltList, SrvChargeList, MarkupDs, MiscList, searchInputs.AgentType, searchInputs.TDS, searchInputs.RTF, searchInputs, CrdType);
////            }
////            catch (Exception ex) { }
////            return ReturnList;

////        }
////        public int InsertCache(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Airline, string Response, string Searchdate,string Cabin)
////        {
////            string Sector = ""; string Key = "";
////            Searchdate = Utility.Right(Searchdate, 4) + "-" + Utility.Mid(Searchdate, 3, 2) + "-" + Utility.Left(Searchdate, 2);
////            Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
////            Key = SearchKey(Org, Dest, Depdate, RetDate, Adt, Chd, Inf, GdsRtf, LccRtf,Cabin);
////            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
////            int InsCnt = objFCDAL.InsertCache(Sector, Searchdate, Key, Airline, Response);
////            return InsCnt;


////        }
////        private string SearchKey(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf,string Cabin)
////        {
////            string SKey = "";
////            if (GdsRtf == true || LccRtf == true)
////            {

////                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Utility.Left(Org.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
////            }
////            else
////            {
////                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
////            }
////            return SKey;
////        }
////        public ArrayList GetCacheList(FlightSearch searchInputs, string CrdType)
////        {
////            ArrayList CList = new ArrayList();
////            try
////            {
////                DataSet Cacheds = new DataSet();
////                FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
////                string Org = searchInputs.HidTxtDepCity.Trim();
////                string Dest = searchInputs.HidTxtArrCity.Trim();
////                string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
////                string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.DepDate.Trim(), 2);
////                string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "-" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "-" + Utility.Left(searchInputs.RetDate.Trim(), 2);
////                string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
////                if (searchInputs.HidTxtAirLine.Length > 1) { AirlineCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
////                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
////                {
////                    UrlR = SearchKey(Dest, Org, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF,searchInputs.Cabin);
////                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
////                    RoundTrip = true;
////                }
////                else
////                {
////                    UrlO = SearchKey(Org, Dest, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, searchInputs.Cabin);
////                }
////                Cacheds = objFCBAL.GetCacheTable(Sector, SearchDate, UrlO, UrlR, AirlineCode, RetDate, RoundTrip);
////                if (Cacheds.Tables.Count > 0)
////                {
////                    ArrayList OBJFS_O = new ArrayList();
////                    ArrayList OBJFS_R = new ArrayList();

////                    foreach (DataRow dr in Cacheds.Tables[0].Rows)
////                    {
////                        string CacheJson = "";
////                        ArrayList ChacheList = new ArrayList();
////                        List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
////                        ArrayList objFltResultList = new ArrayList();
////                        CacheFinalList CacheFinalList = new CacheFinalList();
////                        List<CacheList> clsRetList = new List<CacheList>();
////                        if (RoundTrip == true)
////                        {
////                            clsRetList = (from row in Cacheds.Tables[1].AsEnumerable()
////                                          where row.Field<string>("Airline") == dr["Airline"].ToString()
////                                          select new { Response = row.Field<string>("response"), Airline = row.Field<string>("Airline") }).Distinct().Select(x => new CacheList() { Airline = x.Airline, Json = x.Response }).ToList();
////                            if (clsRetList.Count > 0)
////                            {
////                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
////                                OBJFS_O = CacheAvilability(searchInputs, OBJFS,CrdType);
////                                OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(clsRetList[0].Json, typeof(List<FlightSearchResults>));
////                                OBJFS_R = CacheAvilability(searchInputs, OBJFS, CrdType);
////                                objFltResultList.Add(OBJFS_O);
////                                objFltResultList.Add(OBJFS_R);
////                                ChacheList = objFCBAL.MergeResultList(objFltResultList);
////                                CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
////                                CacheFinalList.Airline = dr["Airline"].ToString();
////                                CacheFinalList.Json = CacheJson;
////                                CList.Add(CacheFinalList);
////                            }

////                        }

////                        else
////                        {
////                            OBJFS = (List<FlightSearchResults>)Newtonsoft.Json.JsonConvert.DeserializeObject(dr["response"].ToString(), typeof(List<FlightSearchResults>));
////                            OBJFS_O = CacheAvilability(searchInputs, OBJFS,CrdType);
////                            ChacheList.Add(OBJFS_O);
////                            CacheJson = Newtonsoft.Json.JsonConvert.SerializeObject(ChacheList);
////                            CacheFinalList.Airline = dr["Airline"].ToString();
////                            CacheFinalList.Json = CacheJson;
////                            CList.Add(CacheFinalList);
////                        }

////                    }
////                }
////            }
////            catch (Exception ex) { }

////            return CList;
////        }
////        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
////        {
////            decimal STaxP = 0;
////            decimal TFeeP = 0;
////            int IATAComm = 0;
////            decimal originalDis = 0;
////            Hashtable STHT = new Hashtable();
////            try
////            {
////                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
////                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
////                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
////                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
////                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
////                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
////                //STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
////                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
////                STHT.Add("IATAComm", IATAComm);
////            }
////            catch
////            {
////                STHT.Add("STax", 0);
////                STHT.Add("TFee", 0);
////                STHT.Add("Tds", 0);
////                STHT.Add("IATAComm", 0);
////            }
////            return STHT;
////        }
////        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
////        {
////            DataRow[] airMrkArray;
////            double mrkamt = 0;
////            try
////            {
////                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

////                if (!(airMrkArray != null && airMrkArray.Length > 0))
////                {
////                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

////                }


////                if (airMrkArray.Length > 0)
////                {
////                    if (Trip == "I")
////                    {
////                        if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
////                        {
////                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
////                        }
////                        else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
////                        {
////                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
////                        }
////                    }
////                    else
////                    {
////                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
////                    }
////                }
////                else
////                {
////                    mrkamt = 0;
////                }
////            }
////            catch (Exception ex)
////            {
////                mrkamt = 0;
////            }
////            return float.Parse(mrkamt.ToString());
////        }
////    }
////}
