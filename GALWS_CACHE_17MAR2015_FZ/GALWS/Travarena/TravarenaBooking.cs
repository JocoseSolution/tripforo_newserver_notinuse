﻿using STD.BAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace GALWS.Travarena
{
    public class TravarenaBooking
    {
        string UserName;
        string Password;
        string ServiceUrl;
        string TargetCode;
        string SecurityGUID;


        public TravarenaBooking(string username, string password, string serviceUrl, string targetCode, string securityGUID)
        {
            UserName = username;
            Password = password;
            ServiceUrl = serviceUrl;
            TargetCode = targetCode;
            SecurityGUID = securityGUID;
        }
        public string BookingRequest(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, ref ArrayList TktNoArray, string Constr, ref string bookingID, ref string airlinpnr)
        {
            string strRes = "";
            Dictionary<string, string> Log = new Dictionary<string, string>();
            string PNR = "";
            string Status = "";
            string exep = "";
            string Bkgid = "";
            try
            {

                DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");

                #region FareQuotel
                TravarenaPricing objAirPrice = new TravarenaPricing();



                #endregion
                #region FareQuote
                //DataSet dsCrd = Crd;

                ArrayList airPriceResp = new ArrayList();
                ArrayList FinalairPriceResp = new ArrayList();

               string SellReferenceId = "";
                airPriceResp = objAirPrice.ReGetFareQuote(Crd, FltDT, ref Log, ref exep, Convert.ToString(FltDT.Rows[0]["SearchId"]), out SellReferenceId);

                float TotPrice = (float)Math.Ceiling((Convert.ToDecimal(airPriceResp[0])));

               float totfare = (float)Math.Ceiling((Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"])));

               //decimal TotPrice = Math.Ceiling(Convert.ToDecimal(airPriceResp[0]));
                //float TotPrice = float.Parse(Convert.ToString(airPriceResp[0]));


                //decimal totfare = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"]);
                //decimal totfare = Convert.ToDecimal(FltDT.Compute("SUM(OriginalTF)", string.Empty));

               if (totfare > 0 && Convert.ToDecimal(TotPrice) >= 0 && totfare != TotPrice)
               {
                   goto FareMisMatch;
               }
               else
               {
                   FinalairPriceResp = objAirPrice.FinalFareQuote(Crd, FltDT, ref Log, ref exep, Convert.ToString(FltDT.Rows[0]["SearchId"]), out SellReferenceId);


                   float FFPrice = (float)Math.Ceiling((Convert.ToDecimal(FinalairPriceResp[0])));
                   if (totfare == FFPrice)
                   {
                       #region
                       StringBuilder LowFareSearchReq = new StringBuilder();
                       LowFareSearchReq.Append("<soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope' xmlns:api='http://apiapps.riyawings.com/'>");
                       LowFareSearchReq.Append("<soap:Body>");
                       LowFareSearchReq.Append("<api:BookReservation>");
                       LowFareSearchReq.Append("<api:objSecurity>");
                       LowFareSearchReq.Append("<api:WebTerminal>RABEK030010101</api:WebTerminal>");
                       LowFareSearchReq.Append("<api:WebUserName>gandhi01</api:WebUserName>");
                       LowFareSearchReq.Append("<api:WebPassword>riya123</api:WebPassword>");
                       //LowFareSearchReq.Append("<api:WebUserName>villa1</api:WebUserName>");
                       //LowFareSearchReq.Append("<api:WebPassword>u2pws01</api:WebPassword>");
                       LowFareSearchReq.Append("<api:WebIp>122.176.101.130</api:WebIp>");
                       LowFareSearchReq.Append("<api:WebAppType>API</api:WebAppType>");
                       LowFareSearchReq.Append("</api:objSecurity>");
                       LowFareSearchReq.Append("<api:strBookRequest>");
                       LowFareSearchReq.Append("<![CDATA[<BookReservation>");
                       LowFareSearchReq.Append("<SellReferenceId>" + SellReferenceId + "</SellReferenceId>");
                       LowFareSearchReq.Append("<CustomerDetails>");

                       string Name = "";
                       var IsPrimary = PaxDs.Tables[0].AsEnumerable().Where(x => x.Field<bool>("IsPrimary") == true).ToList();

                       bool primary = false;
                       for (int i = 0; i < PaxDs.Tables[0].Rows.Count; i++)
                       {
                           if (IsPrimary.Count == 0)
                           {
                               if (i == 0 && primary == false) //(Convert.ToString(PaxDs.Tables[0].Rows[i]["IsPrimary"]) == "0" || Convert.ToString(PaxDs.Tables[0].Rows[i]["IsPrimary"]) == "1")
                               {
                                   LowFareSearchReq.Append("<Title>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["Title"]) + "</Title>");
                                   LowFareSearchReq.Append("<Firstname>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["FName"]) + " " + Convert.ToString(PaxDs.Tables[0].Rows[i]["MName"]) + "</Firstname>");
                                   LowFareSearchReq.Append("<Lastname>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["LName"]) + "</Lastname>");
                                   LowFareSearchReq.Append("<Address1>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["AddressName"]) + "</Address1>");
                                   LowFareSearchReq.Append("<Address2></Address2>");
                                   LowFareSearchReq.Append("<Address3></Address3>");
                                   LowFareSearchReq.Append("<City>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["City"]) + "</City>");
                                   LowFareSearchReq.Append("<State>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["StateName"]) + "</State>");
                                   LowFareSearchReq.Append("<Country>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["Country"]) + "</Country>");
                                   LowFareSearchReq.Append("<MailId>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "</MailId>");
                                   LowFareSearchReq.Append("<PinCode>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["PostalCodeName"]) + "</PinCode>");
                                   LowFareSearchReq.Append("<ContactNo>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]) + "</ContactNo>");
                                   primary = true;
                               }
                           }
                           else
                           {
                               if ((Convert.ToString(PaxDs.Tables[0].Rows[i]["IsPrimary"]) == "0" || Convert.ToString(PaxDs.Tables[0].Rows[i]["IsPrimary"]) == "1" || Convert.ToString(PaxDs.Tables[0].Rows[i]["IsPrimary"]) == "True") && primary == false)
                               {
                                   LowFareSearchReq.Append("<Title>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["Title"]) + "</Title>");
                                   LowFareSearchReq.Append("<Firstname>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["FName"]) + " " + Convert.ToString(PaxDs.Tables[0].Rows[i]["MName"]) + "</Firstname>");
                                   LowFareSearchReq.Append("<Lastname>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["LName"]) + "</Lastname>");
                                   LowFareSearchReq.Append("<Address1>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["AddressName"]) + "</Address1>");
                                   LowFareSearchReq.Append("<Address2></Address2>");
                                   LowFareSearchReq.Append("<Address3></Address3>");
                                   LowFareSearchReq.Append("<City>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["City"]) + "</City>");
                                   LowFareSearchReq.Append("<State>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["StateName"]) + "</State>");
                                   LowFareSearchReq.Append("<Country>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["Country"]) + "</Country>");
                                   LowFareSearchReq.Append("<MailId>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "</MailId>");
                                   LowFareSearchReq.Append("<PinCode>" + Convert.ToString(PaxDs.Tables[0].Rows[i]["PostalCodeName"]) + "</PinCode>");
                                   LowFareSearchReq.Append("<ContactNo>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]) + "</ContactNo>");
                                   primary = true;
                               }
                           }
                       }
                       LowFareSearchReq.Append("</CustomerDetails>");
                       LowFareSearchReq.Append("<NoofAdults>" + ADTPax.Count() + "</NoofAdults>");
                       LowFareSearchReq.Append("<NoofChild>" + CHDPax.Count() + "</NoofChild>");
                       LowFareSearchReq.Append("<NoofInfants>" + INFPax.Count() + "</NoofInfants>");
                       LowFareSearchReq.Append("<TripType>" + FltDT.Rows[0]["TripType"].ToString() + "</TripType>");
                       LowFareSearchReq.Append("<SegmentType>" + FltDT.Rows[0]["Trip"].ToString() + "</SegmentType>");
                       LowFareSearchReq.Append("<TotalAmount>" + Convert.ToDecimal(FinalairPriceResp[0]) + "</TotalAmount>");
                       LowFareSearchReq.Append("<PaymentMode>AT</PaymentMode>");
                       if (!string.IsNullOrEmpty(Convert.ToString(FltHdrs.Tables[0].Rows[0]["GSTNO"])))
                       {
                           LowFareSearchReq.Append("<GSTDetail>");
                           LowFareSearchReq.Append("<GSTNumber>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["GSTNO"]) + "</GSTNumber>");
                           LowFareSearchReq.Append("<GSTCompanyName>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["GST_Company_Name"]) + "</GSTCompanyName>");
                           LowFareSearchReq.Append("<GSTAddress>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["GST_Company_Address"]) + "</GSTAddress>");
                           LowFareSearchReq.Append("<GSTEmailID>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["GST_Email"]) + "</GSTEmailID>");
                           LowFareSearchReq.Append("<GSTMobileNumber>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["GST_PhoneNo"]) + "</GSTMobileNumber>");
                           LowFareSearchReq.Append("<GSTDetail>");
                       }
                       LowFareSearchReq.Append("<Bookingdetails>");
                       LowFareSearchReq.Append("<Item>");
                       LowFareSearchReq.Append("<ValidatingCarrier>" + VC + "</ValidatingCarrier>");
                       LowFareSearchReq.Append("<Payment>");
                       LowFareSearchReq.Append("<item>");
                       LowFareSearchReq.Append("<CurrencyCode>INR</CurrencyCode>");
                       LowFareSearchReq.Append("<Amount>" + Convert.ToDecimal(FinalairPriceResp[0]) + "</Amount>");
                       LowFareSearchReq.Append("</item>");
                       LowFareSearchReq.Append("</Payment>");
                       LowFareSearchReq.Append("<TourCode></TourCode>");
                       LowFareSearchReq.Append("<Passengerlist>");


                       #region adult
                       if (ADTPax.Count() > 0)
                       {
                           for (int i = 0; i < ADTPax.Count(); i++)
                           {

                               LowFareSearchReq.Append("<item>");
                               LowFareSearchReq.Append("<PaxType>ADT</PaxType>");
                               LowFareSearchReq.Append("<Title>" + Convert.ToString(ADTPax[i]["Title"]) + "</Title>");
                               LowFareSearchReq.Append("<FirstName>" + Convert.ToString(ADTPax[i]["FName"]) + " " + Convert.ToString(ADTPax[i]["MName"]) + "</FirstName>");
                               LowFareSearchReq.Append("<LastName>" + Convert.ToString(ADTPax[i]["LName"]) + "</LastName>");
                               //LowFareSearchReq.Append("<DateofBirth>" + ADTPax[i]["DOB"].ToString() + "</DateofBirth>");
                               if (!string.IsNullOrEmpty(ADTPax[i]["DOB"].ToString()))
                               {
                                   // LowFareSearchReq.Append("<DateofBirth>21/02/1952</DateofBirth>");
                                   LowFareSearchReq.Append("<DateofBirth>" + ADTPax[i]["DOB"].ToString() + "</DateofBirth>");
                               }
                               if (Convert.ToString(ADTPax[i]["Title"]).IndexOf("s") != -1 || Convert.ToString(ADTPax[i]["Title"]).IndexOf("S") != -1)
                               {
                                   LowFareSearchReq.Append("<Gender>F</Gender>");
                               }
                               else
                               {
                                   LowFareSearchReq.Append("<Gender>M</Gender>");
                               }

                               LowFareSearchReq.Append("<SpecialReqcode></SpecialReqcode>");
                               LowFareSearchReq.Append("<Mealcode></Mealcode>");
                               LowFareSearchReq.Append("<PassportInfo>");
                               if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["PassportNo"])))
                               {
                                   if (ADTPax[i]["PassportNo"].ToString() != "Passport No")
                                   {
                                       LowFareSearchReq.Append("<IdentityProofId>Passport</IdentityProofId>");
                                       LowFareSearchReq.Append("<IdentityProofNumber>" + ADTPax[i]["PassportNo"].ToString() + "</IdentityProofNumber>");
                                       LowFareSearchReq.Append("<CountryId>" + ADTPax[i]["IssueCountryCode"].ToString() + "</CountryId>");
                                       string[] ExpiryDateSTR = ADTPax[i]["PassportExpireDate"].ToString().Split('/');
                                       int Issueyear = Convert.ToInt32(ExpiryDateSTR[2].Trim());
                                       LowFareSearchReq.Append("<ExpiryDate>" + ExpiryDateSTR[0] + "/" + ExpiryDateSTR[1] + "/" + ExpiryDateSTR[2] + "</ExpiryDate>");
                                   }
                               }
                               LowFareSearchReq.Append("</PassportInfo>");
                               LowFareSearchReq.Append("<Segment>");
                               for (int f = 0; f < FltDT.Rows.Count; f++)
                               {
                                   LowFareSearchReq.Append("<item>");
                                   LowFareSearchReq.Append("<FlightID>" + Convert.ToString(FltDT.Rows[f]["EQ"]) + "</FlightID>");
                                   LowFareSearchReq.Append("<CarrierId>" + Convert.ToString(Convert.ToString(FltDT.Rows[f]["MarketingCarrier"])) + "</CarrierId>");
                                   LowFareSearchReq.Append("<Origin>" + Convert.ToString(FltDT.Rows[f]["DepAirportCode"]) + "</Origin>");
                                   LowFareSearchReq.Append("<Destination>" + Convert.ToString(FltDT.Rows[f]["ArrAirportCode"]) + "</Destination>");
                                   LowFareSearchReq.Append("<DepartureDateTime>" + Convert.ToString(FltDT.Rows[f]["depdatelcc"]) + "</DepartureDateTime>");
                                   LowFareSearchReq.Append("<ArrivalDateTime>" + Convert.ToString(FltDT.Rows[f]["arrdatelcc"]) + "</ArrivalDateTime>");
                                   LowFareSearchReq.Append("<ClassCode>" + Convert.ToString(FltDT.Rows[f]["AdtCabin"]) + "</ClassCode>");
                                   LowFareSearchReq.Append("<FrequentFlyerNumber></FrequentFlyerNumber>");
                                   LowFareSearchReq.Append("</item>");
                               }
                               LowFareSearchReq.Append("</Segment>");

                               LowFareSearchReq.Append("</item>");
                           }
                       }
                       #endregion
                       #region Child
                       if (CHDPax.Count() > 0)
                       {
                           for (int i = 0; i < CHDPax.Count(); i++)
                           {
                               LowFareSearchReq.Append("<item>");
                               LowFareSearchReq.Append("<PaxType>CHD</PaxType>");
                               LowFareSearchReq.Append("<Title>" + Convert.ToString(CHDPax[i]["Title"]) + "</Title>");
                               LowFareSearchReq.Append("<FirstName>" + Convert.ToString(CHDPax[i]["FName"]) + " " + Convert.ToString(CHDPax[i]["MName"]) + "</FirstName>");
                               LowFareSearchReq.Append("<LastName>" + Convert.ToString(CHDPax[i]["LName"]) + "</LastName>");
                               LowFareSearchReq.Append("<DateofBirth>" + CHDPax[i]["DOB"].ToString() + "</DateofBirth>");
                               if (Convert.ToString(CHDPax[i]["Title"]).IndexOf("s") != -1 || Convert.ToString(CHDPax[i]["Title"]).IndexOf("S") != -1)
                               {
                                   LowFareSearchReq.Append("<Gender>F</Gender>");
                               }
                               else
                               {
                                   LowFareSearchReq.Append("<Gender>M</Gender>");
                               }

                               LowFareSearchReq.Append("<SpecialReqcode></SpecialReqcode>");
                               LowFareSearchReq.Append("<Mealcode></Mealcode>");
                               LowFareSearchReq.Append("<PassportInfo>");
                               if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["PassportNo"])))
                               {
                                   if (CHDPax[i]["PassportNo"].ToString() != "Passport No")
                                   {
                                       LowFareSearchReq.Append("<IdentityProofId>Passport</IdentityProofId>");
                                       LowFareSearchReq.Append("<IdentityProofNumber>" + CHDPax[i]["PassportNo"].ToString() + "</IdentityProofNumber>");
                                       LowFareSearchReq.Append("<CountryId>" + CHDPax[i]["IssueCountryCode"].ToString() + "</CountryId>");
                                       string[] ExpiryDateSTR = CHDPax[i]["PassportExpireDate"].ToString().Split('/');
                                       int Issueyear = Convert.ToInt32(ExpiryDateSTR[2].Trim());
                                       LowFareSearchReq.Append("<ExpiryDate>" + ExpiryDateSTR[0] + "/" + ExpiryDateSTR[1] + "/" + ExpiryDateSTR[2] + "</ExpiryDate>");
                                   }
                               }
                               LowFareSearchReq.Append("</PassportInfo>");
                               LowFareSearchReq.Append("<Segment>");
                               for (int f = 0; f < FltDT.Rows.Count; f++)
                               {
                                   LowFareSearchReq.Append("<item>");
                                   LowFareSearchReq.Append("<FlightID>" + Convert.ToString(FltDT.Rows[f]["EQ"]) + "</FlightID>");
                                   LowFareSearchReq.Append("<CarrierId>" + Convert.ToString(Convert.ToString(FltDT.Rows[f]["MarketingCarrier"])) + "</CarrierId>");
                                   LowFareSearchReq.Append("<Origin>" + Convert.ToString(FltDT.Rows[f]["DepAirportCode"]) + "</Origin>");
                                   LowFareSearchReq.Append("<Destination>" + Convert.ToString(FltDT.Rows[f]["ArrAirportCode"]) + "</Destination>");
                                   LowFareSearchReq.Append("<DepartureDateTime>" + Convert.ToString(FltDT.Rows[f]["depdatelcc"]) + "</DepartureDateTime>");
                                   LowFareSearchReq.Append("<ArrivalDateTime>" + Convert.ToString(FltDT.Rows[f]["arrdatelcc"]) + "</ArrivalDateTime>");
                                   LowFareSearchReq.Append("<ClassCode>" + Convert.ToString(FltDT.Rows[f]["AdtCabin"]) + "</ClassCode>");
                                   LowFareSearchReq.Append("<FrequentFlyerNumber></FrequentFlyerNumber>");
                                   LowFareSearchReq.Append("</item>");
                               }
                               LowFareSearchReq.Append("</Segment>");

                               LowFareSearchReq.Append("</item>");
                           }
                       }
                       #endregion

                       #region Infant

                       if (INFPax.Count() > 0)
                       {
                           for (int i = 0; i < INFPax.Count(); i++)
                           {
                               LowFareSearchReq.Append("<item>");
                               LowFareSearchReq.Append("<PaxType>INF</PaxType>");
                               LowFareSearchReq.Append("<Title>" + Convert.ToString(INFPax[i]["Title"]) + "</Title>");
                               LowFareSearchReq.Append("<FirstName>" + Convert.ToString(INFPax[i]["FName"]) + " " + Convert.ToString(INFPax[i]["MName"]) + "</FirstName>");
                               LowFareSearchReq.Append("<LastName>" + Convert.ToString(INFPax[i]["LName"]) + "</LastName>");
                               LowFareSearchReq.Append("<DateofBirth>" + INFPax[i]["DOB"].ToString() + "</DateofBirth>");
                               if (Convert.ToString(INFPax[i]["Title"]).IndexOf("s") != -1 || Convert.ToString(INFPax[i]["Title"]).IndexOf("S") != -1)
                               {
                                   LowFareSearchReq.Append("<Gender>F</Gender>");
                               }
                               else
                               {
                                   LowFareSearchReq.Append("<Gender>M</Gender>");
                               }

                               LowFareSearchReq.Append("<SpecialReqcode></SpecialReqcode>");
                               LowFareSearchReq.Append("<Mealcode></Mealcode>");
                               LowFareSearchReq.Append("<PassportInfo>");
                               if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["PassportNo"])))
                               {
                                   if (INFPax[i]["PassportNo"].ToString() != "Passport No")
                                   {
                                       LowFareSearchReq.Append("<IdentityProofId>Passport</IdentityProofId>");
                                       LowFareSearchReq.Append("<IdentityProofNumber>" + INFPax[i]["PassportNo"].ToString() + "</IdentityProofNumber>");
                                       LowFareSearchReq.Append("<CountryId>" + INFPax[i]["IssueCountryCode"].ToString() + "</CountryId>");
                                       string[] ExpiryDateSTR = INFPax[i]["PassportExpireDate"].ToString().Split('/');
                                       int Issueyear = Convert.ToInt32(ExpiryDateSTR[2].Trim());
                                       LowFareSearchReq.Append("<ExpiryDate>" + ExpiryDateSTR[0] + "/" + ExpiryDateSTR[1] + "/" + ExpiryDateSTR[2] + "</ExpiryDate>");
                                   }
                               }
                               LowFareSearchReq.Append("</PassportInfo>");
                               LowFareSearchReq.Append("<Segment>");
                               for (int f = 0; f < FltDT.Rows.Count; f++)
                               {
                                   LowFareSearchReq.Append("<item>");
                                   LowFareSearchReq.Append("<FlightID>" + Convert.ToString(FltDT.Rows[f]["EQ"]) + "</FlightID>");
                                   LowFareSearchReq.Append("<CarrierId>" + Convert.ToString(Convert.ToString(FltDT.Rows[f]["MarketingCarrier"])) + "</CarrierId>");
                                   LowFareSearchReq.Append("<Origin>" + Convert.ToString(FltDT.Rows[f]["DepAirportCode"]) + "</Origin>");
                                   LowFareSearchReq.Append("<Destination>" + Convert.ToString(FltDT.Rows[f]["ArrAirportCode"]) + "</Destination>");
                                   LowFareSearchReq.Append("<DepartureDateTime>" + Convert.ToString(FltDT.Rows[f]["depdatelcc"]) + "</DepartureDateTime>");
                                   LowFareSearchReq.Append("<ArrivalDateTime>" + Convert.ToString(FltDT.Rows[f]["arrdatelcc"]) + "</ArrivalDateTime>");
                                   LowFareSearchReq.Append("<ClassCode>" + Convert.ToString(FltDT.Rows[f]["AdtCabin"]) + "</ClassCode>");
                                   LowFareSearchReq.Append("<FrequentFlyerNumber></FrequentFlyerNumber>");
                                   LowFareSearchReq.Append("</item>");
                               }
                               LowFareSearchReq.Append("</Segment>");

                               LowFareSearchReq.Append("</item>");
                           }
                       }
                       #endregion

                       LowFareSearchReq.Append("</Passengerlist>");
                       LowFareSearchReq.Append("</Item>");
                       LowFareSearchReq.Append("</Bookingdetails>");
                       LowFareSearchReq.Append("</BookReservation>]]>");
                       LowFareSearchReq.Append("</api:strBookRequest>");
                       LowFareSearchReq.Append("</api:BookReservation>");
                       LowFareSearchReq.Append("</soap:Body>");
                       LowFareSearchReq.Append("</soap:Envelope>");
                       string OrgDest = Convert.ToString(FltDT.Rows[0]["DepAirportCode"]) + "_" + Convert.ToString(FltDT.Rows[0]["ArrAirportCode"]);
                       strRes = TravarenaUtility.GetResponse("http://testapi.travarena.com/travarenaws/rws.asmx", LowFareSearchReq.ToString());
                       //strRes = TravarenaUtility.GetResponse("http://api.travarena.com/travarenaWS/rws.asmx", LowFareSearchReq.ToString());
                       TravarenaUtility.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "BookingRequest_" + OrgDest + "_Req");
                       TravarenaUtility.SaveXml(strRes, SecurityGUID, "BookingRequest_" + OrgDest + "_Res");
                       if (strRes.Contains("Error") == false)
                       {
                           XNamespace soap = "http://www.w3.org/2003/05/soap-envelope";
                           XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                           XNamespace xsd = "http://www.w3.org/2001/XMLSchema";
                           XNamespace xmlns = "http://apiapps.riyawings.com/";
                           string xmlr = "";

                           xmlr = strRes.Replace("xmlns=\"http://tempuri.org/\"", string.Empty);
                           xmlr = strRes.Replace("xmlns=\"http://apiapps.riyawings.com/\"", string.Empty);

                           //  xmlr = responseXml.Replace("xmlns=\"http://tempuri.org/\"", string.Empty);
                           XDocument xd = XDocument.Parse(xmlr);
                           var flts = xd.Descendants(soap + "Body").Descendants("BookReservationResult").ToArray();
                           string xmlrs = flts[0].ToString().Replace("&lt;", "<")
                                                              .Replace("&amp;", "&")
                                                              .Replace("&gt;", ">")
                                                              .Replace("&quot;", "\"")
                                                              .Replace("&apos;", "'");
                           xmlrs = xmlrs.Replace("<?xml", "<xml");
                           xmlrs = xmlrs.Replace("?>", "/>");
                           XDocument xd2 = XDocument.Parse(xmlrs);

                           try
                           {
                               foreach (var M in xd2.Descendants("ItinearyDetails").First().Element("Item").Element("TravellerInfo").Elements("Item"))
                               {
                                   airlinpnr = Convert.ToString(M.Element("SegmentInformation").Element("Item").Element("AirlinePNR").Value);
                                   PNR = xd2.Descendants("ItinearyDetails").Elements("Item").First().Element("RiyaPNR").Value;
                                   bookingID = xd2.Descendants("ItinearyDetails").Elements("Item").First().Element("BookingTrackId").Value;

                                   string tkt = Convert.ToString(M.Element("SegmentInformation").Element("Item").Element("TicketNo").Value);
                                   string fname = "";
                                   fname = Convert.ToString(M.Element("FirstName").Value) + Convert.ToString(M.Element("Title").Value) + Convert.ToString(M.Element("LastName").Value) + "/" + tkt;
                                   TktNoArray.Add(fname);
                               }
                           }
                           catch (Exception ex)
                           {
                               PNR = PNR + "-FQ";
                           }


                           //var Items = xd2.Descendants("ItinearyDetails").Descendants("Item").ToArray();
                           //foreach (var valPnr in Items)
                           //{
                           //    if (!string.IsNullOrEmpty(valPnr.Element("RiyaPNR").Value.ToString()))
                           //    {
                           //        PNR = valPnr.Element("RiyaPNR").Value.ToString();
                           //        Bkgid = valPnr.Element("BookingTrackId").Value.ToString();
                           //    }
                           //    else
                           //    {
                           //        PNR = PNR + "-FQ";
                           //    }

                           //}
                       }
                       else
                       {
                           PNR = PNR + "-FQ";
                       }
                       Log.Add("BookReq", LowFareSearchReq.ToString());
                       // MULTIUtitlity.SaveFile(LowFareSearchReq.ToString(), "BookReq");
                       Log.Add("BookResp", strRes);
                       #endregion
                   }
                   else {

                       goto FareMisMatch;
                   }

               }



                #endregion


            FareMisMatch:
                exep = exep + "Fare Amount Diffrence new fare:" + TotPrice + " old Fare:" + totfare.ToString();
           }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
                PNR = PNR + "-FQ";
            }
            finally
            {
                FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

                //objCommonBAL.InsertMULTIBookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
                //    , Log.ContainsKey("BookReq") == true ? Log["BookReq"] : "", Log.ContainsKey("BookResp") == true ? Log["BookResp"] : ""
                //    , Log.ContainsKey("RepriceReq") == true ? Log["RepriceReq"] : ""
                //    , Log.ContainsKey("RepriceResp") == true ? Log["RepriceResp"] : "", exep);
            }
            return airlinpnr;
        }
    }
}
