﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using STD.Shared;
using GoAirServiceDll; //GoAirServiceDll;
using System.Data;

namespace STD.BAL
{
    public class GoAirBookFlightBAL
    {
        RadixxBooking booking = new RadixxBooking();
        public string CorporateID { get; set; }
        public string UserID { get; set; }
        public string UserPass { get; set; }
        public GoAirBookFlightBAL(string CorporateId, string UserId, string userPass)
        {
            CorporateID = CorporateId;// "DEL83021";
            UserID = UserId;// "springtest";
            UserPass = userPass;// "P@ssword1";          

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public DataTable BookReservation(GoAirBookFlight b)
        {
            DataTable dt = new DataTable();
            string summaryXML;
            decimal amount;
            string fareIds = "";
            try
            {
                List<GoAirSegment> seg = b.segment;
                for (int i = 0; i < b.segment.Count; i++)
                {
                    if (i == 0)
                    {
                        fareIds += b.segment[i].FareInformationID.ToString();
                    }
                    else
                    {
                        fareIds += ", " + b.segment[i].FareInformationID.ToString();
                    }
                }


                RadixxTravelAgents agent = new RadixxTravelAgents();
                if (agent.LoginTravelAgencyUser(b.SecurityGUID, CorporateID, UserID, UserPass))
                {
                    string bookReq = GetBookingRequestCriteria(b);
                    booking.GetApplicableTransactionFeesExtendedXML(b.SecurityGUID, "INR");
                    summaryXML = booking.GetSummary(b.SecurityGUID, bookReq);
                    amount = GetPaymentAmountFromSummary(summaryXML);
                    if (amount == b.paymentDetails.PaymentAmount)
                    {
                        string bookcommitXML = booking.CommitSummary(b.SecurityGUID);
                        Dictionary<string, string> seriesDic = GetSeriesNo(bookcommitXML);
                        b.paymentDetails.VoucherNum = seriesDic["voucherno"];
                        string paymentXmlReq = AddPaymentXML(b);
                        string bookXmlWithPayment = booking.ResAddPayment(b.SecurityGUID, seriesDic["serialno"], seriesDic["confirmno"], paymentXmlReq);
                        string SaveDBookingXML = booking.ResSave(b.SecurityGUID, seriesDic["serialno"], seriesDic["confirmno"]);
                        dt = GetBookResponse(SaveDBookingXML, bookReq, bookcommitXML, paymentXmlReq, bookXmlWithPayment, fareIds, amount.ToString());
                    }
                }

                //return booking.BookReservation(b.SecurityGUID, GetBookingRequestCriteria(b));
            }
            catch (Exception)
            {

            }
            return dt;
        }

        private DataTable GetBookResponse(string BookingResponseXml, string BookReq, string bookcommitXML, string AddPayReq, string AddPayRes, string fareIds, string totalAmount)
        {
            GoairDB Gdb = new GoairDB();
            DataTable dt = new DataTable();
            dt = Gdb.CreateSaveDataTable();

            XDocument xmldoc = XDocument.Parse(BookingResponseXml);


            string serialNo = xmldoc.Element("ReservationBO").Element("Reservation").Element("SeriesNumber").Value;
            string ConfirmNo = xmldoc.Element("ReservationBO").Element("Reservation").Element("ConfirmationNumber").Value;
            string BookingAgent = xmldoc.Element("ReservationBO").Element("Reservation").Element("BookingAgent").Value;
            string BookDate = xmldoc.Element("ReservationBO").Element("Reservation").Element("BookDate").Value;
            decimal ResBal = Convert.ToDecimal(xmldoc.Element("ReservationBO").Element("Reservation").Element("ReservationBalance").Value);
            string cabin = xmldoc.Element("ReservationBO").Element("Reservation").Element("Cabin").Value;
            try
            {


                var Rs = from rr in xmldoc.Element("ReservationBO").Elements("Reservation")//.Element("Airlines").Element("Logical_Flights").Elements("Logical_Flight")//.Element("Physical_Flights").Element("Physical_Flight").Element("Customers").Element("Customer").Element("AirLine_Persons").Element("AirLine_Person").Element("SeatAssignments").Element("Charges").Element("Charge").Element("VoucherNumber");
                         select new
                            {
                                RSC = from rsc in rr.Element("ReservationContacts").Elements("ReservationContact")
                                      select new
                                      {
                                          FirstName = (string)rsc.Element("FirstName"),
                                          LastName = (string)rsc.Element("LastName"),
                                          MiddleName = (string)rsc.Element("MiddleName"),
                                          Age = (string)rsc.Element("Age"),
                                          Title = (string)rsc.Element("Title")

                                      },

                                LF = from lf in rr.Element("Airlines").Element("Logical_Flights").Elements("Logical_Flight")
                                     select new
                                     {
                                         Origin = (string)lf.Element("Origin"),
                                         Destination = (string)lf.Element("Destination"),
                                         DepartureTime = (string)lf.Element("DepartureTime"),
                                         Arrivaltime = (string)lf.Element("Arrivaltime")
                                     }
                            };


                string contactinfo = "<Contacts>";
                string origins = "";
                string destinations = "";
                string arrivalTime = "";
                string departureTime = "";
                foreach (var rs in Rs)
                {
                    foreach (var rsc in rs.RSC)
                    {
                        contactinfo += "<Contact>";
                        contactinfo += "<Title>" + rsc.Title + "</Title>";
                        contactinfo += "<FirstName>" + rsc.FirstName + "</FirstName>";
                        contactinfo += "<MiddleName>" + rsc.MiddleName + "</MiddleName>";
                        contactinfo += "<LastName>" + rsc.LastName + "</LastName>";
                        contactinfo += "<Age>" + rsc.Age + "</Age>";
                        contactinfo += "</Contact>";

                    }
                    int count = 0;
                    foreach (var lf in rs.LF)
                    {
                        if (count == 0)
                        {
                            origins += lf.Origin;
                            destinations += lf.Destination;
                            arrivalTime += lf.Arrivaltime;
                            departureTime += lf.DepartureTime;
                        }
                        else
                        {
                            origins += " :: " + lf.Origin;
                            destinations += " :: " + lf.Destination;
                            arrivalTime += " :: " + lf.Arrivaltime;
                            departureTime += " :: " + lf.DepartureTime;
                        }
                        count++;

                    }
                }

                contactinfo += "</Contacts>";

                DataRow row = dt.NewRow();

                row["SerialNo"] = serialNo;
                row["ConfirmationNo"] = ConfirmNo;
                row["BookingAgent"] = BookingAgent;
                row["BookDate"] = BookDate;
                row["PassengerTitle"] = "";
                row["PassengerFName"] = contactinfo;
                row["PassengerLName"] = "";
                row["PassengerAge"] = "";
                row["Cabin"] = cabin;
                row["Origin"] = origins;
                row["Destination"] = destinations;
                row["Dept_Date"] = departureTime;
                row["Dept_Time"] = departureTime;
                row["Arr_Date"] = arrivalTime;
                row["Arr_Time"] = arrivalTime;
                row["ReservationBalance"] = ResBal;
                row["Total_Fare"] = totalAmount;
                row["Fare_ID"] = fareIds;
                if (ResBal != 0)
                {
                    row["Message"] = "Paid amount is not correct";
                }
                else
                {
                    row["Message"] = "";
                }

                row["BookReq"] = BookReq;
                row["BookRes"] = bookcommitXML;
                row["AddPayReq"] = AddPayReq;
                row["AddPayRes"] = AddPayRes;
                row["ConfirmPayRes"] = BookingResponseXml;
                dt.Rows.Add(row);

            }
            catch (Exception ex)
            {
                DataRow row1 = dt.NewRow();
                row1["SerialNo"] = serialNo != null ? serialNo : "";
                row1["ConfirmationNo"] = ConfirmNo != null ? ConfirmNo : ""; ;
                row1["BookingAgent"] = BookingAgent != null ? BookingAgent : ""; ;
                row1["BookDate"] = BookDate != null ? BookDate : ""; ;
                row1["PassengerTitle"] = "";
                row1["PassengerFName"] = "";
                row1["PassengerLName"] = "";
                row1["PassengerAge"] = "";
                row1["Cabin"] = cabin != null ? cabin : ""; ;
                row1["Origin"] = "";
                row1["Destination"] = "";
                row1["Dept_Date"] = "";
                row1["Dept_Time"] = "";
                row1["Arr_Date"] = "";
                row1["Arr_Time"] = "";
                row1["ReservationBalance"] = ResBal != null ? ResBal.ToString() : ""; ;
                row1["Total_Fare"] = totalAmount;
                row1["Fare_ID"] = fareIds;
                row1["Message"] = ex.Message;



                row1["BookReq"] = BookReq;
                row1["BookRes"] = bookcommitXML;
                row1["AddPayReq"] = AddPayReq;
                row1["AddPayRes"] = AddPayRes;
                row1["ConfirmPayRes"] = BookingResponseXml;
                dt.Rows.Add(row1);
            }
            //dt.AcceptChanges();
            return dt;
        }

        private Dictionary<string, string> GetSeriesNo(string bookingResponseXml)
        {
            XDocument xmldoc = XDocument.Parse(bookingResponseXml);
            string serialNo = xmldoc.Element("ReservationBO").Element("Reservation").Element("SeriesNumber").Value;
            string confirmNo = xmldoc.Element("ReservationBO").Element("Reservation").Element("ConfirmationNumber").Value;
            //  string voucherNo = xmldoc.Element("ReservationBO").Element("Reservation").Element("Airlines").Element("Logical_Flights").Element("Logical_Flight").Element("Physical_Flights").Element("Physical_Flight").Element("Customers").Element("Customer").Element("AirLine_Persons").Element("AirLine_Person").Element("Charges").Element("Charge").Element("VoucherNumber").Value;



            var Rs = from rr in xmldoc.Element("ReservationBO").Element("Reservation").Element("Airlines").Element("Logical_Flights").Elements("Logical_Flight")//.Element("Physical_Flights").Element("Physical_Flight").Element("Customers").Element("Customer").Element("AirLine_Persons").Element("AirLine_Person").Element("SeatAssignments").Element("Charges").Element("Charge").Element("VoucherNumber");
                     select new
                        {
                            PF = from pf in rr.Element("Physical_Flights").Elements("Physical_Flight")
                                 select new
                                 {
                                     Cust = from cus in pf.Element("Customers").Elements("Customer")
                                            select new
                                            {
                                                AP = from ap in cus.Element("AirLine_Persons").Elements("AirLine_Person")
                                                     select new
                                                     {
                                                         SA = from sa in ap.Element("Charges").Elements("Charge")
                                                              select new
                                                              {
                                                                  VchNo = (string)sa.Element("VoucherNumber").Value
                                                              }
                                                     }
                                            }
                                 }

                        };//.Element("Physical_Flights").Element("Physical_Flight").Element("Customers").Element("Customer").Element("AirLine_Persons").Element("AirLine_Person").Element("SeatAssignments").Element("Charges").Element("Charge").Element("VoucherNumber");




            string voucherNo = "";

            foreach (var rs in Rs)
            {
                foreach (var pf in rs.PF)
                {
                    foreach (var cust in pf.Cust)
                    {
                        foreach (var ap in cust.AP)
                        {
                            foreach (var sa in ap.SA)
                            {
                                voucherNo = sa.VchNo;
                            }
                        }
                    }
                }
            }

            Dictionary<string, string> sno = new Dictionary<string, string>();
            sno.Add("serialno", serialNo);
            sno.Add("confirmno", confirmNo);
            sno.Add("voucherno", voucherNo);

            return sno;
        }

        private string GetBookingRequestCriteria(GoAirBookFlight b)
        {

            XNamespace xmlns = "http://services.radixx/WSRadixx/RABookReqV3DS.xsd";
            XDocument xmldoc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"), new XElement(xmlns + "RABookReqV3DS",

                                  new XElement(xmlns + "Reservation",
                                      new XElement(xmlns + "Address", b.Address), new XElement(xmlns + "Address2", b.Address2),
                                      new XElement(xmlns + "City", b.City), new XElement(xmlns + "State", b.State), new XElement(xmlns + "Postal", b.Postal),
                                      new XElement(xmlns + "Country", b.Country), new XElement(xmlns + "HomePhone", b.ContactValue),
                                      new XElement(xmlns + "CarrierCurrency", b.CarrierCurrency), new XElement(xmlns + "DisplayCurrency", b.DisplayCurrency),
                                      new XElement(xmlns + "Email", b.Email), new XElement(xmlns + "Fax", b.Fax), new XElement(xmlns + "Mobile", b.Mobile),
                                      new XElement(xmlns + "IATANum", b.IATANum), new XElement(xmlns + "WebBookingID", b.WebBookingID),
                                      new XElement(xmlns + "PromoCode", b.PromoCode),
                                      new XElement(xmlns + "User", UserID),
                                      new XElement(xmlns + "ReceiptLanguageID", 1),
                                      new XElement(xmlns + "ProfileID", null),

                                      from seg in b.segment
                                      select
                                  new XElement(xmlns + "Segment",
                                   new XElement(xmlns + "FareInformationID", seg.FareInformationID),
                                   new XElement(xmlns + "MarketingCode", null)),


                                   from p in b.CustomerList
                                   select
                                 new XElement(xmlns + "Person",
                                 new XElement(xmlns + "LastName", p.LastName),
                                 new XElement(xmlns + "FirstName", p.FirstName),
                                 new XElement(xmlns + "Title", p.Title),
                                 new XElement(xmlns + "PassengerAge", p.PassengerAge),
                                 new XElement(xmlns + "PTCID", p.PTCID),
                                 new XElement(xmlns + "ContactType", p.ContactType),
                                 new XElement(xmlns + "ContactNum", p.ContactNum),
                                 new XElement(xmlns + "SSR", null)))));

            return xmldoc.ToString();


        }

        private string AddPaymentXML(GoAirBookFlight b)
        {

            XDocument xmldoc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"), new XElement("Payments",
                                        new XElement("Payment",
                                            new XElement("Address", b.Address), new XElement("Address2", b.Address2),
                                            new XElement("BaseAmount", b.paymentDetails.PaymentAmount),
                                            new XElement("BaseCurrency", b.paymentDetails.PaymentCurrency),
                //new XElement("CardHolder", b.paymentDetails.FirstName + " " + b.paymentDetails.LastName),
                //new XElement("CardNumber", b.paymentDetails.CardNum),
                //new XElement("CheckNumber", -1),
                                            new XElement("City", b.City),
                                            new XElement("CompanyName", b.paymentDetails.CompanyName),
                                            new XElement("Country", b.Country),
                                            new XElement("CurrencyPaid", b.paymentDetails.PaymentCurrency),
                //new XElement("CVCode", b.paymentDetails.CVCode),
                                            new XElement("DatePaid", b.paymentDetails.PaymentDate),
                                            new XElement("DocumentReceivedBy", null),
                // new XElement("ExpirationDate", b.paymentDetails.ExpirationDate),
                                            new XElement("ExchangeRate", b.paymentDetails.ExchangeRate),
                                            new XElement("ExchangeRateDate", b.paymentDetails.ExchangeRateDate),
                                            new XElement("FFNumber", 0),
                                            new XElement("FirstName", b.paymentDetails.FirstName),
                                            new XElement("LastName", b.paymentDetails.LastName),
                                            new XElement("PaymentComment", b.paymentDetails.PaymentComment),
                                            new XElement("PaymentAmount", b.paymentDetails.PaymentAmount),
                                            new XElement("PaymentMethod", "INVC"),
                                            new XElement("Postal", b.Postal),
                                            new XElement("Reference", "REF1"),
                                            new XElement("State", b.State),
                                            new XElement("TerminalID", 0),
                                            new XElement("UserData", null),
                                            new XElement("ValueCode", null),
                                            new XElement("VoucherNumber", b.paymentDetails.VoucherNum),
                                            new XElement("UserID", UserID),
                                            new XElement("IataNumber", CorporateID),
                                            new XElement("GcxID", null),
                                            new XElement("GcxOptOption", null),
                                            new XElement("OriginalCurrency", b.paymentDetails.PaymentCurrency),
                                            new XElement("OriginalAmount", b.paymentDetails.PaymentAmount))));


            return xmldoc.ToString();
        }

        private decimal GetPaymentAmountFromSummary(string BookingResponseXml)
        {
            XDocument xmldoc = XDocument.Parse(BookingResponseXml);
            decimal amount = Convert.ToDecimal(xmldoc.Element("ReservationBO").Element("Reservation").Element("ReservationBalance").Value);
            return amount;


        }

    }
}

