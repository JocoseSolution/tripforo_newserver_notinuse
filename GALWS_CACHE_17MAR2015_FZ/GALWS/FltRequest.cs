﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.Shared;
using System.Text;
using System.Data;
using System.Xml.Linq;

namespace STD.BAL
{
    /// <summary>
    /// creating request xml for 1G transactions
    /// </summary>
    public class FltRequest
    {
        public FlightSearch searchInputs { get; set; }
        public List<CredentialList> ProviderList;
        string pcc = "", url = "", Userid = "", Pwd = "", strCorporateID = "";
        string strVc = "", strTrip = "", strProvider = "";

        public FltRequest()
        { }
        public FltRequest(FlightSearch searchParam, List<CredentialList> CrsList)
        {
            searchInputs = searchParam;
            ProviderList = CrsList;
        }

        /// <summary>
        /// Creating xml request for FQSBB_25
        /// Domestic/International
        /// </summary>
        /// <param name="IsRoundTrip">True for RoundTrip , false for oneway</param>
        /// <returns></returns>
        public string GDS1G(bool IsRoundTrip, bool IsSplFare, DataTable PFCodeDt, bool isPrivateFare, List<SearchResultBlock> ResultBlock1GList, string DealCode, string AppliedOn, string ResultCount = "2")
        {
            StringBuilder XmlBuilder = new StringBuilder();
            if (searchInputs.Trip.ToString() == "I")
                XmlBuilder.Append(SoapStart("SSINT", ""));
            else
                XmlBuilder.Append(SoapStart("SS", ""));
            XmlBuilder.Append("<Request>");

            XmlBuilder.Append("<FareQuoteSuperBB_31 xmlns=''>");

            #region AirAvailMods
            if ((IsRoundTrip == false) && (IsSplFare == false))
            {
                XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, false, ResultBlock1GList));
            }
            else if ((IsRoundTrip == true) && (IsSplFare == false))
            {
                XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.RetDate, searchInputs.HidTxtArrCity, searchInputs.HidTxtDepCity, false, ResultBlock1GList));
            }
            else if ((IsRoundTrip == true) && (IsSplFare == true))
            {
                XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, false, ResultBlock1GList));
                XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.RetDate, searchInputs.HidTxtArrCity, searchInputs.HidTxtDepCity, false, ResultBlock1GList));
            }
            else if ((IsRoundTrip == false) && (IsSplFare == true) && searchInputs.TripType1 == "rdbMultiCity")
            {
                //Use For Multicity (IsRoundTrip == false) && (IsSplFare == true)
                XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, true, ResultBlock1GList));
                if (!string.IsNullOrEmpty(searchInputs.DepDate2) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity2) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity2))
                {
                    XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate2, searchInputs.HidTxtDepCity2, searchInputs.HidTxtArrCity2, true, ResultBlock1GList));
                }
                if (!string.IsNullOrEmpty(searchInputs.DepDate3) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity3) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity3))
                {
                    XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate3, searchInputs.HidTxtDepCity3, searchInputs.HidTxtArrCity3, true, ResultBlock1GList));
                }
                if (!string.IsNullOrEmpty(searchInputs.DepDate4) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity4) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity4))
                {
                    XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate4, searchInputs.HidTxtDepCity4, searchInputs.HidTxtArrCity4, true, ResultBlock1GList));
                }
                if (!string.IsNullOrEmpty(searchInputs.DepDate5) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity5) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity5))
                {
                    XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate5, searchInputs.HidTxtDepCity5, searchInputs.HidTxtArrCity6, true, ResultBlock1GList));
                }
                if (!string.IsNullOrEmpty(searchInputs.DepDate6) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity6) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity6))
                {
                    XmlBuilder.Append(AirAvailMods(searchInputs.HidTxtAirLine, searchInputs.Cabin, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.DepDate6, searchInputs.HidTxtDepCity6, searchInputs.HidTxtArrCity6, true, ResultBlock1GList));
                }

            }
            #endregion

            #region Fare Part(FQSBB)
            XmlBuilder.Append("<SuperBBMods>");
            XmlBuilder.Append("<PFInfo>");
            XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");

            #region Credentials with Private and Publish Fare Option
            XmlBuilder.Append("<PFAry>");
            string pubfrind = "Y", actrestric = "N";
            if (searchInputs.HidTxtAirLine.Length > 1)
            {
                XmlBuilder.Append("<PF>");
                XmlBuilder.Append("<StartODRange>00</StartODRange>");
                XmlBuilder.Append("<EndODRange>00</EndODRange>");
                XmlBuilder.Append("<CRS>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRS>");
                DataRow[] PCRow = { };


                if (isPrivateFare == true)//(PFCodeDt.Rows.Count > 0 && isPrivateFare == true)
                {
                    #region New Deal Code Modification -Devesh Date:17-01-2019
                    if (!string.IsNullOrEmpty(DealCode))
                    {
                        XmlBuilder.Append("<Acct>" + DealCode + "</Acct>");
                        pubfrind = "N"; actrestric = "Y";
                    }
                    else
                    {
                        XmlBuilder.Append("<Acct></Acct>");
                        //pubfrind = "N"; actrestric = "N";
                        pubfrind = "Y"; actrestric = "N";
                    }
                    #endregion

                    #region Old Deal Code   Comment Code Date: 17-01-2019
                    //PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "' and   AppliedOn <> 'BOOK' ");
                    //if (PCRow.Count() <= 0)
                    //{
                    //    PCRow = PFCodeDt.Select("AirCode='ALL'   and   AppliedOn <> 'BOOK'");
                    //}
                    //if (PCRow.Count() > 0)
                    //{
                    //    for (int i = 0; i < PCRow.Count(); i++)
                    //    {
                    //        if (!string.IsNullOrEmpty(PCRow[i]["D_T_Code"].ToString()))
                    //        {
                    //            XmlBuilder.Append("<Acct>" + PCRow[i]["D_T_Code"].ToString() + "</Acct>");
                    //            pubfrind = "N"; actrestric = "Y";
                    //        }
                    //        else
                    //        {
                    //            XmlBuilder.Append("<Acct></Acct>");
                    //            pubfrind = "N"; actrestric = "N";
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    XmlBuilder.Append("<Acct></Acct>");
                    //    pubfrind = "N"; actrestric = "N";
                    //}
                    #endregion
                }
                else
                {
                    XmlBuilder.Append("<Acct></Acct>");
                }

                XmlBuilder.Append("<Contract />");
                XmlBuilder.Append("<AirV />");
                if (searchInputs.Trip.ToString() == "I")
                    XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where Utility.Left(crd.Provider, 2) == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                else
                    XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");

                //if (PFCodeDt.Rows.Count > 0 && isPrivateFare == true)
                //{
                XmlBuilder.Append("<PublishedFaresInd>" + pubfrind + "</PublishedFaresInd>");
                XmlBuilder.Append("<AcctCodeRestrict>" + actrestric + "</AcctCodeRestrict>");
                //}
                //else
                //{
                //    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                //    XmlBuilder.Append("<AcctCodeRestrict>N</AcctCodeRestrict>");

                //}


                XmlBuilder.Append("<Type>A</Type>");
                XmlBuilder.Append("</PF>");
            }
            else
            {
                if (isPrivateFare == true)
                {
                    #region New Deal Code International Modification -Devesh Date:17-01-2019
                    if (!string.IsNullOrEmpty(DealCode))
                    {
                        XmlBuilder.Append("<PF>");
                        XmlBuilder.Append("<StartODRange>00</StartODRange>");
                        XmlBuilder.Append("<EndODRange>00</EndODRange>");
                        XmlBuilder.Append("<CRS>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRS>");
                        XmlBuilder.Append("<Acct>" + DealCode + "</Acct>");
                        XmlBuilder.Append("<Contract />");
                        XmlBuilder.Append("<AirV />");
                        if (searchInputs.Trip.ToString() == "I")
                            XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where Utility.Left(crd.Provider, 2) == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                        else
                            XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                        XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                        XmlBuilder.Append("<AcctCodeRestrict>Y</AcctCodeRestrict>");
                        XmlBuilder.Append("<Type>A</Type>");
                        XmlBuilder.Append("</PF>");
                    }
                    #endregion 

                    #region Old Deal Code International
                    //for (int i = 0; i < PFCodeDt.Rows.Count; i++)
                    //{
                    //    if (Convert.ToString(PFCodeDt.Rows[i]["AppliedOn"]) == "BOTH")
                    //    {
                    //        XmlBuilder.Append("<PF>");
                    //        XmlBuilder.Append("<StartODRange>00</StartODRange>");
                    //        XmlBuilder.Append("<EndODRange>00</EndODRange>");
                    //        XmlBuilder.Append("<CRS>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRS>");
                    //        XmlBuilder.Append("<Acct>" + PFCodeDt.Rows[i]["D_T_Code"].ToString() + "</Acct>");
                    //        XmlBuilder.Append("<Contract />");
                    //        XmlBuilder.Append("<AirV />");
                    //        if (searchInputs.Trip.ToString() == "I")
                    //            XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where Utility.Left(crd.Provider, 2) == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                    //        else
                    //            XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                    //        XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                    //        XmlBuilder.Append("<AcctCodeRestrict>Y</AcctCodeRestrict>");
                    //        XmlBuilder.Append("<Type>A</Type>");
                    //        XmlBuilder.Append("</PF>");
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    XmlBuilder.Append("<PF>");
                    XmlBuilder.Append("<StartODRange>00</StartODRange>");
                    XmlBuilder.Append("<EndODRange>00</EndODRange>");
                    XmlBuilder.Append("<CRS>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRS>");
                    XmlBuilder.Append("<Acct></Acct>");
                    XmlBuilder.Append("<Contract />");
                    XmlBuilder.Append("<AirV />");
                    if (searchInputs.Trip.ToString() == "I")
                        XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where Utility.Left(crd.Provider, 2) == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                    else
                        XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                    XmlBuilder.Append("<AcctCodeRestrict>N</AcctCodeRestrict>");
                    XmlBuilder.Append("<Type>A</Type>");
                    XmlBuilder.Append("</PF>");
                }
            }

            XmlBuilder.Append("</PFAry>");
            #endregion

            XmlBuilder.Append("</PFInfo>");

            #region Perticular class type fare
            XmlBuilder.Append("<ClassPrefs>");
            XmlBuilder.Append("<ODPairAry>");

            XmlBuilder.Append("<ODPair>");
            XmlBuilder.Append("<ODNum>1</ODNum>");
            XmlBuilder.Append("<ClassPref>" + searchInputs.Cabin + "</ClassPref>");
            XmlBuilder.Append("</ODPair>");
            if ((IsRoundTrip == true) && (IsSplFare == true))
            {
                XmlBuilder.Append("<ODPair>");
                XmlBuilder.Append("<ODNum>2</ODNum>");
                XmlBuilder.Append("<ClassPref>" + searchInputs.Cabin + "</ClassPref>");
                XmlBuilder.Append("</ODPair>");
            }
            XmlBuilder.Append("</ODPairAry>");
            XmlBuilder.Append("</ClassPrefs>");
            #endregion

            #region Pax Information (total pax<=7 for FQSBB Search)
            XmlBuilder.Append("<PassengerType>");
            XmlBuilder.Append("<PsgrAry>");
            if ((searchInputs.Adult + searchInputs.Child + searchInputs.Infant) > 9)
            {
                if (searchInputs.Adult > 0)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("</Psgr>");
                    }
                }
                if (searchInputs.Child > 0)
                {
                    for (int i = 4; i <= 6; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("</Psgr>");
                    }

                }
                if (searchInputs.Infant > 0)
                {
                    XmlBuilder.Append("<Psgr>");
                    XmlBuilder.Append("<LNameNum>7</LNameNum>");
                    XmlBuilder.Append("<PsgrNum>7</PsgrNum>");
                    XmlBuilder.Append("<AbsNameNum>7</AbsNameNum>");
                    XmlBuilder.Append("<PTC>INF</PTC>");
                    XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                    XmlBuilder.Append("</Psgr>");
                }
            }
            else
            {
                int j = 1;
                if (searchInputs.Adult > 0)
                {
                    for (int i = 1; i <= searchInputs.Adult; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (searchInputs.Child > 0)
                {
                    for (int i = 1; i <= searchInputs.Child; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (searchInputs.Infant > 0)
                {
                    for (int i = 1; i <= searchInputs.Infant; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
            }
            XmlBuilder.Append("</PsgrAry>");
            XmlBuilder.Append("</PassengerType>");
            #endregion
            #region GetQuoteInfo
            XmlBuilder.Append("<GenQuoteInfo>");
            //XmlBuilder.Append("<SellCity />");
            //XmlBuilder.Append("<TktCity />");
            //XmlBuilder.Append("<AltCurrency>INR</AltCurrency>");
            //XmlBuilder.Append("<EquivCurrency/>");
            //XmlBuilder.Append("<TkDt />");
            //XmlBuilder.Append("<BkDtOverride />");
            //XmlBuilder.Append("<EUROverride />");
            //XmlBuilder.Append("<LCUOverride />");
            XmlBuilder.Append("<TkType>E</TkType>");
            //XmlBuilder.Append("<AltCitiesRequired />");
            //XmlBuilder.Append("<AltDatesRequired />");
            //XmlBuilder.Append("<NetFaresOnly />");
            //XmlBuilder.Append("<TkAgncyPCC />");
            //XmlBuilder.Append("<RulesProcess />");
            XmlBuilder.Append("</GenQuoteInfo>");

            #endregion
            #region Optimize 1001
            XmlBuilder.Append("<Optimize>");
            XmlBuilder.Append("<RecType>1001</RecType>");
            //XmlBuilder.Append("<RecType>1750</RecType>");
            //XmlBuilder.Append("<RecType>1720</RecType>");
            XmlBuilder.Append("<KlrIDAry>");
            XmlBuilder.Append("<KlrID>AAFI</KlrID>");
            //XmlBuilder.Append("<KlrID>AABC</KlrID>");
            XmlBuilder.Append("<KlrID>AAB1</KlrID>");
            //XmlBuilder.Append("<KlrID>AAON</KlrID>");
            XmlBuilder.Append("</KlrIDAry>");
            XmlBuilder.Append("</Optimize>");
            #endregion

            #region Optimize 1425
            XmlBuilder.Append("<Optimize>");
            XmlBuilder.Append("<RecType>1425</RecType>");
            XmlBuilder.Append("<KlrIDAry>");
            XmlBuilder.Append("<KlrID>EROR</KlrID>");
            XmlBuilder.Append("<KlrID>GFGQ</KlrID>");
            //XmlBuilder.Append("<KlrID>GFXI</KlrID>");
            XmlBuilder.Append("<KlrID>GFQA</KlrID>");
            XmlBuilder.Append("<KlrID>GRFB</KlrID>");
            //XmlBuilder.Append("<KlrID>GROM</KlrID>");
            XmlBuilder.Append("<KlrID>GFFC</KlrID>");
            XmlBuilder.Append("<KlrID>GFMM</KlrID>");
            XmlBuilder.Append("<KlrID>GFPI</KlrID>");
            XmlBuilder.Append("<KlrID>GFRI</KlrID>");
            XmlBuilder.Append("<KlrID>GFRR</KlrID>");
            XmlBuilder.Append("<KlrID>GFSR</KlrID>");
            //XmlBuilder.Append("<KlrID>GFSU</KlrID>");
            //XmlBuilder.Append("<KlrID>GFTS</KlrID>");
            XmlBuilder.Append("<KlrID>GFPX</KlrID>");
            XmlBuilder.Append("<KlrID>GFIS</KlrID>");
            XmlBuilder.Append("</KlrIDAry>");
            XmlBuilder.Append("</Optimize>");
            #endregion

            #region Result Limit in case of Multicity
            if ((IsRoundTrip == false) && (IsSplFare == true) && searchInputs.TripType1 == "rdbMultiCity")
            {
                XmlBuilder.Append("<ShopServiceValues>");
                XmlBuilder.Append("<NumSolutions>" + ResultCount + "</NumSolutions>");
                XmlBuilder.Append("</ShopServiceValues>");
            }


            #endregion
            XmlBuilder.Append("</SuperBBMods>");
            #endregion

            XmlBuilder.Append("</FareQuoteSuperBB_31>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            //XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("<FareQuoteSuperBB_31>");
            XmlBuilder.Append("<AirAvail>");
            XmlBuilder.Append("<AvailFlt>");
            XmlBuilder.Append("<AirV/>");
            XmlBuilder.Append("<FltNum/>");
            //<!--OpSuf /-->
            XmlBuilder.Append("<StartDt/>");
            XmlBuilder.Append("<StartAirp/>");
            XmlBuilder.Append("<EndAirp/>");
            XmlBuilder.Append("<StartTm/>");
            XmlBuilder.Append("<EndTm/>");
            XmlBuilder.Append("<DayChg/>");
            XmlBuilder.Append("<Conx/>");
            XmlBuilder.Append("<AirpChg/>");
            XmlBuilder.Append("<Equip/>");
            //<!--Spare1 /-->
            XmlBuilder.Append("<NumStops/>");
            XmlBuilder.Append("<OpAirVInd/>");
            //<!--Perf /-->
            XmlBuilder.Append("<LinkSellAgrmnt/>");
            XmlBuilder.Append("<DispOption/>");
            XmlBuilder.Append("<InsideAvailOption/>");
            //<!--GenTrafRestriction /-->
            //<!--DaysOperates/-->
            XmlBuilder.Append("<JrnyTm/>");
            XmlBuilder.Append("<EndDt/>");
            XmlBuilder.Append("<OpAirV />");
            XmlBuilder.Append("<OpFltDesignator />");
            //<!--OpFltSuf /-->
            XmlBuilder.Append("<StartTerminal/>");
            XmlBuilder.Append("<EndTerminal />");
            XmlBuilder.Append("<FltTm/>");
            XmlBuilder.Append("<LSAInd/>");
            //<!--GalileoAirVInd/-->
            XmlBuilder.Append("<ETktEligibility/>");
            //<!--ScheduleLevelCarrier /-->
            XmlBuilder.Append("<FrstDwnlnStp />");
            XmlBuilder.Append("<LastDwnlnStp />");
            //<!--AvailSource /-->
            XmlBuilder.Append("<CodeShareSrc />");
            //<!--AdditionalSrvcs /-->
            XmlBuilder.Append("<OperationalAirV />");
            XmlBuilder.Append("</AvailFlt>");
            XmlBuilder.Append("<BICAvail>");
            XmlBuilder.Append("<BICStatusAry>");
            XmlBuilder.Append(" <_ xmlns=''/>");
            XmlBuilder.Append("</BICStatusAry>");
            XmlBuilder.Append("</BICAvail>");
            XmlBuilder.Append("</AirAvail>");
            XmlBuilder.Append("<FareInfo>");

            XmlBuilder.Append("<ItinSeg>");
            XmlBuilder.Append("<UniqueKey />");
            XmlBuilder.Append("<FltNum/>");
            XmlBuilder.Append("<SegType/>");
            XmlBuilder.Append("<BIC/>");
            XmlBuilder.Append("<AvailSource />");
            XmlBuilder.Append("<BookingCabinClsSeg/>");
            XmlBuilder.Append("</ItinSeg>");


            XmlBuilder.Append("<SegRelatedInfo>");
            XmlBuilder.Append("<UniqueKey />");
            XmlBuilder.Append("<QuoteNum/>");
            XmlBuilder.Append("<RelSegNum/>");
            XmlBuilder.Append("<FIC />");
            XmlBuilder.Append("<BagInfo />");
            XmlBuilder.Append("<FareCabinClsSeg/>");
            XmlBuilder.Append("</SegRelatedInfo>");
            XmlBuilder.Append("<RulesInfo>");
            XmlBuilder.Append("<_ xmlns=''/>");
            XmlBuilder.Append("</RulesInfo>");

            XmlBuilder.Append("<GenQuoteDetails>");
            XmlBuilder.Append("<UniqueKey />");
            XmlBuilder.Append("<QuoteNum />");
            XmlBuilder.Append("<QuoteType />");
            XmlBuilder.Append("<BaseFareCurrency />");
            XmlBuilder.Append("<BaseFareAmt />");
            XmlBuilder.Append("<EquivCurrency/>");
            XmlBuilder.Append("<EquivAmt/>");
            XmlBuilder.Append("<TotCurrency />");
            XmlBuilder.Append("<TotAmt />");
            XmlBuilder.Append("<TaxDataAry>");
            XmlBuilder.Append("<_ xmlns=''/>");
            XmlBuilder.Append("</TaxDataAry>");
            XmlBuilder.Append("</GenQuoteDetails>");

            XmlBuilder.Append("<EnhancedPrivateFare>");
            XmlBuilder.Append("<_ xmlns=''/>");
            XmlBuilder.Append("</EnhancedPrivateFare>");


            XmlBuilder.Append("<InfoMsg>");
            //<!--UniqueKey/>
            //<QuoteNum/-->
            XmlBuilder.Append("<MsgNum/>");
            //<!--AppNum/>
            //<MsgType/>
            //<Lang/-->
            XmlBuilder.Append("<Text/>");
            XmlBuilder.Append("</InfoMsg>");
            XmlBuilder.Append("<RsvnRules>");
            XmlBuilder.Append("<UniqueKey/>");
            XmlBuilder.Append("<PenFeeAry>");
            XmlBuilder.Append("<_ xmlns=''/>");
            XmlBuilder.Append("</PenFeeAry>");
            XmlBuilder.Append("</RsvnRules>");
            XmlBuilder.Append("<FareConstruction>");
            XmlBuilder.Append("<_ xmlns=''/>");
            XmlBuilder.Append("</FareConstruction>");
            XmlBuilder.Append("<FareBasisCodeSummary>");
            XmlBuilder.Append("<_ xmlns=''/>");
            XmlBuilder.Append("</FareBasisCodeSummary>");
            XmlBuilder.Append("<FlightItemRef>");
            XmlBuilder.Append("<_ xmlns=''/>");
            XmlBuilder.Append("</FlightItemRef>");
            XmlBuilder.Append("</FareInfo>");
            XmlBuilder.Append("</FareQuoteSuperBB_31>");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SS"));

            return XmlBuilder.ToString();
        }

        private string AirAvailMods(string Airline, string Cabin, int Adult, int Child, int Infant, string TravelDate, string Origin, string Destination, bool isMcity, List<SearchResultBlock> ResultBlock1GList)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<AirAvailMods>");

            #region Airline Exclude(E),Include(I),Perticular Airline(O)
            if (Airline != "")
            {
                //if (Utility.Right(Airline, 2) != "AI")
                //{
                XmlBuilder.Append("<AirVPrefInd>");
                XmlBuilder.Append("<AirVIncExcInd>O</AirVIncExcInd>");
                XmlBuilder.Append("<RelaxAirVPref>N</RelaxAirVPref>");
                XmlBuilder.Append("</AirVPrefInd>");
                XmlBuilder.Append("<AirVPrefs>");
                XmlBuilder.Append("<AirVAry>");
                XmlBuilder.Append("<AirVInfo>");
                XmlBuilder.Append("<AirV>" + Utility.Right(Airline, 2) + "</AirV>");
                XmlBuilder.Append("</AirVInfo>");
                XmlBuilder.Append("</AirVAry>");
                XmlBuilder.Append("</AirVPrefs>");
                //}
                //else
                //{
                //    XmlBuilder.Append("<AirVPrefInd>");
                //    XmlBuilder.Append("<AirVIncExcInd>E</AirVIncExcInd>");
                //    XmlBuilder.Append("<RelaxAirVPref>N</RelaxAirVPref>");
                //    XmlBuilder.Append("</AirVPrefInd>");
                //    XmlBuilder.Append("<AirVPrefs>");
                //    XmlBuilder.Append("<AirVAry>");
                //    XmlBuilder.Append("<AirVInfo>");
                //    XmlBuilder.Append("<AirV>" + Utility.Right(Airline, 2) + "</AirV>");
                //    XmlBuilder.Append("</AirVInfo>");
                //    XmlBuilder.Append("</AirVAry>");
                //    XmlBuilder.Append("</AirVPrefs>");
                //}

            }
            else if (Airline == "" && searchInputs.Trip.ToString() == "I")
            {
                XmlBuilder.Append("<AirVPrefInd>");
                XmlBuilder.Append("<AirVIncExcInd>E</AirVIncExcInd>");
                XmlBuilder.Append("<RelaxAirVPref>N</RelaxAirVPref>");
                XmlBuilder.Append("</AirVPrefInd>");
                XmlBuilder.Append("<AirVPrefs>");
                XmlBuilder.Append("<AirVAry>");
                try
                {
                    if (ResultBlock1GList.Count > 0)
                    {
                        //for (int i = 0; i <= ResultBlock1GList.Count-1; i++)
                        //{
                        //    XmlBuilder.Append("<AirVInfo>");
                        //    XmlBuilder.Append("<AirV>" + ResultBlock1GList[i].Airline + "</AirV>");
                        //    XmlBuilder.Append("</AirVInfo>");
                        //}
                        List<string> lstairname = ResultBlock1GList.Select(x => x.Airline).Distinct().ToList();
                        for (int i = 0; i <= lstairname.Count - 1; i++)
                        {
                            XmlBuilder.Append("<AirVInfo>");
                            XmlBuilder.Append("<AirV>" + lstairname[i] + "</AirV>");
                            XmlBuilder.Append("</AirVInfo>");
                        }
                    }
                }
                catch (Exception ex)
                { }
                //XmlBuilder.Append("<AirVInfo>");
                //XmlBuilder.Append("<AirV>FZ</AirV>");
                //XmlBuilder.Append("</AirVInfo>");
                //XmlBuilder.Append("<AirVInfo>");
                //XmlBuilder.Append("<AirV>PK</AirV>");
                //XmlBuilder.Append("</AirVInfo>");
                XmlBuilder.Append("<AirVInfo>");
                XmlBuilder.Append("<AirV>FZ</AirV>");
                XmlBuilder.Append("</AirVInfo>");
                XmlBuilder.Append("</AirVAry>");
                XmlBuilder.Append("</AirVPrefs>");
            }
            else if (Airline == "" && searchInputs.Trip.ToString() == "D")
            {
                XmlBuilder.Append("<AirVPrefInd>");
                XmlBuilder.Append("<AirVIncExcInd>O</AirVIncExcInd>");
                XmlBuilder.Append("<RelaxAirVPref>N</RelaxAirVPref>");
                XmlBuilder.Append("</AirVPrefInd>");
                XmlBuilder.Append("<AirVPrefs>");
                XmlBuilder.Append("<AirVAry>");
                XmlBuilder.Append("<AirVInfo>");
                XmlBuilder.Append("<AirV>9W</AirV>");
                XmlBuilder.Append("</AirVInfo>");
                XmlBuilder.Append("<AirVInfo>");
                XmlBuilder.Append("<AirV>S2</AirV>");
                XmlBuilder.Append("</AirVInfo>");
                XmlBuilder.Append("<AirVInfo>");
                XmlBuilder.Append("<AirV>AI</AirV>");
                XmlBuilder.Append("</AirVInfo>");
                XmlBuilder.Append("<AirVInfo>");
                XmlBuilder.Append("<AirV>UK</AirV>");
                XmlBuilder.Append("</AirVInfo>");
                XmlBuilder.Append("</AirVAry>");
                XmlBuilder.Append("</AirVPrefs>");
            }

            #endregion

            #region Availability(GenAvail)
            XmlBuilder.Append("<GenAvail>");
            if ((Adult + Child + Infant) > 7)
            { XmlBuilder.Append("<NumSeats>7</NumSeats>"); }
            else { XmlBuilder.Append("<NumSeats>" + (Adult + Child) + "</NumSeats>"); }
            XmlBuilder.Append("<Class>" + Cabin + "</Class>");
            XmlBuilder.Append("<StartDt>" + Utility.Right(TravelDate, 4) + Utility.Mid(TravelDate, 3, 2) + Utility.Left(TravelDate, 2) + "</StartDt>");
            XmlBuilder.Append("<StartPt>" + Utility.Left(Origin, 3) + "</StartPt>");
            XmlBuilder.Append("<EndPt>" + Utility.Left(Destination, 3) + "</EndPt>");
            //XmlBuilder.Append("<StartTm>1200</StartTm>");
            XmlBuilder.Append("<StartTm>0001</StartTm>");// Change By Devesh - accordingly to Nagender Mail 09-04-2018
            XmlBuilder.Append("<TmWndInd>D</TmWndInd>");
            XmlBuilder.Append("<StartTmWnd>0000</StartTmWnd>");
            XmlBuilder.Append("<EndTmWnd>2359</EndTmWnd>");
            XmlBuilder.Append("<JrnyTm />");
            XmlBuilder.Append("<FltTypeInd>E</FltTypeInd>");
            XmlBuilder.Append("<FltTypePref />");
            XmlBuilder.Append("<StartPtInd>B</StartPtInd>");
            XmlBuilder.Append("<EndPtInd>B</EndPtInd>");
            XmlBuilder.Append("<IgnoreTSPref>N</IgnoreTSPref>");
            //if (searchInputs.Trip.ToString() == "I")
            // XmlBuilder.Append("<MaxNumFlts>20</MaxNumFlts>");
            XmlBuilder.Append("<IncNonStopDirectsInd>Y</IncNonStopDirectsInd>");
            XmlBuilder.Append("<IncStopDirectsInd>Y</IncStopDirectsInd>");

            if (isMcity)
            {
                XmlBuilder.Append("<IncSingleOnlineConxInd>Y</IncSingleOnlineConxInd>");
                XmlBuilder.Append("<IncDoubleOnlineConxInd>N</IncDoubleOnlineConxInd>");
                XmlBuilder.Append("<IncTripleOnlineConxInd>N</IncTripleOnlineConxInd>");
            }
            else
            {
                XmlBuilder.Append("<IncSingleOnlineConxInd>Y</IncSingleOnlineConxInd>");
                XmlBuilder.Append("<IncDoubleOnlineConxInd>Y</IncDoubleOnlineConxInd>");
                XmlBuilder.Append("<IncTripleOnlineConxInd>Y</IncTripleOnlineConxInd>");
            }
            if (searchInputs.Trip.ToString() == "D")
            {
                XmlBuilder.Append("<IncSingleInterlineConxInd>N</IncSingleInterlineConxInd>");
                XmlBuilder.Append("<IncDoubleInterlineConxInd>N</IncDoubleInterlineConxInd>");
                XmlBuilder.Append("<IncTripleInterlineConxInd>N</IncTripleInterlineConxInd>");
            }
            else if (isMcity)
            {

                XmlBuilder.Append("<IncSingleInterlineConxInd>Y</IncSingleInterlineConxInd>");
                XmlBuilder.Append("<IncDoubleInterlineConxInd>N</IncDoubleInterlineConxInd>");
                XmlBuilder.Append("<IncTripleInterlineConxInd>N</IncTripleInterlineConxInd>");
            }
            else
            {
                XmlBuilder.Append("<IncSingleInterlineConxInd>Y</IncSingleInterlineConxInd>");
                XmlBuilder.Append("<IncDoubleInterlineConxInd>Y</IncDoubleInterlineConxInd>");
                XmlBuilder.Append("<IncTripleInterlineConxInd>Y</IncTripleInterlineConxInd>");
            }
            XmlBuilder.Append("<SeqInd />");
            XmlBuilder.Append("</GenAvail>");
            #endregion

            XmlBuilder.Append("</AirAvailMods>");
            return XmlBuilder.ToString();
        }

        private string SoapStart(string opt, string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
            XmlBuilder.Append("<soap:Body>");
            if (opt == "SS")
            {
                XmlBuilder.Append("<SubmitXml xmlns='http://webservices.galileo.com'>");
                if (!string.IsNullOrEmpty(strCorporateID))
                {
                    XmlBuilder.Append("<Profile>" + strCorporateID + "</Profile>");
                }
                else
                {
                    XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                }
                XmlBuilder.Append("<LDVOverride />");
            }
            else if (opt == "SSINT")
            {
                XmlBuilder.Append("<SubmitXml xmlns='http://webservices.galileo.com'>");
                //XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where Utility.Left(crd.Provider, 2) == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                if (!string.IsNullOrEmpty(strCorporateID))
                {
                    XmlBuilder.Append("<Profile>" + strCorporateID + "</Profile>");
                }
                else
                {
                    XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                }
                XmlBuilder.Append("<LDVOverride />");
            }
            else if (opt == "MS")
            {
                XmlBuilder.Append("<MultiSubmitXml xmlns='http://webservices.galileo.com'>");
                //XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                if (!string.IsNullOrEmpty(strCorporateID))
                {
                    XmlBuilder.Append("<Profile>" + strCorporateID + "</Profile>");
                }
                else
                {
                    XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                }
                XmlBuilder.Append("<LDVOverride />");
                XmlBuilder.Append("<Requests>");
            }
            else if (opt == "BS")
            {
                XmlBuilder.Append("<BeginSession xmlns='http://webservices.galileo.com'>");
                //XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                if (!string.IsNullOrEmpty(strCorporateID))
                {
                    XmlBuilder.Append("<Profile>" + strCorporateID + "</Profile>");
                }
                else
                {
                    XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                }
                XmlBuilder.Append("<LDVOverride />");
            }
            else if (opt == "SONS")
            {
                XmlBuilder.Append("<SubmitXmlOnSession xmlns='http://webservices.galileo.com'>");
                XmlBuilder.Append("<Token>" + Token + "</Token>");
                XmlBuilder.Append("<LDVOverride />");
            }
            else if (opt == "ES")
            {
                XmlBuilder.Append("<EndSession xmlns='http://webservices.galileo.com'>");
                //XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                if (!string.IsNullOrEmpty(strCorporateID))
                {
                    XmlBuilder.Append("<Profile>" + strCorporateID + "</Profile>");
                }
                else
                {
                    XmlBuilder.Append("<Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                }
                XmlBuilder.Append("<LDVOverride />");
            }
            else if (opt == "STT")
            {
                XmlBuilder.Append("<SubmitTerminalTransaction xmlns='http://webservices.galileo.com'>");
                XmlBuilder.Append("<Token>" + Token + "</Token>");

            }

            return XmlBuilder.ToString();
        }

        private string SoapEnd(string opt)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            if (opt == "SS")
            {
                XmlBuilder.Append("</SubmitXml>");
            }
            else if (opt == "MS")
            {
                XmlBuilder.Append("</Requests>");
                XmlBuilder.Append("</MultiSubmitXml>");
            }
            else if (opt == "BS")
            {
                XmlBuilder.Append("</BeginSession>");
            }
            else if (opt == "ES")
            {
                XmlBuilder.Append("</EndSession>");
            }
            else if (opt == "SONS")
            {
                XmlBuilder.Append("</SubmitXmlOnSession>");
            }
            else if (opt == "STT")
            {
                XmlBuilder.Append("</SubmitTerminalTransaction>");
            }
            XmlBuilder.Append("</soap:Body>");
            XmlBuilder.Append("</soap:Envelope>");
            return XmlBuilder.ToString();
        }

        /// <summary>
        /// Check fare request for selected Flight
        /// </summary>
        /// <param name="FltDs">flight details dataset (selectedflightdetails_gal table),on basis of track_id</param>
        /// <returns>string</returns>
        public string GDS1G_PNRBFMgmtFareCrossCheck(DataSet FltDs, bool WOCHD, bool CHDOnly, bool InfWIDiffFBC)
        {
            int Adt = 0, Chd = 0, Inf = 0;
            Adt = int.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
            Chd = int.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
            Inf = int.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
            StringBuilder XmlBuilder = new StringBuilder();

            XmlBuilder.Append(SoapStart("SS", ""));
            XmlBuilder.Append("<Request>");

            XmlBuilder.Append("<PNRBFManagement_33 xmlns=''>");

            #region PNRBFPrimaryBldChgMods
            XmlBuilder.Append("<PNRBFPrimaryBldChgMods>");
            XmlBuilder.Append("<ItemAry>");

            for (int i = 1; i <= (Adt + Chd + Inf); i++)
            {
                XmlBuilder.Append("<Item>");
                XmlBuilder.Append("<DataBlkInd>N</DataBlkInd>");
                XmlBuilder.Append("<NameQual>");
                XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                XmlBuilder.Append("<EditTypeIndAppliesTo/>");
                XmlBuilder.Append("<AddChgNameRmkQual>");
                if (i > (Adt + Chd))
                {
                    XmlBuilder.Append("<NameType>I</NameType>");
                }
                XmlBuilder.Append("<LNameID>0" + i.ToString() + "</LNameID>");
                XmlBuilder.Append("<LName>TEST</LName>");
                XmlBuilder.Append("<LNameRmk/>");
                XmlBuilder.Append("<NameTypeQual>");
                XmlBuilder.Append("<FNameAry>");
                XmlBuilder.Append("<FNameItem>");
                XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");

                if (i <= Adt)
                {
                    XmlBuilder.Append("<FName>TEST" + autoPaxName(i) + "MR</FName>");
                    XmlBuilder.Append("<FNameRmk/>");
                }
                else if ((i <= (Adt + Chd)) && (i > Adt))
                {
                    XmlBuilder.Append("<FName>TEST" + autoPaxName(i) + "MSTR</FName>");
                    XmlBuilder.Append("<FNameRmk>P-C09</FNameRmk>");
                }
                else if ((i <= (Adt + Chd + Inf)) && (i > (Adt + Chd)))
                {
                    XmlBuilder.Append("<FName>TEST" + autoPaxName(i) + "</FName>");
                    XmlBuilder.Append("<FNameRmk>03MAY12</FNameRmk>");
                }

                XmlBuilder.Append("</FNameItem>");
                XmlBuilder.Append("</FNameAry>");
                XmlBuilder.Append("</NameTypeQual>");
                XmlBuilder.Append("</AddChgNameRmkQual>");
                XmlBuilder.Append("</NameQual>");
                XmlBuilder.Append("</Item>");
            }

            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>E</DataBlkInd>");
            XmlBuilder.Append("<EndMarkQual>");
            XmlBuilder.Append("<EndMark>E</EndMark>");
            XmlBuilder.Append("</EndMarkQual>");
            XmlBuilder.Append("</Item>");
            XmlBuilder.Append("</ItemAry>");
            XmlBuilder.Append("</PNRBFPrimaryBldChgMods>");
            #endregion

            #region AirSegSellMods
            XmlBuilder.Append("<AirSegSellMods>");
            for (int i = 0; i <= FltDs.Tables[0].Rows.Count - 1; i++)
            {
                XmlBuilder.Append("<AirSegSell>");
                XmlBuilder.Append("<Vnd>" + FltDs.Tables[0].Rows[i]["MarketingCarrier"].ToString().Trim() + "</Vnd>");
                if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 4)
                    XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 3)
                    XmlBuilder.Append("<FltNum>0" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 2)
                    XmlBuilder.Append("<FltNum>00" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 1)
                    XmlBuilder.Append("<FltNum>000" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else
                    XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                // XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                XmlBuilder.Append("<Class>" + FltDs.Tables[0].Rows[i]["RBD"].ToString().Trim() + "</Class>");
                XmlBuilder.Append("<StartDt>" + FltDs.Tables[0].Rows[i]["DepartureDate"].ToString().Trim() + "</StartDt>");
                XmlBuilder.Append("<StartTm>" + FltDs.Tables[0].Rows[i]["DepartureTime"].ToString().Trim() + "</StartTm>");
                XmlBuilder.Append("<EndTm>" + FltDs.Tables[0].Rows[i]["ArrivalTime"].ToString().Trim() + "</EndTm>");
                XmlBuilder.Append("<NumPsgrs>" + (Adt + Chd) + "</NumPsgrs>");
                XmlBuilder.Append("<StartAirp>" + FltDs.Tables[0].Rows[i]["DepartureLocation"].ToString().Trim() + "</StartAirp>");
                XmlBuilder.Append("<EndAirp>" + FltDs.Tables[0].Rows[i]["ArrivalLocation"].ToString().Trim() + "</EndAirp>");
                XmlBuilder.Append("<Status>AK</Status>");
                XmlBuilder.Append("<AvailJrnyNum>" + FltDs.Tables[0].Rows[i]["Flight"].ToString().Trim() + "</AvailJrnyNum>");
                XmlBuilder.Append("<DtChg>00</DtChg>");
                XmlBuilder.Append("<AvailDispType>G</AvailDispType>");
                XmlBuilder.Append("<StopoverIgnoreInd/>");
                XmlBuilder.Append("<VSpec/>");
                XmlBuilder.Append("</AirSegSell>");

            }
            XmlBuilder.Append("</AirSegSellMods>");
            #endregion

            if (FltDs.Tables[0].Rows[0]["Searchvalue"].ToString().Trim() == "PROMOFARE")
            {
                int a = 1, c = 1, d = 1;
                int cnt = 0;
                if (Adt > 0)
                    cnt = cnt + 1;
                if (Chd > 0)
                    cnt = cnt + 1;
                if (Inf > 0)
                    cnt = cnt + 1;

                for (int i = 1; i <= cnt; i++)
                {
                    #region StorePriceMods
                    XmlBuilder.Append("<StorePriceMods>");

                    #region PsgrMods
                    XmlBuilder.Append("<PsgrMods>");
                    XmlBuilder.Append("<PsgrAry>");
                    if (i == 1)
                    {
                        for (a = 1; a <= Adt; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>ADT</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                            c++;
                            d++;
                        }
                    }
                    else if (i == 2 && Chd > 0)
                    {
                        for (a = c; a <= Adt + Chd; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>C09</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                            d++;
                        }
                    }
                    else if (i == 2 && Chd == 0 && Inf > 0)
                    {
                        for (a = c; a <= Adt + Inf; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>INF</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                        }
                    }
                    else if (i == 3 && Inf > 0)
                    {
                        for (a = d; a <= Adt + Chd + Inf; a++)
                        {

                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>INF</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                        }
                    }
                    XmlBuilder.Append("</PsgrAry>");
                    XmlBuilder.Append("</PsgrMods>");
                    #endregion

                    XmlBuilder.Append("<CommissionMod>");
                    XmlBuilder.Append("<Percent>" + FltDs.Tables[0].Rows[0]["IATAComm"].ToString() + "</Percent>");
                    XmlBuilder.Append("</CommissionMod>");

                    #region SegSelection
                    XmlBuilder.Append("<SegSelection>");
                    XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                    XmlBuilder.Append("<SegRangeAry>");
                    XmlBuilder.Append("<SegRange>");
                    XmlBuilder.Append("<StartSeg>01</StartSeg>");
                    XmlBuilder.Append("<EndSeg>01</EndSeg>");
                    XmlBuilder.Append("<FareType>B</FareType>");
                    if (i == 1)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["AdtFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 2 && Chd > 0)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["ChdFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 2 && Chd == 0 && Inf > 0)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["InfFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 3 && Inf > 0)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["InfFarebasis"].ToString().Trim() + "</FIC>");
                    XmlBuilder.Append("<PFQual>");
                    XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
                    XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                    XmlBuilder.Append("<Acct/>");
                    XmlBuilder.Append("<Contract/>");
                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                    XmlBuilder.Append("<Type>A</Type>");
                    XmlBuilder.Append("</PFQual>");
                    XmlBuilder.Append("</SegRange>");
                    XmlBuilder.Append("</SegRangeAry>");
                    XmlBuilder.Append("</SegSelection>");
                    #endregion

                    XmlBuilder.Append("</StorePriceMods>");
                    #endregion
                }
            }
            else
            {
                #region StorePriceMods
                XmlBuilder.Append("<StorePriceMods>");

                #region SegSelection
                XmlBuilder.Append("<SegSelection>");
                XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                XmlBuilder.Append("<SegRangeAry>");
                XmlBuilder.Append("<SegRange>");
                XmlBuilder.Append("<StartSeg>00</StartSeg>");
                XmlBuilder.Append("<EndSeg>00</EndSeg>");
                XmlBuilder.Append("<FareType>P</FareType>");
                XmlBuilder.Append("<PFQual>");
                XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
                XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                XmlBuilder.Append("<Acct/>");
                XmlBuilder.Append("<Contract/>");
                XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                XmlBuilder.Append("<Type>A</Type>");
                XmlBuilder.Append("</PFQual>");
                XmlBuilder.Append("</SegRange>");
                XmlBuilder.Append("</SegRangeAry>");
                XmlBuilder.Append("</SegSelection>");
                #endregion

                #region PassengerType
                XmlBuilder.Append("<PassengerType>");
                XmlBuilder.Append("<PsgrAry>");
                int count = 0;
                if ((WOCHD == true) && (CHDOnly == true))
                    count = Chd;
                //else if ((WOCHD == true) && (CHDOnly == false))
                //    count = Adt + Chd;
                else if ((WOCHD == true) && (CHDOnly == false))
                    count = Adt + Inf;
                else
                    count = Adt + Chd + Inf;
                if (InfWIDiffFBC == true)
                    count = count - Inf;

                for (int i = 1; i <= count; i++)
                {

                    XmlBuilder.Append("<Psgr>");
                    XmlBuilder.Append("<LNameNum>0" + i.ToString() + "</LNameNum>");
                    XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                    XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");
                    if (CHDOnly == true)
                    {
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>05</Age>");
                    }

                    else
                    {
                        if (i <= Adt)
                        {
                            XmlBuilder.Append("<PTC>ADT</PTC>");
                            XmlBuilder.Append("<Age/>");
                        }
                        else if ((i > Adt) && (i <= (Adt + Chd)))
                        {
                            XmlBuilder.Append("<PTC>CNN</PTC>");
                            XmlBuilder.Append("<Age>05</Age>");
                        }
                        else if ((i > (Adt + Chd)) && (i <= count))
                        {
                            XmlBuilder.Append("<PTC>INF</PTC>");
                            XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                            XmlBuilder.Append("<Age/>");
                        }
                    }
                    XmlBuilder.Append("</Psgr>");
                }

                XmlBuilder.Append("</PsgrAry>");
                XmlBuilder.Append("</PassengerType>");
                #endregion

                //XmlBuilder.Append("<CommissionMod>");
                //XmlBuilder.Append("<Percent>" + SlcFltDs.Tables[0].Rows[0]["IATAComm"].ToString() + "</Percent>");
                //XmlBuilder.Append("</CommissionMod>");

                XmlBuilder.Append("</StorePriceMods>");
                #endregion

                #region StorePriceMods In case of Inf Diff FBC
                if (InfWIDiffFBC == true)
                {
                    XmlBuilder.Append("<StorePriceMods>");
                    XmlBuilder.Append("<SegSelection>");
                    XmlBuilder.Append("<ReqAirVPFs>N</ReqAirVPFs>");
                    XmlBuilder.Append("<SegRangeAry>");
                    for (int i = 1; i <= FltDs.Tables[0].Rows.Count; i++)
                    {
                        XmlBuilder.Append("<SegRange>");
                        XmlBuilder.Append("<StartSeg>0" + i.ToString() + "</StartSeg>");
                        XmlBuilder.Append("<EndSeg>0" + i.ToString() + "</EndSeg>");
                        XmlBuilder.Append("<FareType>F</FareType>");
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[i - 1]["InfFarebasis"].ToString() + "</FIC>");
                        XmlBuilder.Append("</SegRange>");
                    }

                    XmlBuilder.Append("</SegRangeAry>");
                    XmlBuilder.Append("</SegSelection>");
                    XmlBuilder.Append("<PassengerType>");
                    XmlBuilder.Append("<PsgrAry>");
                    for (int i = count + 1; i <= (count + Inf); i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>0" + i.ToString() + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("</Psgr>");
                    }
                    XmlBuilder.Append("</PsgrAry>");
                    XmlBuilder.Append("</PassengerType>");
                    XmlBuilder.Append("</StorePriceMods>");
                }
                #endregion
            }
            XmlBuilder.Append("</PNRBFManagement_33>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SS"));

            return XmlBuilder.ToString();
        }
        public string GDS1G_PNRBFMgmtFareCrossCheck(DataSet FltDs)
        {
            int Adt = 0, Chd = 0, Inf = 0;
            Adt = int.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
            Chd = int.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
            Inf = int.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
            StringBuilder XmlBuilder = new StringBuilder();

            XmlBuilder.Append(SoapStart("SS", ""));
            XmlBuilder.Append("<Request>");

            XmlBuilder.Append("<PNRBFManagement_33 xmlns=''>");

            #region PNRBFPrimaryBldChgMods
            XmlBuilder.Append("<PNRBFPrimaryBldChgMods>");
            XmlBuilder.Append("<ItemAry>");

            for (int i = 1; i <= (Adt + Chd + Inf); i++)
            {
                XmlBuilder.Append("<Item>");
                XmlBuilder.Append("<DataBlkInd>N</DataBlkInd>");
                XmlBuilder.Append("<NameQual>");
                XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                XmlBuilder.Append("<EditTypeIndAppliesTo/>");
                XmlBuilder.Append("<AddChgNameRmkQual>");
                if (i > (Adt + Chd))
                {
                    XmlBuilder.Append("<NameType>I</NameType>");
                }
                XmlBuilder.Append("<LNameID>0" + i.ToString() + "</LNameID>");
                XmlBuilder.Append("<LName>TEST</LName>");
                XmlBuilder.Append("<LNameRmk/>");
                XmlBuilder.Append("<NameTypeQual>");
                XmlBuilder.Append("<FNameAry>");
                XmlBuilder.Append("<FNameItem>");
                XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");

                if (i <= Adt)
                {
                    XmlBuilder.Append("<FName>TEST" + autoPaxName(i) + "MR</FName>");
                    XmlBuilder.Append("<FNameRmk/>");
                }
                else if ((i <= (Adt + Chd)) && (i > Adt))
                {
                    XmlBuilder.Append("<FName>TEST" + autoPaxName(i) + "MSTR</FName>");
                    XmlBuilder.Append("<FNameRmk>P-C09</FNameRmk>");
                }
                else if ((i <= (Adt + Chd + Inf)) && (i > (Adt + Chd)))
                {
                    XmlBuilder.Append("<FName>TEST" + autoPaxName(i) + "</FName>");
                    XmlBuilder.Append("<FNameRmk>03MAY12</FNameRmk>");
                }

                XmlBuilder.Append("</FNameItem>");
                XmlBuilder.Append("</FNameAry>");
                XmlBuilder.Append("</NameTypeQual>");
                XmlBuilder.Append("</AddChgNameRmkQual>");
                XmlBuilder.Append("</NameQual>");
                XmlBuilder.Append("</Item>");
            }

            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>E</DataBlkInd>");
            XmlBuilder.Append("<EndMarkQual>");
            XmlBuilder.Append("<EndMark>E</EndMark>");
            XmlBuilder.Append("</EndMarkQual>");
            XmlBuilder.Append("</Item>");
            XmlBuilder.Append("</ItemAry>");
            XmlBuilder.Append("</PNRBFPrimaryBldChgMods>");
            #endregion

            #region AirSegSellMods
            XmlBuilder.Append("<AirSegSellMods>");
            for (int i = 0; i <= FltDs.Tables[0].Rows.Count - 1; i++)
            {
                XmlBuilder.Append("<AirSegSell>");
                XmlBuilder.Append("<Vnd>" + FltDs.Tables[0].Rows[i]["MarketingCarrier"].ToString().Trim() + "</Vnd>");
                if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 4)
                    XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 3)
                    XmlBuilder.Append("<FltNum>0" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 2)
                    XmlBuilder.Append("<FltNum>00" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 1)
                    XmlBuilder.Append("<FltNum>000" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else
                    XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                // XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                XmlBuilder.Append("<Class>" + FltDs.Tables[0].Rows[i]["RBD"].ToString().Trim() + "</Class>");
                XmlBuilder.Append("<StartDt>" + FltDs.Tables[0].Rows[i]["DepartureDate"].ToString().Trim() + "</StartDt>");
                XmlBuilder.Append("<StartTm>" + FltDs.Tables[0].Rows[i]["DepartureTime"].ToString().Trim() + "</StartTm>");
                XmlBuilder.Append("<EndTm>" + FltDs.Tables[0].Rows[i]["ArrivalTime"].ToString().Trim() + "</EndTm>");
                XmlBuilder.Append("<NumPsgrs>" + (Adt + Chd) + "</NumPsgrs>");
                XmlBuilder.Append("<StartAirp>" + FltDs.Tables[0].Rows[i]["DepartureLocation"].ToString().Trim() + "</StartAirp>");
                XmlBuilder.Append("<EndAirp>" + FltDs.Tables[0].Rows[i]["ArrivalLocation"].ToString().Trim() + "</EndAirp>");
                XmlBuilder.Append("<Status>AK</Status>");
                XmlBuilder.Append("<AvailJrnyNum>" + FltDs.Tables[0].Rows[i]["Flight"].ToString().Trim() + "</AvailJrnyNum>");
                XmlBuilder.Append("<DtChg>00</DtChg>");
                XmlBuilder.Append("<AvailDispType>G</AvailDispType>");
                XmlBuilder.Append("<StopoverIgnoreInd/>");
                XmlBuilder.Append("<VSpec/>");
                XmlBuilder.Append("</AirSegSell>");

            }
            XmlBuilder.Append("</AirSegSellMods>");
            #endregion

            if (FltDs.Tables[0].Rows[0]["Searchvalue"].ToString().Trim() == "PROMOFARE")
            {
                int a = 1, c = 1, d = 1;
                int cnt = 0;
                if (Adt > 0)
                    cnt = cnt + 1;
                if (Chd > 0)
                    cnt = cnt + 1;
                if (Inf > 0)
                    cnt = cnt + 1;

                for (int i = 1; i <= cnt; i++)
                {
                    #region StorePriceMods
                    XmlBuilder.Append("<StorePriceMods>");

                    #region PsgrMods
                    XmlBuilder.Append("<PsgrMods>");
                    XmlBuilder.Append("<PsgrAry>");
                    if (i == 1)
                    {
                        for (a = 1; a <= Adt; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>ADT</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                            c++;
                            d++;
                        }
                    }
                    else if (i == 2 && Chd > 0)
                    {
                        for (a = c; a <= Adt + Chd; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>C09</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                            d++;
                        }
                    }
                    else if (i == 2 && Chd == 0 && Inf > 0)
                    {
                        for (a = c; a <= Adt + Inf; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>INF</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                        }
                    }
                    else if (i == 3 && Inf > 0)
                    {
                        for (a = d; a <= Adt + Chd + Inf; a++)
                        {

                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>INF</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                        }
                    }
                    XmlBuilder.Append("</PsgrAry>");
                    XmlBuilder.Append("</PsgrMods>");
                    #endregion

                    XmlBuilder.Append("<CommissionMod>");
                    XmlBuilder.Append("<Percent>" + FltDs.Tables[0].Rows[0]["IATAComm"].ToString() + "</Percent>");
                    XmlBuilder.Append("</CommissionMod>");

                    #region SegSelection
                    XmlBuilder.Append("<SegSelection>");
                    XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                    XmlBuilder.Append("<SegRangeAry>");
                    XmlBuilder.Append("<SegRange>");
                    XmlBuilder.Append("<StartSeg>01</StartSeg>");
                    XmlBuilder.Append("<EndSeg>01</EndSeg>");
                    XmlBuilder.Append("<FareType>B</FareType>");
                    if (i == 1)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["AdtFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 2 && Chd > 0)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["ChdFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 2 && Chd == 0 && Inf > 0)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["InfFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 3 && Inf > 0)
                        XmlBuilder.Append("<FIC>" + FltDs.Tables[0].Rows[0]["InfFarebasis"].ToString().Trim() + "</FIC>");
                    XmlBuilder.Append("<PFQual>");
                    XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
                    XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                    XmlBuilder.Append("<Acct/>");
                    XmlBuilder.Append("<Contract/>");
                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                    XmlBuilder.Append("<Type>A</Type>");
                    XmlBuilder.Append("</PFQual>");
                    XmlBuilder.Append("</SegRange>");
                    XmlBuilder.Append("</SegRangeAry>");
                    XmlBuilder.Append("</SegSelection>");
                    #endregion

                    XmlBuilder.Append("</StorePriceMods>");
                    #endregion
                }
            }
            else
            {
                #region StorePriceMods
                XmlBuilder.Append("<StorePriceMods>");

                #region PassengerType
                XmlBuilder.Append("<PassengerType>");
                XmlBuilder.Append("<PsgrAry>");
                for (int i = 1; i <= (Adt + Chd + Inf); i++)
                {
                    XmlBuilder.Append("<Psgr>");
                    XmlBuilder.Append("<LNameNum>0" + i.ToString() + "</LNameNum>");
                    XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                    XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");
                    if (i <= Adt)
                    {
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("<Age/>");
                    }
                    else if ((i > Adt) && (i <= (Adt + Chd)))
                    {
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>05</Age>");

                    }
                    else if ((i > (Adt + Chd)) && (i <= (Adt + Chd + Inf)))
                    {
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("<Age/>");
                    }
                    XmlBuilder.Append("</Psgr>");
                }

                XmlBuilder.Append("</PsgrAry>");
                XmlBuilder.Append("</PassengerType>");
                #endregion

                #region SegSelection
                XmlBuilder.Append("<SegSelection>");
                XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                XmlBuilder.Append("<SegRangeAry>");
                XmlBuilder.Append("<SegRange>");
                XmlBuilder.Append("<StartSeg>00</StartSeg>");
                XmlBuilder.Append("<EndSeg>00</EndSeg>");
                XmlBuilder.Append("<FareType>P</FareType>");
                XmlBuilder.Append("<PFQual>");
                XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
                XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                XmlBuilder.Append("<Acct/>");
                XmlBuilder.Append("<Contract/>");
                XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                XmlBuilder.Append("<Type>A</Type>");
                XmlBuilder.Append("</PFQual>");
                XmlBuilder.Append("</SegRange>");
                XmlBuilder.Append("</SegRangeAry>");
                XmlBuilder.Append("</SegSelection>");
                #endregion

                XmlBuilder.Append("</StorePriceMods>");
                #endregion
            }
            XmlBuilder.Append("</PNRBFManagement_33>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SS"));

            return XmlBuilder.ToString();
        }

        private string autoPaxName(int i)
        {
            switch (i)
            {
                case 1: { return "A"; }
                case 2: { return "B"; }
                case 3: { return "C"; }
                case 4: { return "D"; }
                case 5: { return "E"; }
                case 6: { return "F"; }
                case 7: { return "G"; }
                case 8: { return "H"; }
                case 9: { return "I"; }
                default: { return ""; }
            }

        }
        /// <summary>
        /// Request For Fare Rule in 1G
        /// SOAPAction: "http://webservices.galileo.com/SubmitXmlOnSession"
        /// </summary>
        /// <param name="Adtfarerule">Adult RuleInfo from FQBBS Response(string)</param>
        /// <param name="Chdfarerule">Child RuleInfo from FQBBS Response(string)</param>
        /// <param name="Inffarerule">Infant RuleInfo from FQBBS Response(string)</param>
        /// <param name="adt">int</param>
        /// <param name="chd">int</param>
        /// <param name="inf">int</param>
        /// <param name="PCC">string</param>
        /// <returns>string</returns>
        public string FQMD_Request(string Adtfarerule, string Chdfarerule, string Inffarerule, int adt, int chd, int inf, string PCC)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("MS", ""));
            if (adt > 0)
            {
                string[] FR;
                FR = Utility.Split(Adtfarerule, "ADTFAR");
                for (int i = 0; i <= FR.Length - 2; i++)
                {
                    XmlBuilder.Append(FQMD_FullRules(FR[i].ToString(), PCC));
                }
            }
            if (chd > 0)
            {
                string[] FR;
                FR = Utility.Split(Chdfarerule, "CHDFAR");
                for (int i = 0; i <= FR.Length - 2; i++)
                {
                    XmlBuilder.Append(FQMD_FullRules(FR[i].ToString(), PCC));
                }
            }
            if (inf > 0)
            {
                string[] FR;
                FR = Utility.Split(Inffarerule, "INFFAR");
                for (int i = 0; i <= FR.Length - 2; i++)
                {
                    XmlBuilder.Append(FQMD_FullRules(FR[i].ToString(), PCC));
                }
            }
            XmlBuilder.Append(SoapEnd("MS"));
            return XmlBuilder.ToString();
        }


        public string FQFSSingleRequest(DataSet FltDs, string type)//, string url, string userId, string pwd, string origin, string dest, string journeyDate)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            try
            {

                int adt = 0, chd = 0, inf = 0;
                string AdtFBC = "", ChdFBC = "", InfFBC = "";
                adt = int.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                chd = int.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                inf = int.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
                AdtFBC = FltDs.Tables[0].Rows[0]["AdtRbd"].ToString().Trim();//AdtFarebasis
                ChdFBC = FltDs.Tables[0].Rows[0]["ChdRbd"].ToString().Trim();//ChdFarebasis
                InfFBC = FltDs.Tables[0].Rows[0]["InfRbd"].ToString().Trim();//InfFarebasis


                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:web='http://webservices.galileo.com'>");
                XmlBuilder.Append("<soap:Body>");


                if (FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim().ToUpper() == "I")
                {
                    XmlBuilder.Append("<web:SubmitXml>");
                    XmlBuilder.Append("<web:Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</web:Profile>");
                    // XmlBuilder.Append("<LDVOverride />");
                }
                else
                {
                    XmlBuilder.Append("<web:SubmitXml xmlns='http://webservices.galileo.com'>");
                    XmlBuilder.Append("<web:Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</web:Profile>");
                    // XmlBuilder.Append("<LDVOverride />");
                }


                //XmlBuilder.Append(SoapStart("MSINT", ""));
                //XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                //XmlBuilder.Append("<soap:Body>");
                // XmlBuilder.Append("<MultiSubmitXml>");
                // XmlBuilder.Append("<Requests xmlns=''>");

                #region Request
                XmlBuilder.Append("<web:Request>");
                // XmlBuilder.Append("<Transaction>");
                XmlBuilder.Append(" <FareQuoteFlightSpecific_23 xmlns=\"\">");
                XmlBuilder.Append(" <FlightSpecificBestBuyMods>");
                XmlBuilder.Append(" <SegInfo>");
                XmlBuilder.Append("<FltSegAry>");

                #region flight segment
                for (int i = 0; i < FltDs.Tables[0].Rows.Count; i++)
                {
                    XmlBuilder.Append("<FltSeg>");
                    XmlBuilder.Append(" <ClassPref>" + Convert.ToString(FltDs.Tables[0].Rows[i]["AdtCabin"]));
                    XmlBuilder.Append("</ClassPref>");
                    XmlBuilder.Append("<AirV>" + FltDs.Tables[0].Rows[i]["MarketingCarrier"].ToString().Trim() + "</AirV>");
                    XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                    XmlBuilder.Append(" <OpSuf />");
                    XmlBuilder.Append(" <Dt>" + FltDs.Tables[0].Rows[i]["DepartureDate"].ToString().Trim() + "</Dt>");
                    XmlBuilder.Append(" <StartAirp>" + FltDs.Tables[0].Rows[i]["DepartureLocation"].ToString().Trim() + "</StartAirp>");
                    XmlBuilder.Append(" <EndAirp>" + FltDs.Tables[0].Rows[i]["ArrivalLocation"].ToString().Trim() + "</EndAirp>");
                    XmlBuilder.Append("<StartTm>" + FltDs.Tables[0].Rows[i]["DepartureTime"].ToString().Trim() + "</StartTm>");
                    XmlBuilder.Append("<EndTm>" + FltDs.Tables[0].Rows[i]["ArrivalTime"].ToString().Trim() + "</EndTm>");
                    XmlBuilder.Append("<AsBookedBIC>" + FltDs.Tables[0].Rows[i]["AdtRbd"].ToString().Trim() + "</AsBookedBIC>");
                    XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
                    XmlBuilder.Append(" </FltSeg>");

                }
                #endregion

                XmlBuilder.Append("</FltSegAry>");
                XmlBuilder.Append("</SegInfo>");

                XmlBuilder.Append("<PassengerType>");
                XmlBuilder.Append("<PsgrAry>");
                #region  Pax details

                int j = 1;
                if (adt > 0)
                {
                    for (int i = 1; i <= adt; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("<Age />");
                        XmlBuilder.Append("<PricePTCOnly />");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (chd > 0)
                {
                    for (int i = 1; i <= chd; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("<PricePTCOnly />");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (inf > 0)
                {
                    for (int i = 1; i <= inf; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }

                #endregion
                XmlBuilder.Append("</PsgrAry>");

                XmlBuilder.Append("</PassengerType>");

                XmlBuilder.Append("<SegSelection>");
                XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                XmlBuilder.Append("<SegRangeAry>");
                XmlBuilder.Append("<SegRange>");
                XmlBuilder.Append("<StartSeg>00</StartSeg>");
                XmlBuilder.Append("<EndSeg>00</EndSeg>");
                XmlBuilder.Append("<FareType>P</FareType>");
                XmlBuilder.Append("<PFQual>");
                XmlBuilder.Append("<CRSInd>1G</CRSInd>");
                XmlBuilder.Append("<PCC>" + Convert.ToString(((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc) + "</PCC>");
                XmlBuilder.Append("<AirV />");
                //XmlBuilder.Append("<Acct />");
                try
                {
                    #region Promotional Code
                    if (Convert.ToString(FltDs.Tables[0].Rows[0]["Searchvalue"]).Length > 1)
                    {
                        XmlBuilder.Append("<Acct>" + Convert.ToString(FltDs.Tables[0].Rows[0]["Searchvalue"]) + "</Acct>");
                    }
                    else
                    {
                        STD.DAL.FltDeal objDeal = new STD.DAL.FltDeal(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                        DataTable PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim(), "", FltDs.Tables[0].Rows[0]["User_id"].ToString().Trim(), FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim(), FltDs.Tables[0].Rows[0]["Track_id"].ToString().Trim(), FltDs.Tables[0].Rows[0]["DepartureLocation"].ToString().Trim(), FltDs.Tables[0].Rows[0]["ArrivalLocation"].ToString().Trim(), "", "");
                        DataRow[] PCRow = { };
                        if (PFCodeDt.Rows.Count > 0 && type.ToUpper().Trim() == "BOOK")
                        {
                            PCRow = PFCodeDt.Select("AirCode='" + FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' and AppliedOn='BOOK'");
                            if (PCRow.Count() <= 0)
                            {
                                PCRow = PFCodeDt.Select("AirCode='ALL and AppliedOn='BOOK'");
                                //PCRow = PFCodeDt.Select("AirCode='" + FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' or AirCode='ALL'");
                            }
                            if (PCRow.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(PCRow[0]["D_T_Code"].ToString()))
                                {
                                    XmlBuilder.Append("<Acct>" + PCRow[0]["D_T_Code"].ToString() + "</Acct>");
                                    XmlBuilder.Append("<Contract />");
                                    XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                                }
                                else
                                {
                                    XmlBuilder.Append("<Acct/>");
                                    XmlBuilder.Append("<Contract />");
                                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                                }
                            }
                            else
                            {
                                XmlBuilder.Append("<Acct/>");
                                XmlBuilder.Append("<Contract />");
                                XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                            }
                        }
                        else
                        {
                            XmlBuilder.Append("<Acct/>");
                            XmlBuilder.Append("<Contract />");
                            XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                        }
                    }
                    #endregion
                }
                catch
                {
                    XmlBuilder.Append("<Acct/>");
                    XmlBuilder.Append("<Contract />");
                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                }

                XmlBuilder.Append("<Type />");
                XmlBuilder.Append("<PFTypeRestrict />");
                XmlBuilder.Append("<AcctCodeRestrict />");
                XmlBuilder.Append("<Spare1 />");
                XmlBuilder.Append("</PFQual>");
                XmlBuilder.Append("</SegRange>");
                XmlBuilder.Append("</SegRangeAry>");
                XmlBuilder.Append("</SegSelection>");

                XmlBuilder.Append("<GenQuoteInfo>");
                XmlBuilder.Append("<SellCity />");
                XmlBuilder.Append("<TktCity />");
                XmlBuilder.Append("<AltCurrency />");
                XmlBuilder.Append("<EquivCurrency />");
                XmlBuilder.Append("<TkDt />");
                XmlBuilder.Append("<BkDtOverride />");
                XmlBuilder.Append(" <EUROverride />");
                XmlBuilder.Append("<LCUOverride />");
                XmlBuilder.Append("<TkType>E</TkType>");
                XmlBuilder.Append("<AltCitiesRequired />");
                XmlBuilder.Append(" <AltDatesRequired />");
                XmlBuilder.Append("<NetFaresOnly />");
                XmlBuilder.Append("<TkAgncyPCC />");
                XmlBuilder.Append("<RulesProcess>Y</RulesProcess>");
                XmlBuilder.Append("<FareConstructionBld />");
                XmlBuilder.Append("<InhibitLinkStatus>N</InhibitLinkStatus>");
                XmlBuilder.Append("</GenQuoteInfo>");
                XmlBuilder.Append("</FlightSpecificBestBuyMods>");
                XmlBuilder.Append("</FareQuoteFlightSpecific_23>");
                // XmlBuilder.Append("</Transaction>");
                XmlBuilder.Append("</web:Request>");
                #endregion

                #region Filters


                XmlBuilder.Append("<web:Filter>");
                XmlBuilder.Append(" <_ xmlns='' />");
                XmlBuilder.Append(" </web:Filter>");
                #endregion
                //  XmlBuilder.Append("</Requests>");
                // XmlBuilder.Append(SoapEnd("SS"));
                XmlBuilder.Append("</web:SubmitXml>");
                XmlBuilder.Append("</soap:Body>");
                XmlBuilder.Append("</soap:Envelope>");
            }
            catch (Exception ex)
            {

            }

            return XmlBuilder.ToString();
        }

        public string FQFSSingleRequestNew(DataSet FltDs, string type)//, string url, string userId, string pwd, string origin, string dest, string journeyDate)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            try
            {

                int adt = 0, chd = 0, inf = 0;
                string AdtFBC = "", ChdFBC = "", InfFBC = "";
                adt = int.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                chd = int.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                inf = int.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
                AdtFBC = FltDs.Tables[0].Rows[0]["AdtRbd"].ToString().Trim();//AdtFarebasis
                ChdFBC = FltDs.Tables[0].Rows[0]["ChdRbd"].ToString().Trim();//ChdFarebasis
                InfFBC = FltDs.Tables[0].Rows[0]["InfRbd"].ToString().Trim();//InfFarebasis


                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:web='http://webservices.galileo.com'>");
                XmlBuilder.Append("<soap:Body>");


                if (FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim().ToUpper() == "I")
                {
                    XmlBuilder.Append("<web:SubmitXml>");
                    XmlBuilder.Append("<web:Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</web:Profile>");
                    // XmlBuilder.Append("<LDVOverride />");
                }
                else
                {
                    XmlBuilder.Append("<web:SubmitXml xmlns='http://webservices.galileo.com'>");
                    XmlBuilder.Append("<web:Profile>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</web:Profile>");
                    // XmlBuilder.Append("<LDVOverride />");
                }


                //XmlBuilder.Append(SoapStart("MSINT", ""));
                //XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                //XmlBuilder.Append("<soap:Body>");
                // XmlBuilder.Append("<MultiSubmitXml>");
                // XmlBuilder.Append("<Requests xmlns=''>");

                #region Request
                XmlBuilder.Append("<web:Request>");
                // XmlBuilder.Append("<Transaction>");
                XmlBuilder.Append(" <FareQuoteFlightSpecific_23 xmlns=\"\">");
                XmlBuilder.Append(" <FlightSpecificBestBuyMods>");
                XmlBuilder.Append(" <SegInfo>");
                XmlBuilder.Append("<FltSegAry>");

                #region flight segment
                for (int i = 0; i < FltDs.Tables[0].Rows.Count; i++)
                {
                    XmlBuilder.Append("<FltSeg>");
                    XmlBuilder.Append(" <ClassPref>" + Convert.ToString(FltDs.Tables[0].Rows[i]["AdtCabin"]));
                    XmlBuilder.Append("</ClassPref>");
                    XmlBuilder.Append("<AirV>" + FltDs.Tables[0].Rows[i]["MarketingCarrier"].ToString().Trim() + "</AirV>");
                    XmlBuilder.Append("<FltNum>" + FltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                    XmlBuilder.Append(" <OpSuf />");
                    XmlBuilder.Append(" <Dt>" + FltDs.Tables[0].Rows[i]["DepartureDate"].ToString().Trim() + "</Dt>");
                    XmlBuilder.Append(" <StartAirp>" + FltDs.Tables[0].Rows[i]["DepartureLocation"].ToString().Trim() + "</StartAirp>");
                    XmlBuilder.Append(" <EndAirp>" + FltDs.Tables[0].Rows[i]["ArrivalLocation"].ToString().Trim() + "</EndAirp>");
                    XmlBuilder.Append("<StartTm>" + FltDs.Tables[0].Rows[i]["DepartureTime"].ToString().Trim() + "</StartTm>");
                    XmlBuilder.Append("<EndTm>" + FltDs.Tables[0].Rows[i]["ArrivalTime"].ToString().Trim() + "</EndTm>");
                    // XmlBuilder.Append("<AsBookedBIC>" + FltDs.Tables[0].Rows[i]["AdtRbd"].ToString().Trim() + "</AsBookedBIC>");
                    // XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
                    try
                    {
                        if (FltDs.Tables[0].Rows[i]["DepartureDate"].ToString().Trim() == FltDs.Tables[0].Rows[i]["ArrivalDate"].ToString().Trim())
                        {
                            XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
                        }
                        else
                        {
                            XmlBuilder.Append("<DayChgInd>01</DayChgInd>");
                        }
                    }
                    catch (Exception ex)
                    {
                        XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
                    }
                    string conx = "";
                    try
                    {
                        if (FltDs.Tables[0].Rows.Count == 1)
                        { conx = "N"; }
                        else
                        {
                            if (i != FltDs.Tables[0].Rows.Count - 1 && Convert.ToString(FltDs.Tables[0].Rows[i]["Flight"]) == Convert.ToString(FltDs.Tables[0].Rows[i + 1]["Flight"]))
                                conx = "Y";
                            else
                                conx = "N";

                        }
                    }
                    catch (Exception ex)
                    {
                        conx = "N";
                    }

                    XmlBuilder.Append("<Conx>" + conx + "</Conx>");
                    XmlBuilder.Append(" </FltSeg>");
                }
                #endregion

                XmlBuilder.Append("</FltSegAry>");
                XmlBuilder.Append("</SegInfo>");

                XmlBuilder.Append("<PassengerType>");
                XmlBuilder.Append("<PsgrAry>");
                #region  Pax details

                int j = 1;
                if (adt > 0)
                {
                    for (int i = 1; i <= adt; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("<Age />");
                        XmlBuilder.Append("<PricePTCOnly />");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (chd > 0)
                {
                    for (int i = 1; i <= chd; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("<PricePTCOnly />");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (inf > 0)
                {
                    for (int i = 1; i <= inf; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }

                #endregion
                XmlBuilder.Append("</PsgrAry>");

                XmlBuilder.Append("</PassengerType>");

                XmlBuilder.Append("<SegSelection>");
                XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                XmlBuilder.Append("<SegRangeAry>");
                XmlBuilder.Append("<SegRange>");
                XmlBuilder.Append("<StartSeg>00</StartSeg>");
                XmlBuilder.Append("<EndSeg>00</EndSeg>");
                XmlBuilder.Append("<FareType>P</FareType>");
                XmlBuilder.Append("<PFQual>");
                XmlBuilder.Append("<CRSInd>1G</CRSInd>");
                XmlBuilder.Append("<PCC>" + Convert.ToString(((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc) + "</PCC>");
                XmlBuilder.Append("<AirV />");
                //XmlBuilder.Append("<Acct />");
                try
                {
                    #region Promotional Code
                    if (Convert.ToString(FltDs.Tables[0].Rows[0]["Searchvalue"]).Length > 1)
                    {
                        XmlBuilder.Append("<Acct>" + Convert.ToString(FltDs.Tables[0].Rows[0]["Searchvalue"]) + "</Acct>");
                    }
                    else
                    {
                        STD.DAL.FltDeal objDeal = new STD.DAL.FltDeal(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                        DataTable PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim(), "", FltDs.Tables[0].Rows[0]["User_id"].ToString().Trim(), FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim(), FltDs.Tables[0].Rows[0]["Track_id"].ToString().Trim(), FltDs.Tables[0].Rows[0]["DepartureLocation"].ToString().Trim(), FltDs.Tables[0].Rows[0]["ArrivalLocation"].ToString().Trim(), "", "");
                        DataRow[] PCRow = { };
                        if (PFCodeDt.Rows.Count > 0 && type.ToUpper().Trim() == "BOOK")
                        {
                            PCRow = PFCodeDt.Select("AirCode='" + FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' and AppliedOn='BOOK'");
                            if (PCRow.Count() <= 0)
                            {
                                PCRow = PFCodeDt.Select("AirCode='ALL and AppliedOn='BOOK'");
                                //PCRow = PFCodeDt.Select("AirCode='" + FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' or AirCode='ALL'");
                            }
                            if (PCRow.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(PCRow[0]["D_T_Code"].ToString()))
                                {
                                    XmlBuilder.Append("<Acct>" + PCRow[0]["D_T_Code"].ToString() + "</Acct>");
                                    XmlBuilder.Append("<Contract />");
                                    XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                                }
                                else
                                {
                                    XmlBuilder.Append("<Acct/>");
                                    XmlBuilder.Append("<Contract />");
                                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                                }
                            }
                            else
                            {
                                XmlBuilder.Append("<Acct/>");
                                XmlBuilder.Append("<Contract />");
                                XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                            }
                        }
                        else
                        {
                            XmlBuilder.Append("<Acct/>");
                            XmlBuilder.Append("<Contract />");
                            XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                        }
                    }
                    #endregion
                }
                catch
                {
                    XmlBuilder.Append("<Acct/>");
                    XmlBuilder.Append("<Contract />");
                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                }

                XmlBuilder.Append("<Type />");
                XmlBuilder.Append("<PFTypeRestrict />");
                XmlBuilder.Append("<AcctCodeRestrict />");
                XmlBuilder.Append("<Spare1 />");
                XmlBuilder.Append("</PFQual>");
                XmlBuilder.Append("</SegRange>");
                XmlBuilder.Append("</SegRangeAry>");
                XmlBuilder.Append("</SegSelection>");

                XmlBuilder.Append("<GenQuoteInfo>");
                XmlBuilder.Append("<SellCity />");
                XmlBuilder.Append("<TktCity />");
                XmlBuilder.Append("<AltCurrency />");
                XmlBuilder.Append("<EquivCurrency />");
                XmlBuilder.Append("<TkDt />");
                XmlBuilder.Append("<BkDtOverride />");
                XmlBuilder.Append(" <EUROverride />");
                XmlBuilder.Append("<LCUOverride />");
                XmlBuilder.Append("<TkType>E</TkType>");
                XmlBuilder.Append("<AltCitiesRequired />");
                XmlBuilder.Append(" <AltDatesRequired />");
                XmlBuilder.Append("<NetFaresOnly />");
                XmlBuilder.Append("<TkAgncyPCC />");
                XmlBuilder.Append("<RulesProcess>Y</RulesProcess>");
                XmlBuilder.Append("<FareConstructionBld />");
                XmlBuilder.Append("<InhibitLinkStatus>N</InhibitLinkStatus>");
                XmlBuilder.Append("</GenQuoteInfo>");
                XmlBuilder.Append("</FlightSpecificBestBuyMods>");
                XmlBuilder.Append("</FareQuoteFlightSpecific_23>");
                // XmlBuilder.Append("</Transaction>");
                XmlBuilder.Append("</web:Request>");
                #endregion

                #region Filters


                XmlBuilder.Append("<web:Filter>");
                XmlBuilder.Append(" <_ xmlns='' />");
                XmlBuilder.Append(" </web:Filter>");
                #endregion
                //  XmlBuilder.Append("</Requests>");
                // XmlBuilder.Append(SoapEnd("SS"));
                XmlBuilder.Append("</web:SubmitXml>");
                XmlBuilder.Append("</soap:Body>");
                XmlBuilder.Append("</soap:Envelope>");
            }
            catch (Exception ex)
            {

            }

            return XmlBuilder.ToString();
        }


        private string FQMD_FullRules(string fareRule, string PCC)
        {
            StringBuilder XmlBuilder = new StringBuilder();

            //XmlBuilder.Append(SoapStart());
            XmlBuilder.Append("<Request><Transaction>");
            XmlBuilder.Append("<FareQuoteMultiDisplay_19>"); //xmlns=''
            XmlBuilder.Append("<FareDisplayMods>");
            #region QueryHeader
            XmlBuilder.Append("<QueryHeader>");
            XmlBuilder.Append("<UniqueKey>0000</UniqueKey>");
            XmlBuilder.Append("<LangNum>00</LangNum>");
            XmlBuilder.Append("<Action>135</Action>");
            XmlBuilder.Append("<RetCRTOutput>N</RetCRTOutput>");
            XmlBuilder.Append("<NoMsg>N</NoMsg>");
            XmlBuilder.Append("<NoTrunc>Y</NoTrunc>");
            XmlBuilder.Append("<IMInd>N</IMInd>");
            XmlBuilder.Append("<FIPlus>N</FIPlus>");
            XmlBuilder.Append("<PEInd>N</PEInd>");
            XmlBuilder.Append("<NBInd>N</NBInd>");
            XmlBuilder.Append("<ActionOnlyInd>N</ActionOnlyInd>");
            XmlBuilder.Append("<TranslatePeriod>N</TranslatePeriod>");
            XmlBuilder.Append("<IntFrame1>Y</IntFrame1>");
            XmlBuilder.Append("<SmartParsed>N</SmartParsed>");
            XmlBuilder.Append("<PDCodes>N</PDCodes>");
            XmlBuilder.Append("<BkDtOverride>N</BkDtOverride>");
            XmlBuilder.Append("<HostUse25>N</HostUse25>");
            XmlBuilder.Append("<DefCurrency/>");
            XmlBuilder.Append("<PFPWInd>N</PFPWInd>");
            XmlBuilder.Append("<HostUse29>N</HostUse29>");
            XmlBuilder.Append("<HostUse30>N</HostUse30>");
            XmlBuilder.Append("<HostUse31>N</HostUse31>");
            XmlBuilder.Append("<HostUse33/>");
            XmlBuilder.Append("</QueryHeader>");
            #endregion
            //XmlBuilder.Append("<PFMods>");
            //XmlBuilder.Append("<PCC>" + PCC + "</PCC>");
            //XmlBuilder.Append("</PFMods>");
            #region FollowUpEntries
            XmlBuilder.Append("<FollowUpEntries>");
            XmlBuilder.Append("<UniqueKey>0000</UniqueKey>");
            XmlBuilder.Append("<QuoteNum>001</QuoteNum>");
            XmlBuilder.Append("<Spare1>N</Spare1>");
            XmlBuilder.Append("<AllParaReqind>Y</AllParaReqind>");
            XmlBuilder.Append("<SumRuleReqInd>N</SumRuleReqInd>");
            XmlBuilder.Append("<FulltextoptInd>N</FulltextoptInd>");
            XmlBuilder.Append("<Spare2>NNNN</Spare2>");
            XmlBuilder.Append("<Text/>");
            XmlBuilder.Append("</FollowUpEntries>");
            #endregion
            #region GenInfo
            XmlBuilder.Append("<GenInfo>");
            XmlBuilder.Append("<UniqueKey>0000</UniqueKey>");
            XmlBuilder.Append("<TkCity/>");
            XmlBuilder.Append("<QuoteDt/>");
            XmlBuilder.Append("<QuoteCity/>");
            XmlBuilder.Append("<Currency/>");
            XmlBuilder.Append("<RsvnDt>" + DateTime.Now.Date.ToString("yyyyMMdd") + "</RsvnDt>");
            XmlBuilder.Append("<RsvnTm>0000</RsvnTm>");
            XmlBuilder.Append("<PlatingCarrier/>");
            XmlBuilder.Append("<BusinessOfferedSeg1>N</BusinessOfferedSeg1>");
            XmlBuilder.Append("<BusinessOfferedSeg2>N</BusinessOfferedSeg2>");
            XmlBuilder.Append("<BusinessOfferedSeg3>N</BusinessOfferedSeg3>");
            XmlBuilder.Append("<BusinessOfferedSeg4>N</BusinessOfferedSeg4>");
            XmlBuilder.Append("<BusinessOfferedSeg5>N</BusinessOfferedSeg5>");
            XmlBuilder.Append("<BusinessOfferedSeg6>N</BusinessOfferedSeg6>");
            XmlBuilder.Append("<BusinessOfferedSeg7>N</BusinessOfferedSeg7>");
            XmlBuilder.Append("<BusinessOfferedSeg8>N</BusinessOfferedSeg8>");
            XmlBuilder.Append("<BusinessOfferedSeg9>N</BusinessOfferedSeg9>");
            XmlBuilder.Append("<BusinessOfferedSeg10>N</BusinessOfferedSeg10>");
            XmlBuilder.Append("<BusinessOfferedSeg11>N</BusinessOfferedSeg11>");
            XmlBuilder.Append("<BusinessOfferedSeg12>N</BusinessOfferedSeg12>");
            XmlBuilder.Append("<BusinessOfferedSeg13>N</BusinessOfferedSeg13>");
            XmlBuilder.Append("<BusinessOfferedSeg14>N</BusinessOfferedSeg14>");
            XmlBuilder.Append("<BusinessOfferedSeg15>N</BusinessOfferedSeg15>");
            XmlBuilder.Append("<BusinessOfferedSeg16>N</BusinessOfferedSeg16>");
            XmlBuilder.Append("<SellCurrency/>");
            XmlBuilder.Append("<HostSysDt/>");
            XmlBuilder.Append("<TkGuaranteed>N</TkGuaranteed>");
            XmlBuilder.Append("<EUROverride>N</EUROverride>");
            XmlBuilder.Append("<LCUOverride>N</LCUOverride>");
            XmlBuilder.Append("<Spares1>NNNNN</Spares1>");
            XmlBuilder.Append("<Spares2>NNNNNNNNNNNNNNNNNNNNNNNN</Spares2>");
            XmlBuilder.Append("</GenInfo>");
            #endregion
            //RulesInfo Here
            XmlBuilder.Append(fareRule);
            //end

            XmlBuilder.Append("</FareDisplayMods>");
            XmlBuilder.Append("</FareQuoteMultiDisplay_19>");


            #region Filters
            XmlBuilder.Append("</Transaction><Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append("</Request>");
            // XmlBuilder.Append(SoapEnd());

            return XmlBuilder.ToString();
        }

        /// <summary>
        /// SOAPAction: "http://webservices.galileo.com/BeginSession"
        /// </summary>
        /// <returns></returns>
        public string BeginSessionReq()
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("BS", ""));
            XmlBuilder.Append(SoapEnd("BS"));
            return XmlBuilder.ToString();
        }

        /// <summary>
        /// SOAPAction: "http://webservices.galileo.com/EndSession"
        /// </summary>
        /// <param name="Token">string</param>
        /// <returns>string</returns>
        public string EndSessionReq(string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("ES", ""));
            XmlBuilder.Append("<Token>" + Token + "</Token>");
            XmlBuilder.Append(SoapEnd("ES"));
            return XmlBuilder.ToString();
        }

        /// <summary>
        /// Pnr creation request
        /// </summary>
        /// <param name="SlcFltDs">flight details dataset (selectedflightdetails_gal table),on basis of track_id</param>
        /// <param name="FltHdr">(dataset) FltHeader table details basis of orderid</param>
        /// <param name="FltPaxDs">pax dataset from FltPaxDetails Table basis of orderid</param>
        /// <param name="WOCHD">true/false</param>
        /// <param name="CHDOnly">true/false</param>
        /// <param name="InfWIDiffFBC">true/false</param>
        /// <returns>string</returns>
        public string PNRBFMgnt_CreatePnr(string Token, DataSet SlcFltDs, DataSet FltHdr, DataSet FltPaxDs, bool WOCHD, bool CHDOnly, bool InfWIDiffFBC, DataTable FOPDt, string strCode, bool PrivateFare)
        {
            int Adt = 0, Chd = 0, Inf = 0;
            Adt = int.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString());
            Chd = int.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString());
            Inf = int.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString());


            StringBuilder XmlBuilder = new StringBuilder();

            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");

            XmlBuilder.Append("<PNRBFManagement_33 xmlns=''>");
            XmlBuilder.Append(PNRBFPrimaryBldChgMods(FltPaxDs, Adt, Chd, Inf, WOCHD, CHDOnly));

            #region AirSegSellMods
            XmlBuilder.Append("<AirSegSellMods>");
            for (int i = 0; i <= SlcFltDs.Tables[0].Rows.Count - 1; i++)
            {
                XmlBuilder.Append("<AirSegSell>");
                XmlBuilder.Append("<Vnd>" + SlcFltDs.Tables[0].Rows[i]["MarketingCarrier"].ToString().Trim() + "</Vnd>");
                //if (SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length > 3)
                //    XmlBuilder.Append("<FltNum>" + SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                //else
                //    XmlBuilder.Append("<FltNum>0" + SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");

                if (SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 4)
                    XmlBuilder.Append("<FltNum>" + SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 3)
                    XmlBuilder.Append("<FltNum>0" + SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 2)
                    XmlBuilder.Append("<FltNum>00" + SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else if (SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim().Length == 1)
                    XmlBuilder.Append("<FltNum>000" + SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");
                else
                    XmlBuilder.Append("<FltNum>" + SlcFltDs.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim() + "</FltNum>");


                if (CHDOnly)
                    XmlBuilder.Append("<Class>" + SlcFltDs.Tables[0].Rows[i]["ChdRbd"].ToString().Trim() + "</Class>");
                else
                    XmlBuilder.Append("<Class>" + SlcFltDs.Tables[0].Rows[i]["AdtRbd"].ToString().Trim() + "</Class>");
                XmlBuilder.Append("<StartDt>" + SlcFltDs.Tables[0].Rows[i]["DepartureDate"].ToString().Trim() + "</StartDt>");
                XmlBuilder.Append("<StartTm>" + SlcFltDs.Tables[0].Rows[i]["DepartureTime"].ToString().Trim() + "</StartTm>");
                XmlBuilder.Append("<EndTm>" + SlcFltDs.Tables[0].Rows[i]["ArrivalTime"].ToString().Trim() + "</EndTm>");
                XmlBuilder.Append("<NumPsgrs>" + (Adt + Chd) + "</NumPsgrs>");
                XmlBuilder.Append("<StartAirp>" + SlcFltDs.Tables[0].Rows[i]["DepartureLocation"].ToString().Trim() + "</StartAirp>");
                XmlBuilder.Append("<EndAirp>" + SlcFltDs.Tables[0].Rows[i]["ArrivalLocation"].ToString().Trim() + "</EndAirp>");
                XmlBuilder.Append("<Status>NN</Status>");
                if (SlcFltDs.Tables[0].Rows.Count == 1)
                    XmlBuilder.Append("<AvailJrnyNum/>");
                else
                    XmlBuilder.Append("<AvailJrnyNum>" + SlcFltDs.Tables[0].Rows[i]["Flight"].ToString().Trim() + "</AvailJrnyNum>");

                XmlBuilder.Append("<DtChg>00</DtChg>");
                XmlBuilder.Append("<AvailDispType>G</AvailDispType>");
                XmlBuilder.Append("<StopoverIgnoreInd/>");
                XmlBuilder.Append("<VSpec/>");
                XmlBuilder.Append("</AirSegSell>");

            }
            XmlBuilder.Append("</AirSegSellMods>");
            #endregion

            #region PNRBFSecondaryBldChgMods
            XmlBuilder.Append("<PNRBFSecondaryBldChgMods>");
            XmlBuilder.Append("<ItemAry>");

            #region FOP
            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>F</DataBlkInd>");
            XmlBuilder.Append("<FOPQual>");
            XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
            XmlBuilder.Append("<AddChgQual>");
            try
            {
                StringBuilder XmlBuilder1 = new StringBuilder();
                if (FOPDt.Rows[0]["FOP"].ToString().Trim().ToUpper() == "CA")
                {
                    XmlBuilder1.Append("<TypeInd>1</TypeInd>");
                    XmlBuilder1.Append("<VarLenQual>");
                    XmlBuilder1.Append("<FOP>S</FOP>");
                    XmlBuilder1.Append("</VarLenQual>");
                }
                else if (FOPDt.Rows[0]["FOP"].ToString().Trim().ToUpper() == "CC")
                {
                    XmlBuilder1.Append("<TypeInd>2</TypeInd>");
                    XmlBuilder1.Append("<CCQual>");
                    XmlBuilder1.Append("<CC>" + FOPDt.Rows[0]["CardType"].ToString().Trim().ToUpper() + "</CC>");
                    XmlBuilder1.Append("<ExpDt>" + FOPDt.Rows[0]["CardExpDate"].ToString().Trim().ToUpper() + "</ExpDt>");
                    XmlBuilder1.Append("<ExtTxt/>");
                    XmlBuilder1.Append("<Acct>" + FOPDt.Rows[0]["CardNumber"].ToString().Trim().ToUpper() + "</Acct>");
                    XmlBuilder1.Append("</CCQual>");
                }
                XmlBuilder.Append(XmlBuilder1.ToString());
            }
            catch (Exception ex)
            {
                XmlBuilder.Append("<TypeInd>1</TypeInd>");
                XmlBuilder.Append("<VarLenQual>");
                XmlBuilder.Append("<FOP>S</FOP>");
                XmlBuilder.Append("</VarLenQual>");
            }


            XmlBuilder.Append("</AddChgQual>");
            XmlBuilder.Append("</FOPQual>");
            XmlBuilder.Append("</Item>");
            #endregion

            #region Add Mail Id
            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>R</DataBlkInd>");
            XmlBuilder.Append("<EmailQual>");
            XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
            XmlBuilder.Append("<AddQual>");
            XmlBuilder.Append("<LineNum>1</LineNum>");
            XmlBuilder.Append("<Type>T</Type>");
            XmlBuilder.Append("<EmailData>" + FltHdr.Tables[0].Rows[0]["PgEmail"].ToString() + "</EmailData>");
            XmlBuilder.Append("</AddQual>");
            XmlBuilder.Append("</EmailQual>");
            XmlBuilder.Append("</Item>");
            #endregion

            #region Add Meal Type
            int j = 0;
            for (int i = 0; i <= FltPaxDs.Tables[0].Rows.Count - 1; i++)
            {
                if ((FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() != "INF") && (FltPaxDs.Tables[0].Rows[i]["MealType"].ToString().Trim() != ""))
                {
                    if (CHDOnly == true)
                    {
                        j++;
                        if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "CHD")
                        {
                            if (FltPaxDs.Tables[0].Rows[i]["MealType"].ToString().Trim() != "")
                            {
                                XmlBuilder.Append("<Item>");
                                XmlBuilder.Append("<DataBlkInd>S</DataBlkInd>");
                                XmlBuilder.Append("<SSRQual>");
                                XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                                XmlBuilder.Append("<AddQual>");
                                XmlBuilder.Append("<SSRCode>" + FltPaxDs.Tables[0].Rows[i]["MealType"].ToString().Trim() + "</SSRCode>");
                                XmlBuilder.Append("<LNameNum>0" + j.ToString() + "</LNameNum>");
                                XmlBuilder.Append("<PsgrNum>0" + j.ToString() + "</PsgrNum>");
                                XmlBuilder.Append("<AbsNameNum>0" + j.ToString() + "</AbsNameNum>");
                                XmlBuilder.Append("</AddQual>");
                                XmlBuilder.Append("</SSRQual>");
                                XmlBuilder.Append("</Item>");
                            }
                        }
                    }
                    else
                    {
                        if ((FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "CHD") && (WOCHD == true))
                        { }
                        else
                        {
                            j++;
                            if (FltPaxDs.Tables[0].Rows[i]["MealType"].ToString().Trim() != "")
                            {
                                XmlBuilder.Append("<Item>");
                                XmlBuilder.Append("<DataBlkInd>S</DataBlkInd>");
                                XmlBuilder.Append("<SSRQual>");
                                XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                                XmlBuilder.Append("<AddQual>");
                                XmlBuilder.Append("<SSRCode>" + FltPaxDs.Tables[0].Rows[i]["MealType"].ToString().Trim() + "</SSRCode>");
                                XmlBuilder.Append("<LNameNum>0" + j.ToString() + "</LNameNum>");
                                XmlBuilder.Append("<PsgrNum>0" + j.ToString() + "</PsgrNum>");
                                XmlBuilder.Append("<AbsNameNum>0" + j.ToString() + "</AbsNameNum>");
                                XmlBuilder.Append("</AddQual>");
                                XmlBuilder.Append("</SSRQual>");
                                XmlBuilder.Append("</Item>");
                            }
                        }
                    }
                }

            }
            #endregion


            #region Add SSR Docs
            int jj = 0;
            if (Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Trip"]).Trim().ToUpper() == "I")
            {
                for (int i = 0; i <= FltPaxDs.Tables[0].Rows.Count - 1; i++)
                {
                    string[] dobArry = FltPaxDs.Tables[0].Rows[i]["DOB"].ToString().Split('/');

                    string dobb = DateTime.Parse(dobArry[2] + "-" + dobArry[1] + "-" + dobArry[0]).ToString("ddMMMyy");
                    string expp = "";
                    try
                    {

                        if (FltPaxDs.Tables[0].Rows[i]["PassportExpireDate"].ToString().Contains("/"))
                        {

                            string[] expArry = FltPaxDs.Tables[0].Rows[i]["PassportExpireDate"].ToString().Split('/');

                            expp = DateTime.Parse(expArry[2] + "-" + expArry[1] + "-" + expArry[0]).ToString("ddMMMyy");

                        }
                        else
                        {
                            expp = "";
                        }
                    }
                    catch
                    {
                        expp = "";
                    }

                    // if ((FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() != "INF"))
                    //{
                    if (CHDOnly == true)
                    {
                        jj++;
                        if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "CHD")
                        {
                            if (!string.IsNullOrEmpty(expp))
                            {
                                XmlBuilder.Append("<Item>");
                                XmlBuilder.Append("<DataBlkInd>S</DataBlkInd>");
                                XmlBuilder.Append("<SSRQual>");
                                XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                                XmlBuilder.Append("<AddQual>");
                                XmlBuilder.Append("<SSRCode>DOCS</SSRCode>");
                                XmlBuilder.Append("<AbsNameNum>0" + jj.ToString() + "</AbsNameNum>");
                                XmlBuilder.Append("<FltNum/>");
                                XmlBuilder.Append("<OpSuf/>");
                                XmlBuilder.Append("<AirV/>");
                                XmlBuilder.Append("<Dt/>");
                                XmlBuilder.Append("<BIC/>");
                                XmlBuilder.Append("<StartAirp/>");
                                XmlBuilder.Append("<EndAirp/>");
                                XmlBuilder.Append("<Text> P/" + FltPaxDs.Tables[0].Rows[i]["IssueCountryCode"].ToString() + "/" + FltPaxDs.Tables[0].Rows[i]["PassportNo"].ToString() + "/" + FltPaxDs.Tables[0].Rows[i]["NationalityCode"].ToString() + "/" + dobb + "/" + FltPaxDs.Tables[0].Rows[i]["Gender"].ToString() + "/" + expp + "/" + FltPaxDs.Tables[0].Rows[i]["LName"].ToString() + "/" + FltPaxDs.Tables[0].Rows[i]["FName"].ToString() + " </Text>");
                                XmlBuilder.Append("</AddQual>");
                                XmlBuilder.Append("</SSRQual>");
                                XmlBuilder.Append("</Item>");
                            }
                        }
                    }
                    else
                    {
                        if ((FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "CHD") && (WOCHD == true))
                        { }
                        else
                        {
                            jj++;
                            if (!string.IsNullOrEmpty(expp))
                            {
                                XmlBuilder.Append("<Item>");
                                XmlBuilder.Append("<DataBlkInd>S</DataBlkInd>");
                                XmlBuilder.Append("<SSRQual>");
                                XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                                XmlBuilder.Append("<AddQual>");
                                XmlBuilder.Append("<SSRCode>DOCS</SSRCode>");
                                XmlBuilder.Append("<AbsNameNum>0" + jj.ToString() + "</AbsNameNum>");
                                XmlBuilder.Append("<FltNum/>");
                                XmlBuilder.Append("<OpSuf/>");
                                XmlBuilder.Append("<AirV/>");
                                XmlBuilder.Append("<Dt/>");
                                XmlBuilder.Append("<BIC/>");
                                XmlBuilder.Append("<StartAirp/>");
                                XmlBuilder.Append("<EndAirp/>");
                                XmlBuilder.Append("<Text>P/" + (FltPaxDs.Tables[0].Rows[i]["IssueCountryCode"].ToString() + "/" + FltPaxDs.Tables[0].Rows[i]["PassportNo"].ToString() + "/" + FltPaxDs.Tables[0].Rows[i]["NationalityCode"].ToString() + "/" + dobb + "/" + FltPaxDs.Tables[0].Rows[i]["Gender"].ToString() + "/" + expp + "/" + FltPaxDs.Tables[0].Rows[i]["LName"].ToString() + "/" + FltPaxDs.Tables[0].Rows[i]["FName"].ToString()).ToUpper() + "</Text>");
                                XmlBuilder.Append("</AddQual>");
                                XmlBuilder.Append("</SSRQual>");
                                XmlBuilder.Append("</Item>");
                            }
                        }
                    }
                    //}

                }
            }
            #endregion

            #region Add GST Info
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(FltHdr.Tables[0].Rows[0]["GSTNO"])))
                {
                    int g = 0;
                    for (int i = 0; i <= FltPaxDs.Tables[0].Rows.Count - 1; i++)
                    {
                        g++;
                        //if ((FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() != "INF") )
                        //{
                        //    g++;                        
                        // GST Address

                        #region SET GST Info IN XML
                        XmlBuilder.Append("<Item>");
                        XmlBuilder.Append("<DataBlkInd>S </DataBlkInd>");
                        XmlBuilder.Append("<SSRQual>");
                        XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                        XmlBuilder.Append("<AddQual>");
                        XmlBuilder.Append("<SSRCode>GSTA</SSRCode>");
                        XmlBuilder.Append("<LNameNum>0" + g.ToString() + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>0" + g.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + g.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<AirV>" + Convert.ToString(FltHdr.Tables[0].Rows[0]["VC"]).ToUpper() + "</AirV>");
                        //XmlBuilder.Append("<Text>IND/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_Company_Address"]).ToUpper() + "-1" + FltPaxDs.Tables[0].Rows[0]["LName"].ToString().ToUpper() + "/" + FltPaxDs.Tables[0].Rows[0]["FName"].ToString().ToUpper() + FltPaxDs.Tables[0].Rows[0]["Title"].ToString().ToUpper() + "</Text>");
                        XmlBuilder.Append("<Text>IND/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_Company_Address"]).ToUpper() + "</Text>");
                        XmlBuilder.Append("</AddQual>");
                        XmlBuilder.Append("</SSRQual>");
                        XmlBuilder.Append("</Item>");

                        //GST NO
                        XmlBuilder.Append("<Item>");
                        XmlBuilder.Append("<DataBlkInd>S </DataBlkInd>");
                        XmlBuilder.Append("<SSRQual>");
                        XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                        XmlBuilder.Append("<AddQual>");
                        XmlBuilder.Append("<SSRCode>GSTN</SSRCode>");
                        XmlBuilder.Append("<LNameNum>0" + g.ToString() + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>0" + g.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + g.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<AirV>" + Convert.ToString(FltHdr.Tables[0].Rows[0]["VC"]).ToUpper() + "</AirV>");
                        //XmlBuilder.Append("<Text>IND/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GSTNO"]) + "/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_Company_Name"]).ToUpper() + "-1" + FltPaxDs.Tables[0].Rows[0]["LName"].ToString().ToUpper() + "/" + FltPaxDs.Tables[0].Rows[0]["FName"].ToString().ToUpper() + FltPaxDs.Tables[0].Rows[0]["Title"].ToString().ToUpper() + "</Text>");
                        XmlBuilder.Append("<Text>IND/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GSTNO"]) + "/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_Company_Name"]).ToUpper() + "</Text>");
                        XmlBuilder.Append("</AddQual>");
                        XmlBuilder.Append("</SSRQual>");
                        XmlBuilder.Append("</Item>");

                        //GST Phone no.
                        XmlBuilder.Append("<Item>");
                        XmlBuilder.Append("<DataBlkInd>S </DataBlkInd>");
                        XmlBuilder.Append("<SSRQual>");
                        XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                        XmlBuilder.Append("<AddQual>");
                        XmlBuilder.Append("<SSRCode>GSTP</SSRCode>");
                        XmlBuilder.Append("<LNameNum>0" + g.ToString() + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>0" + g.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + g.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<AirV>" + Convert.ToString(FltHdr.Tables[0].Rows[0]["VC"]).ToUpper() + "</AirV>");
                        //XmlBuilder.Append("<Text>IND/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_PhoneNo"]) + "-1" + FltPaxDs.Tables[0].Rows[0]["LName"].ToString().ToUpper() + "/" + FltPaxDs.Tables[0].Rows[0]["FName"].ToString().ToUpper() + FltPaxDs.Tables[0].Rows[0]["Title"].ToString().ToUpper() + "</Text>");
                        XmlBuilder.Append("<Text>IND/" + Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_PhoneNo"]) + "</Text>");
                        XmlBuilder.Append("</AddQual>");
                        XmlBuilder.Append("</SSRQual>");
                        XmlBuilder.Append("</Item>");

                        //Gst Email
                        XmlBuilder.Append("<Item>");
                        XmlBuilder.Append("<DataBlkInd>S </DataBlkInd>");
                        XmlBuilder.Append("<SSRQual>");
                        XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                        XmlBuilder.Append("<AddQual>");
                        XmlBuilder.Append("<SSRCode>GSTE</SSRCode>");
                        XmlBuilder.Append("<LNameNum>0" + g.ToString() + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>0" + g.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + g.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<AirV>" + Convert.ToString(FltHdr.Tables[0].Rows[0]["VC"]).ToUpper() + "</AirV>");
                        //XmlBuilder.Append("<Text>IND/" + (Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_Email"]).ToUpper()).Replace("@", "//") + "-1" + FltPaxDs.Tables[0].Rows[0]["LName"].ToString().ToUpper() + "/" + FltPaxDs.Tables[0].Rows[0]["FName"].ToString().ToUpper() + FltPaxDs.Tables[0].Rows[0]["Title"].ToString().ToUpper() + "</Text>");
                        XmlBuilder.Append("<Text>IND/" + (Convert.ToString(FltHdr.Tables[0].Rows[0]["GST_Email"]).ToUpper()).Replace("@", "//") + "</Text>");
                        XmlBuilder.Append("</AddQual>");
                        XmlBuilder.Append(" </SSRQual>");
                        XmlBuilder.Append("</Item>");
                        #endregion

                        //}  //infant IF END
                    }

                }
            }
            catch { }
            #endregion

            #region Add Mobile Number
            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>O</DataBlkInd>");
            XmlBuilder.Append("<OSIQual>");
            XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
            XmlBuilder.Append("<AddQual>");
            XmlBuilder.Append("<OSIV>YY</OSIV>");
            XmlBuilder.Append("<OSI>MOBILE-" + FltHdr.Tables[0].Rows[0]["PgMobile"].ToString() + "</OSI>");
            XmlBuilder.Append("</AddQual>");
            XmlBuilder.Append("</OSIQual>");
            XmlBuilder.Append("</Item>");
            #endregion

            #region Genral Remark
            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>G</DataBlkInd>");
            XmlBuilder.Append("<GenRmkQual>");
            XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
            XmlBuilder.Append("<AddQual>");
            XmlBuilder.Append("<Qual1/>");
            XmlBuilder.Append("<Qual2 />");
            XmlBuilder.Append("<Rmk>ON-LINE BOOKING - " + FltHdr.Tables[0].Rows[0]["AgentId"].ToString().ToUpper() + "</Rmk>");
            XmlBuilder.Append("</AddQual>");
            XmlBuilder.Append("</GenRmkQual>");
            XmlBuilder.Append("</Item>");
            #endregion

            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>E</DataBlkInd>");
            XmlBuilder.Append("<EndQual>");
            XmlBuilder.Append("<EndMark>E</EndMark>");
            XmlBuilder.Append("</EndQual>");
            XmlBuilder.Append("</Item>");

            XmlBuilder.Append("</ItemAry>");
            XmlBuilder.Append("</PNRBFSecondaryBldChgMods>");

            #endregion

            if (SlcFltDs.Tables[0].Rows[0]["Searchvalue"].ToString().Trim() == "PROMOFARE")
            {
                int a = 1, c = 1, d = 1;
                int cnt = 0;
                if (Adt > 0)
                    cnt = cnt + 1;
                if (Chd > 0)
                    cnt = cnt + 1;
                if (Inf > 0)
                    cnt = cnt + 1;

                for (int i = 1; i <= cnt; i++)
                {
                    #region StorePriceMods
                    XmlBuilder.Append("<StorePriceMods>");

                    #region PsgrMods
                    XmlBuilder.Append("<PsgrMods>");
                    XmlBuilder.Append("<PsgrAry>");
                    if (i == 1)
                    {
                        for (a = 1; a <= Adt; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>ADT</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                            c++;
                            d++;
                        }
                    }
                    else if (i == 2 && Chd > 0)
                    {
                        for (a = c; a <= Adt + Chd; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>C" + Utility.ConvertToAgeFromDOB(FltPaxDs.Tables[0].Rows[a - 1]["DOB"].ToString()) + "</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                            d++;
                        }
                    }
                    else if (i == 2 && Chd == 0 && Inf > 0)
                    {
                        for (a = c; a <= Adt + Inf; a++)
                        {
                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>INF</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                        }
                    }
                    else if (i == 3 && Inf > 0)
                    {
                        for (a = d; a <= Adt + Chd + Inf; a++)
                        {

                            XmlBuilder.Append("<Psgr>");
                            XmlBuilder.Append("<LNameNum>0" + a.ToString() + "</LNameNum>");
                            XmlBuilder.Append("<PsgrNum>0" + a.ToString() + "</PsgrNum>");
                            XmlBuilder.Append("<AbsNameNum>0" + a.ToString() + "</AbsNameNum>");
                            XmlBuilder.Append("<PIC>INF</PIC>");
                            XmlBuilder.Append("<TIC />");
                            XmlBuilder.Append("</Psgr>");
                        }
                    }
                    XmlBuilder.Append("</PsgrAry>");
                    XmlBuilder.Append("</PsgrMods>");
                    #endregion

                    XmlBuilder.Append("<CommissionMod>");
                    XmlBuilder.Append("<Percent>" + SlcFltDs.Tables[0].Rows[0]["IATAComm"].ToString() + "</Percent>");
                    XmlBuilder.Append("</CommissionMod>");

                    try
                    {
                        //mordified by abhilash 13-jul-2013
                        //this is for deal code or net remittance code
                        if (strCode.Contains("PNR") && (strCode.Contains("DEALCODE") || strCode.Contains("NETREMITTANCE")))
                        {
                            XmlBuilder.Append("<AccountingInfo>");
                            XmlBuilder.Append("<Info>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Info>");
                            XmlBuilder.Append("</AccountingInfo>");
                        }
                        //end deal code or net remittance code
                        //this is for tour code
                        if (strCode.Contains("PNR") && strCode.Contains("TOURCODE"))
                        {
                            XmlBuilder.Append("<TourCode>");
                            XmlBuilder.Append("<Rules>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Rules>");
                            XmlBuilder.Append("</TourCode>");
                        }
                        //end of tour code
                        ////If you wanted to add Tour code in Endorsement. 
                        if (strCode.Contains("PNR") && strCode.Contains("TCEndorsement"))
                        {
                            XmlBuilder.Append("<TourCode>");
                            XmlBuilder.Append("<Rules>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Rules>");
                            XmlBuilder.Append("</TourCode>");
                        }
                        ////end of add Tour code in Endorsement. 
                        //end of mordification
                    }
                    catch (Exception ex)
                    { }



                    #region SegSelection
                    XmlBuilder.Append("<SegSelection>");
                    XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                    XmlBuilder.Append("<SegRangeAry>");
                    XmlBuilder.Append("<SegRange>");
                    XmlBuilder.Append("<StartSeg>01</StartSeg>");
                    XmlBuilder.Append("<EndSeg>01</EndSeg>");
                    XmlBuilder.Append("<FareType>B</FareType>");
                    if (i == 1)
                        XmlBuilder.Append("<FIC>" + SlcFltDs.Tables[0].Rows[0]["AdtFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 2 && Chd > 0)
                        XmlBuilder.Append("<FIC>" + SlcFltDs.Tables[0].Rows[0]["ChdFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 2 && Chd == 0 && Inf > 0)
                        XmlBuilder.Append("<FIC>" + SlcFltDs.Tables[0].Rows[0]["InfFarebasis"].ToString().Trim() + "</FIC>");
                    else if (i == 3 && Inf > 0)
                        XmlBuilder.Append("<FIC>" + SlcFltDs.Tables[0].Rows[0]["InfFarebasis"].ToString().Trim() + "</FIC>");
                    XmlBuilder.Append("<PFQual>");
                    XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
                    XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
                    try
                    {
                        if (PrivateFare == true)
                        {
                            #region Promotional Code
                            if (Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]).Length > 1)
                            {
                                XmlBuilder.Append("<Acct>" + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]) + "</Acct>");
                                XmlBuilder.Append("<Contract/>");
                                XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                            }
                            else
                            {
                                STD.DAL.FltDeal objDeal = new STD.DAL.FltDeal(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                                DataTable PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", SlcFltDs.Tables[0].Rows[0]["Trip"].ToString().Trim(), "", SlcFltDs.Tables[0].Rows[0]["User_id"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["Track_id"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["DepartureLocation"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["ArrivalLocation"].ToString().Trim(), "", "");
                                DataRow[] PCRow = { };
                                if (PFCodeDt.Rows.Count > 0)
                                {
                                    PCRow = PFCodeDt.Select("AirCode='" + SlcFltDs.Tables[0].Rows[i]["ValiDatingCarrier"].ToString().Trim() + "' and AppliedOn='BOOK'");
                                    if (PCRow.Count() <= 0)
                                    {
                                        PCRow = PFCodeDt.Select("AirCode='ALL' and AppliedOn='BOOK'");
                                        //PCRow = PFCodeDt.Select("AirCode='" + SlcFltDs.Tables[0].Rows[i]["ValiDatingCarrier"].ToString().Trim() + "' or AirCode='ALL'");
                                    }
                                    if (PCRow.Count() > 0)
                                    {
                                        if (!string.IsNullOrEmpty(PCRow[0]["D_T_Code"].ToString()))
                                        {
                                            XmlBuilder.Append("<Acct>" + PCRow[0]["D_T_Code"].ToString() + "</Acct>");
                                            XmlBuilder.Append("<Contract/>");
                                            XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                                        }
                                        else
                                        {
                                            XmlBuilder.Append("<Acct/>");
                                            XmlBuilder.Append("<Contract/>");
                                            XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                                        }
                                    }
                                    else
                                    {
                                        XmlBuilder.Append("<Acct/>");
                                        XmlBuilder.Append("<Contract/>");
                                        XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                                    }
                                }
                                else
                                {
                                    XmlBuilder.Append("<Acct/>");
                                    XmlBuilder.Append("<Contract/>");
                                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            XmlBuilder.Append("<Acct/>");
                            XmlBuilder.Append("<Contract/>");
                            XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                        }
                    }
                    catch
                    {
                        XmlBuilder.Append("<Acct/>");
                        XmlBuilder.Append("<Contract/>");
                        XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                    }

                    XmlBuilder.Append("<Type>A</Type>");
                    XmlBuilder.Append("</PFQual>");
                    XmlBuilder.Append("</SegRange>");
                    XmlBuilder.Append("</SegRangeAry>");
                    XmlBuilder.Append("</SegSelection>");
                    #endregion

                    XmlBuilder.Append("</StorePriceMods>");
                    #endregion
                }
            }
            else
            {
                #region StorePriceMods
                XmlBuilder.Append("<StorePriceMods>");

                #region SegSelection
                XmlBuilder.Append("<SegSelection>");
                XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
                XmlBuilder.Append("<SegRangeAry>");
                XmlBuilder.Append("<SegRange>");
                XmlBuilder.Append("<StartSeg>00</StartSeg>");
                XmlBuilder.Append("<EndSeg>00</EndSeg>");
                XmlBuilder.Append("<FareType>P</FareType>");
                XmlBuilder.Append("<PFQual>");
                XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
                XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");


                try
                {
                    if (PrivateFare == true)
                    {
                        #region Promotional Code
                        if (Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]).Length > 1)
                        {
                            XmlBuilder.Append("<Acct>" + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]) + "</Acct>");
                            XmlBuilder.Append("<Contract/>");
                            XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                        }
                        else
                        {
                            STD.DAL.FltDeal objDeal = new STD.DAL.FltDeal(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                            DataTable PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", SlcFltDs.Tables[0].Rows[0]["Trip"].ToString().Trim(), "", SlcFltDs.Tables[0].Rows[0]["User_id"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["Track_id"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["DepartureLocation"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["ArrivalLocation"].ToString().Trim(), "", "");
                            DataRow[] PCRow = { };
                            if (PFCodeDt.Rows.Count > 0)
                            {
                                PCRow = PFCodeDt.Select("AirCode='" + SlcFltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' and AppliedOn='BOOK'");
                                if (PCRow.Count() <= 0)
                                {
                                    PCRow = PFCodeDt.Select("AirCode='ALL' and AppliedOn='BOOK'");
                                    //PCRow = PFCodeDt.Select("AirCode='" + FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' or AirCode='ALL'");
                                }
                                if (PCRow.Count() > 0)
                                {
                                    if (!string.IsNullOrEmpty(PCRow[0]["D_T_Code"].ToString()))
                                    {
                                        XmlBuilder.Append("<Acct>" + PCRow[0]["D_T_Code"].ToString() + "</Acct>");
                                        XmlBuilder.Append("<Contract/>");
                                        XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");

                                    }
                                    else
                                    {
                                        XmlBuilder.Append("<Acct/>");
                                        XmlBuilder.Append("<Contract/>");
                                        XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

                                    }
                                }
                                else
                                {
                                    XmlBuilder.Append("<Acct/>");
                                    XmlBuilder.Append("<Contract/>");
                                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

                                }
                            }
                            else
                            {
                                XmlBuilder.Append("<Acct/>");
                                XmlBuilder.Append("<Contract/>");
                                XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

                            }
                        }


                        #endregion
                    }
                    else
                    {
                        XmlBuilder.Append("<Acct/>");
                        XmlBuilder.Append("<Contract/>");
                        XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                    }
                }
                catch
                {
                    XmlBuilder.Append("<Acct/>");
                    XmlBuilder.Append("<Contract/>");
                    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

                }

                //XmlBuilder.Append("<Contract/>");
                //XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                XmlBuilder.Append("<Type>A</Type>");
                XmlBuilder.Append("</PFQual>");
                XmlBuilder.Append("</SegRange>");
                XmlBuilder.Append("</SegRangeAry>");
                XmlBuilder.Append("</SegSelection>");
                #endregion

                #region PassengerType
                XmlBuilder.Append("<PassengerType>");
                XmlBuilder.Append("<PsgrAry>");
                int count = 0;
                //if ((WOCHD == true) && (CHDOnly == true))
                //    count = Chd;
                //else if ((WOCHD == true) && (CHDOnly == false))
                //    count = Adt + Chd;
                //else if ((WOCHD == true) && (CHDOnly == false))
                //    count = Adt + Chd + Inf;
                //else
                //    count = Adt + Chd + Inf;
                //if (InfWIDiffFBC == true)
                //    count = count - Inf;
                if ((WOCHD == true) && (CHDOnly == true))
                    count = Chd;
                //else if ((WOCHD == true) && (CHDOnly == false))
                //    count = Adt + Chd;
                else if ((WOCHD == true) && (CHDOnly == false))
                    count = Adt + Inf;
                else
                    count = Adt + Chd + Inf;
                if (InfWIDiffFBC == true)
                    count = count - Inf;

                for (int i = 1; i <= count; i++)
                {

                    XmlBuilder.Append("<Psgr>");
                    XmlBuilder.Append("<LNameNum>0" + i.ToString() + "</LNameNum>");
                    XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                    XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");
                    if (CHDOnly == true)
                    {
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>05</Age>");
                    }

                    else
                    {
                        if (i <= Adt)
                        {
                            XmlBuilder.Append("<PTC>ADT</PTC>");
                            XmlBuilder.Append("<Age/>");
                        }
                        else if ((i > Adt) && (i <= (Adt + Chd)))
                        {
                            XmlBuilder.Append("<PTC>CNN</PTC>");
                            XmlBuilder.Append("<Age>" + Utility.ConvertToAgeFromDOB(FltPaxDs.Tables[0].Rows[i - 1]["DOB"].ToString()) + "</Age>");
                        }
                        else if ((i > (Adt + Chd)) && (i <= count))
                        {
                            XmlBuilder.Append("<PTC>INF</PTC>");
                            XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                            XmlBuilder.Append("<Age/>");
                        }
                    }
                    XmlBuilder.Append("</Psgr>");
                }

                XmlBuilder.Append("</PsgrAry>");
                XmlBuilder.Append("</PassengerType>");
                #endregion

                XmlBuilder.Append("<CommissionMod>");
                XmlBuilder.Append("<Percent>" + SlcFltDs.Tables[0].Rows[0]["IATAComm"].ToString() + "</Percent>");
                XmlBuilder.Append("</CommissionMod>");

                try
                {
                    //mordified by abhilash 13-jul-2013
                    //this is for deal code or net remittance code
                    if (strCode.Contains("PNR") && (strCode.Contains("DEALCODE") || strCode.Contains("NETREMITTANCE")))
                    {
                        XmlBuilder.Append("<AccountingInfo>");
                        XmlBuilder.Append("<Info>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Info>");
                        XmlBuilder.Append("</AccountingInfo>");
                    }
                    //end deal code or net remittance code
                    //this is for tour code
                    if (strCode.Contains("PNR") && strCode.Contains("TOURCODE"))
                    {
                        XmlBuilder.Append("<TourCode>");
                        XmlBuilder.Append("<Rules>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Rules>");
                        XmlBuilder.Append("</TourCode>");
                    }
                    //end of tour code
                    ////If you wanted to add Tour code in Endorsement. 
                    if (strCode.Contains("PNR") && strCode.Contains("TCEndorsement"))
                    {
                        XmlBuilder.Append("<TourCode>");
                        XmlBuilder.Append("<Rules>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Rules>");
                        XmlBuilder.Append("</TourCode>");
                    }
                    ////end of add Tour code in Endorsement. 
                    //end of mordification
                }
                catch (Exception ex)
                { }


                ////mordified by abhilash  13-jul 2013
                ////To insert a Tour code 
                //XmlBuilder.Append("<TourCode>");
                //XmlBuilder.Append("<Rules>TESTTOURCODE</Rules>");
                //XmlBuilder.Append("</TourCode>");

                ////If you wanted to add Tour code in Endorsement. 
                //XmlBuilder.Append("<EndorsementBox>");
                //XmlBuilder.Append("<Endors1>TOURCODEENDO</Endors1>");
                //XmlBuilder.Append("</EndorsementBox>");

                ////Here you can add Deal code/ net remittance code.
                //XmlBuilder.Append("<AccountingInfo>");
                //XmlBuilder.Append("<Info>DEALCODE</Info>");
                //XmlBuilder.Append("</AccountingInfo>");
                //// You can add net remittance under this tag as well.
                //XmlBuilder.Append("<NettRemittance />");

                ////To add value code
                //XmlBuilder.Append("<ValueCode>");
                //XmlBuilder.Append("<ValueInd>D</ValueInd>");//'D' indicate net fare amount.
                //XmlBuilder.Append("<Amt>AB9999</Amt>");
                //XmlBuilder.Append("</ValueCode>");
                ////mordification end


                XmlBuilder.Append("</StorePriceMods>");
                #endregion

                #region StorePriceMods In case of Inf Diff FBC
                if (InfWIDiffFBC == true)
                {
                    XmlBuilder.Append("<StorePriceMods>");
                    XmlBuilder.Append("<SegSelection>");
                    XmlBuilder.Append("<ReqAirVPFs>N</ReqAirVPFs>");
                    XmlBuilder.Append("<SegRangeAry>");
                    for (int i = 1; i <= SlcFltDs.Tables[0].Rows.Count; i++)
                    {
                        XmlBuilder.Append("<SegRange>");
                        XmlBuilder.Append("<StartSeg>0" + i.ToString() + "</StartSeg>");
                        XmlBuilder.Append("<EndSeg>0" + i.ToString() + "</EndSeg>");
                        XmlBuilder.Append("<FareType>F</FareType>");
                        XmlBuilder.Append("<FIC>" + SlcFltDs.Tables[0].Rows[i - 1]["InfFarebasis"].ToString() + "</FIC>");
                        XmlBuilder.Append("</SegRange>");
                    }

                    XmlBuilder.Append("</SegRangeAry>");
                    XmlBuilder.Append("</SegSelection>");
                    XmlBuilder.Append("<PassengerType>");
                    XmlBuilder.Append("<PsgrAry>");
                    for (int i = count + 1; i <= (count + Inf); i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>0" + i.ToString() + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("</Psgr>");
                    }
                    XmlBuilder.Append("</PsgrAry>");
                    XmlBuilder.Append("</PassengerType>");
                    XmlBuilder.Append("</StorePriceMods>");
                }
                #endregion
            }
            XmlBuilder.Append("</PNRBFManagement_33>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SONS"));

            return XmlBuilder.ToString();

        }


        private string PNRBFPrimaryBldChgMods(DataSet FltPaxDs, int Adt, int Chd, int Inf, bool WOCHD, bool CHDOnly)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<PNRBFPrimaryBldChgMods>");
            XmlBuilder.Append("<ItemAry>");
            int j = 0;

            #region Pax
            for (int i = 0; i <= FltPaxDs.Tables[0].Rows.Count - 1; i++)
            {
                if (CHDOnly == true)
                {
                    if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "CHD")
                    {
                        j++;
                        #region Pax Entry
                        XmlBuilder.Append("<Item>");
                        XmlBuilder.Append("<DataBlkInd>N</DataBlkInd>");
                        XmlBuilder.Append("<NameQual>");
                        XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                        XmlBuilder.Append("<EditTypeIndAppliesTo/>");
                        XmlBuilder.Append("<AddChgNameRmkQual>");
                        XmlBuilder.Append("<LNameID>0" + j.ToString() + "</LNameID>");
                        XmlBuilder.Append("<LName>" + FltPaxDs.Tables[0].Rows[i]["LName"].ToString() + "</LName>");
                        XmlBuilder.Append("<LNameRmk/>");
                        XmlBuilder.Append("<NameTypeQual>");
                        XmlBuilder.Append("<FNameAry>");
                        XmlBuilder.Append("<FNameItem>");
                        XmlBuilder.Append("<PsgrNum>0" + j.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + j.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<FName>" + FltPaxDs.Tables[0].Rows[i]["FName"].ToString() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString() + " " + FltPaxDs.Tables[0].Rows[i]["Title"].ToString() + "</FName>");
                        XmlBuilder.Append("<FNameRmk>P-C" + Utility.Left(FltPaxDs.Tables[0].Rows[i]["DOB"].ToString(), 2) + Utility.datecon(Utility.Mid(FltPaxDs.Tables[0].Rows[i]["FName"].ToString(), 4, 2)) + Utility.Right(FltPaxDs.Tables[0].Rows[i]["FName"].ToString(), 2) + "</FNameRmk>");//this tag only support in 1G, Entry must be in CAPITAL LETTER,In case of infant age must be enter(DDMMMYY)
                        XmlBuilder.Append("</FNameItem>");
                        XmlBuilder.Append("</FNameAry>");
                        XmlBuilder.Append("</NameTypeQual>");
                        XmlBuilder.Append("</AddChgNameRmkQual>");
                        XmlBuilder.Append("</NameQual>");
                        XmlBuilder.Append("</Item>");
                        #endregion
                    }
                }
                else
                {
                    if ((FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString().ToUpper().Trim() == "CHD") && (WOCHD == true))
                    { }
                    else
                    {
                        j++;
                        #region Pax Entry
                        XmlBuilder.Append("<Item>");
                        XmlBuilder.Append("<DataBlkInd>N</DataBlkInd>");
                        XmlBuilder.Append("<NameQual>");
                        XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                        XmlBuilder.Append("<EditTypeIndAppliesTo/>");
                        XmlBuilder.Append("<AddChgNameRmkQual>");
                        if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "INF")
                        {
                            XmlBuilder.Append("<NameType>I</NameType>");
                        }
                        //name condition
                        string fName = "", lName = "";
                        #region SET PAX NAME IN GDS FORMATE
                        if (!string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                        {
                            //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].FirstName + "' Middle='" + objPax[i].MiddleName + "' Last='" + objPax[i].LastName + "' Prefix='" + objPax[i].Title + "' />");
                            //Name = objPax[i].LastName + "/" + objPax[i].FirstName + objPax[i].Title;

                            lName = FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim();
                            fName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["Title"].ToString().ToUpper().Trim();
                            //XmlBuilder.Append("<FName>" + FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["Title"].ToString().ToUpper().Trim() + "</FName>");
                        }

                        else if (!string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                        {
                            //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].FirstName + "' Last='" + objPax[i].MiddleName + "' Prefix='" + objPax[i].Title + "' />");
                            //Name = objPax[i].MiddleName + "/" + objPax[i].FirstName + objPax[i].Title;
                            fName = FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim();
                            lName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString().ToUpper().Trim();
                        }
                        //else if (!string.IsNullOrEmpty(objPax[i].Title) && !string.IsNullOrEmpty(objPax[i].FirstName) && string.IsNullOrEmpty(objPax[i].MiddleName) && string.IsNullOrEmpty(objPax[i].LastName))
                        //{
                        //    XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].Title + "' Last='" + objPax[i].FirstName + "' />");
                        //    Name = objPax[i].FirstName + "/" + objPax[i].Title;
                        //}
                        else if (!string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && (string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()) || FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim().Length == 1))
                        {
                            if (string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                            {
                                //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].Title + "' Last='" + objPax[i].FirstName + "' />");
                                //Name = objPax[i].FirstName + "/" + objPax[i].Title;
                                fName = FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim();
                                lName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim();
                            }
                            else
                            {
                                //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].Title + "' Last='" + objPax[i].FirstName + " " + objPax[i].LastName + "' />");
                                //Name = objPax[i].FirstName + " " + objPax[i].LastName + "/" + objPax[i].Title;
                                fName = FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim();
                                lName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["LName"].ToString().ToUpper().Trim();
                            }
                        }
                        else if (!string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                        {
                            //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].FirstName + "' Last='" + objPax[i].LastName + "' Prefix='" + objPax[i].Title + "' />");
                            //Name = objPax[i].LastName + "/" + objPax[i].FirstName + objPax[i].Title;
                            fName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim();
                            lName = FltPaxDs.Tables[0].Rows[i]["LName"].ToString().ToUpper().Trim();
                        }
                        else if (!string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                        {
                            //XmlBuilder.Append("<com:BookingTravelerName Last='" + objPax[i].LastName + "' Prefix='" + objPax[i].Title + "' />");
                            //Name = objPax[i].LastName + "/" + objPax[i].Title;
                            fName = FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim();
                            lName = FltPaxDs.Tables[0].Rows[i]["LName"].ToString().ToUpper().Trim();
                        }
                        else if (string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                        {
                            //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].FirstName + "' Middle='" + objPax[i].MiddleName + "' Last='" + objPax[i].LastName + "' />");
                            //Name = objPax[i].LastName + "/" + objPax[i].FirstName;
                            fName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim();
                            lName = FltPaxDs.Tables[0].Rows[i]["LName"].ToString().ToUpper().Trim();
                        }
                        else if (string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                        {
                            //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].FirstName + "' Last='" + objPax[i].MiddleName + "' />");
                            //Name = objPax[i].MiddleName + "/" + objPax[i].FirstName;
                            fName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim();
                            lName = FltPaxDs.Tables[0].Rows[i]["MName"].ToString().ToUpper().Trim();
                        }
                        else if (string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["Title"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["FName"].ToString().Trim()) && string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["MName"].ToString().Trim()) && !string.IsNullOrEmpty(FltPaxDs.Tables[0].Rows[i]["LName"].ToString().Trim()))
                        {
                            //XmlBuilder.Append("<com:BookingTravelerName First='" + objPax[i].FirstName + "' Last='" + objPax[i].LastName + "' />");
                            //Name = objPax[i].LastName + "/" + objPax[i].FirstName;
                            fName = FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim();
                            lName = FltPaxDs.Tables[0].Rows[i]["LName"].ToString().ToUpper().Trim();
                        }
                        #endregion




                        XmlBuilder.Append("<LNameID>0" + j.ToString() + "</LNameID>");
                        XmlBuilder.Append("<LName>" + lName.ToUpper() + "</LName>");//FltPaxDs.Tables[0].Rows[i]["LName"].ToString().ToUpper()
                        XmlBuilder.Append("<LNameRmk/>");
                        XmlBuilder.Append("<NameTypeQual>");
                        XmlBuilder.Append("<FNameAry>");
                        XmlBuilder.Append("<FNameItem>");
                        XmlBuilder.Append("<PsgrNum>0" + j.ToString() + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>0" + j.ToString() + "</AbsNameNum>");
                        XmlBuilder.Append("<FName>" + fName.ToUpper() + "</FName>");
                        if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "ADT")
                        {
                            //XmlBuilder.Append("<FName>" + FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["Title"].ToString().ToUpper().Trim() + "</FName>");
                            XmlBuilder.Append("<FNameRmk/>");//this tag only support in 1G, Entry must be in CAPITAL LETTER,In case of infant age must be enter(DDMMMYY)
                        }
                        else if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "CHD")
                        {
                            //XmlBuilder.Append("<FName>" + FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["Title"].ToString().ToUpper().Trim() + "</FName>");
                            XmlBuilder.Append("<FNameRmk>P-C" + Utility.ConvertToAgeFromDOB(FltPaxDs.Tables[0].Rows[i]["DOB"].ToString()) + "</FNameRmk>");//this tag only support in 1G, Entry must be in CAPITAL LETTER,In case of infant age must be enter(DDMMMYY)
                        }
                        else if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "INF")
                        {
                            //XmlBuilder.Append("<FName>" + FltPaxDs.Tables[0].Rows[i]["FName"].ToString().ToUpper().Trim() + " " + FltPaxDs.Tables[0].Rows[i]["MName"].ToString().ToUpper().Trim() + "</FName>");
                            XmlBuilder.Append("<FNameRmk>" + Utility.Left(FltPaxDs.Tables[0].Rows[i]["DOB"].ToString(), 2) + Utility.datecon(Utility.Mid(FltPaxDs.Tables[0].Rows[i]["DOB"].ToString(), 3, 2)) + Utility.Right(FltPaxDs.Tables[0].Rows[i]["DOB"].ToString(), 2) + "</FNameRmk>");//this tag only support in 1G, Entry must be in CAPITAL LETTER,In case of infant age must be enter(DDMMMYY)
                        }

                        XmlBuilder.Append("</FNameItem>");
                        XmlBuilder.Append("</FNameAry>");
                        XmlBuilder.Append("</NameTypeQual>");
                        XmlBuilder.Append("</AddChgNameRmkQual>");
                        XmlBuilder.Append("</NameQual>");
                        XmlBuilder.Append("</Item>");
                        #endregion

                        #region Frequent Flyer Entry start
                        if (FltPaxDs.Tables[0].Rows[i]["PaxType"].ToString() == "ADT")
                        {
                            if (FltPaxDs.Tables[0].Rows[i]["FFAirline"].ToString().Trim() != "")
                            {
                                XmlBuilder.Append("<Item>");
                                XmlBuilder.Append("<DataBlkInd>M</DataBlkInd>");
                                XmlBuilder.Append("<FreqCustQual>");
                                XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
                                XmlBuilder.Append("<AddQual>");
                                XmlBuilder.Append("<LNameID>0" + j.ToString() + "</LNameID>");
                                XmlBuilder.Append("<PsgrNum>0" + j.ToString() + "</PsgrNum>");
                                XmlBuilder.Append("<AbsNameNum>0" + j.ToString() + "</AbsNameNum>");
                                XmlBuilder.Append("<FreqCustNum>" + FltPaxDs.Tables[0].Rows[i]["FFAirline"].ToString() + FltPaxDs.Tables[0].Rows[i]["FFNumber"].ToString() + "</FreqCustNum>");
                                XmlBuilder.Append("</AddQual>");
                                XmlBuilder.Append("</FreqCustQual>");
                                XmlBuilder.Append("</Item>");
                            }

                        }
                        #endregion
                    }
                }
            }
            #endregion

            #region Add Agency Phone number start
            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>P</DataBlkInd>");
            XmlBuilder.Append("<PhoneQual>");
            XmlBuilder.Append("<EditTypeInd>A</EditTypeInd>");
            XmlBuilder.Append("<AddPhoneQual>");
            XmlBuilder.Append("<City>DEL</City>");
            XmlBuilder.Append("<Type>A</Type>");
            //XmlBuilder.Append("<PhoneNumber>011 47677777</PhoneNumber>");
            XmlBuilder.Append("<PhoneNumber>" + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["1GReqPhoneNo"]) + "</PhoneNumber>");
            XmlBuilder.Append("</AddPhoneQual>");
            XmlBuilder.Append("</PhoneQual>");
            XmlBuilder.Append("</Item>");
            #endregion

            #region TK
            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>T</DataBlkInd>");
            XmlBuilder.Append("<TkQual>");
            XmlBuilder.Append("<Tk>TAU/" + DateTime.Now.Date.ToString("ddMMM").ToUpper() + "</Tk>");
            XmlBuilder.Append("</TkQual>");
            XmlBuilder.Append("</Item>");
            #endregion

            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<DataBlkInd>E</DataBlkInd>");
            XmlBuilder.Append("<EndMarkQual>");
            XmlBuilder.Append("<EndMark>E</EndMark>");
            XmlBuilder.Append("</EndMarkQual>");
            XmlBuilder.Append("</Item>");
            XmlBuilder.Append("</ItemAry>");
            XmlBuilder.Append("</PNRBFPrimaryBldChgMods>");
            return XmlBuilder.ToString();
        }


        /// <summary>
        /// End Transaction
        /// SOAPAction: "http://webservices.galileo.com/SubmitXmlOnSession"
        /// </summary>
        /// <returns></returns>
        public string PNRBFMgntWithET(string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_33 xmlns=''>");
            XmlBuilder.Append("<EndTransactionMods>");
            XmlBuilder.Append("<EndTransactRequest>");
            XmlBuilder.Append("<ETInd>R</ETInd>");
            XmlBuilder.Append("<RcvdFrom>PASSENGER</RcvdFrom>");
            XmlBuilder.Append("</EndTransactRequest>");
            XmlBuilder.Append("</EndTransactionMods>");
            XmlBuilder.Append("</PNRBFManagement_33>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SONS"));

            return XmlBuilder.ToString();
        }

        public string CheckAvailabilityReq(DataSet FltDs)
        {
            StringBuilder XmlBuilder = new StringBuilder();

            XmlBuilder.Append(SoapStart("SS", ""));
            XmlBuilder.Append("<Request>");

            XmlBuilder.Append("<AirAvailability_12>");
            XmlBuilder.Append("<AirAvailMods>");
            XmlBuilder.Append("<BICFilter>");
            XmlBuilder.Append("<BICFilterAry>");
            XmlBuilder.Append("<BICFilterInfo>");
            XmlBuilder.Append("<BIC>L</BIC>");
            XmlBuilder.Append("<AllowSmlrInd>N</AllowSmlrInd>");
            XmlBuilder.Append("</BICFilterInfo>");
            XmlBuilder.Append("</BICFilterAry>");
            XmlBuilder.Append("</BICFilter>");
            XmlBuilder.Append("<AirVSpecificAvail>");
            XmlBuilder.Append("<NumSeats>0</NumSeats>");
            XmlBuilder.Append("<Class />");
            XmlBuilder.Append("<StartDt>20120515</StartDt>");
            XmlBuilder.Append("<StartPt>BOM</StartPt>");
            XmlBuilder.Append("<EndPt>DEL</EndPt>");
            XmlBuilder.Append("<StartTm>0225</StartTm>");
            XmlBuilder.Append("<TmWndInd>D</TmWndInd>");
            XmlBuilder.Append("<StartTmWnd>0225</StartTmWnd>");
            XmlBuilder.Append("<EndTmWnd>0225</EndTmWnd>");
            XmlBuilder.Append("<FltTypeInd>E</FltTypeInd>");
            XmlBuilder.Append("<FltTypePref />");
            XmlBuilder.Append("<IgnoreTSPref>N</IgnoreTSPref>");
            XmlBuilder.Append("<WidenSearchWndInd>N</WidenSearchWndInd>");
            XmlBuilder.Append("<MaxNumFlts>99</MaxNumFlts>");
            XmlBuilder.Append("<IncNonStopDirectsInd>Y</IncNonStopDirectsInd>");
            XmlBuilder.Append("<IncStopDirectsInd>Y</IncStopDirectsInd>");
            XmlBuilder.Append("<IncSingleOnlineConxInd>Y</IncSingleOnlineConxInd>");
            XmlBuilder.Append("<IncDoubleOnlineConxInd>N</IncDoubleOnlineConxInd>");
            XmlBuilder.Append("<IncTripleOnlineConxInd>N</IncTripleOnlineConxInd>");
            XmlBuilder.Append("<IncSingleInterlineConxInd>N</IncSingleInterlineConxInd>");
            XmlBuilder.Append("<IncDoubleInterlineConxInd>N</IncDoubleInterlineConxInd>");
            XmlBuilder.Append("<IncTripleInterlineConxInd>N</IncTripleInterlineConxInd>");
            XmlBuilder.Append("<SeqInd>P</SeqInd>");
            XmlBuilder.Append("</AirVSpecificAvail>");
            XmlBuilder.Append("<AirVPrefInd>");
            XmlBuilder.Append("<AirVIncExcInd>O</AirVIncExcInd>");
            XmlBuilder.Append("<RelaxAirVPref>N</RelaxAirVPref>");
            XmlBuilder.Append("</AirVPrefInd>");
            XmlBuilder.Append("<AirVPrefs>");
            XmlBuilder.Append("<AirVAry>");
            XmlBuilder.Append("<AirVInfo>");
            XmlBuilder.Append("<AirV>9W</AirV>");
            XmlBuilder.Append("<FltRangeStart>2237</FltRangeStart>");
            XmlBuilder.Append("<FltRangeEnd>2237</FltRangeEnd>");
            XmlBuilder.Append("<PrefTypeInd>I</PrefTypeInd>");
            XmlBuilder.Append("</AirVInfo>");
            XmlBuilder.Append("</AirVAry>");
            XmlBuilder.Append("</AirVPrefs>");
            XmlBuilder.Append("</AirAvailMods>");
            XmlBuilder.Append("</AirAvailability_12>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SS"));

            return XmlBuilder.ToString();
            return "";
        }

        /// <summary>
        /// Queue the pnr in another PCC
        /// </summary>
        /// <param name="Token">token from begin session response</param>
        /// <param name="QNum">Queue Number</param>
        /// <param name="Pcc">PCC</param>
        /// <returns>request xml string</returns>
        public string QueuePnrReq(string Token, string QNum, string Pcc)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_33>");
            XmlBuilder.Append("<EndTransactionMods>");
            XmlBuilder.Append("<EndTransactRequest>");
            XmlBuilder.Append("<ETInd>Q</ETInd>");
            XmlBuilder.Append("<RcvdFrom>PASSENGER</RcvdFrom>");
            XmlBuilder.Append("</EndTransactRequest>");
            XmlBuilder.Append("<GlobalAccessInfo>");
            XmlBuilder.Append("<GlobAccessCRSAry>");
            XmlBuilder.Append("<GlobAccessCRS>");
            XmlBuilder.Append("<CRS>1G</CRS>");
            XmlBuilder.Append("<PCC>" + Pcc + "</PCC>");
            XmlBuilder.Append("<QNum>" + QNum + "</QNum>");
            XmlBuilder.Append("</GlobAccessCRS>");
            XmlBuilder.Append("</GlobAccessCRSAry>");
            XmlBuilder.Append("</GlobalAccessInfo>");
            XmlBuilder.Append("</EndTransactionMods>");
            XmlBuilder.Append("</PNRBFManagement_33>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SONS"));

            return XmlBuilder.ToString();

        }

        /// <summary>
        /// Retrieve Pnr details using Pnr with/wothout fare details
        /// </summary>
        /// <param name="Pnr">PNR</param>
        /// <param name="Token">get the token from begin session response</param>
        /// <param name="IsFare">True/False</param>
        /// <returns>request xml string</returns>
        public string RetrivePnrReq(string Pnr, string Token, bool IsFare)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_24>");
            XmlBuilder.Append("<PNRBFRetrieveMods>");
            XmlBuilder.Append("<PNRAddr>");
            XmlBuilder.Append("<FileAddr/>");
            XmlBuilder.Append("<CodeCheck/>");
            XmlBuilder.Append("<RecLoc>" + Pnr + "</RecLoc>");
            XmlBuilder.Append("</PNRAddr>");
            XmlBuilder.Append("</PNRBFRetrieveMods>");
            if (IsFare == true)
            {
                XmlBuilder.Append("<FareRedisplayMods>");
                XmlBuilder.Append("<DisplayAction>");
                XmlBuilder.Append("<Action>I</Action>");
                XmlBuilder.Append("</DisplayAction>");
                XmlBuilder.Append("</FareRedisplayMods>");
            }
            XmlBuilder.Append("</PNRBFManagement_24>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SONS"));

            return XmlBuilder.ToString();
        }

        /// <summary>
        /// For Online ticketing
        /// </summary>
        /// <param name="Token">get the token from session begin response</param>
        /// <param name="strCode">TKT/TOURCODE/XXXXX</param>
        /// <returns>request xml string</returns>
        public string DocProdFareManipulationReq(string Token, string strCode, string FareNum)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<DocProdFareManipulation_11>");
            XmlBuilder.Append("<TicketingMods>");
            XmlBuilder.Append("<ElectronicTicketFailed>");
            XmlBuilder.Append("<CancelInd>Y</CancelInd>");
            XmlBuilder.Append("</ElectronicTicketFailed>");
            XmlBuilder.Append("<FareNumInfo>");
            XmlBuilder.Append("<FareNumAry>");
            XmlBuilder.Append("<FareNum>00" + FareNum + "</FareNum>");
            XmlBuilder.Append("</FareNumAry>");
            XmlBuilder.Append("</FareNumInfo>");
            XmlBuilder.Append("<TicketingControl>");
            XmlBuilder.Append("<TransType>TK</TransType>");
            XmlBuilder.Append("</TicketingControl>");
            try
            {
                //mordified by abhilash 13-jul-2013
                //this is for deal code or net remittance code
                if (strCode.Contains("TKT") && (strCode.Contains("DEALCODE") || strCode.Contains("NETREMITTANCE")))
                {
                    XmlBuilder.Append("<AccountingInfo>");
                    XmlBuilder.Append("<Info>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Info>");
                    XmlBuilder.Append("</AccountingInfo>");
                }
                //end deal code or net remittance code
                //this is for tour code
                if (strCode.Contains("TKT") && strCode.Contains("TOURCODE"))
                {
                    XmlBuilder.Append("<TourCode>");
                    XmlBuilder.Append("<Rules>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Rules>");
                    XmlBuilder.Append("</TourCode>");
                }
                //end of tour code
                //end of mordification
            }
            catch (Exception ex)
            { }

            XmlBuilder.Append("<ProgEndorsement />");
            XmlBuilder.Append("</TicketingMods>");
            XmlBuilder.Append("</DocProdFareManipulation_11>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SONS"));
            return XmlBuilder.ToString();

        }
        /// <summary>
        /// for Terminal transactions(CRS entry)
        /// </summary>
        /// <param name="Token">get the token from begin session response</param>
        /// <param name="crsCmmand">crs entry</param>
        /// <returns>request xml string</returns>
        public string TerminalTransaction(string Token, string crsCmmand)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("STT", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append(crsCmmand);
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("STT"));

            return XmlBuilder.ToString();
        }

        /// <summary>
        /// Retrieve pnr 
        /// </summary>
        /// <param name="Token">get the token from begin session response</param>
        /// <returns>request xml string</returns>


        public string RetriveCurrentPnrReq(string Token, string val)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_43>");
            XmlBuilder.Append("<PNRBFRetrieveMods>");
            XmlBuilder.Append("<CurrentPNR></CurrentPNR>");
            XmlBuilder.Append("</PNRBFRetrieveMods>");
            XmlBuilder.Append("<FareRedisplayMods>");
            XmlBuilder.Append("<DisplayAction>");
            XmlBuilder.Append("<Action>D</Action>");
            XmlBuilder.Append("</DisplayAction>");
            XmlBuilder.Append("<FareNumInfo>");
            XmlBuilder.Append("<FareNumAry>");
            XmlBuilder.Append("<FareNum>1</FareNum>");
            XmlBuilder.Append("</FareNumAry>");
            XmlBuilder.Append("</FareNumInfo>");
            XmlBuilder.Append("</FareRedisplayMods>");
            XmlBuilder.Append("</PNRBFManagement_43>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SONS"));
            return XmlBuilder.ToString();
            //StringBuilder XmlBuilder = new StringBuilder();
            //XmlBuilder.Append(SoapStart("SONS", Token));
            //XmlBuilder.Append("<Request>");
            //XmlBuilder.Append("<PNRBFManagement_13>");
            //XmlBuilder.Append("<PNRBFRetrieveMods>");
            //XmlBuilder.Append("<CurrentPNR />");
            //XmlBuilder.Append("</PNRBFRetrieveMods>");
            //XmlBuilder.Append("</PNRBFManagement_13>");
            //XmlBuilder.Append("</Request>");
            //#region Filters
            //XmlBuilder.Append("<Filter>");
            //XmlBuilder.Append("<_ xmlns='' />");
            //XmlBuilder.Append("</Filter>");
            //#endregion
            //XmlBuilder.Append(SoapEnd("SONS"));
            //return XmlBuilder.ToString();
        }


        public string RetriveCurrentPnrReq(string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_13>");
            XmlBuilder.Append("<PNRBFRetrieveMods>");
            XmlBuilder.Append("<CurrentPNR />");
            XmlBuilder.Append("</PNRBFRetrieveMods>");
            XmlBuilder.Append("</PNRBFManagement_13>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SONS"));

            return XmlBuilder.ToString();
        }

        public string FQCSReq(FlightSearch searchInputs, List<FlightSearchResults> FltRes)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SS", ""));
            XmlBuilder.Append("<Request>");

            XmlBuilder.Append("<FareQuoteClassSpecific_12>");
            XmlBuilder.Append("<ClassSpecificMods>");
            XmlBuilder.Append("<FQAMods>");
            XmlBuilder.Append("<FQARequest>Y</FQARequest>");
            XmlBuilder.Append("</FQAMods>");

            #region Pax Information (total pax<=7 for FQSBB Search)
            XmlBuilder.Append("<PassengerType>");
            XmlBuilder.Append("<PsgrAry>");
            if ((searchInputs.Adult + searchInputs.Child + searchInputs.Infant) > 7)
            {
                if (searchInputs.Adult > 0)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("</Psgr>");
                    }
                }
                if (searchInputs.Child > 0)
                {
                    for (int i = 4; i <= 6; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("</Psgr>");
                    }

                }
                if (searchInputs.Infant > 0)
                {
                    XmlBuilder.Append("<Psgr>");
                    XmlBuilder.Append("<LNameNum>7</LNameNum>");
                    XmlBuilder.Append("<PsgrNum>7</PsgrNum>");
                    XmlBuilder.Append("<AbsNameNum>7</AbsNameNum>");
                    XmlBuilder.Append("<PTC>INF</PTC>");
                    XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                    XmlBuilder.Append("</Psgr>");
                }
            }
            else
            {
                int j = 1;
                if (searchInputs.Adult > 0)
                {
                    for (int i = 1; i <= searchInputs.Adult; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (searchInputs.Child > 0)
                {
                    for (int i = 1; i <= searchInputs.Child; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (searchInputs.Infant > 0)
                {
                    for (int i = 1; i <= searchInputs.Infant; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
            }
            XmlBuilder.Append("</PsgrAry>");
            XmlBuilder.Append("</PassengerType>");
            #endregion

            #region Segment details
            foreach (FlightSearchResults Flt in FltRes)
            {
                XmlBuilder.Append("<SegInfo>");
                XmlBuilder.Append("<FltSegAry>");
                XmlBuilder.Append("<FltSeg>");
                XmlBuilder.Append("<ClassPref/>");
                XmlBuilder.Append("<AirV>" + Flt.MarketingCarrier + "</AirV>");
                if (Flt.FlightIdentification.ToString().Trim().Length == 4)
                    XmlBuilder.Append("<FltNum>" + Flt.FlightIdentification.ToString().Trim() + "</FltNum>");
                else if (Flt.FlightIdentification.ToString().Trim().Length == 3)
                    XmlBuilder.Append("<FltNum>0" + Flt.FlightIdentification.ToString().Trim() + "</FltNum>");
                else if (Flt.FlightIdentification.ToString().Trim().Length == 2)
                    XmlBuilder.Append("<FltNum>00" + Flt.FlightIdentification.ToString().Trim() + "</FltNum>");
                else if (Flt.FlightIdentification.ToString().Trim().Length == 1)
                    XmlBuilder.Append("<FltNum>000" + Flt.FlightIdentification.ToString().Trim() + "</FltNum>");
                else
                    XmlBuilder.Append("<FltNum>" + Flt.FlightIdentification.ToString().Trim() + "</FltNum>");
                XmlBuilder.Append("<OpSuf/>");
                XmlBuilder.Append("<Dt>" + Flt.DepartureDate + "</Dt>");
                XmlBuilder.Append("<StartAirp>" + Flt.DepartureLocation + "</StartAirp>");
                XmlBuilder.Append("<EndAirp>" + Flt.ArrivalLocation + "</EndAirp>");
                XmlBuilder.Append("<StartTm/>");
                XmlBuilder.Append("<EndTm/>");
                XmlBuilder.Append("<AsBookedBIC>" + Flt.AdtRbd + "</AsBookedBIC>");
                //XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
                try
                {
                    if (Flt.DepartureDate.Trim() == Flt.ArrivalDate.Trim())
                    {
                        XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
                    }
                    else
                    {
                        XmlBuilder.Append("<DayChgInd>01</DayChgInd>");
                    }
                }
                catch (Exception ex)
                {
                    XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
                }
                if (FltRes.Count > 1)
                    XmlBuilder.Append("<Conx>Y</Conx>");
                else
                    XmlBuilder.Append("<Conx>N</Conx>");
                XmlBuilder.Append("</FltSeg>");
                XmlBuilder.Append("</FltSegAry>");
                XmlBuilder.Append("</SegInfo>");
            }
            #endregion

            #region Credentials with airline and Private and Publish Fare Option
            XmlBuilder.Append("<SegSelection>");
            XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
            XmlBuilder.Append("<SegRangeAry>");
            XmlBuilder.Append("<SegRange>");
            XmlBuilder.Append("<StartSeg>00</StartSeg>");
            XmlBuilder.Append("<EndSeg>00</EndSeg>");
            XmlBuilder.Append("<FareType>P</FareType>");
            XmlBuilder.Append("<PFQual>");
            XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
            XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");
            XmlBuilder.Append("<AirV>" + FltRes[0].ValiDatingCarrier.ToString() + "</AirV>");
            XmlBuilder.Append("<Acct/>");
            XmlBuilder.Append("<Contract/>");
            XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
            XmlBuilder.Append("<Type/>");
            XmlBuilder.Append("<PFTypeRestrict/>");
            XmlBuilder.Append("<AcctCodeRestrict/>");
            XmlBuilder.Append("<Spare1/>");
            XmlBuilder.Append("</PFQual>");
            XmlBuilder.Append("</SegRange>");
            XmlBuilder.Append("</SegRangeAry>");
            XmlBuilder.Append("</SegSelection>");
            #endregion

            XmlBuilder.Append("</ClassSpecificMods>");
            XmlBuilder.Append("</FareQuoteClassSpecific_12>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            //XmlBuilder.Append("<FareQuoteClassSpecific_12>");
            //XmlBuilder.Append("<FareInfo>");
            //XmlBuilder.Append("<ItinSeg>");
            //XmlBuilder.Append("<UniqueKey/>");
            //XmlBuilder.Append("<FltNum/>");
            //XmlBuilder.Append("<StartPt/>");
            //XmlBuilder.Append("<EndPt/>");
            //XmlBuilder.Append("<DayDifferential/>");
            //XmlBuilder.Append("<NoStopAtBoardPt/>");
            //XmlBuilder.Append("<BIC/>");
            //XmlBuilder.Append("</ItinSeg>");
            //XmlBuilder.Append("<ErrorCode/>");
            //XmlBuilder.Append("<RulesInfo>");
            //XmlBuilder.Append("<_ xmlns=''/>");
            //XmlBuilder.Append("</RulesInfo>");
            //XmlBuilder.Append("<EnhancedPrivateFare>");
            //XmlBuilder.Append("<_ xmlns=''/>");
            //XmlBuilder.Append("</EnhancedPrivateFare>");
            //XmlBuilder.Append("<GenQuoteDetails>");
            //XmlBuilder.Append("<UniqueKey/>");
            //XmlBuilder.Append("<QuoteDt/>");
            //XmlBuilder.Append("<BaseFareCurrency/>");
            //XmlBuilder.Append("<BaseFareAmt/>");
            //XmlBuilder.Append("<TotCurrency/>");
            //XmlBuilder.Append("<TotAmt/>");
            //XmlBuilder.Append("<TaxDataAry>");
            //XmlBuilder.Append("<_ xmlns=''/>");
            //XmlBuilder.Append("</TaxDataAry>");
            //XmlBuilder.Append("</GenQuoteDetails>");
            //XmlBuilder.Append("<InfoMsg>");
            //XmlBuilder.Append("<_ xmlns=''/>");
            //XmlBuilder.Append("</InfoMsg>");
            //XmlBuilder.Append("<PsgrTypes>");
            //XmlBuilder.Append("<UniqueKey/>");
            //XmlBuilder.Append("<PICReq/>");
            //XmlBuilder.Append("<RespPIC/>");
            //XmlBuilder.Append("<PICPsgrs/>");
            //XmlBuilder.Append("<PsgrNum/>");
            //XmlBuilder.Append("</PsgrTypes>");
            //XmlBuilder.Append("<PrevBICSegMapping>");
            //XmlBuilder.Append("<_ xmlns=''/>");
            //XmlBuilder.Append("</PrevBICSegMapping>");
            //XmlBuilder.Append("</FareInfo>");
            //XmlBuilder.Append("</FareQuoteClassSpecific_12>");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SS"));
            return XmlBuilder.ToString();
        }


        // for Gal FareRule
        public string FQMD_FullRules_23(string fareRule, string strConn, string ProviderUserId)
        {
            fareinfo objFI = new fareinfo();
            XDocument XDoc = XDocument.Parse(fareRule.Substring(fareRule.IndexOf("<FareQuoteClassSpecific_26"), (fareRule.IndexOf("</FareQuoteClassSpecific_26>")) - fareRule.IndexOf("<FareQuoteClassSpecific_26")).Replace("<FareQuoteClassSpecific_26 xmlns=\"\">", "").Trim());

            if (XDoc.Descendants("RulesInfo").Any() == true)
            {
                objFI.StartPt = XDoc.Descendants("StartPt").Any() == true ? XDoc.Descendants("StartPt").First().Value : " ";
                objFI.EndPt = XDoc.Descendants("EndPt").Any() == true ? XDoc.Descendants("EndPt").First().Value : " ";
                objFI.FirstTravDt = XDoc.Descendants("FirstTravDt").Any() == true ? XDoc.Descendants("FirstTravDt").First().Value : " ";
                objFI.AirV = XDoc.Descendants("AirV").Any() == true ? XDoc.Descendants("AirV").First().Value : " ";
                objFI.FIC = XDoc.Descendants("FIC").Any() == true ? XDoc.Descendants("FIC").First().Value : " ";
                objFI.TotFareComponent = XDoc.Descendants("TotFareComponent").Any() == true ? XDoc.Descendants("TotFareComponent").First().Value : " ";
                objFI.FareAmt = XDoc.Descendants("FareAmt").Any() == true ? XDoc.Descendants("FareAmt").First().Value : " ";
                objFI.RuleNumOrdinal = XDoc.Descendants("RuleNumOrdinal").Any() == true ? XDoc.Descendants("RuleNumOrdinal").First().Value : " ";
                objFI.RuleTextOrdinalNum = XDoc.Descendants("RuleTextOrdinalNum").Any() == true ? XDoc.Descendants("RuleTextOrdinalNum").First().Value : " ";
                objFI.OTWTransportingAirV = XDoc.Descendants("OTWTransportingAirV").Any() == true ? XDoc.Descendants("OTWTransportingAirV").First().Value : " ";
                objFI.Key = XDoc.Descendants("Key").Any() == true ? XDoc.Descendants("Key").First().Value : " ";
            }
            GetCredentials(strConn, ProviderUserId);
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SS", ""));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<FareQuoteMultiDisplay_23>"); //xmlns=''
            XmlBuilder.Append("<FareDisplayMods>");
            #region QueryHeader
            XmlBuilder.Append("<QueryHeader>");
            XmlBuilder.Append("<UniqueKey>0000</UniqueKey>");
            XmlBuilder.Append("<LangNum>00</LangNum>");
            XmlBuilder.Append("<Action>135</Action>");
            XmlBuilder.Append("<RetCRTOutput>N</RetCRTOutput>");
            XmlBuilder.Append("<NoMsg>N</NoMsg>");
            XmlBuilder.Append("<NoTrunc>Y</NoTrunc>");
            XmlBuilder.Append("<IMInd>N</IMInd>");
            XmlBuilder.Append("<FIPlus>N</FIPlus>");
            XmlBuilder.Append("<PEInd>N</PEInd>");
            XmlBuilder.Append("<NBInd>N</NBInd>");
            XmlBuilder.Append("<ActionOnlyInd>N</ActionOnlyInd>");
            XmlBuilder.Append("<TranslatePeriod>N</TranslatePeriod>");
            XmlBuilder.Append("<PIInd>N</PIInd>");
            XmlBuilder.Append("<IntFrame1>Y</IntFrame1>");
            XmlBuilder.Append("<SmartParsed>N</SmartParsed>");
            XmlBuilder.Append("<PDCodes>N</PDCodes>");
            XmlBuilder.Append("<BkDtOverride>N</BkDtOverride>");
            XmlBuilder.Append("<PFPWInd>Y</PFPWInd>");
            XmlBuilder.Append("<PFPQInd>N</PFPQInd>");
            XmlBuilder.Append("</QueryHeader>");
            #endregion
            #region FollowUpEntries
            XmlBuilder.Append("<FollowUpEntries>");
            XmlBuilder.Append("<UniqueKey>0000</UniqueKey>");
            XmlBuilder.Append("<QuoteNum>1</QuoteNum>");
            XmlBuilder.Append("<Spare1>N</Spare1>");
            XmlBuilder.Append("<AllParaReqind>Y</AllParaReqind>");
            XmlBuilder.Append("<SumRuleReqInd>N</SumRuleReqInd>");
            XmlBuilder.Append("<FulltextoptInd>N</FulltextoptInd>");
            XmlBuilder.Append("<Spare2>NNNN</Spare2>");
            XmlBuilder.Append("<Text/>");
            XmlBuilder.Append("</FollowUpEntries>");
            #endregion
            #region RulesInfo

            XmlBuilder.Append("<RulesInfo>");
            XmlBuilder.Append("<UniqueKey>1</UniqueKey>");
            XmlBuilder.Append("<QuoteNum>1</QuoteNum>");
            XmlBuilder.Append("<FareNum>1</FareNum>");
            XmlBuilder.Append("<FareRuleInfo>Y</FareRuleInfo>");
            XmlBuilder.Append("<PermittedDisc>N</PermittedDisc>");
            XmlBuilder.Append("<DiscAdultFare>N</DiscAdultFare>");
            XmlBuilder.Append("<GenFare>N</GenFare>");
            XmlBuilder.Append("<NetFare>N</NetFare>");
            XmlBuilder.Append("<FareRestricted>N</FareRestricted>");
            XmlBuilder.Append("<NGGFIntlInd>Y</NGGFIntlInd>");
            XmlBuilder.Append("<Spare1>N</Spare1>");
            XmlBuilder.Append("<StartPt>" + objFI.StartPt + "</StartPt>");
            XmlBuilder.Append("<EndPt>" + objFI.EndPt + "</EndPt>");
            XmlBuilder.Append("<FirstTravDt>" + objFI.FirstTravDt + "</FirstTravDt>");
            XmlBuilder.Append("<AirV>" + objFI.AirV + "</AirV>");
            XmlBuilder.Append("<FIC>" + objFI.FIC + "</FIC>");
            XmlBuilder.Append("<TotFareComponent>" + objFI.TotFareComponent + "</TotFareComponent>");
            XmlBuilder.Append("<Currency>INR</Currency>");
            XmlBuilder.Append("<DecPos>0</DecPos>");
            XmlBuilder.Append("<FareAmt>" + objFI.FareAmt + "</FareAmt>");
            XmlBuilder.Append("<RuleSupplierID>0</RuleSupplierID>");
            XmlBuilder.Append("<RuleNumOrdinal>" + objFI.RuleNumOrdinal + "</RuleNumOrdinal>");
            XmlBuilder.Append("<FareTariffNum>012E</FareTariffNum>");
            XmlBuilder.Append("<RuleTextOrdinalNum>" + objFI.RuleTextOrdinalNum + "</RuleTextOrdinalNum>");
            XmlBuilder.Append("<RulesApply>Y</RulesApply>");
            XmlBuilder.Append("<RtesApply>Y</RtesApply>");
            XmlBuilder.Append("<NoRulesExist>N</NoRulesExist>");
            XmlBuilder.Append("<Spare2>NNNNN</Spare2>");
            XmlBuilder.Append("<DBInd>N</DBInd>");
            XmlBuilder.Append("<HostUseOnly91>N</HostUseOnly91>");
            XmlBuilder.Append("<PFQuoted>N</PFQuoted>");
            XmlBuilder.Append("<Spare3>YNNNN</Spare3>");
            XmlBuilder.Append("<DBID/>");
            XmlBuilder.Append("<FareRuleInfoYQual>");
            XmlBuilder.Append("<GlobDirOrdinal>EH</GlobDirOrdinal>");
            XmlBuilder.Append("<HIFCity1/>");
            XmlBuilder.Append("<HIFCity2/>");
            XmlBuilder.Append("<MileSurchargeRtgInd>7</MileSurchargeRtgInd>");
            XmlBuilder.Append("<FlownMileComponent>0</FlownMileComponent>");
            XmlBuilder.Append("<MPMComponent>0</MPMComponent>");
            XmlBuilder.Append("<DifBetween>0</DifBetween>");
            XmlBuilder.Append("<ExtraMileCity1/>");
            XmlBuilder.Append("<ExtraMileCity2/>");
            XmlBuilder.Append("<ExtraMileCity3/>");
            XmlBuilder.Append("<OTWTransportingAirV>" + objFI.OTWTransportingAirV + "</OTWTransportingAirV>");
            XmlBuilder.Append("<ComponentAirV1/>");
            XmlBuilder.Append("<ComponentAirV2/>");
            XmlBuilder.Append("<Key>" + objFI.Key + "</Key>");
            XmlBuilder.Append("</FareRuleInfoYQual>");
            XmlBuilder.Append("</RulesInfo>");
            #endregion
            XmlBuilder.Append("</FareDisplayMods>");
            XmlBuilder.Append("</FareQuoteMultiDisplay_23>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append(SoapEnd("SS"));
            return XmlBuilder.ToString();
        }
        public string FQMD_ResponseResult_23(string StrResult, string strConn, string Sector)
        {
            List<string> UniqueKey = new List<string>();
            List<string> UniqueVlaue = new List<string>();
            fareinfo objFI = new fareinfo();
            XDocument XDoc = XDocument.Parse(StrResult.Substring(StrResult.IndexOf("<FareQuoteMultiDisplay_23"), (StrResult.IndexOf("</FareQuoteMultiDisplay_23>")) - StrResult.IndexOf("<FareQuoteMultiDisplay_23")).Replace("<FareQuoteMultiDisplay_23 xmlns=\"\">", "").Trim());

            if (XDoc.Descendants("RulesData").Any() == true)
            {
                foreach (XElement element in XDoc.Descendants("RulesData").Elements("UniqueKey"))
                {
                    UniqueKey.Add(element.Value);
                }
                UniqueVlaue = UniqueKey.Distinct().ToList();
                if (UniqueVlaue.Count > 0)
                {
                    for (int i = 0; i < UniqueVlaue.Count; i++)
                    {
                        foreach (XElement element in XDoc.Descendants("RulesData").Where(x => x.Element("UniqueKey").Value == UniqueVlaue[i]).Select(x => x.Element("RulesText")).ToList())
                        {
                            objFI.RulesText += " " + element.Value;
                        }
                        if (objFI.RulesText == " " || objFI.RulesText == null)
                        {
                            objFI.RulesText += "Fare Not Available!!";
                        }
                        objFI.RulesText = objFI.RulesText + "<br/>";
                    }
                }
            }
            objFI.RulesText = "<div id='" + Sector + "' class='tabcontent'><h3>" + Sector + "</h3><p>" + objFI.RulesText + "</p></div>";
            return objFI.RulesText.ToString();
        }
        private void GetCredentials(string conn, string ProviderUserId)
        {
            //string UserId
            ProviderList = Data.GetProviderCrd(conn);
            // ProviderList = Data.GetGalBookingCrd(connectionString, strProvider, strTrip, strVc);
            if (!string.IsNullOrEmpty(ProviderUserId))
            {
                url = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].AvailabilityURL;
                Userid = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].UserID;
                Pwd = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].Password;
                pcc = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].CarrierAcc;
                strCorporateID = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].CorporateID;
            }
            else
            {
                url = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                Userid = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                Pwd = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Password;
                pcc = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc;
                strCorporateID = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID;
            }

        }
        public string FQCSReq_FareRule(Dictionary<string, object> Rec, string StrConn)
        {
            //string ProviderUserId
            // List<FlightSearchResults> FltRes = new List<FlightSearchResults>();
            FlightSearchResults FltRes = new FlightSearchResults();
            FltRes = SetVal(Rec);
            //GetCredentials(StrConn, ProviderUserId);
            GetCredentials(StrConn, Convert.ToString(Rec["SearchId"]));
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SS", ""));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<FareQuoteClassSpecific_26>");
            XmlBuilder.Append("<ClassSpecificMods>");
            XmlBuilder.Append("<FQAMods>");
            XmlBuilder.Append("<FQARequest>Y</FQARequest>");
            XmlBuilder.Append("</FQAMods>");

            #region Pax Information (total pax<=7 for FQSBB Search)
            XmlBuilder.Append("<PassengerType>");
            XmlBuilder.Append("<PsgrAry>");
            if ((FltRes.Adult + FltRes.Child + FltRes.Infant) > 7)
            {
                if (FltRes.Adult > 0)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("</Psgr>");
                    }
                }
                if (FltRes.Child > 0)
                {
                    for (int i = 4; i <= 6; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("</Psgr>");
                    }

                }
                if (FltRes.Infant > 0)
                {
                    XmlBuilder.Append("<Psgr>");
                    XmlBuilder.Append("<LNameNum>7</LNameNum>");
                    XmlBuilder.Append("<PsgrNum>7</PsgrNum>");
                    XmlBuilder.Append("<AbsNameNum>7</AbsNameNum>");
                    XmlBuilder.Append("<PTC>INF</PTC>");
                    XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                    XmlBuilder.Append("</Psgr>");
                }
            }
            else
            {
                int j = 1;
                if (FltRes.Adult > 0)
                {
                    for (int i = 1; i <= FltRes.Adult; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + i + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + i + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + i + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (FltRes.Child > 0)
                {
                    for (int i = 1; i <= FltRes.Child; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>09</Age>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
                if (FltRes.Infant > 0)
                {
                    for (int i = 1; i <= FltRes.Infant; i++)
                    {
                        XmlBuilder.Append("<Psgr>");
                        XmlBuilder.Append("<LNameNum>" + j + "</LNameNum>");
                        XmlBuilder.Append("<PsgrNum>" + j + "</PsgrNum>");
                        XmlBuilder.Append("<AbsNameNum>" + j + "</AbsNameNum>");
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("</Psgr>");
                        j++;
                    }
                }
            }
            XmlBuilder.Append("</PsgrAry>");
            XmlBuilder.Append("</PassengerType>");
            #endregion

            #region Segment details
            //foreach (FlightSearchResults Flt in FltRes)
            //{
            XmlBuilder.Append("<SegInfo>");
            XmlBuilder.Append("<FltSegAry>");
            XmlBuilder.Append("<FltSeg>");
            XmlBuilder.Append("<ClassPref/>");
            XmlBuilder.Append("<AirV>" + FltRes.MarketingCarrier + "</AirV>");
            if (FltRes.FlightIdentification.ToString().Trim().Length == 4)
                XmlBuilder.Append("<FltNum>" + FltRes.FlightIdentification.ToString().Trim() + "</FltNum>");
            else if (FltRes.FlightIdentification.ToString().Trim().Length == 3)
                XmlBuilder.Append("<FltNum>0" + FltRes.FlightIdentification.ToString().Trim() + "</FltNum>");
            else if (FltRes.FlightIdentification.ToString().Trim().Length == 2)
                XmlBuilder.Append("<FltNum>00" + FltRes.FlightIdentification.ToString().Trim() + "</FltNum>");
            else if (FltRes.FlightIdentification.ToString().Trim().Length == 1)
                XmlBuilder.Append("<FltNum>000" + FltRes.FlightIdentification.ToString().Trim() + "</FltNum>");
            else
                XmlBuilder.Append("<FltNum>" + FltRes.FlightIdentification.ToString().Trim() + "</FltNum>");
            XmlBuilder.Append("<OpSuf/>");
            XmlBuilder.Append("<Dt>" + FltRes.DepartureDate + "</Dt>");
            XmlBuilder.Append("<StartAirp>" + FltRes.DepartureLocation + "</StartAirp>");
            XmlBuilder.Append("<EndAirp>" + FltRes.ArrivalLocation + "</EndAirp>");
            XmlBuilder.Append("<StartTm/>");
            XmlBuilder.Append("<EndTm/>");
            XmlBuilder.Append("<AsBookedBIC>" + FltRes.AdtRbd + "</AsBookedBIC>");
            XmlBuilder.Append("<DayChgInd>00</DayChgInd>");
            //if (FltRes.Count > 1)
            //    XmlBuilder.Append("<Conx>Y</Conx>");
            //else
            //    XmlBuilder.Append("<Conx>N</Conx>");
            XmlBuilder.Append("</FltSeg>");
            XmlBuilder.Append("</FltSegAry>");
            XmlBuilder.Append("</SegInfo>");
            //}
            #endregion

            #region Credentials with airline and Private and Publish Fare Option
            XmlBuilder.Append("<SegSelection>");
            XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
            XmlBuilder.Append("<SegRangeAry>");
            XmlBuilder.Append("<SegRange>");
            XmlBuilder.Append("<StartSeg>00</StartSeg>");
            XmlBuilder.Append("<EndSeg>00</EndSeg>");
            XmlBuilder.Append("<FareType>P</FareType>");
            XmlBuilder.Append("<PFQual>");
            if (!string.IsNullOrEmpty(Convert.ToString(Rec["SearchId"])))
            {
                string ProviderUserId = Convert.ToString(Rec["SearchId"]);
                XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].Provider + "</CRSInd>");
                XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].CarrierAcc + "</PCC>");//63HZ
            }
            else
            {
                XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
                XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");//63HZ
            }
            XmlBuilder.Append("<AirV>" + FltRes.ValiDatingCarrier.ToString() + "</AirV>");
            // XmlBuilder.Append("<Acct/>");
            XmlBuilder.Append("<Contract/>");
            XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
            XmlBuilder.Append("<Type/>");
            XmlBuilder.Append("<PFTypeRestrict/>");
            XmlBuilder.Append("<AcctCodeRestrict/>");
            XmlBuilder.Append("<Spare1/>");
            XmlBuilder.Append("</PFQual>");
            XmlBuilder.Append("</SegRange>");
            XmlBuilder.Append("</SegRangeAry>");
            XmlBuilder.Append("</SegSelection>");
            #endregion

            XmlBuilder.Append("</ClassSpecificMods>");
            XmlBuilder.Append("</FareQuoteClassSpecific_26>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");

            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append(SoapEnd("SS"));
            return XmlBuilder.ToString();
        }
        public static FlightSearchResults SetVal(Dictionary<string, object> Row)
        {
            FlightSearchResults obj = new FlightSearchResults();
            int length = Row.Count;
            int i = 0;
            Type t;
            //List<object> a = new List<object>();
            Dictionary<string, object> InsertRow = new Dictionary<string, object>();
            try
            {
                // Add Records to Final Dictionary  by Removing Null values.
                foreach (KeyValuePair<string, object> pair in Row)
                {
                    if (pair.Value == null)
                    {
                        InsertRow.Add(pair.Key, "");
                    }
                    else
                    {
                        InsertRow.Add(pair.Key, pair.Value);
                    }
                }
                //Set Values in the Object
                //Note :-Insert TrackId generated by Random Function.
                //obj.DISTRID = InsertRow["FltInfoID"].ToString();
                //obj.DISTRID = InsertRow["DISTRID"].ToString(); //From Session
                obj.ProviderUserID = InsertRow["SearchId"].ToString();
                obj.OrgDestFrom = InsertRow["OrgDestFrom"].ToString();
                obj.OrgDestTo = InsertRow["OrgDestTo"].ToString();
                obj.DepartureLocation = InsertRow["DepartureLocation"].ToString();
                obj.DepartureCityName = InsertRow["DepartureCityName"].ToString();
                obj.DepAirportCode = InsertRow["DepAirportCode"].ToString();
                obj.DepartureAirportName = InsertRow["DepartureTerminal"].ToString();
                obj.ArrivalLocation = InsertRow["ArrivalLocation"].ToString();
                obj.ArrivalCityName = InsertRow["ArrivalCityName"].ToString();
                obj.ArrAirportCode = InsertRow["ArrAirportCode"].ToString();
                obj.ArrivalTerminal = InsertRow["ArrivalTerminal"].ToString();
                obj.DepartureDate = InsertRow["DepartureDate"].ToString();
                obj.Departure_Date = InsertRow["Departure_Date"].ToString();
                obj.DepartureTime = InsertRow["DepartureTime"].ToString();
                obj.ArrivalDate = InsertRow["ArrivalDate"].ToString();
                obj.Arrival_Date = InsertRow["Arrival_Date"].ToString();
                obj.ArrivalTime = InsertRow["ArrivalTime"].ToString();
                obj.Adult = Convert.ToInt32(InsertRow["Adult"].ToString());
                obj.Child = Convert.ToInt32(InsertRow["Child"].ToString());
                obj.Infant = Convert.ToInt32(InsertRow["Infant"].ToString());
                obj.TotPax = obj.Adult + obj.Child + obj.Infant;
                obj.MarketingCarrier = InsertRow["MarketingCarrier"].ToString();
                obj.OperatingCarrier = InsertRow["OperatingCarrier"].ToString();
                obj.FlightIdentification = InsertRow["FlightIdentification"].ToString();
                obj.ValiDatingCarrier = InsertRow["ValiDatingCarrier"].ToString();
                obj.AirLineName = InsertRow["AirLineName"].ToString();
                obj.AvailableSeats = InsertRow["AvailableSeats"].ToString();
                obj.AdtCabin = InsertRow["AdtCabin"].ToString();
                obj.ChdCabin = InsertRow["ChdCabin"].ToString();
                obj.InfCabin = InsertRow["InfCabin"].ToString();
                obj.AdtRbd = InsertRow["AdtRbd"].ToString();
                obj.ChdRbd = InsertRow["ChdRbd"].ToString();
                obj.InfRbd = InsertRow["InfRbd"].ToString();
                obj.RBD = InsertRow["RBD"].ToString();
                obj.AdtFareType = InsertRow["AdtFare"].ToString();
                obj.AdtFarebasis = InsertRow["AdtBfare"].ToString();
                obj.AdtTax = float.Parse(InsertRow["AdtTax"].ToString());
                obj.ChdFare = float.Parse(InsertRow["ChdFare"].ToString());
                if (InsertRow.ContainsKey("ChdBFare"))
                {
                    obj.ChdBFare = float.Parse(InsertRow["ChdBFare"].ToString());
                }
                obj.ChdTax = float.Parse(InsertRow["ChdTax"].ToString());
                obj.InfFare = float.Parse(InsertRow["InfFare"].ToString());
                obj.InfBfare = float.Parse(InsertRow["InfBfare"].ToString());
                if (InsertRow.ContainsKey("InfBfare"))
                {
                    obj.InfBfare = float.Parse(InsertRow["InfBfare"].ToString());
                }
                obj.InfTax = float.Parse(InsertRow["InfTax"].ToString());
                if (InsertRow.ContainsKey("TotBfare"))
                {
                    obj.TotBfare = float.Parse(InsertRow["TotBfare"].ToString());
                }
                if (InsertRow.ContainsKey("TotalFare"))
                {
                    obj.TotalFare = float.Parse(InsertRow["TotalFare"].ToString()); //TotalFare
                }

                obj.TotalTax = float.Parse(InsertRow["TotalTax"].ToString());
                obj.NetFare = float.Parse(InsertRow["NetFare"].ToString());
                obj.STax = float.Parse(InsertRow["STax"].ToString());
                obj.TFee = float.Parse(InsertRow["TFee"].ToString());
                if (InsertRow.ContainsKey("TotDis"))
                {
                    obj.TotDis = float.Parse(InsertRow["TotDis"].ToString());
                }

                obj.Searchvalue = InsertRow["Searchvalue"].ToString();
                // obj.LineNumber = Convert.ToInt32(InsertRow["LineNumber"].ToString());
                obj.Leg = Convert.ToInt32(InsertRow["Leg"].ToString());
                obj.Flight = InsertRow["Flight"].ToString();
                obj.Provider = InsertRow["Provider"].ToString();
                if (InsertRow.ContainsKey("TotDur"))
                {
                    obj.TotDur = InsertRow["TotDur"].ToString();
                }
                obj.TripType = InsertRow["TripType"].ToString();
                obj.EQ = InsertRow["EQ"].ToString();
                obj.Stops = InsertRow["Stops"].ToString();
                obj.Trip = InsertRow["Trip"].ToString();
                obj.Sector = InsertRow["Sector"].ToString();
                obj.TripCnt = InsertRow["TripCnt"].ToString();
                obj.ADTAdminMrk = float.Parse(InsertRow["ADTAdminMrk"].ToString());
                obj.ADTAgentMrk = float.Parse(InsertRow["ADTAgentMrk"].ToString());
                obj.CHDAdminMrk = float.Parse(InsertRow["CHDAdminMrk"].ToString());
                obj.CHDAgentMrk = float.Parse(InsertRow["CHDAgentMrk"].ToString());
                obj.InfAdminMrk = float.Parse(InsertRow["InfAdminMrk"].ToString());
                obj.InfAgentMrk = float.Parse(InsertRow["InfAgentMrk"].ToString());

                obj.IATAComm = float.Parse(InsertRow["IATAComm"].ToString());
                obj.AdtFareType = InsertRow["AdtFareType"].ToString();

                obj.AdtFareType = InsertRow["AdtFarebasis"].ToString();
                if (InsertRow.ContainsKey("ChdfareType"))
                {
                    obj.ChdfareType = InsertRow["ChdfareType"].ToString();
                }
                if (InsertRow.ContainsKey("ChdFarebasis"))
                {
                    obj.ChdFarebasis = InsertRow["ChdFarebasis"].ToString();
                }
                obj.InfFareType = InsertRow["InfFareType"].ToString();
                obj.InfFarebasis = InsertRow["InfFarebasis"].ToString();
                if (InsertRow.ContainsKey("fareBasis"))
                {
                    obj.fareBasis = InsertRow["fareBasis"].ToString();
                }
                obj.FBPaxType = InsertRow["FBPaxType"].ToString();
                obj.AdtFSur = float.Parse(InsertRow["AdtFSur"].ToString());
                obj.ChdFSur = float.Parse(InsertRow["ChdFSur"].ToString());
                obj.InfFSur = float.Parse(InsertRow["InfFSur"].ToString());
                obj.TotalFuelSur = float.Parse(InsertRow["AdtFSur"].ToString()) + float.Parse(InsertRow["ChdFSur"].ToString()) + float.Parse(InsertRow["InfFSur"].ToString());
                obj.sno = InsertRow["sno"].ToString();
                obj.depdatelcc = InsertRow["depdatelcc"].ToString();
                obj.arrdatelcc = InsertRow["arrdatelcc"].ToString();
                obj.OriginalTF = float.Parse(InsertRow["OriginalTF"].ToString());
                obj.OriginalTT = float.Parse(InsertRow["OriginalTT"].ToString());
                //obj.TRACK_ID = TID;  //TrackId by Random Function //InsertRow["Track_id"].ToString();
                //Fare BreakUp Fields 
                if (InsertRow.ContainsKey("FType"))
                {
                    obj.FType = InsertRow["FType"].ToString();
                }
                //obj.ADT_TAX = Append_Tax(InsertRow["AdtFSur"].ToString(), InsertRow["AdtYR"].ToString(), InsertRow["AdtWO"].ToString(), InsertRow["AdtIN"].ToString(), InsertRow["AdtQ"].ToString(), InsertRow["AdtJN"].ToString(), InsertRow["AdtOT"].ToString());
                obj.AdtOT = float.Parse(InsertRow["AdtOT"].ToString());
                obj.AdtSrvTax = float.Parse(InsertRow["AdtSrvTax"].ToString());
                obj.AdtSrvTax1 = float.Parse(InsertRow["AdtSrvTax1"].ToString());
                //obj.CHD_TAX = Append_Tax(InsertRow["ChdFSur"].ToString(), InsertRow["ChdYR"].ToString(), InsertRow["ChdWO"].ToString(), InsertRow["ChdIN"].ToString(), InsertRow["ChdQ"].ToString(), InsertRow["ChdJN"].ToString(), InsertRow["ChdOT"].ToString());
                obj.ChdOT = float.Parse(InsertRow["ChdOT"].ToString());
                obj.ChdSrvTax = float.Parse(InsertRow["ChdSrvTax"].ToString());
                obj.ChdSrvTax1 = float.Parse(InsertRow["ChdSrvTax1"].ToString());
                //obj.INF_TAX = Append_Tax(InsertRow["InfFSur"].ToString(), InsertRow["InfYR"].ToString(), InsertRow["InfWO"].ToString(), InsertRow["InfIN"].ToString(), InsertRow["InfQ"].ToString(), InsertRow["InfJN"].ToString(), InsertRow["InfOT"].ToString());
                obj.InfOT = float.Parse(InsertRow["InfOT"].ToString());
                obj.InfSrvTax = float.Parse(InsertRow["InfSrvTax"].ToString());
                obj.STax = float.Parse(InsertRow["STax"].ToString());//Convert.ToDecimal(obj.ADTSRVTAX) + Convert.ToDecimal(obj.CHDSRVTAX) + Convert.ToDecimal(obj.INFSRVTAX);
                // obj.TF = Convert.ToDecimal(InsertRow["TFee"].ToString());
                // obj.TC = Convert.ToDecimal(InsertRow["TotMrkUp"].ToString());//((Convert.ToDecimal(obj.ADTADMINMRK) + Convert.ToDecimal(obj.ADTAGENTMRK)) * obj.ADULT) + ((Convert.ToDecimal(obj.CHDADMINMRK) + Convert.ToDecimal(obj.CHDAGENTMRK)) * obj.CHILD) + ((Convert.ToDecimal(obj.INFADMINMRK) + Convert.ToDecimal(obj.INFAGENTMRK)) * obj.INFANT);
                obj.AdtTds = float.Parse(InsertRow["AdtTds"].ToString());
                obj.ChdTds = float.Parse(InsertRow["ChdTds"].ToString());
                obj.InfTds = float.Parse(InsertRow["InfTds"].ToString());

                if (InsertRow.ContainsKey("AdtDiscount"))
                {
                    obj.AdtDiscount = float.Parse(InsertRow["AdtDiscount"].ToString());
                }
                if (InsertRow.ContainsKey("AdtDiscount1"))
                {
                    obj.AdtDiscount1 = float.Parse(InsertRow["AdtDiscount1"].ToString());
                }
                if (InsertRow.ContainsKey("ChdDiscount"))
                {
                    obj.ChdDiscount = float.Parse(InsertRow["ChdDiscount"].ToString());
                }
                if (InsertRow.ContainsKey("ChdDiscount1"))
                {
                    obj.ChdDiscount1 = float.Parse(InsertRow["ChdDiscount1"].ToString());
                }
                if (InsertRow.ContainsKey("InfDiscount"))
                {
                    obj.InfDiscount = float.Parse(InsertRow["InfDiscount"].ToString());
                }
                obj.AdtCB = float.Parse(InsertRow["AdtCB"].ToString());
                obj.ChdCB = float.Parse(InsertRow["ChdCB"].ToString());
                obj.InfCB = float.Parse(InsertRow["InfCB"].ToString());
                obj.ElectronicTicketing = InsertRow["ElectronicTicketing"].ToString();
                obj.AdtMgtFee = float.Parse(InsertRow["AdtMgtFee"].ToString());
                obj.ChdMgtFee = float.Parse(InsertRow["ChdMgtFee"].ToString());
                obj.InfMgtFee = float.Parse(InsertRow["InfMgtFee"].ToString());
                obj.TotMgtFee = float.Parse(InsertRow["TotMgtFee"].ToString());
                //obj.User_id = InsertRow["User_id"].ToString();
                obj.IsCorp = Convert.ToBoolean(InsertRow["IsCorp"].ToString());
                if (InsertRow.ContainsKey("ProductDetailQualifier"))
                {
                    obj.ProductDetailQualifier = InsertRow["ProductDetailQualifier"].ToString();
                }

            }
            catch (Exception ex)
            {
                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
            }

            return obj;
        }


        #region Reprice (Date 16-11-2017 Devesh)

        #region Create cancel fare file Request
        public string FareQuoteCancelModsReq(string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<FareQuoteCancel_6_0>");
            XmlBuilder.Append("<FareQuoteCancelMods>");
            XmlBuilder.Append("<ItemAry>");
            XmlBuilder.Append("<Item>");
            XmlBuilder.Append("<BlkInd>A</BlkInd>");
            XmlBuilder.Append("<SpecificQual>");
            XmlBuilder.Append("<RelFare>0</RelFare>");
            XmlBuilder.Append("</SpecificQual>");
            XmlBuilder.Append("</Item>");
            XmlBuilder.Append("</ItemAry>");
            XmlBuilder.Append("</FareQuoteCancelMods>");
            XmlBuilder.Append("</FareQuoteCancel_6_0>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append(SoapEnd("SONS"));
            return XmlBuilder.ToString();
        }
        #endregion

        #region Create save pnr Request
        public string PNRBFManagementSavePnrReq(string RcvdFrom, string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_43>");
            XmlBuilder.Append("<EndTransactionMods>");
            XmlBuilder.Append("<EndTransactRequest>");
            XmlBuilder.Append("<ETInd>R</ETInd>");
            XmlBuilder.Append("<RcvdFrom>" + RcvdFrom + "</RcvdFrom>");
            XmlBuilder.Append("</EndTransactRequest>");
            XmlBuilder.Append("</EndTransactionMods>");
            XmlBuilder.Append("</PNRBFManagement_43>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append(SoapEnd("SONS"));
            return XmlBuilder.ToString();
        }
        #endregion

        #region Create reprice pnr Request
        public string PNRBFManagementRepricePnrReq(DataSet SlcFltDs, DataSet FltHdr, DataSet FltPaxDs, DataSet TktDS, bool WOCHD, bool CHDOnly, bool InfWIDiffFBC, string strCode, bool PrivateFare, string Token)
        {
            int Adt = 0, Chd = 0, Inf = 0;
            Adt = int.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString());
            Chd = int.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString());
            Inf = int.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString());
            #region New Code
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_43>");
            //XmlBuilder.Append("<StorePriceMods>");
            //XmlBuilder.Append("<AssocSegs>");//Not
            //XmlBuilder.Append("<SegNumAry>");//Not
            //XmlBuilder.Append("<SegNum>00</SegNum>");//Not
            //XmlBuilder.Append("</SegNumAry>");//Not
            //XmlBuilder.Append("</AssocSegs>");//Not
            //XmlBuilder.Append("<PassengerType>");
            //XmlBuilder.Append("<PsgrAry>");
            //XmlBuilder.Append("<Psgr>");
            //XmlBuilder.Append("<LNameNum>01</LNameNum>");
            //XmlBuilder.Append("<PsgrNum>01</PsgrNum>");
            //XmlBuilder.Append("<AbsNameNum>01</AbsNameNum>");
            //XmlBuilder.Append("<PTC>ADT</PTC>");
            //XmlBuilder.Append("</Psgr>");
            //XmlBuilder.Append("</PsgrAry>");
            //XmlBuilder.Append("</PassengerType>");
            #region StorePriceMods
            XmlBuilder.Append("<StorePriceMods>");

            #region SegSelection
            XmlBuilder.Append("<SegSelection>");
            XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
            XmlBuilder.Append("<SegRangeAry>");
            XmlBuilder.Append("<SegRange>");
            XmlBuilder.Append("<StartSeg>00</StartSeg>");
            XmlBuilder.Append("<EndSeg>00</EndSeg>");
            XmlBuilder.Append("<FareType>P</FareType>");
            XmlBuilder.Append("<PFQual>");
            XmlBuilder.Append("<CRSInd>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Provider + "</CRSInd>");
            XmlBuilder.Append("<PCC>" + ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc + "</PCC>");


            try
            {
                //if (PrivateFare == true)
                //{
                #region Promotional Code
                if (Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]).Length > 1)
                {
                    XmlBuilder.Append("<Acct>" + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]) + "</Acct>");
                    XmlBuilder.Append("<Contract/>");
                    XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");
                }
                else
                {
                    STD.DAL.FltDeal objDeal = new STD.DAL.FltDeal(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                    DataTable PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", SlcFltDs.Tables[0].Rows[0]["Trip"].ToString().Trim(), "", SlcFltDs.Tables[0].Rows[0]["User_id"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["Track_id"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["DepartureLocation"].ToString().Trim(), SlcFltDs.Tables[0].Rows[0]["ArrivalLocation"].ToString().Trim(), "", "");
                    DataRow[] PCRow = { };
                    if (PFCodeDt.Rows.Count > 0)
                    {
                        PCRow = PFCodeDt.Select("AirCode='" + SlcFltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' and AppliedOn='BOOK'");
                        if (PCRow.Count() <= 0)
                        {
                            PCRow = PFCodeDt.Select("AirCode='ALL' and AppliedOn='BOOK'");
                            //PCRow = PFCodeDt.Select("AirCode='" + FltDs.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim() + "' or AirCode='ALL'");
                        }
                        if (PCRow.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(PCRow[0]["D_T_Code"].ToString()))
                            {
                                XmlBuilder.Append("<Acct>" + PCRow[0]["D_T_Code"].ToString() + "</Acct>");
                                XmlBuilder.Append("<Contract/>");
                                XmlBuilder.Append("<PublishedFaresInd>N</PublishedFaresInd>");

                            }
                            else
                            {
                                XmlBuilder.Append("<Acct/>");
                                XmlBuilder.Append("<Contract/>");
                                XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

                            }
                        }
                        else
                        {
                            XmlBuilder.Append("<Acct/>");
                            XmlBuilder.Append("<Contract/>");
                            XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

                        }
                    }
                    else
                    {
                        XmlBuilder.Append("<Acct/>");
                        XmlBuilder.Append("<Contract/>");
                        XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

                    }
                }


                #endregion
                //}
                //else
                //{
                //    XmlBuilder.Append("<Acct/>");
                //    XmlBuilder.Append("<Contract/>");
                //    XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");
                //}
            }
            catch
            {
                XmlBuilder.Append("<Acct/>");
                XmlBuilder.Append("<Contract/>");
                XmlBuilder.Append("<PublishedFaresInd>Y</PublishedFaresInd>");

            }


            XmlBuilder.Append("<Type>A</Type>");
            XmlBuilder.Append("</PFQual>");
            XmlBuilder.Append("</SegRange>");
            XmlBuilder.Append("</SegRangeAry>");
            XmlBuilder.Append("</SegSelection>");
            #endregion

            #region PassengerType
            XmlBuilder.Append("<PassengerType>");
            XmlBuilder.Append("<PsgrAry>");
            int count = 0;

            if ((WOCHD == true) && (CHDOnly == true))
                count = Chd;
            //else if ((WOCHD == true) && (CHDOnly == false))
            //    count = Adt + Chd;
            else if ((WOCHD == true) && (CHDOnly == false))
                count = Adt + Inf;
            else
                count = Adt + Chd + Inf;
            if (InfWIDiffFBC == true)
                count = count - Inf;

            for (int i = 1; i <= count; i++)
            {

                XmlBuilder.Append("<Psgr>");
                XmlBuilder.Append("<LNameNum>0" + i.ToString() + "</LNameNum>");
                XmlBuilder.Append("<PsgrNum>0" + i.ToString() + "</PsgrNum>");
                XmlBuilder.Append("<AbsNameNum>0" + i.ToString() + "</AbsNameNum>");
                if (CHDOnly == true)
                {
                    XmlBuilder.Append("<PTC>CNN</PTC>");
                    XmlBuilder.Append("<Age>05</Age>");
                }

                else
                {
                    if (i <= Adt)
                    {
                        XmlBuilder.Append("<PTC>ADT</PTC>");
                        XmlBuilder.Append("<Age/>");
                    }
                    else if ((i > Adt) && (i <= (Adt + Chd)))
                    {
                        XmlBuilder.Append("<PTC>CNN</PTC>");
                        XmlBuilder.Append("<Age>" + Utility.ConvertToAgeFromDOB(FltPaxDs.Tables[0].Rows[i - 1]["DOB"].ToString()) + "</Age>");
                    }
                    else if ((i > (Adt + Chd)) && (i <= count))
                    {
                        XmlBuilder.Append("<PTC>INF</PTC>");
                        XmlBuilder.Append("<PricePTCOnly>Y</PricePTCOnly>");
                        XmlBuilder.Append("<Age/>");
                    }
                }
                XmlBuilder.Append("</Psgr>");
            }

            XmlBuilder.Append("</PsgrAry>");
            XmlBuilder.Append("</PassengerType>");
            #endregion

            XmlBuilder.Append("<CommissionMod>");
            XmlBuilder.Append("<Percent>" + SlcFltDs.Tables[0].Rows[0]["IATAComm"].ToString() + "</Percent>");
            XmlBuilder.Append("</CommissionMod>");

            try
            {
                //mordified by abhilash 13-jul-2013
                //this is for deal code or net remittance code
                if (strCode.Contains("PNR") && (strCode.Contains("DEALCODE") || strCode.Contains("NETREMITTANCE")))
                {
                    XmlBuilder.Append("<AccountingInfo>");
                    XmlBuilder.Append("<Info>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Info>");
                    XmlBuilder.Append("</AccountingInfo>");
                }
                //end deal code or net remittance code
                //this is for tour code
                if (strCode.Contains("PNR") && strCode.Contains("TOURCODE"))
                {
                    XmlBuilder.Append("<TourCode>");
                    XmlBuilder.Append("<Rules>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Rules>");
                    XmlBuilder.Append("</TourCode>");
                }
                //end of tour code
                ////If you wanted to add Tour code in Endorsement. 
                if (strCode.Contains("PNR") && strCode.Contains("TCEndorsement"))
                {
                    XmlBuilder.Append("<TourCode>");
                    XmlBuilder.Append("<Rules>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</Rules>");
                    XmlBuilder.Append("</TourCode>");
                }
                ////end of add Tour code in Endorsement. 
                //end of mordification
            }
            catch (Exception ex)
            { }

            XmlBuilder.Append("</StorePriceMods>");
            #endregion

            XmlBuilder.Append("</PNRBFManagement_43>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append(SoapEnd("SONS"));
            #endregion




            return XmlBuilder.ToString();
        }
        #endregion

        #region Create save fare in pnr Request
        public string PNRBFManagementSaveFareInPnrReq(string RcvdFrom, string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("SONS", Token));
            //XmlBuilder.Append("<?xml version='1.0' encoding='utf-16'?>");
            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_43>");
            XmlBuilder.Append("<EndTransactionMods>");
            XmlBuilder.Append("<EndTransactRequest>");
            XmlBuilder.Append("<ETInd>R</ETInd>");
            XmlBuilder.Append("<RcvdFrom>" + RcvdFrom + "</RcvdFrom>");
            XmlBuilder.Append("</EndTransactRequest>");
            XmlBuilder.Append("</EndTransactionMods>");
            XmlBuilder.Append("</PNRBFManagement_43>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append(SoapEnd("SONS"));
            return XmlBuilder.ToString();
        }
        #endregion

        #endregion Reprice



    }
}
