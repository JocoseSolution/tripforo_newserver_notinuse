﻿using STD.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

//namespace GALWS.G8CPAPI
//{
//    class IXReservation
//    {
//    }
//}
namespace STD.BAL
{
    public class IXReservation
    {
        string SecurityToken;
        string IATANumber;
        string UserName;
        string Password;
        string IP;

        public IXReservation(string securityToken, string Iatanumber, string username, string password, string ip)
        {
            SecurityToken = securityToken;
            IATANumber = Iatanumber;
            UserName = username;
            Password = password;
            IP = ip;

        }


        public decimal GetSummaryPNR(string methodUrl, string serviceUrl, FZBookFlightRequest req, ref Dictionary<string, string> reqRes, ref string exep)
        {
            string result = "";
            StringBuilder PNRReq = new StringBuilder();
            XDocument xmlDoc = null;
            try
            {

                PNRReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request\">");
                PNRReq.Append("<soapenv:Header/> <soapenv:Body><tem:SummaryPNR><!--Optional:-->");
                PNRReq.Append("<tem:SummaryPnrRequest>");
                PNRReq.Append("<rad:SecurityGUID>" + SecurityToken + "</rad:SecurityGUID>");
                PNRReq.Append("<rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode>");
                PNRReq.Append("<rad:AccessibleCarrierCode>IX</rad:AccessibleCarrierCode>");
                PNRReq.Append("</rad:CarrierCode></rad:CarrierCodes>");
                PNRReq.Append("<rad:ClientIPAddress>" + IP + "</rad:ClientIPAddress>");
                PNRReq.Append("<rad:HistoricUserName>" + UserName + "</rad:HistoricUserName>");
                PNRReq.Append("<rad1:ActionType>GetSummary</rad1:ActionType>");
                PNRReq.Append("<rad1:ReservationInfo>");
                PNRReq.Append("<rad:SeriesNumber>299</rad:SeriesNumber>");
                PNRReq.Append("<rad:ConfirmationNumber></rad:ConfirmationNumber>");
                PNRReq.Append("</rad1:ReservationInfo>");
                PNRReq.Append("<rad1:SecurityToken>" + SecurityToken + "</rad1:SecurityToken>");
                PNRReq.Append("<rad1:CarrierCurrency>INR</rad1:CarrierCurrency>");
                PNRReq.Append("<rad1:DisplayCurrency>INR</rad1:DisplayCurrency>");
                PNRReq.Append("<rad1:IATANum>" + IATANumber + "</rad1:IATANum>");
                PNRReq.Append("<rad1:User>" + UserName + "</rad1:User>");
                PNRReq.Append("<rad1:ReceiptLanguageID>1</rad1:ReceiptLanguageID>");
                PNRReq.Append("<rad1:PromoCode></rad1:PromoCode>");
                PNRReq.Append("<rad1:ExternalBookingID></rad1:ExternalBookingID>");
                PNRReq.Append("<rad1:Address>");
                PNRReq.Append("<rad1:Address1>" + req.Address + "</rad1:Address1>");
                PNRReq.Append("<rad1:Address2>" + req.Address2 + "</rad1:Address2>");
                PNRReq.Append("<rad1:City>" + req.City + "</rad1:City>");
                PNRReq.Append("<rad1:State>" + req.State + "</rad1:State>");
                PNRReq.Append("<rad1:Postal>" + req.Postal + "</rad1:Postal>");
                PNRReq.Append("<rad1:Country>" + req.Country + "</rad1:Country> ");
                PNRReq.Append("<rad1:CountryCode>" + req.CountryCode + "</rad1:CountryCode>");
                PNRReq.Append("<rad1:AreaCode>" + req.AreaCode + "</rad1:AreaCode>");
                PNRReq.Append("<rad1:PhoneNumber>" + req.Mobile + "</rad1:PhoneNumber>");
                PNRReq.Append("<rad1:Display></rad1:Display>");
                PNRReq.Append("</rad1:Address>");
                PNRReq.Append("<rad1:ContactInfos><!--Zero or more repetitions:-->");
                PNRReq.Append("<rad1:ContactInfo>");
                PNRReq.Append("<rad1:ContactID>-2141</rad1:ContactID>");
                PNRReq.Append("<rad1:PersonOrgID>-214</rad1:PersonOrgID>");
                PNRReq.Append("<rad1:ContactField>" + req.Email + "</rad1:ContactField>");
                PNRReq.Append("<rad1:ContactType>Email</rad1:ContactType>");
                PNRReq.Append("<rad1:Extension>na</rad1:Extension>");
                PNRReq.Append("<rad1:CountryCode>na</rad1:CountryCode>");
                PNRReq.Append("<rad1:AreaCode>na</rad1:AreaCode>");
                PNRReq.Append("<rad1:PhoneNumber>na</rad1:PhoneNumber>");
                PNRReq.Append("<rad1:Display>na</rad1:Display>");
                PNRReq.Append("<rad1:PreferredContactMethod>true</rad1:PreferredContactMethod>");
                PNRReq.Append("</rad1:ContactInfo>");
                PNRReq.Append("</rad1:ContactInfos>");
                #region Passengers Info
                PNRReq.Append(" <rad1:Passengers>");
                PNRReq.Append("<!--Zero or more repetitions:-->");

                //int paxid = 214;
                int k = 0;
                for (int i = 0; i < req.CustomerList.Count; i++)
                {


                    PNRReq.Append("<rad1:Person>");
                    //if (req.CustomerList[i].PTCID == 1)
                    //{
                    //    PNRReq.Append("<rad1:PersonOrgID>-215</rad1:PersonOrgID>");
                    //}
                    //else if (req.CustomerList[i].PTCID == 6)
                    //{
                    //    PNRReq.Append("<rad1:PersonOrgID>-216</rad1:PersonOrgID>");
                    //}
                    //else if (req.CustomerList[i].PTCID == 5)
                    //{
                    //    PNRReq.Append("<rad1:PersonOrgID>-217</rad1:PersonOrgID>");
                    //}
                    PNRReq.Append("<rad1:PersonOrgID>-" + req.CustomerList[i].PersonOrgID.ToString() + "</rad1:PersonOrgID>");
                    PNRReq.Append("<rad1:FirstName>" + req.CustomerList[i].FirstName + "</rad1:FirstName>");
                    PNRReq.Append("<rad1:LastName>" + req.CustomerList[i].LastName + "</rad1:LastName>");
                    PNRReq.Append("<rad1:MiddleName>" + req.CustomerList[i].MiddleName + "</rad1:MiddleName>");
                    PNRReq.Append("<rad1:Age>-214</rad1:Age>");
                    PNRReq.Append("<rad1:DOB>" + req.CustomerList[i].DOB + "</rad1:DOB>");
                    PNRReq.Append("<rad1:Gender>" + req.CustomerList[i].Gender + "</rad1:Gender>");
                    PNRReq.Append("<rad1:Title>" + req.CustomerList[i].Title + "</rad1:Title>");
                    PNRReq.Append("<rad1:NationalityLaguageID>-214</rad1:NationalityLaguageID>");
                    PNRReq.Append("<rad1:RelationType>Self</rad1:RelationType>");
                    PNRReq.Append("<rad1:WBCID>" + req.CustomerList[i].PTCID + "</rad1:WBCID>");
                    PNRReq.Append("<rad1:PTCID>" + req.CustomerList[i].PTCID + "</rad1:PTCID>");
                    PNRReq.Append("<rad1:PTC>" + req.CustomerList[i].PTCID + "</rad1:PTC>");
                    if (req.CustomerList[i].PTCID == 5)
                    {
                        PNRReq.Append("<rad1:TravelsWithPersonOrgID>-" + req.CustomerList[k].PersonOrgID.ToString() + "</rad1:TravelsWithPersonOrgID>");
                        k++;
                    }
                    else
                    {
                        PNRReq.Append("<rad1:TravelsWithPersonOrgID>-2147483648</rad1:TravelsWithPersonOrgID>");
                    }
                    PNRReq.Append("<rad1:RedressNumber>na</rad1:RedressNumber>");
                    PNRReq.Append("<rad1:KnownTravelerNumber>na</rad1:KnownTravelerNumber>");
                    PNRReq.Append("<rad1:MarketingOptIn>true</rad1:MarketingOptIn>");
                    PNRReq.Append("<rad1:UseInventory>false</rad1:UseInventory>");
                    PNRReq.Append("<rad1:Address>");
                    PNRReq.Append("<rad1:Address1>" + req.Address + "</rad1:Address1>");
                    PNRReq.Append("<rad1:Address2>" + req.Address2 + "</rad1:Address2>");
                    PNRReq.Append("<rad1:City>" + req.City + "</rad1:City>");
                    PNRReq.Append("<rad1:State>" + req.State + "</rad1:State>");
                    PNRReq.Append("<rad1:Postal>" + req.Postal + "</rad1:Postal>");
                    PNRReq.Append("<rad1:Country>" + req.Country + "</rad1:Country> ");
                    PNRReq.Append("<rad1:CountryCode>" + req.CountryCode + "</rad1:CountryCode>");
                    PNRReq.Append("<rad1:AreaCode>" + req.AreaCode + "</rad1:AreaCode>");
                    PNRReq.Append("<rad1:PhoneNumber>" + req.Mobile + "</rad1:PhoneNumber>");
                    PNRReq.Append("<rad1:Display></rad1:Display>");
                    PNRReq.Append("</rad1:Address>");

                    PNRReq.Append("<rad1:Company>na</rad1:Company>");
                    PNRReq.Append("<rad1:Comments>na</rad1:Comments>");
                    PNRReq.Append("<rad1:Passport>na</rad1:Passport>");
                    PNRReq.Append("<rad1:Nationality>na</rad1:Nationality>");
                    PNRReq.Append("<rad1:ProfileId>-2147483648</rad1:ProfileId>");
                    // if (i == 0)
                    // {

                    //}
                    //else
                    //{
                    // PNRReq.Append("<rad1:IsPrimaryPassenger>false</rad1:IsPrimaryPassenger>");
                    //}
                    //Mobile
                    PNRReq.Append("<rad1:IsPrimaryPassenger>" + (i == 0 ? "true" : "false") + "</rad1:IsPrimaryPassenger>");
                    PNRReq.Append("<rad1:ContactInfos>");
                    PNRReq.Append("<rad1:ContactInfo>");
                    PNRReq.Append("<rad1:ContactID>-" + req.CustomerList[i].PersonOrgID.ToString() + "1</rad1:ContactID>");
                    PNRReq.Append("<rad1:PersonOrgID>-" + req.CustomerList[i].PersonOrgID.ToString() + "</rad1:PersonOrgID>");
                    PNRReq.Append("<rad1:ContactField>" + req.Mobile + "</rad1:ContactField>");
                    PNRReq.Append("<rad1:ContactType>MobilePhone</rad1:ContactType>");
                    PNRReq.Append("<rad1:Extension>na</rad1:Extension>");
                    PNRReq.Append("<rad1:CountryCode>" + req.CountryCode + "</rad1:CountryCode>");
                    PNRReq.Append("<rad1:AreaCode>" + req.AreaCode + "</rad1:AreaCode>");
                    PNRReq.Append("<rad1:PhoneNumber></rad1:PhoneNumber>");
                    PNRReq.Append("<rad1:Display>na</rad1:Display>");
                    PNRReq.Append("<rad1:PreferredContactMethod>true</rad1:PreferredContactMethod>");
                    PNRReq.Append("</rad1:ContactInfo>");

                    // email 
                    PNRReq.Append("<rad1:ContactInfo>");
                    PNRReq.Append("<rad1:ContactID>-" + req.CustomerList[i].PersonOrgID.ToString() + "2</rad1:ContactID>");
                    PNRReq.Append("<rad1:PersonOrgID>-" + req.CustomerList[i].PersonOrgID.ToString() + "</rad1:PersonOrgID>");
                    PNRReq.Append("<rad1:ContactField>" + req.Email + "</rad1:ContactField>");
                    PNRReq.Append("<rad1:ContactType>Email</rad1:ContactType>");
                    PNRReq.Append("<rad1:Extension>na</rad1:Extension>");
                    PNRReq.Append("<rad1:CountryCode>na</rad1:CountryCode>");
                    PNRReq.Append("<rad1:AreaCode>na</rad1:AreaCode>");
                    PNRReq.Append("<rad1:PhoneNumber>na</rad1:PhoneNumber>");
                    PNRReq.Append("<rad1:Display>na</rad1:Display>");
                    PNRReq.Append("<rad1:PreferredContactMethod>true</rad1:PreferredContactMethod>");
                    PNRReq.Append("</rad1:ContactInfo>");

                    // home phone

                    PNRReq.Append("<rad1:ContactInfo>");
                    PNRReq.Append("<rad1:ContactID>-" + req.CustomerList[i].PersonOrgID.ToString() + "3</rad1:ContactID>");
                    PNRReq.Append("<rad1:PersonOrgID>-" + req.CustomerList[i].PersonOrgID.ToString() + "</rad1:PersonOrgID>");
                    PNRReq.Append("<rad1:ContactField>" + req.Mobile + "</rad1:ContactField>");// (if HomePhone is not there,supply same number as Mobile)
                    PNRReq.Append("<rad1:ContactType>HomePhone</rad1:ContactType>");
                    PNRReq.Append("<rad1:Extension>na</rad1:Extension>");
                    PNRReq.Append("<rad1:CountryCode>" + req.CountryCode + "</rad1:CountryCode>");
                    PNRReq.Append("<rad1:AreaCode>" + req.AreaCode + "</rad1:AreaCode>");
                    PNRReq.Append("<rad1:PhoneNumber></rad1:PhoneNumber>");
                    PNRReq.Append("<rad1:Display>na</rad1:Display>");
                    PNRReq.Append("<rad1:PreferredContactMethod>false</rad1:PreferredContactMethod>");
                    PNRReq.Append("</rad1:ContactInfo>");
                    PNRReq.Append("</rad1:ContactInfos>");
                    PNRReq.Append(" </rad1:Person>");
                    // paxid++;

                }
                PNRReq.Append(" </rad1:Passengers>");
                #endregion

                #region Segments
                PNRReq.Append("<rad1:Segments>");
                for (int s = 0; s < req.segment.Count; s++)
                {
                    #region segment
                    PNRReq.Append("<rad1:Segment>");
                    PNRReq.Append("<rad1:PersonOrgID>-214</rad1:PersonOrgID>");
                    PNRReq.Append("<rad1:FareInformationID>" + req.segment[s].FareInformationID + "</rad1:FareInformationID>");
                    PNRReq.Append("<rad1:MarketingCode>na</rad1:MarketingCode>");
                    PNRReq.Append("<rad1:StoreFrontID>na</rad1:StoreFrontID>");

                    #region SpecialServices
                    PNRReq.Append("<rad1:SpecialServices>");
                    for (int ss = 0; ss < req.segment[s].SpecialServices.Count; ss++)
                    {
                        PNRReq.Append("<rad1:SpecialService>");
                        PNRReq.Append("<rad1:CodeType>" + req.segment[s].SpecialServices[ss].CodeType + "</rad1:CodeType>");
                        PNRReq.Append("<rad1:ServiceID>" + req.segment[s].SpecialServices[ss].ServiceID + "</rad1:ServiceID>");
                        PNRReq.Append("<rad1:SSRCategory>" + req.segment[s].SpecialServices[ss].SSRCategory + "</rad1:SSRCategory>");
                        PNRReq.Append("<rad1:LogicalFlightID>" + req.segment[s].SpecialServices[ss].LogicalFlightID + "</rad1:LogicalFlightID>");
                        PNRReq.Append("<rad1:DepartureDate>" + req.segment[s].SpecialServices[ss].DepartureDate + "</rad1:DepartureDate>");
                        PNRReq.Append("<rad1:Amount>" + req.segment[s].SpecialServices[ss].Amount + "</rad1:Amount>");
                        PNRReq.Append("<rad1:OverrideAmount>true</rad1:OverrideAmount>");
                        PNRReq.Append("<rad1:CurrencyCode>INR</rad1:CurrencyCode>");
                        PNRReq.Append("<rad1:ChargeComment>" + req.segment[s].SpecialServices[ss].CodeType.ToLower() + "</rad1:ChargeComment>");
                        PNRReq.Append(" <rad1:PersonOrgID>-" + req.segment[s].SpecialServices[ss].PersonOrgID + "</rad1:PersonOrgID>");
                        PNRReq.Append("</rad1:SpecialService>");

                    }
                    PNRReq.Append("</rad1:SpecialServices>");

                    #endregion

                    PNRReq.Append("<rad1:Seats></rad1:Seats>");
                    PNRReq.Append("</rad1:Segment>");
                    #endregion

                }
                PNRReq.Append("</rad1:Segments>");
                #endregion

                #region Payments
                PNRReq.Append("<rad1:Payments>");
                PNRReq.Append("<rad1:Payment>");
                PNRReq.Append("<rad1:ReservationPaymentID>-2147483648</rad1:ReservationPaymentID>");
                PNRReq.Append("<rad1:CompanyName>" + req.paymentDetails.CompanyName + "</rad1:CompanyName>");
                PNRReq.Append("<rad1:FirstName>" + req.paymentDetails.FirstName + "</rad1:FirstName>");
                PNRReq.Append("<rad1:LastName>" + req.paymentDetails.LastName + "</rad1:LastName>");
                PNRReq.Append("<rad1:CardType></rad1:CardType>");
                PNRReq.Append("<rad1:CardHolder></rad1:CardHolder>");
                PNRReq.Append("<rad1:PaymentCurrency>INR</rad1:PaymentCurrency>");
                PNRReq.Append("<rad1:ISOCurrency>1</rad1:ISOCurrency>");
                PNRReq.Append("<rad1:PaymentAmount>0</rad1:PaymentAmount>");
                PNRReq.Append("<rad1:PaymentMethod>INVC</rad1:PaymentMethod>");
                PNRReq.Append("<rad1:CardNum></rad1:CardNum>");
                PNRReq.Append("<rad1:CVCode></rad1:CVCode>");
                PNRReq.Append("<rad1:ExpirationDate>" + req.paymentDetails.ExchangeRateDate.ToString("yyyy-MM-dd") + "</rad1:ExpirationDate>");
                PNRReq.Append("<rad1:IsTACreditCard>false</rad1:IsTACreditCard>");
                PNRReq.Append("<rad1:VoucherNum>-2147483648</rad1:VoucherNum>");
                PNRReq.Append("<rad1:GcxID>1</rad1:GcxID>");
                PNRReq.Append("<rad1:GcxOpt>1</rad1:GcxOpt>");
                PNRReq.Append("<rad1:OriginalCurrency>INR</rad1:OriginalCurrency>");
                PNRReq.Append("<rad1:OriginalAmount>0</rad1:OriginalAmount>");
                PNRReq.Append("<rad1:ExchangeRate>" + req.paymentDetails.ExchangeRate + "</rad1:ExchangeRate>");
                PNRReq.Append("<rad1:ExchangeRateDate>" + req.paymentDetails.ExchangeRateDate.ToString("yyyy-MM-dd") + "</rad1:ExchangeRateDate>");
                PNRReq.Append("<rad1:PaymentComment>" + req.paymentDetails.PaymentComment + "</rad1:PaymentComment>");
                PNRReq.Append("<rad1:BillingCountry>IN</rad1:BillingCountry>");
                PNRReq.Append("</rad1:Payment>");
                PNRReq.Append("</rad1:Payments>");

                #endregion
                PNRReq.Append("</tem:SummaryPnrRequest>");
                PNRReq.Append("</tem:SummaryPNR>");
                PNRReq.Append("</soapenv:Body>");
                PNRReq.Append("</soapenv:Envelope>");

                result = G8Utility.PostXml(PNRReq.ToString(), methodUrl, serviceUrl, ref exep);
                string str = result.Replace("xmlns:a=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Response\"", "").Replace("a:", "").Replace("xmlns=\"http://tempuri.org/\"", "");
                xmlDoc = XDocument.Parse(str);
                reqRes.Add("SummaryPnrReq", PNRReq.ToString());
                reqRes.Add("SummaryPnrRes", result);

                G8Utility.SaveXml(PNRReq.ToString(), SecurityToken, "SummaryPnr_Req");
                G8Utility.SaveXml(result, SecurityToken, "SummaryPnr_Res");
            }
            catch (Exception ex)
            {
                reqRes.Add("SummaryPnrReq", PNRReq.ToString());
                reqRes.Add("SummaryPnrRes", result);
                exep = exep + " GetSummaryPNR Method: " + ex.Message + " StackTrace: " + ex.StackTrace;
            }
            return Convert.ToDecimal(xmlDoc.Descendants("ReservationBalance").First().Value);

        }

        
        public decimal ProcessPNRPayment(string methodUrl, string serviceUrl, FZBookFlightRequest req, ref Dictionary<string, string> reqRes, ref string exep)
        {

            string result = "";
            StringBuilder PNRPaymentReq = new StringBuilder();
            XDocument xmlDoc = null;
            decimal val = -1;
            try
            {

                PNRPaymentReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Fulfillment.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad2=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request\">");
                PNRPaymentReq.Append("<soapenv:Header/><soapenv:Body><tem:ProcessPNRPayment><!--Optional:-->");
                PNRPaymentReq.Append("<tem:PNRPaymentRequest><rad:TransactionInfo>");
                PNRPaymentReq.Append("<rad1:SecurityGUID>" + SecurityToken + "</rad1:SecurityGUID>");
                PNRPaymentReq.Append("<rad1:CarrierCodes><!--Zero or more repetitions:-->");
                PNRPaymentReq.Append("<rad1:CarrierCode>");
                PNRPaymentReq.Append("<rad1:AccessibleCarrierCode>IX</rad1:AccessibleCarrierCode>");
                PNRPaymentReq.Append("</rad1:CarrierCode>");
                PNRPaymentReq.Append("</rad1:CarrierCodes>");
                PNRPaymentReq.Append("<rad1:ClientIPAddress>" + IP + "</rad1:ClientIPAddress>");
                PNRPaymentReq.Append("<rad1:HistoricUserName>" + UserName + "</rad1:HistoricUserName>");
                PNRPaymentReq.Append("</rad:TransactionInfo>");
                PNRPaymentReq.Append("<rad:ReservationInfo>");
                PNRPaymentReq.Append("<rad1:SeriesNumber>299</rad1:SeriesNumber>");
                PNRPaymentReq.Append("<rad1:ConfirmationNumber xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>");
                PNRPaymentReq.Append("</rad:ReservationInfo>");
                PNRPaymentReq.Append("<rad:PNRPayments> <!--Zero or more repetitions:-->");
                PNRPaymentReq.Append("<rad:ProcessPNRPayment>");
                PNRPaymentReq.Append("<rad:BaseAmount>" + req.paymentDetails.OriginalAmount + "</rad:BaseAmount>");
                PNRPaymentReq.Append("<rad:BaseCurrency>INR</rad:BaseCurrency>");
                PNRPaymentReq.Append("<rad:CardHolder></rad:CardHolder>");
                PNRPaymentReq.Append("<rad:CardNumber></rad:CardNumber>");
                PNRPaymentReq.Append("<rad:CheckNumber>1234</rad:CheckNumber>");
                PNRPaymentReq.Append("<rad:CurrencyPaid>INR</rad:CurrencyPaid>");
                PNRPaymentReq.Append("<rad:CVCode></rad:CVCode>");
                PNRPaymentReq.Append("<rad:DatePaid>" + req.paymentDetails.ExchangeRateDate.ToString("yyyy-MM-dd") + "</rad:DatePaid>");
                PNRPaymentReq.Append("<rad:DocumentReceivedBy>IX</rad:DocumentReceivedBy>");
                PNRPaymentReq.Append("<rad:ExpirationDate>" + req.paymentDetails.ExchangeRateDate.ToString("yyyy-MM-dd") + "</rad:ExpirationDate>");
                PNRPaymentReq.Append("<rad:ExchangeRate>" + req.paymentDetails.ExchangeRate + "</rad:ExchangeRate>");
                PNRPaymentReq.Append("<rad:ExchangeRateDate>" + req.paymentDetails.ExchangeRateDate.ToString("yyyy-MM-dd") + "</rad:ExchangeRateDate>");
                PNRPaymentReq.Append("<rad:FFNumber>11</rad:FFNumber>");
                PNRPaymentReq.Append("<rad:PaymentComment>" + req.paymentDetails.PaymentComment + "</rad:PaymentComment>");
                PNRPaymentReq.Append("<rad:PaymentAmount>" + req.paymentDetails.OriginalAmount + "</rad:PaymentAmount>");
                PNRPaymentReq.Append("<rad:PaymentMethod>INVC</rad:PaymentMethod>");
                PNRPaymentReq.Append("<rad:Reference></rad:Reference>");
                PNRPaymentReq.Append("<rad:TerminalID>1</rad:TerminalID>");
                PNRPaymentReq.Append("<rad:UserData></rad:UserData>");
                PNRPaymentReq.Append("<rad:UserID>" + UserName + "</rad:UserID>");
                PNRPaymentReq.Append("<rad:IataNumber>" + IATANumber + "</rad:IataNumber>");
                PNRPaymentReq.Append("<rad:ValueCode></rad:ValueCode>");
                PNRPaymentReq.Append("<rad:VoucherNumber>-2147483648</rad:VoucherNumber>");
                PNRPaymentReq.Append("<rad:IsTACreditCard>false</rad:IsTACreditCard>");
                PNRPaymentReq.Append("<rad:GcxID>1</rad:GcxID>");
                PNRPaymentReq.Append("<rad:GcxOptOption>1</rad:GcxOptOption>");
                PNRPaymentReq.Append("<rad:OriginalCurrency>INR</rad:OriginalCurrency>");
                PNRPaymentReq.Append("<rad:OriginalAmount>" + req.paymentDetails.OriginalAmount + "</rad:OriginalAmount>");
                PNRPaymentReq.Append("<rad:TransactionStatus>APPROVED</rad:TransactionStatus>");
                PNRPaymentReq.Append("<rad:AuthorizationCode></rad:AuthorizationCode>");
                PNRPaymentReq.Append("<rad:PaymentReference></rad:PaymentReference>");
                PNRPaymentReq.Append("<rad:ResponseMessage></rad:ResponseMessage>");
                PNRPaymentReq.Append("<rad:CardCurrency>INR</rad:CardCurrency>");
                PNRPaymentReq.Append("<rad:BillingCountry>IN</rad:BillingCountry>");
                PNRPaymentReq.Append("<rad:FingerPrintingSessionID></rad:FingerPrintingSessionID>");
                PNRPaymentReq.Append("<rad:Payor>");
                PNRPaymentReq.Append("<rad2:PersonOrgID>-2147483648</rad2:PersonOrgID>");
                PNRPaymentReq.Append("<rad2:FirstName>" + req.paymentDetails.FirstName + "</rad2:FirstName>");
                PNRPaymentReq.Append("<rad2:LastName>" + req.paymentDetails.LastName + "</rad2:LastName>");
                PNRPaymentReq.Append("<rad2:MiddleName xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>");
                PNRPaymentReq.Append("<rad2:Age>-214</rad2:Age>");
                PNRPaymentReq.Append("<rad2:DOB>" + req.CustomerList[0].DOB + "</rad2:DOB>");
                PNRPaymentReq.Append("<rad2:Gender>" + req.CustomerList[0].Gender + "</rad2:Gender>");
                PNRPaymentReq.Append("<rad2:Title>" + req.CustomerList[0].Title + "</rad2:Title>");
                PNRPaymentReq.Append("<rad2:NationalityLaguageID>-214</rad2:NationalityLaguageID>");
                PNRPaymentReq.Append("<rad2:RelationType>Self</rad2:RelationType>");
                PNRPaymentReq.Append("<rad2:WBCID>1</rad2:WBCID>");
                PNRPaymentReq.Append("<rad2:PTCID>" + req.CustomerList[0].PTCID + "</rad2:PTCID>");
                PNRPaymentReq.Append("<rad2:PTC>1</rad2:PTC>");
                PNRPaymentReq.Append("<rad2:TravelsWithPersonOrgID>-214</rad2:TravelsWithPersonOrgID>");
                PNRPaymentReq.Append("<rad2:RedressNumber>na</rad2:RedressNumber>");
                PNRPaymentReq.Append("<rad2:KnownTravelerNumber>NA</rad2:KnownTravelerNumber>");
                PNRPaymentReq.Append("<rad2:MarketingOptIn>false</rad2:MarketingOptIn>");
                PNRPaymentReq.Append("<rad2:UseInventory>false</rad2:UseInventory>");
                PNRPaymentReq.Append("<rad2:Address>");
                PNRPaymentReq.Append("<rad2:Address1>na</rad2:Address1>");
                PNRPaymentReq.Append("<rad2:Address2>na</rad2:Address2>");
                PNRPaymentReq.Append("<rad2:City>na</rad2:City>");
                PNRPaymentReq.Append("<rad2:State>na</rad2:State>");
                PNRPaymentReq.Append("<rad2:Postal>na</rad2:Postal>");
                PNRPaymentReq.Append("<rad2:Country>na</rad2:Country>");
                PNRPaymentReq.Append("<rad2:CountryCode>na</rad2:CountryCode>");
                PNRPaymentReq.Append("<rad2:AreaCode>na</rad2:AreaCode>");
                PNRPaymentReq.Append("<rad2:PhoneNumber>na</rad2:PhoneNumber>");
                PNRPaymentReq.Append("<rad2:Display>na</rad2:Display>");
                PNRPaymentReq.Append("</rad2:Address>");
                PNRPaymentReq.Append("<rad2:Company>na</rad2:Company>");
                PNRPaymentReq.Append("<rad2:Comments>na</rad2:Comments>");
                PNRPaymentReq.Append("<rad2:Passport>na</rad2:Passport>");
                PNRPaymentReq.Append("<rad2:Nationality>na</rad2:Nationality>");
                PNRPaymentReq.Append("<rad2:ProfileId>-214</rad2:ProfileId>");
                PNRPaymentReq.Append("<rad2:IsPrimaryPassenger>true</rad2:IsPrimaryPassenger>");
                PNRPaymentReq.Append("<rad2:ContactInfos>");
                PNRPaymentReq.Append("<rad2:ContactInfo>");
                PNRPaymentReq.Append("<rad2:ContactID>4</rad2:ContactID>");
                PNRPaymentReq.Append("<rad2:PersonOrgID>-214</rad2:PersonOrgID>");
                PNRPaymentReq.Append("<rad2:ContactField>2</rad2:ContactField>");
                PNRPaymentReq.Append("<rad2:ContactType>WorkPhone</rad2:ContactType>");
                PNRPaymentReq.Append("<rad2:Extension></rad2:Extension>");
                PNRPaymentReq.Append("<rad2:CountryCode>" + req.CountryCode + "</rad2:CountryCode>");
                PNRPaymentReq.Append("<rad2:AreaCode>" + req.AreaCode + "</rad2:AreaCode>");
                PNRPaymentReq.Append("<rad2:PhoneNumber>" + req.Mobile + "</rad2:PhoneNumber>");
                PNRPaymentReq.Append("<rad2:Display>test</rad2:Display>");
                PNRPaymentReq.Append("<rad2:PreferredContactMethod>true</rad2:PreferredContactMethod>");
                PNRPaymentReq.Append("</rad2:ContactInfo>");
                PNRPaymentReq.Append("</rad2:ContactInfos>");
                PNRPaymentReq.Append("</rad:Payor>");
                PNRPaymentReq.Append("</rad:ProcessPNRPayment>");
                PNRPaymentReq.Append("</rad:PNRPayments>");
                PNRPaymentReq.Append("</tem:PNRPaymentRequest>");
                PNRPaymentReq.Append("</tem:ProcessPNRPayment>");
                PNRPaymentReq.Append("</soapenv:Body></soapenv:Envelope>");

                result = G8Utility.PostXml(PNRPaymentReq.ToString(), methodUrl, serviceUrl, ref exep);

                string str = result.Replace("xmlns:a=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Response\"", "").Replace("a:", "").Replace("xmlns=\"http://tempuri.org/\"", "")
                                   .Replace("xmlns:b=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Exceptions\"", "")
                                   .Replace("b:", "");

                xmlDoc = XDocument.Parse(str);
                reqRes.Add("ProcessPNRPaymentReq", PNRPaymentReq.ToString());
                reqRes.Add("ProcessPNRPaymentRes", result);

                val = Convert.ToDecimal(xmlDoc.Descendants("ReservationBalance").First().Value);

                string exceptionLevel = xmlDoc.Descendants("ExceptionLevel").First().Value.Trim();

                if (exceptionLevel.Trim().ToLower() != "success")
                {
                    val = -1;
                    exep = exep + "Error in ProcessPNRPaymentRes- ExceptionLevel is not success ";
                }
                G8Utility.SaveXml(PNRPaymentReq.ToString(), SecurityToken, "ProcessPNRPayment_Req");
                G8Utility.SaveXml(result, SecurityToken, "ProcessPNRPayment_Res");
            }
            catch (Exception ex)
            {
                reqRes.Add("ProcessPNRPaymentReq", PNRPaymentReq.ToString());
                reqRes.Add("ProcessPNRPaymentRes", result);
                exep = exep + " ProcessPNRPayment Method: " + ex.Message + " StackTrace: " + ex.StackTrace;
            }



            return val;
        }

        public string CreatePNR(string methodUrl, string serviceUrl, ref Dictionary<string, string> reqRes, ref string exep)
        {

            string result = "";

            StringBuilder CreatePNRReq = new StringBuilder();
            try
            {

                CreatePNRReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request\">");
                CreatePNRReq.Append("<soapenv:Header/><soapenv:Body>");
                CreatePNRReq.Append("<tem:CreatePNR><!--Optional:-->");
                CreatePNRReq.Append("<tem:CreatePnrRequest>");
                CreatePNRReq.Append("<rad:SecurityGUID>" + SecurityToken + "</rad:SecurityGUID>");
                CreatePNRReq.Append("<rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode>");
                CreatePNRReq.Append("<rad:AccessibleCarrierCode>IX</rad:AccessibleCarrierCode>");
                CreatePNRReq.Append("</rad:CarrierCode></rad:CarrierCodes><!--Optional:-->");
                CreatePNRReq.Append("<rad:ClientIPAddress>" + IP + "</rad:ClientIPAddress>");
                CreatePNRReq.Append("<rad:HistoricUserName>" + UserName + "</rad:HistoricUserName>");
                CreatePNRReq.Append("<rad1:ActionType>CommitSummary</rad1:ActionType>");
                CreatePNRReq.Append("<rad1:ReservationInfo>");
                CreatePNRReq.Append("<rad:SeriesNumber>299</rad:SeriesNumber>");
                CreatePNRReq.Append("<rad:ConfirmationNumber></rad:ConfirmationNumber>");
                CreatePNRReq.Append("</rad1:ReservationInfo>");
                CreatePNRReq.Append("</tem:CreatePnrRequest>");
                CreatePNRReq.Append("</tem:CreatePNR>");
                CreatePNRReq.Append("</soapenv:Body></soapenv:Envelope>");

                result = G8Utility.PostXml(CreatePNRReq.ToString(), methodUrl, serviceUrl, ref exep);
                reqRes.Add("CreatePNRReq", CreatePNRReq.ToString());
                reqRes.Add("CreatePNRRes", result);

                G8Utility.SaveXml(CreatePNRReq.ToString(), SecurityToken, "CreatePNR_Req");
                G8Utility.SaveXml(result, SecurityToken, "CreatePNR_Res");
            }
            catch (Exception ex)
            {
                reqRes.Add("CreatePNRReq", CreatePNRReq.ToString());
                reqRes.Add("CreatePNRRes", result);
                exep = exep + " CreatePNR Method: " + ex.Message + " StackTrace: " + ex.StackTrace;
            }

            return result;
        }
    }
}
