﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web.UI;

using System.Data;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for booking
/// </summary>
namespace AirArabia
{


    public class flightsegment
    {
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string TransactionIdentifier { get; set; }
        public string journeyDuration { get; set; }
        public string dTotalNo_Adt { get; set; }
        public string dTotalNo_CHD { get; set; }
        public string dTotalNo_INF { get; set; }
        public string FlightNumber { get; set; }
        public string RPH { get; set; }
        public string OriginLocation { get; set; }
        public string DestinationLocation { get; set; }
        public string terminal { get; set; }
        public string PaymentAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string Totalpax { get; set; }
        public string CompanyNameCode { get; set; }




    }
    public class Passenger
    {


        public string PassengerTypeCode { get; set; }
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string OriginLocation { get; set; }
        public string DestinationLocation { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string NameTitle { get; set; }
        public string AreaCityCode { get; set; }
        public string CountryAccessCode { get; set; }
        public string PhoneNumber { get; set; }
        public string CountryNameCode { get; set; }
        public string DocHolderNationality { get; set; }
        public string RPH { get; set; }
        public string BirthDate { get; set; }
        public string TRAVELREFERNCENO { get; set; }
        public string Email { get; set; }
        public string CountryName { get; set; }
        public string CountryNameAccessCode { get; set; }
        public string CityName { get; set; }

        public string adult { get; set; }
        public string child { get; set; }
        public string infant { get; set; }




    }
    public class AirArabiaBooking
    {

        public string PostXml(string url, string xml, string JSessionId)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Headers.Add(HttpRequestHeader.Cookie, JSessionId);
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        //throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();

                    }
                }
            }
            catch (WebException ex)
            {

            }
            return sbResult.ToString();

        }



        public DataTable Booking(DataTable dt, Hashtable custinfo, string Adt, string Chd, string Inf, string totpax, string CompanyNameCode, string TotalFare, string UserId, string Password, string Url)
        {

            // --------------------------- FareQuote before Booking ---------------------------- //

            DataTable BookingDT = null;
            string BookResult = "";
            StringBuilder sb = new StringBuilder();
            try
            {
                #region[FareQuote before booking]
                GetFinalResult objFinalResult = new GetFinalResult();
                Request_Input ObjReqInput1 = new Request_Input();

                ObjReqInput1.ArrivalDateTime = new string[dt.Rows.Count];
                ObjReqInput1.DepartureDateTime = new string[dt.Rows.Count];
                ObjReqInput1.OriginLocation = new string[dt.Rows.Count];
                ObjReqInput1.DestinationLocation = new string[dt.Rows.Count];
                ObjReqInput1.FlightNumber = new string[dt.Rows.Count];
                ObjReqInput1.RPH = new string[dt.Rows.Count];

                for (int k = 0; k < dt.Rows.Count; k++)
                {

                    ObjReqInput1.ArrivalDateTime[k] = dt.Rows[k]["arrdatelcc"].ToString();
                    ObjReqInput1.DepartureDateTime[k] = dt.Rows[k]["depdatelcc"].ToString();
                    ObjReqInput1.OriginLocation[k] = dt.Rows[k]["DepartureLocation"].ToString();
                    ObjReqInput1.DestinationLocation[k] = dt.Rows[k]["ArrivalLocation"].ToString();
                    ObjReqInput1.FlightNumber[k] = dt.Rows[k]["ValidatingCarrier"].ToString() + dt.Rows[k]["FlightIdentification"].ToString();
                    ObjReqInput1.RPH[k] = dt.Rows[k]["sno"].ToString().Split('~')[1].ToString();
                }
                ObjReqInput1.TransactionIdentifier = dt.Rows[0]["sno"].ToString().Split('~')[0].ToString();
                ObjReqInput1.dTotalNo_Adt = Adt;
                ObjReqInput1.dTotalNo_CHD = Chd;
                ObjReqInput1.dTotalNo_INF = Inf;
                if (dt.Rows[dt.Rows.Count - 1]["Flight"].ToString() == "1")
                {
                    ObjReqInput1.TripType = "OneWay";
                }
                else
                {
                    ObjReqInput1.TripType = "Return";

                }
                ObjReqInput1.NoOfStop = (dt.Rows.Count) - 1;
                ObjReqInput1.UID = dt.Rows[0]["User_id"].ToString();
                ObjReqInput1.DistId = "SPRING";
                ObjReqInput1.UserType = "TA";
                ObjReqInput1.CorporateId = UserId;
                ObjReqInput1.Password = Password;
                ObjReqInput1.ServerUrl = Url;
                ObjReqInput1.JSessionId = dt.Rows[0]["sno"].ToString().Split('~')[2].ToString();
                DataTable dtAgentMarkup = new DataTable();
                DataTable dtAdminMarkup = new DataTable();

                List<STD.Shared.FlightSearchResults> objResultG9 = new List<STD.Shared.FlightSearchResults>();
                objResultG9 = objFinalResult.GetPriceQuote(ObjReqInput1, dtAgentMarkup, dtAdminMarkup);

                #endregion
                // --------------------------- end ---------------------------- //


                if (float.Parse(dt.Rows[0]["OriginalTF"].ToString()) == objResultG9[0].OriginalTF)
                {

                    int TotalPax = Convert.ToInt32(Adt) + Convert.ToInt32(Chd) + Convert.ToInt32(Inf);

                    #region Booking Request
                    sb.Append(string.Format("<?xml version='1.0' encoding='utf-8'?>"));
                    sb.Append(string.Format("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>"));
                    sb.Append(string.Format("<soap:Header>"));
                    sb.Append(string.Format("<wsse:Security soap:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>"));
                    sb.Append(string.Format("<wsse:UsernameToken wsu:Id='UsernameToken-17855236' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>"));
                    sb.Append(string.Format("<wsse:Username>" + UserId + "</wsse:Username>"));
                    sb.Append(string.Format("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>" + Password + "</wsse:Password>"));
                    sb.Append(string.Format("</wsse:UsernameToken>"));
                    sb.Append(string.Format("</wsse:Security>"));
                    sb.Append(string.Format("</soap:Header>"));
                    sb.Append(string.Format("<soap:Body xmlns:ns1='http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05' xmlns:ns2='http://www.opentravel.org/OTA/2003/05'>"));
                    sb.Append(string.Format("<ns2:OTA_AirBookRQ EchoToken='11868765275150-1300257933' PrimaryLangID='en-us' SequenceNmbr='1' TimeStamp='2011-01-12T17:15:04' TransactionIdentifier='" + dt.Rows[0]["Sno"].ToString().Substring(0, dt.Rows[0]["Sno"].ToString().IndexOf('~')) + "' Version='20061.00'>"));
                    sb.Append(string.Format("<ns2:POS>"));
                    sb.Append(string.Format("<ns2:Source TerminalID='TestUser/Test Runner'>"));
                    sb.Append(string.Format("<ns2:RequestorID ID='WSSPRINGT' Type='4' />"));
                    sb.Append(string.Format("<ns2:BookingChannel Type='12' />"));
                    sb.Append(string.Format("</ns2:Source>"));
                    sb.Append(string.Format("</ns2:POS>"));
                    sb.Append(string.Format("<ns2:AirItinerary>"));
                    sb.Append(string.Format("<ns2:OriginDestinationOptions>"));

                    for (int m = 0; m < dt.Rows.Count; m++)
                    {

                        sb.Append(string.Format("<ns2:OriginDestinationOption>"));
                        sb.Append(string.Format("<ns2:FlightSegment ArrivalDateTime='" + dt.Rows[m]["arrdatelcc"].ToString() + "' DepartureDateTime='" + dt.Rows[m]["depdatelcc"].ToString() + "' FlightNumber='" + dt.Rows[m]["ValiDatingCarrier"].ToString() + dt.Rows[m]["FlightIdentification"].ToString() + "' RPH='" + dt.Rows[m]["Sno"].ToString().Split('~')[1].ToString() + "'>"));
                        sb.Append(string.Format("<ns2:DepartureAirport LocationCode='" + dt.Rows[m]["DepartureLocation"].ToString() + "' Terminal='TerminalX' />"));
                        sb.Append(string.Format("<ns2:ArrivalAirport LocationCode='" + dt.Rows[m]["ArrivalLocation"].ToString() + "' Terminal='TerminalX' />"));
                        sb.Append(string.Format("</ns2:FlightSegment>"));
                        sb.Append(string.Format("</ns2:OriginDestinationOption>"));
                    }

                    sb.Append(string.Format("</ns2:OriginDestinationOptions>"));
                    sb.Append(string.Format("</ns2:AirItinerary>"));
                    sb.Append(string.Format("<ns2:TravelerInfo>"));


                    if (Convert.ToInt32(Adt) > 0)
                    {
                        for (int iAdt = 1; iAdt <= Convert.ToInt32(Adt); iAdt++)
                        {

                            sb.Append(string.Format("<ns2:AirTraveler BirthDate='" + custinfo["BirthDate_ADT" + iAdt].ToString() + "' PassengerTypeCode='" + custinfo["PaxTypeCode_ADT" + iAdt].ToString() + "'>"));
                            sb.Append(string.Format("<ns2:PersonName>"));
                            sb.Append(string.Format("<ns2:GivenName>" + custinfo["FNameADT" + iAdt].ToString().Trim() + "</ns2:GivenName>"));
                            sb.Append(string.Format("<ns2:Surname>" + custinfo["LnameADT" + iAdt].ToString() + "</ns2:Surname>"));
                            sb.Append(string.Format("<ns2:NameTitle>" + custinfo["Title_ADT" + iAdt].ToString() + "</ns2:NameTitle>"));
                            sb.Append(string.Format("</ns2:PersonName>"));
                            sb.Append(string.Format("<ns2:Telephone AreaCityCode='" + custinfo["sAreaCityCode"].ToString() + "' CountryAccessCode='" + custinfo["sCountryAccCode"].ToString() + "' PhoneNumber='" + custinfo["sHomePhn"].ToString() + "'/>"));
                            sb.Append(string.Format("<ns2:Address>"));
                            sb.Append(string.Format("<ns2:CountryName Code='" + custinfo["CountryNameCode"].ToString() + "'/>"));
                            sb.Append(string.Format("</ns2:Address>"));
                            sb.Append(string.Format("<ns2:Document DocHolderNationality='" + custinfo["Nationality"].ToString() + "'/>"));
                            sb.Append(string.Format("<ns2:TravelerRefNumber RPH='A" + iAdt.ToString() + "'/>"));

                            sb.Append(string.Format("</ns2:AirTraveler>"));

                        }
                    }
                    if (Convert.ToInt32(Chd) > 0)
                    {

                        for (int jChd = 1; jChd <= Convert.ToInt32(Chd); jChd++)
                        {
                            sb.Append(string.Format("<ns2:AirTraveler BirthDate='" + custinfo["BirthDate_CHD" + jChd] + "' PassengerTypeCode='" + custinfo["PaxTypeCode_CHD" + jChd] + "'>"));
                            sb.Append(string.Format("<ns2:PersonName>"));
                            sb.Append(string.Format("<ns2:GivenName>" + custinfo["FNameCHD" + jChd] + "</ns2:GivenName>"));
                            sb.Append(string.Format("<ns2:Surname>" + custinfo["LnameCHD" + jChd] + "</ns2:Surname>"));
                            sb.Append(string.Format("<ns2:NameTitle>" + custinfo["Title_CHD" + jChd] + "</ns2:NameTitle>"));
                            sb.Append(string.Format("</ns2:PersonName>"));
                            sb.Append(string.Format("<ns2:Telephone AreaCityCode='" + custinfo["sAreaCityCode"] + "' CountryAccessCode='" + custinfo["sCountryAccCode"] + "' PhoneNumber='" + custinfo["sHomePhn"] + "' />"));
                            sb.Append(string.Format("<ns2:Address>"));
                            sb.Append(string.Format("<ns2:CountryName Code='" + custinfo["CountryNameCode"] + "'/>"));
                            sb.Append(string.Format("</ns2:Address>"));
                            sb.Append(string.Format("<ns2:Document DocHolderNationality='" + custinfo["Nationality"] + "' />"));
                            sb.Append(string.Format("<ns2:TravelerRefNumber RPH='C" + Convert.ToInt32(Adt) + jChd + "' />"));
                            sb.Append(string.Format("</ns2:AirTraveler>"));
                        }
                    }

                    if (Convert.ToInt32(Inf) > 0)
                    {
                        for (int kInf = 1; kInf <= Convert.ToInt32(Inf); kInf++)
                        {
                            sb.Append(string.Format("<ns2:AirTraveler BirthDate='" + custinfo["BirthDate_INF" + kInf] + "' PassengerTypeCode='" + custinfo["PaxTypeCode_INF" + kInf] + "'>"));
                            sb.Append(string.Format("<ns2:PersonName>"));
                            sb.Append(string.Format("<ns2:GivenName>" + custinfo["FNameINF" + kInf] + "</ns2:GivenName>"));
                            sb.Append(string.Format("<ns2:Surname>" + custinfo["LnameINF" + kInf] + "</ns2:Surname>"));
                            sb.Append(string.Format("<ns2:NameTitle>" + custinfo["Title_INF" + kInf] + "</ns2:NameTitle>"));
                            sb.Append(string.Format("</ns2:PersonName>"));
                            sb.Append(string.Format("<ns2:TravelerRefNumber RPH='I" + (Convert.ToInt32(Adt) + Convert.ToInt32(Chd) + kInf) + "/A" + kInf + "' /> "));

                            sb.Append(string.Format("</ns2:AirTraveler>"));
                        }


                    }

                    sb.Append(string.Format("</ns2:TravelerInfo>"));
                    sb.Append(string.Format("<ns2:Fulfillment>"));
                    sb.Append(string.Format("<ns2:PaymentDetails>"));
                    sb.Append(string.Format("<ns2:PaymentDetail>"));
                    sb.Append(string.Format("<ns2:DirectBill>"));
                    sb.Append(string.Format("<ns2:CompanyName Code='" + CompanyNameCode + "' />"));
                    sb.Append(string.Format("</ns2:DirectBill>"));
                    sb.Append(string.Format("<ns2:PaymentAmount Amount='" + TotalFare + "' CurrencyCode='" + custinfo["sCurrency"] + "' DecimalPlaces='2' />"));
                    sb.Append(string.Format("</ns2:PaymentDetail>"));
                    sb.Append(string.Format("</ns2:PaymentDetails>"));
                    sb.Append(string.Format("</ns2:Fulfillment>"));
                    sb.Append(string.Format("</ns2:OTA_AirBookRQ>"));
                    sb.Append(string.Format("<ns1:AAAirBookRQExt>"));
                    sb.Append(string.Format("<ns1:ContactInfo>"));
                    sb.Append(string.Format("<ns1:PersonName>"));
                    sb.Append(string.Format("<ns1:Title>" + custinfo["Ttl"] + "</ns1:Title>"));
                    sb.Append(string.Format("<ns1:FirstName>" + custinfo["FName"] + "</ns1:FirstName>"));
                    sb.Append(string.Format("<ns1:LastName>" + custinfo["LName"] + "</ns1:LastName>"));
                    sb.Append(string.Format("</ns1:PersonName>"));
                    sb.Append(string.Format("<ns1:Telephone>"));
                    sb.Append(string.Format("<ns1:PhoneNumber>" + custinfo["sAgencyPhn"] + "</ns1:PhoneNumber>"));
                    sb.Append(string.Format("<ns1:CountryCode>" + custinfo["sCountryAccCode"] + "</ns1:CountryCode>"));
                    sb.Append(string.Format("<ns1:AreaCode>" + custinfo["sAreaCityCode"] + "</ns1:AreaCode>"));
                    sb.Append(string.Format("</ns1:Telephone>"));
                    sb.Append(string.Format("<ns1:Email>" + custinfo["sEmailId"] + "</ns1:Email>"));
                    sb.Append(string.Format("<ns1:Address>"));
                    sb.Append(string.Format("<ns1:CountryName>"));
                    sb.Append(string.Format("<ns1:CountryName>" + custinfo["sCountry"] + "</ns1:CountryName>"));
                    sb.Append(string.Format("<ns1:CountryCode>" + custinfo["CountryNameCode"] + "</ns1:CountryCode>"));
                    sb.Append(string.Format("</ns1:CountryName>"));
                    sb.Append(string.Format("<ns1:CityName>" + custinfo["sCity"] + "</ns1:CityName>"));
                    sb.Append(string.Format("</ns1:Address>"));
                    sb.Append(string.Format("</ns1:ContactInfo>"));
                    sb.Append(string.Format("</ns1:AAAirBookRQExt>"));
                    sb.Append(string.Format("</soap:Body>"));
                    sb.Append(string.Format("</soap:Envelope>"));

                    #endregion
                    sb.ToString();

                    BookResult = PostXml(Url, sb.ToString(), dt.Rows[0]["sno"].ToString().Split('~')[2].ToString());
                    string Pnr = "";
                    XDocument BookingRes = XDocument.Parse(BookResult.ToString());
                    XNamespace Fns = "http://www.opentravel.org/OTA/2003/05";
                    var BookingRefId = from item in BookingRes.Descendants(Fns + "BookingReferenceID")
                                       select new
                                       {
                                           BookingRefId = item.Attribute("ID").Value
                                       };

                    BookingDT = new DataTable();
                    BookingDT.Columns.Add("PNRId");
                    BookingDT.Columns.Add("ReqXml");
                    BookingDT.Columns.Add("ResXml");
                    BookingDT.Rows.Add(0);
                    try
                    {
                        Pnr = BookingRefId.ElementAt(0).BookingRefId;
                        BookingDT.Rows[0]["PNRId"] = Pnr;
                        BookingDT.Rows[0]["ReqXml"] = sb;
                        BookingDT.Rows[0]["ResXml"] = BookResult;

                    }
                    catch (Exception ex1)
                    {
                        BookingDT.Rows[0]["PNRId"] = "";
                        BookingDT.Rows[0]["ReqXml"] = sb.ToString();
                        BookingDT.Rows[0]["ResXml"] = BookResult + ex1.Message;
                    }
                }
                else
                {
                    BookingDT = new DataTable();
                    BookingDT.Columns.Add("PNRId");
                    BookingDT.Columns.Add("ReqXml");
                    BookingDT.Columns.Add("ResXml");
                    BookingDT.Rows.Add(0);
                    BookingDT.Rows[0]["PNRId"] = "";
                    BookingDT.Rows[0]["ReqXml"] = "";
                    BookingDT.Rows[0]["ResXml"] = objResultG9[0].OriginalTF.ToString() + " FARE CHANGED OR NOT AVAILABLE";
                }
            }

            catch (Exception ex)
            {
                BookingDT = new DataTable();
                BookingDT.Columns.Add("PNRId");
                BookingDT.Columns.Add("ReqXml");
                BookingDT.Columns.Add("ResXml");
                BookingDT.Rows.Add(0);
                BookingDT.Rows[0]["PNRId"] = "";
                BookingDT.Rows[0]["ReqXml"] = sb.ToString();
                BookingDT.Rows[0]["ResXml"] = BookResult + ex.Message;
                return BookingDT;


            }
            return BookingDT;

        }



    }







}



