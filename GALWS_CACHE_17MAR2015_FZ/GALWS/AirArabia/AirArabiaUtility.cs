﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
namespace GALWS.AirArabia
{
  public  class AirArabiaUtility
    {
        public static void SaveFile(string Res, string module)
        {
            string newFileName = module + DateTime.Now.ToString("hh_mm_ss");
            string activeDir = "";
            if (newFileName.Contains("SearchReq_") || newFileName.Contains("SearchResp_") || newFileName.Contains("AirPricingReq_") || newFileName.Contains("AirPricingResp_"))
                activeDir = ConfigurationManager.AppSettings["AirArabiaSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\";
            else
            {
                string[] orderid = module.Split('_');
                activeDir = ConfigurationManager.AppSettings["AirArabiaSaveUrl"] + @"\" + orderid[1] + @"\";
                newFileName = newFileName.Replace(orderid[1], "");
            }

            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }
            try
            {
                string path = activeDir + newFileName + ".xml";//@"c:\temp\MyTest.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.Write(Res);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static string AirArabiaPostXML(string url,  string requestData,string JSessionId)
        {
            string responseXML = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                request.Method = "POST";
                request.ContentType = "text/xml";
                request.Headers.Add("Accept-Encoding", "gzip");
                request.ReadWriteTimeout = 200000;
                request.Timeout = 200000;
                if (requestData.Contains("OTA_AirAvailRQ") == false)
                    request.Headers.Add(HttpRequestHeader.Cookie, JSessionId);
                //else
                //    request.Headers.Add("SOAPAction", "getAllPriceAvailability");
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                if (rsp == null)
                {
                    //throw exception
                }
                using (HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        //throw new ApplicationException(message);
                    }
                    else
                    {
                         if (requestData.Contains("OTA_AirAvailRQ") == true)
                            JSessionId = STD.BAL.Utility.Split(WebResponse.Headers["Set-Cookie"].ToString(), ";")[0].ToString();
                        if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
                            {
                                responseXML = readStream.ReadToEnd();
                            }
                        }
                        else
                        {
                            StreamReader reader = new StreamReader(rsp, Encoding.Default);
                            responseXML = reader.ReadToEnd();
                        }
                    }

                }
                
            }
            catch (WebException webEx)
            {
                if (webEx != null)
                {
                    WebResponse response = webEx.Response;
                    Stream stream = response.GetResponseStream();
                    responseXML = new StreamReader(stream).ReadToEnd();
                }
                else
                    responseXML = "<Errors>" + webEx.Message + "</Errors>";
            }
            catch (Exception ex)
            {
                responseXML = "<Errors>" + ex.Message + "</Errors>";
            }
            if (requestData.Contains("OTA_AirAvailRQ") == true)
                return JSessionId + "##" + responseXML;
            else
                return responseXML;
        }
        public static string AirArabiaPricingPostXML(string url, string requestData)
        {
            string responseXML = string.Empty,  JSessionId="";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                request.Method = "POST";
                request.ContentType = "text/xml";
                request.Headers.Add("Accept-Encoding", "gzip");
                request.ReadWriteTimeout = 200000;
                request.Timeout = 200000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                if (rsp == null)
                {
                    //throw exception
                }
                using (HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        //throw new ApplicationException(message);
                    }
                    else
                    {
                        JSessionId = STD.BAL.Utility.Split(WebResponse.Headers["Set-Cookie"].ToString(), ";")[0].ToString();
                        if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
                            {
                                responseXML = readStream.ReadToEnd();
                            }
                        }
                        else
                        {
                            StreamReader reader = new StreamReader(rsp, Encoding.Default);
                            responseXML = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException webEx)
            {
                if (webEx != null)
                {
                    WebResponse response = webEx.Response;
                    Stream stream = response.GetResponseStream();
                    responseXML = new StreamReader(stream).ReadToEnd();
                }
                else
                    responseXML = "<Errors>" + webEx.Message + "</Errors>";
            }
            catch (Exception ex)
            {
                responseXML = "<Errors>" + ex.Message + "</Errors>";
            }
            return JSessionId + "~" + responseXML;
        }
        public int UpdateAirArabiaSessionId(string NewSessionId, string TrackId, string connectionString)
        {
            int i = 0;
            try
            {
                SqlDatabase DBHelper = new SqlDatabase(connectionString);
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "SP_UpdateAirAsiaSessionId";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Track_id", DbType.String, TrackId);
                DBHelper.AddInParameter(cmd, "@Searchvalue", DbType.String, NewSessionId);
                i = Convert.ToInt32(DBHelper.ExecuteNonQuery(cmd));
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("UpdateAirAsiaSessionId", "Error_001", ex, "AirArabiaFlight");
            }
            return i;
        }
        public int UpdateAirArabiaLogInfo(string LogType, string OrderId, string Request, string Response, string PnrNo, string connectionString)
        {
            int i = 0;
            try
            {
                SqlDatabase DBHelper = new SqlDatabase(connectionString);
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "USP_InsertAirArabiaLog";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@LogType", DbType.String, LogType);
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, OrderId);
                DBHelper.AddInParameter(cmd, "@Request", DbType.String, Request);
                DBHelper.AddInParameter(cmd, "@Response", DbType.String, Response);
                DBHelper.AddInParameter(cmd, "@PnrNo", DbType.String, PnrNo);            
                i = Convert.ToInt32(DBHelper.ExecuteNonQuery(cmd));
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("UpdateAirArabiaLogInfo", "Error_001", ex, "AirArabiaFlight");
            }
            return i;
        }
        public static List<GALWS.AirAsia.CurrencyRate> CurrancyExchangeRate(string CountryCode, string constr, List<GALWS.AirAsia.CurrencyRate> currancyinfo)
        {
            FlightCancellationDAL objdals = new FlightCancellationDAL(constr);
            return objdals.CurrancyExchangeRate(CountryCode, currancyinfo);
        }
    }
}
