﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using ITZERRORLOG;
using STD.Shared;
using STD.BAL;
namespace GALWS.AirArabia
{
   public class AirArabiaFlightSearch
    {
       public List<FlightSearchResults> GetAirArabiaAvailibility(FlightSearch objFlt, bool RTFS, string fareType, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, List<FlightCityList> CityList, CredentialList CrdList, DataTable PromoCodeDt, List<MISCCharges> MiscList)
       {
           List<FlightSearchResults> FinalLists = new List<FlightSearchResults>();
           FinalLists = GetAirArabiaAvailibilityWithLowestPrice(objFlt, RTFS, fareType, SrvchargeList, MarkupDs, constr,  CityList, CrdList, PromoCodeDt, MiscList);
          // FinalLists = AirArabiaAvailibilityWithCallPricing(objFlt, RTFS, fareType, SrvchargeList, MarkupDs, constr, srvCharge, CityList, CrdList, PromoCodeDt);
           return FinalLists;
       }
       public List<FlightSearchResults> GetAirArabiaAvailibilityWithLowestPrice(FlightSearch objFlt, bool RTFS, string fareType, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr,  List<FlightCityList> CityList, CredentialList CrdList, DataTable PromoCodeDt, List<MISCCharges> MiscList)
        {
            List<FlightSearchResults> fsrList = new List<FlightSearchResults>(); List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            try
            {
                string AvailibilityRequest = AirArabiaAvailibilityRequest(objFlt, fareType,  CrdList.UserID, CrdList.Password);
                AirArabiaUtility.SaveFile(AvailibilityRequest, "SearchReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");
                 string[] res=Utility.Split(AirArabiaUtility.AirArabiaPostXML(CrdList.LoginPWD, AvailibilityRequest,""),"##");
                String JSessionId = res[0].ToString();
                string AvailibilityResponse = res[1].ToString();
                AirArabiaUtility.SaveFile(AvailibilityResponse, "SearchResp_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");
                
                XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; XNamespace a = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";
                try
                {
                    if (AvailibilityResponse.IndexOf("No availability") < 1 || AvailibilityResponse.IndexOf("Invalid") < 1 || AvailibilityResponse == "" || AvailibilityResponse == null)
                    {
                        if (AvailibilityResponse.Contains("<ns1:Success />") || AvailibilityResponse.Contains("<ns1:Success/>"))
                        {
                            XDocument AirArabisDocument = XDocument.Parse(AvailibilityResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                            //XElement AvailabilityResult = AirArabisDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "AA_OTA_AirAllPriceAvailRS");
                            XElement AvailabilityResult = AirArabisDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirAvailRS");
                            string EchoToken = AvailabilityResult.Attribute("EchoToken").Value;
                            string TransactionIdentifier = AvailabilityResult.Attribute("TransactionIdentifier").Value;
                            string Version = AvailabilityResult.Attribute("Version").Value;
                         
#region Sectors 
                            //IEnumerable<XElement> OriginDestinationInfo = AvailabilityResult.Elements(ns1 + "OriginDestinationInformation");
                            //  List<OriginDestinationInformation> objArrDept= new List<OriginDestinationInformation>();
                            //   int Sequence = 0; 
                            //foreach (var OrginDepartur in OriginDestinationInfo)
                            //{
                            //    List<OriginDestinationOption> objOrgdesc = new List<OriginDestinationOption>();
                            //    int Segment = 0;string FlightNOKey="";
                            //    IEnumerable<XElement> OriginDestinationOption = OrginDepartur.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption");
                            //    foreach (var OrginDepartures in OriginDestinationOption)
                            //    {
                                    
                            //        objOrgdesc.Add(new OriginDestinationOption
                            //            {
                            //                ArrivalDateTime = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value,
                            //                DepartureDateTime = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value,
                            //                FlightNumber = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value,
                            //                RPH = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("RPH").Value,
                            //                JourneyDuration = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("JourneyDuration").Value,
                            //                ArrivalAirport = OrginDepartures.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value,
                            //                DepartureAirport = OrginDepartures.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value,
                            //                Segment = Segment++,
                            //            });
                            //         if(Segment > 1)
                            //            FlightNOKey +="|";
                            //        FlightNOKey += OrginDepartures.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value;
                            //    }
                            //    objArrDept.Add(new OriginDestinationInformation
                            //    {
                            //        ArrivalDateTime = OrginDepartur.Element(ns1 +"ArrivalDateTime").Value,
                            //        DepartureDateTime = OrginDepartur.Element(ns1 +"DepartureDateTime").Value,
                            //        OriginLocationCode = OrginDepartur.Element(ns1 + "OriginLocation").Attribute("LocationCode").Value,
                            //        DestinationLocationCode = OrginDepartur.Element(ns1 + "DestinationLocation").Attribute("LocationCode").Value,
                            //        DestinationLocationName = OrginDepartur.Element(ns1 + "DestinationLocation").Value,
                            //        OriginLocationName = OrginDepartur.Element(ns1 + "DestinationLocation").Value,
                            //        OriginDestinationOptions = objOrgdesc,
                            //        LineNo = Sequence++,
                            //        FlightNOKey=FlightNOKey,
                            //    });
                            //}
#endregion
                            try
                            {
                                XElement objorgdept = AvailabilityResult.Element(ns1 + "OriginDestinationInformation");
                                IEnumerable<XElement> PricedItineraries = AvailabilityResult.Element(ns1 + "AAAirAvailRSExt").Elements(ns1 + "PricedItineraries");
                                int lineNum = 1;
                                foreach (var Priceetenry in PricedItineraries)
                                {
                                    //OriginDestinationInformation OrginDepartur= objArrDept.Where( x => x.FlightNOKey =="")
                                    XElement PriceQuetResult = Priceetenry.Element(ns1 + "PricedItinerary");
                                    XElement AirItineraryPricingInfo = PriceQuetResult.Element(ns1 + "AirItineraryPricingInfo");
                                    XElement AirItinerary = PriceQuetResult.Element(ns1 + "AirItinerary");
                                    IEnumerable<XElement> OriginDestinationS = AirItinerary.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption");

                                    TimeSpan tsTimeSpan = new TimeSpan();
                                    IEnumerable<XElement> OriginDestinationSDep = OriginDestinationS.Where(x => x.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value.Trim() == objFlt.HidTxtDepCity.Split(',')[0].Trim());
                                    IEnumerable<XElement> OriginDestinationSArr = OriginDestinationS.Where(x => x.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value.Trim() == objFlt.HidTxtArrCity.Split(',')[0].Trim());
                                    if (OriginDestinationSDep.Count() > 0 && OriginDestinationSArr.Count() > 0)
                                    {
                                        DateTime STA = Convert.ToDateTime(OriginDestinationSArr.ElementAt(0).Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value.Trim());
                                        tsTimeSpan = STA.Subtract(Convert.ToDateTime(OriginDestinationSDep.ElementAt(0).Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value.Trim()));
                                    }
                                        //Sectoresdtails.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value.Trim()
                                    //objFlt.HidTxtDepCity.Split(',')[0] || objfsr.DepAirportCode == objFlt.HidTxtArrCity.Split(',')[0]
                                    DataTable CommDt = new DataTable(); Hashtable STTFTDS = new Hashtable();
                                    int seg = OriginDestinationS.Count(); 
                                    if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                        seg = Decimal.ToInt32(seg / 2);
                                    seg = seg - 1;
                                    int legno = 1, flight = 1;
                                    foreach (var Sectoresdtails in OriginDestinationS)
                                            {
                                                FlightSearchResults objfsr = new FlightSearchResults();
                                                float srvChargeAdt = 0, srvChargeChd = 0;
                                                objfsr.Stops = seg.ToString() + "-Stop";
                                                objfsr.LineNumber = lineNum; //Segment.Count()
                                                objfsr.Flight = flight.ToString();
                                                objfsr.Leg = legno;
                                                legno++;
                                                objfsr.Adult = objFlt.Adult;
                                                objfsr.Child = objFlt.Child;
                                                objfsr.Infant = objFlt.Infant;

                                                objfsr.depdatelcc = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value.Trim();//Convert.ToDateTime(fltdtl.Element(a + "STD").Value.Trim()).ToString();
                                                objfsr.arrdatelcc = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value.Trim(); //Convert.ToDateTime(fltdtl.Element(a + "STA").Value.Trim()).ToString();
                                                objfsr.Departure_Date = Convert.ToDateTime(objfsr.depdatelcc).ToString("dd MMM");
                                                objfsr.Arrival_Date = Convert.ToDateTime(objfsr.arrdatelcc).ToString("dd MMM");
                                                objfsr.DepartureDate = Convert.ToDateTime(objfsr.depdatelcc).ToString("ddMMyy");
                                                objfsr.ArrivalDate = Convert.ToDateTime(objfsr.arrdatelcc).ToString("ddMMyy");
                                                //
                                                objfsr.FlightIdentification = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value.Trim();
                                                objfsr.AirLineName = "Air Arabia";
                                                objfsr.ValiDatingCarrier = "G9";
                                                objfsr.OperatingCarrier = "G9";
                                                objfsr.MarketingCarrier = "G9";
                                                objfsr.DepartureLocation = Sectoresdtails.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value.Trim();
                                                objfsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objfsr.DepartureLocation);
                                                objfsr.DepartureTime = Convert.ToDateTime(objfsr.depdatelcc).ToString("HHmm");
                                                objfsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.DepartureLocation);
                                                objfsr.DepartureTerminal = "" ;
                                                objfsr.DepAirportCode = objfsr.DepartureLocation;

                                                objfsr.ArrivalLocation = Sectoresdtails.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value.Trim();
                                                objfsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objfsr.ArrivalLocation);
                                                objfsr.ArrivalTime = Convert.ToDateTime(objfsr.arrdatelcc).ToString("HHmm");
                                                objfsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.ArrivalLocation);
                                                objfsr.ArrivalTerminal = "" ;
                                                objfsr.ArrAirportCode = objfsr.ArrivalLocation;
                                                objfsr.Trip = objFlt.Trip.ToString();
                                                DateTime STANew = Convert.ToDateTime(objfsr.arrdatelcc);
                                                TimeSpan tsTimeSpanNew = STANew.Subtract(Convert.ToDateTime(objfsr.depdatelcc));
                                                objfsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpan.TotalMinutes));
                                                objfsr.TripCnt = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpanNew.TotalMinutes));
                                                objfsr.Trip = objFlt.Trip.ToString();
                                                #region Adding Fare
                                                #region Fare Calculation
                                                IEnumerable<XElement> Faresbreakups = AirItineraryPricingInfo.Element(ns1 + "PTC_FareBreakdowns").Elements(ns1 + "PTC_FareBreakdown");
                                                objfsr.fareBasis = Faresbreakups.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.EQ = "";
                                                
                                                objfsr.AvailableSeats = "";//Sectoresdtails
                                              
                                                IEnumerable<XElement> objAdt = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "ADT");
                                                IEnumerable<XElement> objChd = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "CHD");
                                                IEnumerable<XElement> objInf = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "INF");

                                                #region Adult Fare calculation  
                                                decimal ABfare = 0, ATax = 0, AOT = 0;
                                                foreach (var adt in objAdt)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge = 0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    ABfare += (basefare * Quantity);
                                                    ATax += (Tax * Quantity);
                                                    AOT += (othrcharge * Quantity);
                                                }
                                               // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;

                                                objfsr.AdtBfare = (float)Math.Ceiling(ABfare / objfsr.Adult);
                                                objfsr.AdtFSur = 0;
                                                objfsr.AdtWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                #region Get MISC Markup Charges Date 26-12-2018
                                                try
                                                {
                                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, CrdList.CrdType, Convert.ToString(objfsr.AdtBfare), Convert.ToString(objfsr.AdtFSur));
                                                }
                                                catch { srvChargeAdt = 0; }
                                                #endregion
                                                objfsr.AdtTax = (float)Math.Ceiling((ATax + AOT) / objfsr.Adult) + objfsr.AdtWO +  srvChargeAdt;
                                                objfsr.AdtOT = objfsr.AdtTax;
                                                objfsr.AdtCabin = "Y" ;
                                                objfsr.AdtRbd = "EC";
                                                objfsr.AdtFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.AdtFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                objfsr.AdtFare = objfsr.AdtTax + objfsr.AdtBfare + objfsr.AdtWO + objfsr.AdtFSur; //  + SegmentSellKey

                                                objfsr.FBPaxType = "ADT";
                                                objfsr.ElectronicTicketing = "";
                                                objfsr.AdtFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                                // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;

                                                //objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());
                                                objfsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());
                                                if ((objFlt.Trip.ToString() == JourneyType.I.ToString()) && (objFlt.IsCorp == true))
                                                {
                                                    objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                    objfsr.AdtBfare = objfsr.AdtBfare + objfsr.ADTAdminMrk;
                                                    objfsr.AdtFare = objfsr.AdtFare + objfsr.ADTAdminMrk;
                                                }
                                                CommDt.Clear(); STTFTDS.Clear();
                                                // CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo);
                                                //CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                         try
                                            {
                                            objfsr.AdtDiscount = 0;
                                            objfsr.AdtCB = 0;
                                            objfsr.AdtSrvTax = 0;
                                            objfsr.AdtTF = 0;
                                            objfsr.AdtTds = 0;
                                            objfsr.IATAComm = 0;
                                            objfsr.AdtEduCess = 0;
                                            objfsr.AdtHighEduCess = 0;
                                            //CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                            //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, objFlt.TDS);
                                            //objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                            //objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());

                                            //objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                            //objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                            //objfsr.AdtEduCess = 0;
                                            //objfsr.AdtHighEduCess = 0;
                                            //objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                            //objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                        }
                                        catch (Exception es)
                                         {
                                             objfsr.AdtDiscount1 = 0; objfsr.AdtCB = 0; objfsr.AdtSrvTax1 = 0; objfsr.AdtTds = 0;
                                             objfsr.AdtDiscount = 0; objfsr.AdtEduCess = 0; objfsr.AdtHighEduCess = 0; objfsr.AdtTF = 0;
                                         }
                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.AdtFare.ToString()));
                                                        objfsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion

                                                #region Child Fare calculation
                                                decimal ChdBFare = 0, CTax = 0, COT = 0;
                                                foreach (var adt in objChd)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge = 0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    ChdBFare += (basefare * Quantity);
                                                    CTax += (Tax * Quantity);
                                                    COT += (othrcharge * Quantity);
                                                }
                                                // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc; 
                                                if (objfsr.Child > 0 && ChdBFare >0)
                                                    objfsr.ChdBFare = (float)Math.Ceiling(ChdBFare / objfsr.Child);
                                                else
                                                    objfsr.ChdBFare = (float)Math.Ceiling(ChdBFare);
                                                objfsr.ChdFSur = 0;
                                                
                                                #endregion
                                                objfsr.ChdWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                if (objfsr.Child > 0 && CTax > 0)
                                                    objfsr.ChdTax = (float)Math.Ceiling((CTax + COT )/objfsr.Child) + objfsr.ChdWO ;
                                                else
                                                    objfsr.ChdTax = (float)Math.Ceiling(CTax + COT) + objfsr.ChdWO ;
                                                if (objfsr.Child > 0)
                                                {
                                                    #region Get MISC Markup Charges Date 21-12-2018
                                                    try
                                                    {
                                                        srvChargeChd = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, CrdList.CrdType, Convert.ToString(objfsr.ChdBFare), Convert.ToString(objfsr.ChdFSur));
                                                    }
                                                    catch { srvChargeChd = 0; }
                                                }
                                                objfsr.ChdTax = objfsr.ChdTax + srvChargeChd;
                                                objfsr.ChdOT = objfsr.ChdTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                objfsr.ChdCabin = "Y";
                                                objfsr.ChdRbd = "";
                                                objfsr.ChdFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.ChdfareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                objfsr.ChdFare = objfsr.ChdTax + objfsr.ChdBFare + objfsr.ChdWO + objfsr.ChdFSur; //  + SegmentSellKey

                                                objfsr.ChdFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                                // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;

                                              //  objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                               // objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                objfsr.CHDAdminMrk = 0;
                                                objfsr.CHDAgentMrk = 0;
                                                if (objfsr.Child > 0)
                                                {
                                                    objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                    if ((objFlt.Trip.ToString() == JourneyType.I.ToString()) && (objFlt.IsCorp == true))
                                                    {
                                                        objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                        objfsr.ChdBFare = objfsr.ChdBFare + objfsr.CHDAdminMrk;
                                                        objfsr.ChdFare = objfsr.ChdFare + objfsr.CHDAdminMrk;
                                                    }
                                                }
                                                CommDt.Clear(); STTFTDS.Clear();
                                                // CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo);
                                                //CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.ChdCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                               // STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, objFlt.TDS);
                                                try
                                                {
                                            objfsr.ChdDiscount = 0;
                                            objfsr.ChdCB = 0;
                                            objfsr.ChdSrvTax = 0;
                                            objfsr.ChdTF = 0;
                                            objfsr.ChdTds = 0;
                                            objfsr.IATAComm = 0;
                                            objfsr.ChdEduCess = 0;
                                            objfsr.ChdHighEduCess = 0;
                                            objfsr.ChdEduCess = 0;
                                            objfsr.ChdHighEduCess = 0;
                                        }
                                                catch (Exception ex)
                                                {
                                                    objfsr.ChdEduCess = 0; objfsr.ChdHighEduCess = 0; objfsr.ChdDiscount1 = 0; objfsr.ChdCB = 0;
                                                    objfsr.ChdSrvTax1 = 0; objfsr.ChdDiscount = 0; objfsr.ChdTF = 0; objfsr.ChdTds = 0;
                                                }  
                                              
                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.ChdFare.ToString()));
                                                        objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion

                                                #region Infant Fare calculation
                                                decimal INFBFare = 0, INFTax = 0, INFOtherTax = 0;
                                                foreach (var adt in objInf)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge=0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    INFBFare += basefare * Quantity;
                                                    INFTax += Tax * Quantity;
                                                    INFOtherTax += othrcharge * Quantity;
                                                }
                                                // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;
                                                if (objfsr.Infant > 0 && INFBFare > 0)
                                                    objfsr.InfBfare = (float)Math.Ceiling(INFBFare / objfsr.Infant);
                                                else
                                                    objfsr.InfBfare = (float)Math.Ceiling(INFBFare);
                                                objfsr.InfFSur = 0;
                                                objfsr.InfWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                if (objfsr.Infant > 0 && (INFTax + INFOtherTax) > 0)
                                                    objfsr.InfTax = (float)Math.Ceiling((INFTax + INFOtherTax) / objfsr.Infant);
                                                else
                                                    objfsr.InfTax = (float)Math.Ceiling(INFTax + INFOtherTax) + objfsr.InfWO ;
                                                objfsr.InfOT = objfsr.InfTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                objfsr.InfCabin = "Y";
                                                objfsr.InfRbd = "";
                                                objfsr.InfFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.InfFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                objfsr.InfFare = objfsr.InfTax + objfsr.InfBfare + objfsr.InfWO + objfsr.InfFSur; 
                                                objfsr.InfFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.InfBfare.ToString()), decimal.Parse(objfsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.InfFare.ToString()));
                                                        objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion
                                               
                                                #endregion
                                                //XElement MealBaggageDescription = Sectoresdtails.Parent;
                                                //XElement bundledService = MealBaggageDescription.Element(ns1 + "AABundledServiceExt");
                                                string bundleinfo = "";
                                                //if (bundledService != null)
                                                //{
                                                //    objfsr.BagInfo = bundledService.Element(ns1 + "bundledService").Element(ns1 + "description").Value;
                                                //    bundleinfo = bundledService.Element(ns1 + "bundledService").Element(ns1 + "bunldedServiceId").Value.Trim();
                                                //    bundleinfo += ":" + bundledService.Element(ns1 + "bundledService").Element(ns1 + "bundledServiceName").Value.Trim();
                                                //    bundleinfo += ":" + bundledService.Element(ns1 + "bundledService").Element(ns1 + "perPaxBundledFee").Value.Trim();
                                                //    bundleinfo += bundledService.Element(ns1 + "bundledService").Element(ns1 + "bookingClasses") != null ? ":" + bundledService.Element(ns1 + "bundledService").Element(ns1 + "bookingClasses").Value.Trim() : ":";
                                                //}
                                                   
                                                //else
                                                    objfsr.BagInfo = "Hand Baggage: 7 KG included.";
                                                objfsr.RBD = objfsr.AdtRbd + ":" + objfsr.ChdRbd + ":" + objfsr.InfRbd;
                                                objfsr.FareRule = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("RPH").Value.Trim();
                                                objfsr.sno = TransactionIdentifier + ":" + Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("RPH").Value.Trim() + "::" + EchoToken + ":" + Version + ":" + PriceQuetResult.Attribute("SequenceNumber").Value + ":" + AirItineraryPricingInfo.Element(ns1 + "ItinTotalFare").Element(ns1 + "BaseFare").Attribute("CurrencyCode").Value + ":" + seg.ToString() + ":" + bundleinfo;
                                                objfsr.Searchvalue = JSessionId;
                                                objfsr.OriginalTF = (float)(ABfare + ATax + AOT  + ChdBFare + CTax + COT + INFBFare + INFTax + INFOtherTax);
                                                objfsr.OriginalTT = (srvChargeAdt * objfsr.Adult) + (srvChargeChd * objfsr.Child);
                                                objfsr.Provider = "LCC";
                                                //Rounding the fare
                                                objfsr.AdtFare = (float)Math.Ceiling(objfsr.AdtFare);
                                                objfsr.ChdFare = (float)Math.Ceiling(objfsr.ChdFare);
                                                objfsr.InfFare = (float)Math.Ceiling(objfsr.InfFare);

                                                objfsr.STax = (objfsr.AdtSrvTax * objfsr.Adult) + (objfsr.ChdSrvTax * objfsr.Child)+ (objfsr.InfSrvTax * objfsr.Infant);
                                                objfsr.TFee = (objfsr.AdtTF * objfsr.Adult) + (objfsr.ChdTF * objfsr.Child) + (objfsr.InfTF * objfsr.Infant);
                                                objfsr.TotDis = (objfsr.AdtDiscount * objfsr.Adult) + (objfsr.ChdDiscount * objfsr.Child);
                                                objfsr.TotCB = (objfsr.AdtCB * objfsr.Adult) + (objfsr.ChdCB * objfsr.Child);
                                                objfsr.TotTds = (objfsr.AdtTds * objfsr.Adult) + (objfsr.ChdTds * objfsr.Child) + (objfsr.InfTds * objfsr.Infant);
                                                objfsr.TotMgtFee = (objfsr.AdtMgtFee * objfsr.Adult) + (objfsr.ChdMgtFee * objfsr.Child) + (objfsr.InfMgtFee * objfsr.Infant);
                                                objfsr.TotalFuelSur = (objfsr.AdtFSur * objfsr.Adult) + (objfsr.ChdFSur * objfsr.Child) + (objfsr.InfFSur * objfsr.Infant);
                                                objfsr.TotalTax = (objfsr.AdtTax * objfsr.Adult) + (objfsr.ChdTax * objfsr.Child) + (objfsr.InfTax * objfsr.Infant);
                                                objfsr.TotBfare = (objfsr.AdtBfare * objfsr.Adult) + (objfsr.ChdBFare * objfsr.Child) + (objfsr.InfBfare * objfsr.Infant);
                                                objfsr.TotMrkUp = ((objfsr.ADTAdminMrk + objfsr.ADTAgentMrk) * objfsr.Adult) + ((objfsr.CHDAdminMrk + objfsr.CHDAgentMrk) * objfsr.Child);
                                                objfsr.TotalFare = (objfsr.AdtFare * objfsr.Adult) + (objfsr.ChdFare * objfsr.Child) + (objfsr.InfFare * objfsr.Infant) + objfsr.TotMrkUp + objfsr.STax + objfsr.TFee + objfsr.TotMgtFee;
                                                objfsr.NetFare = (objfsr.TotalFare + objfsr.TotTds) - (objfsr.TotDis + objfsr.TotCB + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAgentMrk * objfsr.Child));
                                                    #endregion  

                                                if (objorgdept.Element(ns1 + "DestinationLocation").Attribute("LocationCode").Value == objorgdept.Element(ns1 + "OriginLocation").Attribute("LocationCode").Value)
                                                    objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                else
                                                    objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                                if (objfsr.ArrAirportCode == objFlt.HidTxtDepCity.Split(',')[0] || objfsr.DepAirportCode == objFlt.HidTxtArrCity.Split(',')[0])
                                                {
                                                    flight++;
                                                    if (flight % 2 == 1)
                                                        flight--;
                                                    legno = 1;
                                                    objfsr.Leg = legno;
                                                    objfsr.Flight = flight.ToString();
                                                }
                                                if (objorgdept.Element(ns1 + "DestinationLocation").Attribute("LocationCode").Value == objorgdept.Element(ns1 + "OriginLocation").Attribute("LocationCode").Value && flight == 2)
                                                {
                                                        objfsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                                        objfsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                                        TimeSpan tsTimeSpanRet = new TimeSpan();
                                                        IEnumerable<XElement> OriginDestinationSDepRet = OriginDestinationS.Where(x => x.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value.Trim() == objFlt.HidTxtArrCity.Split(',')[0].Trim());
                                                        IEnumerable<XElement> OriginDestinationSArrRet = OriginDestinationS.Where(x => x.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value.Trim() == objFlt.HidTxtDepCity.Split(',')[0].Trim());
                                                        if (OriginDestinationSDepRet.Count() > 0 && OriginDestinationSArrRet.Count() > 0)
                                                        {
                                                            DateTime STARet = Convert.ToDateTime(OriginDestinationSArrRet.ElementAt(0).Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value.Trim());
                                                            tsTimeSpanRet = STARet.Subtract(Convert.ToDateTime(OriginDestinationSDepRet.ElementAt(0).Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value.Trim()));
                                                            objfsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpanRet.TotalMinutes));
                                                        }
                                                }
                                                else
                                                {
                                                    objfsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                                    objfsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                                }
                                                if (flight == 1)
                                                    objfsr.TripType = "O";
                                                else if (flight == 2)
                                                    objfsr.TripType = "R";
                                                else objfsr.TripType = "M";
                                                fsrList.Add(objfsr);    
                                                //if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                                //{
                                                //    if (flight == 1)
                                                //        fsrList.Add(objfsr);
                                                //    else
                                                //        RfsrList.Add(objfsr);
                                                //}
                                                //else
                                                //    fsrList.Add(objfsr);    
                                            }
                                            lineNum++;  
                                }
                            }
                            catch(Exception ex)
                            {
                                ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight");
                            }
                        }
                    }
                  
                }
                catch (Exception ex)
                { ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight"); }
                
                FinalList =fsrList;
                if (RfsrList.Count != 0)
                    FinalList =RfsrList;
                if (objFlt.TripType == TripType.RoundTrip && fsrList.Count >0 && RfsrList.Count >0)  //RTF True
                    FinalList = RoundTripFare(fsrList, RfsrList);
            }
            catch(Exception ex)
            {
                ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight");
            }
            return objFltComm.AddFlightKey(FinalList, RTFS);
       }
        public List<FlightSearchResults> AirArabiaAvailibilityWithCallPricing(FlightSearch objFlt, bool RTFS, string fareType, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, float srvCharge, List<FlightCityList> CityList, CredentialList CrdList, DataTable PromoCodeDt)
        {
            List<FlightSearchResults> fsrList = new List<FlightSearchResults>(); List<FlightSearchResults> RfsrList = new List<FlightSearchResults>(); List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            try
            {
                string AvailibilityRequest = AirArabiaAvailibilityRequest(objFlt, fareType, CrdList.UserID, CrdList.Password);
                AirArabiaUtility.SaveFile(AvailibilityRequest, "SearchReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");
                string[] res = Utility.Split(AirArabiaUtility.AirArabiaPostXML(CrdList.LoginPWD, AvailibilityRequest, ""), "##");
                String JSessionId = res[0].ToString();
                string AvailibilityResponse = res[1].ToString();
                AirArabiaUtility.SaveFile(AvailibilityResponse, "SearchResp_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");

                XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; XNamespace a = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";
                try
                {
                    if (AvailibilityResponse.IndexOf("No availability") < 1 || AvailibilityResponse.IndexOf("Invalid") < 1 || AvailibilityResponse == "" || AvailibilityResponse == null)
                    {
                        if (AvailibilityResponse.Contains("<ns1:Success />") || AvailibilityResponse.Contains("<ns1:Success/>"))
                        {
                            XDocument AirArabisDocument = XDocument.Parse(AvailibilityResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                            XElement AvailabilityResult = AirArabisDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirAvailRS");
                            string EchoToken = AvailabilityResult.Attribute("EchoToken").Value;
                            string TransactionIdentifier = AvailabilityResult.Attribute("TransactionIdentifier").Value;
                            string Version = AvailabilityResult.Attribute("Version").Value;

                            #region Sectors
                            IEnumerable<XElement> OriginDestinationInfo = AvailabilityResult.Elements(ns1 + "OriginDestinationInformation");
                            List<OriginDestinationInformation> objArrDept = new List<OriginDestinationInformation>();
                            int Sequence = 0;
                            foreach (var OrginDepartur in OriginDestinationInfo)
                            {
                                List<OriginDestinationOption> objOrgdesc = new List<OriginDestinationOption>();
                                int Segment = 0; string FlightNOKey = "";
                                IEnumerable<XElement> OriginDestinationOption = OrginDepartur.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption");
                                foreach (var OrginDepartures in OriginDestinationOption)
                                {

                                    objOrgdesc.Add(new OriginDestinationOption
                                        {
                                            ArrivalDateTime = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value,
                                            DepartureDateTime = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value,
                                            FlightNumber = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value,
                                            RPH = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("RPH").Value,
                                            JourneyDuration = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("JourneyDuration").Value,
                                            ArrivalAirport = OrginDepartures.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value,
                                            DepartureAirport = OrginDepartures.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value,
                                            Segment = Segment++,
                                        });
                                    if (Segment > 1)
                                        FlightNOKey += "|";
                                    FlightNOKey += OrginDepartures.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value;
                                }
                                objArrDept.Add(new OriginDestinationInformation
                                {
                                    ArrivalDateTime = OrginDepartur.Element(ns1 + "ArrivalDateTime").Value,
                                    DepartureDateTime = OrginDepartur.Element(ns1 + "DepartureDateTime").Value,
                                    OriginLocationCode = OrginDepartur.Element(ns1 + "OriginLocation").Attribute("LocationCode").Value,
                                    DestinationLocationCode = OrginDepartur.Element(ns1 + "DestinationLocation").Attribute("LocationCode").Value,
                                    DestinationLocationName = OrginDepartur.Element(ns1 + "DestinationLocation").Value,
                                    OriginLocationName = OrginDepartur.Element(ns1 + "DestinationLocation").Value,
                                    OriginDestinationOptions = objOrgdesc,
                                    LineNo = Sequence++,
                                    FlightNOKey = FlightNOKey,
                                });
                            }
                            #endregion
                            try
                            {
                                XElement PricedItinerary = AvailabilityResult.Element(ns1 + "AAAirAvailRSExt").Element(ns1 + "PricedItineraries").Element(ns1 + "PricedItinerary");
                                string AirPricingreq = "", AirPricingResp = ""; int lineNum = 1;
                                foreach (OriginDestinationInformation OrginDepartur in objArrDept)
                                {
                                    string AirPricingRequest = PriceQuoteRequest_WithAvaibilty(objFlt, OrginDepartur, fareType, CrdList.UserID, CrdList.Password, EchoToken, TransactionIdentifier, Version);
                                    string AirPricingResponse = AirArabiaUtility.AirArabiaPostXML(CrdList.LoginPWD, AirPricingRequest, JSessionId);
                                    AirPricingreq += AirPricingRequest;
                                    AirPricingResp += AirPricingResponse;
                                    if (AirPricingResponse.IndexOf("No availability") < 1 || AirPricingResponse.IndexOf("Invalid") < 1 || AirPricingResponse == "" || AirPricingResponse == null)
                                    {
                                        if (AirPricingResponse.Contains("<ns1:Success />") || AvailibilityResponse.Contains("<ns1:Success/>"))
                                        { 
                                          //  XElement objorgdept = AvailabilityResult.Element(ns1 + "OriginDestinationInformation");
                                           // IEnumerable<XElement> PricedItineraries = AvailabilityResult.Element(ns1 + "AAAirAvailRSExt").Elements(ns1 + "PricedItineraries");

                                           
                                            XDocument AirArabispricingDocument = XDocument.Parse(AirPricingResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                            XElement OTA_AirPriceRS = AirArabispricingDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirPriceRS");
                                            XElement PriceQuetResult = OTA_AirPriceRS.Element(ns1 + "PricedItineraries").Element(ns1 + "PricedItinerary");
                                            XElement AirItineraryPricingInfo = PriceQuetResult.Element(ns1 + "AirItineraryPricingInfo");
                                            XElement AirItinerary = PriceQuetResult.Element(ns1 + "AirItinerary");

                                            DataTable CommDt = new DataTable(); Hashtable STTFTDS = new Hashtable();
                                            int seg = AirItinerary.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption").Count();
                                            if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                                seg = Decimal.ToInt32(seg / 2);
                                            else
                                                seg = seg - 1;
                                            int legno = 1, flight = 1;
                                            foreach (var Sectoresdtails in AirItinerary.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption"))
                                            {
                                               
                                        FlightSearchResults objfsr = new FlightSearchResults();
                                        objfsr.Stops = seg.ToString() + "-Stop";
                                        objfsr.LineNumber = lineNum; //Segment.Count()
                                        objfsr.Flight = flight.ToString();
                                        objfsr.Leg = legno;
                                        legno++;
                                        objfsr.Adult = objFlt.Adult;
                                        objfsr.Child = objFlt.Child;
                                        objfsr.Infant = objFlt.Infant;

                                        objfsr.depdatelcc = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value.Trim();//Convert.ToDateTime(fltdtl.Element(a + "STD").Value.Trim()).ToString();
                                        objfsr.arrdatelcc = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value.Trim(); //Convert.ToDateTime(fltdtl.Element(a + "STA").Value.Trim()).ToString();
                                        objfsr.Departure_Date = Convert.ToDateTime(objfsr.depdatelcc).ToString("dd MMM");
                                        objfsr.Arrival_Date = Convert.ToDateTime(objfsr.arrdatelcc).ToString("dd MMM");
                                        objfsr.DepartureDate = Convert.ToDateTime(objfsr.depdatelcc).ToString("ddMMyy");
                                        objfsr.ArrivalDate = Convert.ToDateTime(objfsr.arrdatelcc).ToString("ddMMyy");
                                        //
                                        objfsr.FlightIdentification = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value.Trim();
                                        objfsr.AirLineName = "Air Arabia";
                                        objfsr.ValiDatingCarrier = "G9";
                                        objfsr.OperatingCarrier = "G9";
                                        objfsr.MarketingCarrier = "G9";
                                        objfsr.DepartureLocation = Sectoresdtails.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value.Trim();
                                        objfsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objfsr.DepartureLocation);
                                        objfsr.DepartureTime = Convert.ToDateTime(objfsr.depdatelcc).ToString("HHmm");
                                        objfsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.DepartureLocation);
                                        objfsr.DepartureTerminal = "";
                                        objfsr.DepAirportCode = objfsr.DepartureLocation;

                                        objfsr.ArrivalLocation = Sectoresdtails.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value.Trim();
                                        objfsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objfsr.ArrivalLocation);
                                        objfsr.ArrivalTime = Convert.ToDateTime(objfsr.arrdatelcc).ToString("HHmm");
                                        objfsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.ArrivalLocation);
                                        objfsr.ArrivalTerminal = "";
                                        objfsr.ArrAirportCode = objfsr.ArrivalLocation;
                                        objfsr.Trip = objFlt.Trip.ToString();
                                        DateTime STA = Convert.ToDateTime(objfsr.arrdatelcc);
                                        TimeSpan tsTimeSpan = STA.Subtract(Convert.ToDateTime(objfsr.depdatelcc));
                                        objfsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpan.TotalMinutes));
                                        objfsr.TripCnt = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpan.TotalMinutes));
                                        objfsr.Trip = objFlt.Trip.ToString();
                                        #region Adding Fare
                                        #region Fare Calculation
                                        IEnumerable<XElement> Faresbreakups = AirItineraryPricingInfo.Element(ns1 + "PTC_FareBreakdowns").Elements(ns1 + "PTC_FareBreakdown");
                                        objfsr.fareBasis = Faresbreakups.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                        objfsr.EQ = "";

                                        objfsr.AvailableSeats = "";//Sectoresdtails

                                        IEnumerable<XElement> objAdt = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "ADT");
                                        IEnumerable<XElement> objChd = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "CHD");
                                        IEnumerable<XElement> objInf = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "INF");

                                        #region Adult Fare calculation
                                        decimal ABfare = 0, ATax = 0, AOT = 0;
                                        foreach (var adt in objAdt)
                                        {
                                            XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                            IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                            IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                            decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                            decimal Tax = 0, othrcharge = 0;//
                                            foreach (var ChargeType in ServiceCharges)
                                            {
                                                Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                            }
                                            foreach (var ChargeType in Fees)
                                            {
                                                othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                            }
                                            int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                            ABfare += (basefare * Quantity);
                                            ATax += (Tax * Quantity);
                                            AOT += (othrcharge * Quantity);
                                        }
                                        // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;

                                        objfsr.AdtBfare = (float)Math.Ceiling(ABfare / objfsr.Adult);
                                        objfsr.AdtFSur = 0;
                                        objfsr.AdtWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                        objfsr.AdtTax = (float)Math.Ceiling((ATax + AOT) / objfsr.Adult) + objfsr.AdtWO + srvCharge;
                                        objfsr.AdtOT = objfsr.AdtTax;
                                        objfsr.AdtCabin = "Y";
                                        objfsr.AdtRbd = "EC";
                                        objfsr.AdtFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                        objfsr.AdtFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                        //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                        objfsr.AdtFare = objfsr.AdtTax + objfsr.AdtBfare + objfsr.AdtWO + objfsr.AdtFSur; //  + SegmentSellKey

                                        objfsr.FBPaxType = "ADT";
                                        objfsr.ElectronicTicketing = "";
                                        objfsr.AdtFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                        // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;

                                        objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());
                                        objfsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());

                                        CommDt.Clear(); STTFTDS.Clear();
                                        // CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo);
                                        //CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                        try
                                        {
                                                    //CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                                    //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, objFlt.TDS);
                                                    //objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                    //objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());

                                                    //objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                    //objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                                    //objfsr.AdtEduCess = 0;
                                                    //objfsr.AdtHighEduCess = 0;
                                                    //objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    //objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    objfsr.AdtDiscount = 0;
                                                    objfsr.AdtCB = 0;
                                                    objfsr.AdtSrvTax = 0;
                                                    objfsr.AdtTF = 0;
                                                    objfsr.AdtTds = 0;
                                                    objfsr.IATAComm = 0;
                                                    objfsr.AdtEduCess = 0;
                                                    objfsr.AdtHighEduCess = 0;
                                                }
                                        catch (Exception es)
                                        {
                                            objfsr.AdtDiscount1 = 0; objfsr.AdtCB = 0; objfsr.AdtSrvTax1 = 0; objfsr.AdtTds = 0;
                                            objfsr.AdtDiscount = 0; objfsr.AdtEduCess = 0; objfsr.AdtHighEduCess = 0; objfsr.AdtTF = 0;
                                        }
                                        if (objFlt.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.AdtFare.ToString()));
                                                objfsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objfsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }

                                        #endregion

                                        #region Child Fare calculation
                                        decimal ChdBFare = 0, CTax = 0, COT = 0;
                                        foreach (var adt in objChd)
                                        {
                                            XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                            IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                            IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                            decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                            decimal Tax = 0, othrcharge = 0;//
                                            foreach (var ChargeType in ServiceCharges)
                                            {
                                                Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                            }
                                            foreach (var ChargeType in Fees)
                                            {
                                                othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                            }
                                            int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                            ChdBFare += (basefare * Quantity);
                                            CTax += (Tax * Quantity);
                                            COT += (othrcharge * Quantity);
                                        }
                                        // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc; 
                                        if (objfsr.Child > 0 && ChdBFare > 0)
                                            objfsr.ChdBFare = (float)Math.Ceiling(ChdBFare / objfsr.Child);
                                        else
                                            objfsr.ChdBFare = (float)Math.Ceiling(ChdBFare);
                                        objfsr.ChdFSur = 0;
                                        objfsr.ChdWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                        if (objfsr.Child > 0 && CTax > 0)
                                            objfsr.ChdTax = (float)Math.Ceiling((CTax + COT) / objfsr.Child) + objfsr.ChdWO + srvCharge;
                                        else
                                            objfsr.ChdTax = (float)Math.Ceiling(CTax + COT) + objfsr.ChdWO + srvCharge;
                                        objfsr.ChdOT = objfsr.ChdTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                        objfsr.ChdCabin = "Y";
                                        objfsr.ChdRbd = "";
                                        objfsr.ChdFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                        objfsr.ChdfareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                        //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                        objfsr.ChdFare = objfsr.ChdTax + objfsr.ChdBFare + objfsr.ChdWO + objfsr.ChdFSur; //  + SegmentSellKey

                                        objfsr.ChdFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                        // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;

                                        objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                        objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());

                                        CommDt.Clear(); STTFTDS.Clear();
                                        // CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo);
                                        //CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.ChdCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                        // STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, objFlt.TDS);
                                        try
                                        {
                                                    //CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                                    //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, objFlt.TDS);
                                                    //objfsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                    //objfsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());

                                                    //objfsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                    //objfsr.ChdDiscount = objfsr.ChdDiscount1 - objfsr.ChdSrvTax1;
                                                    //objfsr.ChdEduCess = 0;
                                                    //objfsr.ChdHighEduCess = 0;
                                                    //objfsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    //objfsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    objfsr.ChdDiscount = 0;
                                                    objfsr.ChdCB = 0;
                                                    objfsr.ChdSrvTax = 0;
                                                    objfsr.ChdTF = 0;
                                                    objfsr.ChdTds = 0;
                                                    objfsr.IATAComm = 0;
                                                    objfsr.ChdEduCess = 0;
                                                    objfsr.ChdHighEduCess = 0;
                                                    objfsr.ChdEduCess = 0;
                                                    objfsr.ChdHighEduCess = 0;
                                                }
                                        catch (Exception ex)
                                        {
                                            objfsr.ChdEduCess = 0; objfsr.ChdHighEduCess = 0; objfsr.ChdDiscount1 = 0; objfsr.ChdCB = 0;
                                            objfsr.ChdSrvTax1 = 0; objfsr.ChdDiscount = 0; objfsr.ChdTF = 0; objfsr.ChdTds = 0;
                                        }

                                        if (objFlt.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.ChdFare.ToString()));
                                                objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }

                                        #endregion

                                        #region Infant Fare calculation
                                        decimal INFBFare = 0, INFTax = 0, INFOtherTax = 0;
                                        foreach (var adt in objInf)
                                        {
                                            XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                            IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                            IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                            decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                            decimal Tax = 0, othrcharge = 0;//
                                            foreach (var ChargeType in ServiceCharges)
                                            {
                                                Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                            }
                                            foreach (var ChargeType in Fees)
                                            {
                                                othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                            }
                                            int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                            INFBFare += basefare * Quantity;
                                            INFTax += Tax * Quantity;
                                            INFOtherTax += othrcharge * Quantity;
                                        }
                                        // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;
                                        if (objfsr.Infant > 0 && INFBFare > 0)
                                            objfsr.InfBfare = (float)Math.Ceiling(INFBFare / objfsr.Infant);
                                        else
                                            objfsr.InfBfare = (float)Math.Ceiling(INFBFare);
                                        objfsr.InfFSur = 0;
                                        objfsr.InfWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                        if (objfsr.Infant > 0 && (INFTax + INFOtherTax) > 0)
                                            objfsr.InfTax = (float)Math.Ceiling((INFTax + INFOtherTax) / objfsr.Infant);
                                        else
                                            objfsr.InfTax = (float)Math.Ceiling(INFTax + INFOtherTax) + objfsr.InfWO + srvCharge;
                                        objfsr.InfOT = objfsr.InfTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                        objfsr.InfCabin = "Y";
                                        objfsr.InfRbd = "";
                                        objfsr.InfFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                        objfsr.InfFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                        //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                        objfsr.InfFare = objfsr.InfTax + objfsr.InfBfare + objfsr.InfWO + objfsr.InfFSur;
                                        objfsr.InfFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                        if (objFlt.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.InfBfare.ToString()), decimal.Parse(objfsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.InfFare.ToString()));
                                                objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }

                                        #endregion

                                        #endregion
                                        XElement MealBaggageDescription = Sectoresdtails.Parent;
                                        XElement bundledService = MealBaggageDescription.Element(ns1 + "AABundledServiceExt");
                                        string bundleinfo = "";
                                        if (bundledService != null)
                                        {
                                            objfsr.BagInfo = bundledService.Element(ns1 + "bundledService").Element(ns1 + "description").Value;
                                            bundleinfo = bundledService.Element(ns1 + "bundledService").Element(ns1 + "bunldedServiceId").Value.Trim();
                                            bundleinfo += ":" + bundledService.Element(ns1 + "bundledService").Element(ns1 + "bundledServiceName").Value.Trim();
                                            bundleinfo += ":" + bundledService.Element(ns1 + "bundledService").Element(ns1 + "perPaxBundledFee").Value.Trim();
                                            bundleinfo += bundledService.Element(ns1 + "bundledService").Element(ns1 + "bookingClasses") != null ? ":" + bundledService.Element(ns1 + "bundledService").Element(ns1 + "bookingClasses").Value.Trim() : ":";
                                        }

                                        else
                                            objfsr.BagInfo = "";
                                        objfsr.RBD = objfsr.AdtRbd + ":" + objfsr.ChdRbd + ":" + objfsr.InfRbd;
                                        objfsr.FareRule = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("RPH").Value.Trim();
                                        objfsr.sno = TransactionIdentifier + ":" + Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("RPH").Value.Trim() + "::" + EchoToken + ":" + Version + ":" + PriceQuetResult.Attribute("SequenceNumber").Value + ":" + AirItineraryPricingInfo.Element(ns1 + "ItinTotalFare").Element(ns1 + "BaseFare").Attribute("CurrencyCode").Value + ":" + seg.ToString() + ":" + bundleinfo;
                                        objfsr.Searchvalue = JSessionId;
                                        objfsr.OriginalTF = (float)(ABfare + ATax + AOT + ChdBFare + CTax + COT + INFBFare + INFTax + INFOtherTax);
                                        objfsr.OriginalTT = srvCharge;
                                        objfsr.Provider = "LCC";
                                        //Rounding the fare
                                        objfsr.AdtFare = (float)Math.Ceiling(objfsr.AdtFare);
                                        objfsr.ChdFare = (float)Math.Ceiling(objfsr.ChdFare);
                                        objfsr.InfFare = (float)Math.Ceiling(objfsr.InfFare);

                                        objfsr.STax = (objfsr.AdtSrvTax * objfsr.Adult) + (objfsr.ChdSrvTax * objfsr.Child) + (objfsr.InfSrvTax * objfsr.Infant);
                                        objfsr.TFee = (objfsr.AdtTF * objfsr.Adult) + (objfsr.ChdTF * objfsr.Child) + (objfsr.InfTF * objfsr.Infant);
                                        objfsr.TotDis = (objfsr.AdtDiscount * objfsr.Adult) + (objfsr.ChdDiscount * objfsr.Child);
                                        objfsr.TotCB = (objfsr.AdtCB * objfsr.Adult) + (objfsr.ChdCB * objfsr.Child);
                                        objfsr.TotTds = (objfsr.AdtTds * objfsr.Adult) + (objfsr.ChdTds * objfsr.Child) + (objfsr.InfTds * objfsr.Infant);
                                        objfsr.TotMgtFee = (objfsr.AdtMgtFee * objfsr.Adult) + (objfsr.ChdMgtFee * objfsr.Child) + (objfsr.InfMgtFee * objfsr.Infant);
                                        objfsr.TotalFuelSur = (objfsr.AdtFSur * objfsr.Adult) + (objfsr.ChdFSur * objfsr.Child) + (objfsr.InfFSur * objfsr.Infant);
                                        objfsr.TotalTax = (objfsr.AdtTax * objfsr.Adult) + (objfsr.ChdTax * objfsr.Child) + (objfsr.InfTax * objfsr.Infant);
                                        objfsr.TotBfare = (objfsr.AdtBfare * objfsr.Adult) + (objfsr.ChdBFare * objfsr.Child) + (objfsr.InfBfare * objfsr.Infant);
                                        objfsr.TotMrkUp = ((objfsr.ADTAdminMrk + objfsr.ADTAgentMrk) * objfsr.Adult) + ((objfsr.CHDAdminMrk + objfsr.CHDAgentMrk) * objfsr.Child);
                                        objfsr.TotalFare = (objfsr.AdtFare * objfsr.Adult) + (objfsr.ChdFare * objfsr.Child) + (objfsr.InfFare * objfsr.Infant) + objfsr.TotMrkUp + objfsr.STax + objfsr.TFee + objfsr.TotMgtFee;
                                        objfsr.NetFare = (objfsr.TotalFare + objfsr.TotTds) - (objfsr.TotDis + objfsr.TotCB + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAgentMrk * objfsr.Child));
                                        #endregion

                                        if (OrginDepartur.OriginLocationCode == OrginDepartur.DestinationLocationCode)
                                            objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                        else
                                            objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                        if (objfsr.ArrAirportCode == objFlt.HidTxtDepCity.Split(',')[0] || objfsr.DepAirportCode == objFlt.HidTxtArrCity.Split(',')[0])
                                        {
                                            flight++;
                                            if (flight % 2 == 1)
                                                flight--;
                                            legno = 1;
                                            objfsr.Leg = legno;
                                            objfsr.Flight = flight.ToString();
                                        }
                                        if (OrginDepartur.OriginLocationCode == OrginDepartur.DestinationLocationCode && flight == 2)
                                        {
                                            objfsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                            objfsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                        }
                                        else
                                        {
                                            objfsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                            objfsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                        }
                                        if (flight == 1)
                                            objfsr.TripType = "O";
                                        else if (flight == 2)
                                            objfsr.TripType = "R";
                                        else objfsr.TripType = "M";
                                        fsrList.Add(objfsr);  
                                    }
                                    lineNum++;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight");
                            }
                        }
                    }

                }
                catch (Exception ex)
                { ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight"); }

                FinalList = fsrList;
                if (RfsrList.Count != 0)
                    FinalList = RfsrList;
                if (objFlt.TripType == TripType.RoundTrip && fsrList.Count > 0 && RfsrList.Count > 0)  //RTF True
                    FinalList = RoundTripFare(fsrList, RfsrList);
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight");
            }
            return objFltComm.AddFlightKey(FinalList, RTFS);
        }
       
        public string AirArabiaAvailibilityRequest(FlightSearch objFlt, string fareType, string Username, string Password)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                sb.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.Append("<soap:Header>");
                sb.Append("<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                sb.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-17855236\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                sb.Append("<wsse:Username>"+ Username +"</wsse:Username>");
                sb.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
                sb.Append("</wsse:UsernameToken>");
                sb.Append("</wsse:Security>");
                sb.Append("</soap:Header>");
                sb.Append("<soap:Body xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">");
                sb.Append("<ns2:OTA_AirAvailRQ EchoToken=\"11868765275150-1300257933\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" Target=\"LIVE\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" Version=\"20061.00\">");
                sb.Append("<ns2:POS>");
                sb.Append("<ns2:Source TerminalID=\"TestUser/Test Runner\">");
                sb.Append("<ns2:RequestorID ID=\"" + Username + "\" Type=\"4\" />");
                sb.Append("<ns2:BookingChannel Type=\"12\" />");
                sb.Append("</ns2:Source>");
                sb.Append("</ns2:POS>");

                if (objFlt.TripType.ToString() == "RoundTrip")
                {
                    sb.Append("<ns2:OriginDestinationInformation>");
                    sb.Append("<ns2:DepartureDateTime>" + Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2) + "T00:00:00</ns2:DepartureDateTime>");
                    sb.Append("<ns2:OriginLocation LocationCode=\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\"/>");
                    sb.Append("<ns2:DestinationLocation LocationCode=\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\"/>");
                    if (objFlt.Cabin != "")
                        sb.Append("<TravelPreferences><CabinPref Cabin=\"" + objFlt.Cabin + "\" PreferLevel=\"Preferred\" /></TravelPreferences>");
                    sb.Append("</ns2:OriginDestinationInformation>");
                
                    sb.Append("<ns2:OriginDestinationInformation>");
                    sb.Append("<ns2:DepartureDateTime>" + Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) + "T00:00:00</ns2:DepartureDateTime>");
                    sb.Append("<ns2:OriginLocation LocationCode=\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\"/>");
                    sb.Append("<ns2:DestinationLocation LocationCode=\""+ objFlt.HidTxtDepCity.Split(',')[0] +"\"/>");
                    if (objFlt.Cabin != "")
                        sb.Append("<TravelPreferences><CabinPref Cabin=\"" + objFlt.Cabin + "\" PreferLevel=\"Preferred\" /></TravelPreferences>");
                    sb.Append("</ns2:OriginDestinationInformation>");
                }
                else if (objFlt.TripType.ToString() == "OneWay")
                {
                    sb.Append("<ns2:OriginDestinationInformation>");
                    sb.Append("<ns2:DepartureDateTime>" + Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2) + "T00:00:00</ns2:DepartureDateTime>");
                    sb.Append("<ns2:OriginLocation LocationCode=\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\"/>");
                    sb.Append("<ns2:DestinationLocation LocationCode=\""+ objFlt.HidTxtArrCity.Split(',')[0] +"\"/>");
                    if (objFlt.Cabin != "")
                        sb.Append("<TravelPreferences><CabinPref Cabin=\"" + objFlt.Cabin + "\" PreferLevel=\"Preferred\" /></TravelPreferences>");
                    sb.Append("</ns2:OriginDestinationInformation>");
                }
                sb.Append("<ns2:TravelerInfoSummary>");
                sb.Append("<ns2:AirTravelerAvail>");
                sb.Append("<ns2:PassengerTypeQuantity Code=\"ADT\"  Quantity=\"" + objFlt.Adult + "\"/>");
                if (objFlt.Child >0)
                    sb.Append("<ns2:PassengerTypeQuantity Code=\"CHD\"  Quantity=\"" + objFlt.Child + "\"/>");
                if (objFlt.Infant > 0)
                    sb.Append("<ns2:PassengerTypeQuantity Code=\"INF\"  Quantity=\"" + objFlt.Infant + "\"/>");
                sb.Append("</ns2:AirTravelerAvail>");
                sb.Append("</ns2:TravelerInfoSummary>");
                sb.Append("</ns2:OTA_AirAvailRQ>");
                sb.Append("</soap:Body>");
                sb.Append("</soap:Envelope>");
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("AirArabiaAvailibilityRequest", "Error_001", ex, "AirArabiaFlight"); 
            }
            return sb.ToString();
        }
      
        private List<FlightSearchResults> RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR)
        {
            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;

            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            for (int k = 1; k <= LnOb; k++)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                for (int l = 1; l <= LnIb; l++)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    //List<FlightSearchResults> st = new List<FlightSearchResults>();

                    if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            // st = Merge(OB, IB, ln);
                            Final.AddRange(Merge(OB, IB, ln));
                            ln++;///Increment Total Ln
                        }
                    }
                    else
                    {
                        Final.AddRange(Merge(OB, IB, ln));
                        ln++;
                    }

                }

            }
            return Final;
        }
        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
            //ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
            //CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;//,
            //InfSrvTax = 0, InfTF = 0;
            float ADTAgentMrk = 0, CHDAgentMrk = 0;
            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT ;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare ;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax ;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT ;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare ;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax ;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;

            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child);
            float NetFare = TotalFare - ((ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }
        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].City;
            }

            return name;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        public List<FlightSearchResults> AirArabiaPricingRQ(DataSet Fltdtlds, DataSet Credencilas, DataSet MarkupDs, List<FltSrvChargeList> SrvChargeList, string agentType, string TDS, string ConStr, List<FlightSearchResults> objlist, List<MISCCharges> MiscList)
        {
            try
            {
                if (Fltdtlds != null)
                {
                    DataTable fltdt = Fltdtlds.Tables[0]; DataTable Credencialdt = Credencilas.Tables[0];
                    DataView dv1 = Credencialdt.DefaultView;
                    dv1.RowFilter = "TripType = '" + fltdt.Rows[0]["Trip"].ToString().Trim() + "'";
                    Credencialdt = dv1.ToTable();
                    if (fltdt.Rows.Count > 0 && Credencialdt.Rows.Count > 0)
                    {
                        string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                        string AirPricingRequest = PriceQuoteRequest(fltdt, Credencialdt.Rows[0]["UserID"].ToString(), Credencialdt.Rows[0]["Password"].ToString());
                        string AirPricingResponse = AirArabiaUtility.AirArabiaPostXML(Credencialdt.Rows[0]["ServerUrlOrIP"].ToString(), AirPricingRequest, fltdt.Rows[0]["Searchvalue"].ToString());
                       // string[] res = Utility.Split(AirArabiaUtility.AirArabiaPricingPostXML(Credencialdt.Rows[0]["ServerUrlOrIP"].ToString(), AirPricingRequest),"~");
                       // string AirPricingResponse=res[1];
                        // string JSessionId=res[0];
                        AirArabiaUtility.SaveFile(AirPricingRequest, "PricingReq_" + fltdt.Rows[0]["Track_id"].ToString());
                        AirArabiaUtility.SaveFile(AirPricingResponse, "PricingRes_" + fltdt.Rows[0]["Track_id"].ToString());
                        if (AirPricingResponse.IndexOf("No availability") < 1 || AirPricingResponse.IndexOf("Invalid") < 1 || AirPricingResponse == "" || AirPricingResponse == null)
                        {
                            if (AirPricingResponse.Contains("<ns1:Success />") || AirPricingResponse.Contains("<ns1:Success/>"))
                            {
                                 DataTable CommDt = new DataTable(); Hashtable STTFTDS = new Hashtable();
                                 FlightCommonBAL objFltComm = new FlightCommonBAL(ConStr);
                                 XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; XNamespace a = "http://www.w3.org/2001/XMLSchema-instance";
                                 XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";

                                 XDocument AirArabispricingDocument = XDocument.Parse(AirPricingResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                 XElement OTA_AirPriceRS = AirArabispricingDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirPriceRS");
                                 XElement PriceQuetResult = OTA_AirPriceRS.Element(ns1 + "PricedItineraries").Element(ns1 + "PricedItinerary");
                                 IEnumerable<XElement> Faresbreakups = PriceQuetResult.Element(ns1 + "AirItineraryPricingInfo").Element(ns1 + "PTC_FareBreakdowns").Elements(ns1 + "PTC_FareBreakdown");
                                foreach (var fltdtl in PriceQuetResult.Element(ns1 + "AirItinerary").Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption"))
                                {
                                    foreach (FlightSearchResults objfsr in objlist.Where(x => x.FlightIdentification.Trim() == fltdtl.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value.Trim() && x.DepartureLocation.Trim() == fltdtl.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value.Trim() && x.ArrivalLocation.Trim() == fltdtl.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value.Trim()))
                                    {
                                        float srvChargeAdt = 0, srvChargeChd = 0;
                                        IEnumerable<XElement> objAdt = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "ADT");
                                        IEnumerable<XElement> objChd = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "CHD");
                                        IEnumerable<XElement> objInf = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "INF");
                                        #region Adult Fare calculation  
                                            decimal ABfare = 0, ATax = 0, AOT = 0;
                                            foreach (var adt in objAdt)
                                            {
                                                XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                decimal Tax = 0, othrcharge = 0;//
                                                foreach (var ChargeType in ServiceCharges)
                                                {
                                                    Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                }
                                                foreach (var ChargeType in Fees)
                                                {
                                                    othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                }
                                                int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                ABfare += (basefare * Quantity);
                                                ATax += (Tax * Quantity);
                                                AOT += (othrcharge * Quantity);
                                            }

                                            objfsr.AdtBfare = (float)Math.Ceiling(ABfare / objfsr.Adult);
                                            objfsr.AdtFSur = 0;
                                            objfsr.AdtWO = 0;
                                            #region Get MISC Markup Charges Date 26-12-2018
                                            try
                                            {
                                                srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, Credencialdt.Rows[0]["CrdType"].ToString(), Convert.ToString(objfsr.AdtBfare), Convert.ToString(objfsr.AdtFSur));
                                            }
                                            catch { srvChargeAdt = 0; }
                                            #endregion
                                            objfsr.AdtTax = (float)Math.Ceiling((ATax + AOT) / objfsr.Adult) + objfsr.AdtWO +  srvChargeAdt;
                                            objfsr.AdtOT = objfsr.AdtTax;
                                            objfsr.AdtCabin = "Y" ;
                                            objfsr.AdtRbd = "EC";
                                            objfsr.AdtFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                            objfsr.AdtFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                            //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                            objfsr.AdtFare = objfsr.AdtTax + objfsr.AdtBfare + objfsr.AdtWO + objfsr.AdtFSur; //  + SegmentSellKey

                                            objfsr.FBPaxType = "ADT";
                                            objfsr.ElectronicTicketing = "";
                                            objfsr.AdtFar = "NRM"; 
                                            //  objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objfsr.Trip.ToString());
                                            objfsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objfsr.Trip.ToString());
                                            objfsr.ADTAdminMrk = 0;
                                            if (objfsr.Trip.ToString() == JourneyType.I.ToString() && objfsr.IsCorp == true)
                                            {
                                                objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objfsr.Trip.ToString());
                                                objfsr.AdtBfare = objfsr.AdtBfare + objfsr.ADTAdminMrk;
                                                objfsr.AdtFare = objfsr.AdtFare + objfsr.ADTAdminMrk;
                                            }
                                            CommDt.Clear(); STTFTDS.Clear();
                                            try
                                            {
                                                objfsr.AdtDiscount = 0;
                                                objfsr.AdtCB = 0;
                                                objfsr.AdtSrvTax = 0;
                                                objfsr.AdtTF = 0;
                                                objfsr.AdtTds = 0;
                                                objfsr.IATAComm = 0;
                                                objfsr.AdtEduCess = 0;
                                                objfsr.AdtHighEduCess = 0;
                                            //CommDt = objFltComm.GetFltComm_WithouDB(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.fareBasis, objfsr.OrgDestFrom, objfsr.OrgDestTo, objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", objfsr.Stops.Replace("-Stop",""));
                                            //objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                            //objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());

                                            //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, TDS);
                                            //objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); 
                                            //objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                            //objfsr.AdtEduCess = 0;
                                            //objfsr.AdtHighEduCess = 0;
                                            //objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                            //objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                            }
                                            catch (Exception sd)
                                            {
                                                objfsr.AdtDiscount1 = 0;objfsr.AdtCB = 0; objfsr.AdtSrvTax1 = 0;objfsr.AdtDiscount = 0;
                                                objfsr.AdtEduCess = 0; objfsr.AdtHighEduCess = 0; objfsr.AdtTF = 0; objfsr.AdtTds = 0;
                                            }
                                            if (objfsr.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), objfsr.Trip, decimal.Parse(objfsr.AdtFare.ToString()));
                                                    objfsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    objfsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }

                                            #endregion

                                                #region Child Fare calculation
                                                decimal ChdBFare = 0, CTax = 0, COT = 0;
                                                foreach (var adt in objChd)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge = 0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    ChdBFare += (basefare * Quantity);
                                                    CTax += (Tax * Quantity);
                                                    COT += (othrcharge * Quantity);
                                                }
                                                if (objfsr.Child > 0 && ChdBFare >0)
                                                    objfsr.ChdBFare = (float)Math.Ceiling(ChdBFare / objfsr.Child);
                                                else
                                                    objfsr.ChdBFare = (float)Math.Ceiling(ChdBFare);
                                                objfsr.ChdFSur = 0;
                                                objfsr.ChdWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                if (objfsr.Child > 0)
                                                {
                                                    #region Get MISC Markup Charges Date 26-12-2018
                                                    try
                                                    {
                                                        srvChargeChd = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, Credencialdt.Rows[0]["CrdType"].ToString(), Convert.ToString(objfsr.ChdBFare), Convert.ToString(objfsr.ChdFSur));
                                                    }
                                                    catch { srvChargeChd = 0; }
                                                }
                                                    #endregion
                                                if (objfsr.Child > 0 && CTax > 0)
                                                    objfsr.ChdTax = (float)Math.Ceiling((CTax + COT) / objfsr.Child) + objfsr.ChdWO +  srvChargeChd;
                                                else
                                                    objfsr.ChdTax = (float)Math.Ceiling(CTax + COT) + objfsr.ChdWO +  srvChargeChd;
                                               
                                                objfsr.ChdOT = objfsr.ChdTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                objfsr.ChdCabin = "Y";
                                                objfsr.ChdRbd = "";
                                                objfsr.ChdFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.ChdfareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                objfsr.ChdFare = objfsr.ChdTax + objfsr.ChdBFare + objfsr.ChdWO + objfsr.ChdFSur; //  + SegmentSellKey

                                                objfsr.ChdFar = "NRM"; 
                                                //objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objfsr.Trip);
                                               // objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objfsr.Trip);
                                                objfsr.CHDAdminMrk = 0;
                                                objfsr.CHDAgentMrk = 0;
                                                if (objfsr.Child > 0)
                                                {
                                                    objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objfsr.Trip);
                                                    if (objfsr.Trip.ToString() == JourneyType.I.ToString() && objfsr.IsCorp == true)
                                                    {
                                                        objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objfsr.Trip.ToString());
                                                        objfsr.ChdBFare = objfsr.ChdBFare + objfsr.CHDAdminMrk;
                                                        objfsr.ChdFare = objfsr.ChdFare + objfsr.CHDAdminMrk;
                                                    }
                                                }
                                                CommDt.Clear(); STTFTDS.Clear();
                                        try
                                        {
                                            objfsr.ChdDiscount = 0;
                                            objfsr.ChdCB = 0;
                                            objfsr.ChdSrvTax = 0;
                                            objfsr.ChdTF = 0;
                                            objfsr.ChdTds = 0;
                                            objfsr.IATAComm = 0;
                                            objfsr.ChdEduCess = 0;
                                            objfsr.ChdHighEduCess = 0;
                                            objfsr.ChdEduCess = 0;
                                            objfsr.ChdHighEduCess = 0;
                                        }
                                        catch (Exception ex)
                                        {
                                            objfsr.ChdDiscount1 = 0; objfsr.ChdCB = 0; objfsr.ChdSrvTax1 = 0;objfsr.ChdDiscount = 0;
                                            objfsr.ChdTF = 0; objfsr.ChdTds = 0; objfsr.ChdEduCess = 0; objfsr.ChdHighEduCess = 0;
                                        }
                                               
                                                if (objfsr.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), objfsr.Trip, decimal.Parse(objfsr.ChdFare.ToString()));
                                                        objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion

                                                #region Infant Fare calculation
                                                decimal INFBFare = 0, INFTax = 0, INFOtherTax = 0;
                                                foreach (var adt in objInf)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge=0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    INFBFare += basefare * Quantity;
                                                    INFTax += Tax * Quantity;
                                                    INFOtherTax += othrcharge * Quantity;
                                                }
                                                // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;
                                                if (objfsr.Infant > 0 && INFBFare > 0)
                                                    objfsr.InfBfare = (float)Math.Ceiling(INFBFare / objfsr.Infant);
                                                else
                                                    objfsr.InfBfare = (float)Math.Ceiling(INFBFare);
                                                objfsr.InfFSur = 0;
                                                objfsr.InfWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                if (objfsr.Infant > 0 && (INFTax + INFOtherTax) > 0)
                                                    objfsr.InfTax = (float)Math.Ceiling((INFTax + INFOtherTax) / objfsr.Infant);
                                                else
                                                    objfsr.InfTax = (float)Math.Ceiling(INFTax + INFOtherTax) + objfsr.InfWO ;
                                                objfsr.InfOT = objfsr.InfTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                objfsr.InfCabin = "Y";
                                                objfsr.InfRbd = "";
                                                objfsr.InfFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.InfFareType = "";
                                                objfsr.InfFare = objfsr.InfTax + objfsr.InfBfare + objfsr.InfWO + objfsr.InfFSur; 
                                                objfsr.InfFar = "NRM"; 
                                                if (objfsr.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.InfBfare.ToString()), decimal.Parse(objfsr.InfFSur.ToString()), objfsr.Trip, decimal.Parse(objfsr.InfFare.ToString()));
                                                        objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion

                                        objfsr.sno = OTA_AirPriceRS.Attribute("TransactionIdentifier").Value + ":" + fltdtl.Element(ns1 + "FlightSegment").Attribute("RPH").Value.Trim() + "::" + OTA_AirPriceRS.Attribute("EchoToken").Value + ":" + OTA_AirPriceRS.Attribute("Version").Value + ":" + OTA_AirPriceRS.Attribute("SequenceNmbr").Value + ":" + PriceQuetResult.Element(ns1 + "AirItineraryPricingInfo").Element(ns1 + "ItinTotalFare").Element(ns1 + "BaseFare").Attribute("CurrencyCode").Value + ":" + objfsr.Stops.Replace("-Stop","")  ;                                       
                                        objfsr.OriginalTF = (float)(ABfare + ATax + AOT + ChdBFare + CTax + COT + INFBFare + INFTax + INFOtherTax);
                                        objfsr.OriginalTT =  srvChargeAdt + srvChargeChd;
                                        //Rounding the fare
                                        objfsr.AdtFare = (float)Math.Ceiling(objfsr.AdtFare);
                                        objfsr.ChdFare = (float)Math.Ceiling(objfsr.ChdFare);
                                        objfsr.InfFare = (float)Math.Ceiling(objfsr.InfFare);

                                        objfsr.STax = objfsr.AdtSrvTax + objfsr.ChdSrvTax + objfsr.InfSrvTax;
                                        objfsr.TFee = objfsr.AdtTF + objfsr.ChdTF + objfsr.InfTF;
                                        objfsr.TotDis = objfsr.AdtDiscount + objfsr.ChdDiscount;
                                        objfsr.TotCB = objfsr.AdtCB + objfsr.ChdCB;
                                        objfsr.TotTds = objfsr.AdtTds + objfsr.ChdTds + objfsr.InfTds;
                                        objfsr.TotMgtFee = objfsr.AdtMgtFee + objfsr.ChdMgtFee + objfsr.InfMgtFee;
                                        objfsr.TotalFuelSur = objfsr.AdtFSur + objfsr.ChdFSur + objfsr.InfFSur;
                                        objfsr.TotalTax = objfsr.AdtTax + objfsr.ChdTax + objfsr.InfTax;
                                        objfsr.TotBfare = objfsr.AdtBfare + objfsr.ChdBFare + objfsr.InfBfare;
                                        objfsr.TotMrkUp = objfsr.ADTAdminMrk + objfsr.ADTAgentMrk + objfsr.CHDAdminMrk + objfsr.CHDAgentMrk;
                                        objfsr.TotalFare = objfsr.AdtFare + objfsr.ChdFare + objfsr.InfFare + objfsr.TotMrkUp + objfsr.STax + objfsr.TFee + objfsr.TotMgtFee;
                                        objfsr.NetFare = (objfsr.TotalFare + objfsr.TotTds) - (objfsr.TotDis + objfsr.TotCB + objfsr.ADTAgentMrk + objfsr.CHDAgentMrk);
                                     
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("AirArabiaPricingRQ", "Error_001", ex, "AirArabiaFlight");
            }
            return objlist;
        }
        public List<AirArabiaSSR> MealBaggageAvailability(List<AirArabiaSSR> ssrlists, DataSet Credencilas, DataTable fltdt, string constr)
        {
            try
            {
                if (fltdt.Rows.Count > 0)
                {
                    DataRow[] newFltDsO = fltdt.Select("TripType='O'");
                    DataRow[] newFltDsR = fltdt.Select("TripType='R'");
                    #region Baggage Request
                    string BaggageAvailabilityReq = BaggageAvailabilityRequest(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), newFltDsO);
                    AirArabiaUtility.SaveFile(BaggageAvailabilityReq, "BaggageDetailsRequest_" + fltdt.Rows[0]["Track_id"].ToString());
                    string BaggageAvailabilityRes = AirArabiaUtility.AirArabiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), BaggageAvailabilityReq, fltdt.Rows[0]["Searchvalue"].ToString());
                    AirArabiaUtility.SaveFile(BaggageAvailabilityRes, "BaggageDetailsResponse_" + fltdt.Rows[0]["Track_id"].ToString());

                    ssrlists=  BaggageAvailability(ssrlists, BaggageAvailabilityRes,"O");
                    if(newFltDsR.Length >0)
                    {
                        string BaggageAvailabilityReq2 = BaggageAvailabilityRequest(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), newFltDsR);
                        AirArabiaUtility.SaveFile(BaggageAvailabilityReq2, "BaggageDetailsRequest2_" + fltdt.Rows[0]["Track_id"].ToString());
                        string BaggageAvailabilityRes2 = AirArabiaUtility.AirArabiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), BaggageAvailabilityReq2, fltdt.Rows[0]["Searchvalue"].ToString());
                        AirArabiaUtility.SaveFile(BaggageAvailabilityRes2, "BaggageDetailsResponse2_" + fltdt.Rows[0]["Track_id"].ToString());
                        ssrlists = BaggageAvailability(ssrlists, BaggageAvailabilityRes2, "R");
                    }
                    #endregion
                    #region Meal Request;
                     string[] SNO = fltdt.Rows[0]["sno"].ToString().Split(':');
                    string EchoToken=SNO[3],  TransactionIdentifier=SNO[0],  Version=SNO[4];
                    for (int m = 0; m < fltdt.Rows.Count; m++)
                    {
                        List<AirArabiaSSR> ssrlistsNew = new List<AirArabiaSSR>();
                         string[] SNONew = fltdt.Rows[m]["sno"].ToString().Split(':');
                        string MealAvailabilityReq = MealAvailabilityRequest(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), EchoToken, TransactionIdentifier, Version, fltdt.Rows[m]["arrdatelcc"].ToString(), fltdt.Rows[m]["depdatelcc"].ToString(), fltdt.Rows[m]["FlightIdentification"].ToString(), fltdt.Rows[m]["DepartureLocation"].ToString(), fltdt.Rows[m]["ArrivalLocation"].ToString(), SNONew[1] );
                        AirArabiaUtility.SaveFile(MealAvailabilityReq , "MealDetailsRequest" + m.ToString() + "_" + fltdt.Rows[0]["Track_id"].ToString());
                        string MealAvailabilityRes = AirArabiaUtility.AirArabiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), MealAvailabilityReq, fltdt.Rows[0]["Searchvalue"].ToString());
                        AirArabiaUtility.SaveFile(MealAvailabilityRes, "MealDetailsResponse" + m.ToString() + "_" + fltdt.Rows[0]["Track_id"].ToString());
                        ssrlistsNew = MealAvailability(ssrlistsNew, MealAvailabilityRes, constr);
                        if (ssrlistsNew.Count >0)
                            ssrlists = ssrlists.Union(ssrlistsNew).ToList();
                    }
                    //string MealAvailabilityReq = MealAvailabilityRequest(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), fltdt);
                    //AirArabiaUtility.SaveFile(MealAvailabilityReq, "MealDetailsRequest_" + fltdt.Rows[0]["Track_id"].ToString());
                    //string MealAvailabilityRes = AirArabiaUtility.AirArabiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), MealAvailabilityReq, fltdt.Rows[0]["Searchvalue"].ToString());
                    //AirArabiaUtility.SaveFile(MealAvailabilityRes, "MealDetailsResponse_" + fltdt.Rows[0]["Track_id"].ToString());
                    //ssrlists = MealAvailability(ssrlists, MealAvailabilityRes, constr);
                   
                    #endregion
                }
            }
            catch(Exception ex)
            {
                ExecptionLogger.FileHandling("MealBaggageAvailability", "Error_001", ex, "AirArabiaFlight");
            }
            return ssrlists;
        }
        public List<AirArabiaSSR> BaggageAvailability(List<AirArabiaSSR> ssrlists, string SSRAvailabilityResponse, string TripTypes)
        {
            try
            {
                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/";
                    XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";

                    //string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                    //string SSRAvailabilityRequest = BaggageAvailabilityRequest(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), fltdt);
                    //AirArabiaUtility.SaveFile(SSRAvailabilityRequest, "BaggageDetailsRequest_" + fltdt.Rows[0]["Track_id"].ToString());
                    //string SSRAvailabilityResponse = AirArabiaUtility.AirArabiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), SSRAvailabilityRequest, fltdt.Rows[0]["Searchvalue"].ToString());
                    //AirArabiaUtility.SaveFile(SSRAvailabilityResponse, "BaggageDetailsResponse_" + fltdt.Rows[0]["Track_id"].ToString());

                    XDocument AirArabiaDocument = XDocument.Parse(SSRAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    XElement SSRAvailabilityResult = AirArabiaDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "AA_OTA_AirBaggageDetailsRS").Element(ns1 + "BaggageDetailsResponses");
                    if (SSRAvailabilityResponse.Contains("<ns1:Success />")|| SSRAvailabilityResponse.Contains("<ns1:Success/>"))
                    {
                        IEnumerable<XElement> OnDBaggageDetailsResponse = SSRAvailabilityResult.Elements(ns1 + "OnDBaggageDetailsResponse");
                        int WayType = 0;
                        foreach (var baggageresp in OnDBaggageDetailsResponse)
                        {
                            IEnumerable<XElement> SegmentInfo = baggageresp.Elements(ns1 + "OnDFlightSegmentInfo");
                            IEnumerable<XElement> BaggageList = baggageresp.Elements(ns1 + "Baggage");

                            List<OriginDestinationOption> objOrgdesc = new List<OriginDestinationOption>();
                            int Segment = 0; WayType ++;
                            string SecterinfoOut = "", SecterinfoIn="";
                            foreach (var OrginDepartures in SegmentInfo)
                            {
                                objOrgdesc.Add(new OriginDestinationOption
                                    {
                                        ArrivalDateTime = OrginDepartures.Attribute("ArrivalDateTime").Value,
                                        DepartureDateTime = OrginDepartures.Attribute("DepartureDateTime").Value,
                                        FlightNumber = OrginDepartures.Attribute("FlightNumber").Value,
                                        RPH = OrginDepartures.Attribute("RPH").Value,
                                        JourneyDuration = "", //OrginDepartures.Element(ns1 + "FlightSegment").Attribute("JourneyDuration").Value,
                                        ArrivalAirport = OrginDepartures.Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value,
                                        DepartureAirport = OrginDepartures.Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value,
                                        Segment = Segment++,
                                    });
                                if (WayType == 1)
                                {
                                    if (Segment > 1)
                                        SecterinfoOut += ",";
                                    SecterinfoOut += OrginDepartures.Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value + "_" + OrginDepartures.Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value  ;
                                }
                                else
                                {
                                     if (Segment > 1 )
                                         SecterinfoIn += ",";
                                     SecterinfoIn += OrginDepartures.Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value + "_" + OrginDepartures.Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value;
                                }
                            }
                            foreach (var SSRL in BaggageList.Where(x => x.Element(ns1 + "baggageCode").Value != "No Bag"))
                            {
                                ssrlists.Add(new AirArabiaSSR
                                {
                                    Code = SSRL.Element(ns1 + "baggageCode").Value.Trim(),
                                    Description = SSRL.Element(ns1 + "baggageDescription").Value.Trim(),
                                    Destination = SecterinfoIn,
                                    Origin = SecterinfoOut,
                                    Amount = Math.Round(Convert.ToDecimal(SSRL.Element(ns1 + "baggageCharge").Value.ToString().Trim()), 2),
                                    SSRType = "Baggage",
                                    WayType = TripTypes,
                                    OriginDestinationOptions = objOrgdesc
                                });
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("BaggageAvailability", "Error_001", ex, "AirArabiaFlight");
            }
            return ssrlists;
        }
        public List<AirArabiaSSR> MealAvailability(List<AirArabiaSSR> ssrlists, string SSRAvailabilityResponse, string constr)
        {
            try
            {
                    List<GALWS.AirAsia.CurrencyRate> currancyinfo = new List<GALWS.AirAsia.CurrencyRate>();
                    currancyinfo = GALWS.AirAsia.AirAsiaUtility.CurrancyExchangeRate("AED", constr, currancyinfo);
                    //currancyinfo[0].ExchangeRate
                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/";
                    XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";
                    XDocument AirArabiaDocument = XDocument.Parse(SSRAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    XElement SSRAvailabilityResult = AirArabiaDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "AA_OTA_AirMealDetailsRS").Element(ns1 + "MealDetailsResponses");
                    if ((SSRAvailabilityResponse.Contains("<ns1:Success />") || SSRAvailabilityResponse.Contains("<ns1:Success/>")) && currancyinfo.Count > 0)
                    {
                        IEnumerable<XElement> MealDetailsResponse = SSRAvailabilityResult.Elements(ns1 + "MealDetailsResponse");
                        int WayType = 0;
                        foreach (var baggageresp in MealDetailsResponse)
                        {
                            XElement OrginDepartures = baggageresp.Element(ns1 + "FlightSegmentInfo");
                            IEnumerable<XElement> MealList = baggageresp.Elements(ns1 + "Meal");
                            List<OriginDestinationOption> objOrgdesc = new List<OriginDestinationOption>();
                            objOrgdesc.Add(new OriginDestinationOption
                                {
                                    ArrivalDateTime = OrginDepartures.Attribute("ArrivalDateTime").Value,
                                    DepartureDateTime = OrginDepartures.Attribute("DepartureDateTime").Value,
                                    FlightNumber = OrginDepartures.Attribute("FlightNumber").Value,
                                    RPH = OrginDepartures.Attribute("RPH").Value,
                                    JourneyDuration = "", //OrginDepartures.Element(ns1 + "FlightSegment").Attribute("JourneyDuration").Value,
                                    ArrivalAirport = OrginDepartures.Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value,
                                    DepartureAirport = OrginDepartures.Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value,
                                    Segment = 1,
                                });
                                foreach (var SSRL in MealList.Where(x => x.Element(ns1 + "availableMeals").Value != "0"))
                                {
                                    if (Convert.ToInt32(SSRL.Element(ns1 + "availableMeals").Value) > 0 )
                                    {
                                        ssrlists.Add(new AirArabiaSSR
                                        {
                                            Code = SSRL.Element(ns1 + "mealCode").Value.Trim(),
                                            Description = SSRL.Element(ns1 + "mealName").Value.Trim(),
                                            Destination =  OrginDepartures.Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value,
                                            Origin = OrginDepartures.Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value ,
                                            Amount = Math.Round((Convert.ToDecimal(SSRL.Element(ns1 + "mealCharge").Value.ToString().Trim()) * currancyinfo[0].ExchangeRate), 2),
                                            SSRType = "Meal",
                                            WayType = WayType.ToString(),
                                            OriginDestinationOptions = objOrgdesc
                                        });
                                    }
                                }

                        }
                    }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("MealAvailability", "Error_001", ex, "AirArabiaFlight");
            }
            return ssrlists;
        }

        private string BaggageAvailabilityRequest(string UserId, string Password, DataRow[] newFltDs)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                string[] SNO = newFltDs[0]["sno"].ToString().Split(':');
                objreqXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://www.opentravel.org/OTA/2003/05\">");
                objreqXml.Append("<soapenv:Header>");
                objreqXml.Append("<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                objreqXml.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-17855236\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                objreqXml.Append("<wsse:Username>" + UserId + "</wsse:Username>");
                objreqXml.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
                objreqXml.Append("</wsse:UsernameToken>");
                objreqXml.Append("</wsse:Security>");
                objreqXml.Append("</soapenv:Header>");
                objreqXml.Append("<soapenv:Body>");
                objreqXml.Append("<ns:AA_OTA_AirBaggageDetailsRQ EchoToken=\"" + SNO[3] + "\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" TransactionIdentifier=\"" + SNO[0] + "\" Version=\"" + SNO[4] + "\">");
                objreqXml.Append("<ns:POS>");
                objreqXml.Append("<ns:Source TerminalID=\"TestUser/Test Runner\">");
                objreqXml.Append("<ns:RequestorID ID=\"" + UserId + "\" Type=\"4\" />");
                objreqXml.Append("<ns:BookingChannel Type=\"12\" />");
                objreqXml.Append("</ns:Source>");
                objreqXml.Append("</ns:POS>");
                objreqXml.Append("<ns:BaggageDetailsRequests>");

                #region Sector info
                for (int m = 0; m < newFltDs.Length; m++)
                {
                    string[] SNONew = newFltDs[m]["sno"].ToString().Split(':');
                    objreqXml.Append("<ns:BaggageDetailsRequest>");
                    objreqXml.Append("<ns:FlightSegmentInfo ArrivalDateTime=\"" + Convert.ToDateTime(newFltDs[m]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" DepartureDateTime=\"" + Convert.ToDateTime(newFltDs[m]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + newFltDs[m]["FlightIdentification"].ToString() + "\" RPH=\"" + SNONew[1] + "\">");
                    objreqXml.Append("<ns:DepartureAirport LocationCode=\"" + newFltDs[m]["DepartureLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                    objreqXml.Append("<ns:ArrivalAirport LocationCode=\"" + newFltDs[m]["ArrivalLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                    objreqXml.Append("<ns:OperatingAirline Code=\"G9\"/> ");
                    objreqXml.Append("</ns:FlightSegmentInfo>");
                    objreqXml.Append("</ns:BaggageDetailsRequest>");
                }
                #endregion

                objreqXml.Append("</ns:BaggageDetailsRequests>");
                objreqXml.Append("</ns:AA_OTA_AirBaggageDetailsRQ></soapenv:Body>");
                objreqXml.Append("</soapenv:Envelope>");
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("BaggageAvailabilityRequest", "Error_001", ex, "AirArabiaFlight");
            }
            return objreqXml.ToString();
        }
        private string MealAvailabilityRequest(string UserId, string Password, string EchoToken, string TransactionIdentifier, string Version, string arrdatelcc, string depdatelcc, string FlightIdentification, string DepartureLocation, string ArrivalLocation, string RPH)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                objreqXml.Append("<soapenv:Header>");
                objreqXml.Append("<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                objreqXml.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-17855236\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                objreqXml.Append("<wsse:Username>" + UserId + "</wsse:Username>");
                objreqXml.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
                objreqXml.Append("</wsse:UsernameToken>");
                objreqXml.Append("</wsse:Security>");
                objreqXml.Append("</soapenv:Header>");
                objreqXml.Append("<soapenv:Body xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">");
                objreqXml.Append("<ns2:AA_OTA_AirMealDetailsRQ EchoToken=\"" + EchoToken + "\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" TransactionIdentifier=\"" + TransactionIdentifier + "\" Version=\"" + Version + "\">");
                objreqXml.Append("<ns2:POS>");
                objreqXml.Append("<ns2:Source TerminalID=\"TestUser/Test Runner\">");
                objreqXml.Append("<ns2:RequestorID ID=\"" + UserId + "\" Type=\"4\" />");
                objreqXml.Append("<ns2:BookingChannel Type=\"12\" />");
                objreqXml.Append("</ns2:Source>");
                objreqXml.Append("</ns2:POS>");
                objreqXml.Append("<ns2:MealDetailsRequests>");
                objreqXml.Append("<ns2:MealDetailsRequest>");
                objreqXml.Append("<ns2:FlightSegmentInfo ArrivalDateTime=\"" + Convert.ToDateTime(arrdatelcc).ToString("yyyy-MM-ddTHH:mm:ss") + "\" DepartureDateTime=\"" + Convert.ToDateTime(depdatelcc).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + FlightIdentification + "\" RPH=\"" + RPH + "\">");
                objreqXml.Append("<ns2:DepartureAirport LocationCode=\"" + DepartureLocation + "\" Terminal=\"TerminalX\" />");
                objreqXml.Append("<ns2:ArrivalAirport LocationCode=\"" + ArrivalLocation + "\" Terminal=\"TerminalX\" />");
                objreqXml.Append("<ns2:OperatingAirline Code=\"G9\"/> ");
                objreqXml.Append("</ns2:FlightSegmentInfo>");
                objreqXml.Append("</ns2:MealDetailsRequest>");
                objreqXml.Append("</ns2:MealDetailsRequests>");
                objreqXml.Append("</ns2:AA_OTA_AirMealDetailsRQ></soapenv:Body>");
                objreqXml.Append("</soapenv:Envelope>");
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("MealAvailabilityRequest", "Error_001", ex, "AirArabiaFlight");
            }
            return objreqXml.ToString();
        }
        public string PriceQuoteRequest(DataTable fltdt, string Username, string Password)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string[] SNO = fltdt.Rows[0]["sno"].ToString().Split(':');
                DataRow[] newFltDsR = fltdt.Select("TripType='R'");
                sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                sb.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.Append("<soap:Header>");
                sb.Append("<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                sb.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-32124385\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                sb.Append("<wsse:Username>" + Username + "</wsse:Username>");
                sb.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
                sb.Append("</wsse:UsernameToken>");
                sb.Append("</wsse:Security>");
                sb.Append("</soap:Header>");
                sb.Append("<soap:Body xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\">");
                sb.Append("<ns1:OTA_AirPriceRQ EchoToken=\"" + SNO[3] + "\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" TransactionIdentifier=\"" + SNO[0] + "\" Version=\"" + SNO[4] + "\">");
                sb.Append("<ns1:POS>");
                sb.Append("<ns1:Source TerminalID=\"TestUser/Test Runner\">");
                sb.Append("<ns1:RequestorID ID=\"" + Username + "\" Type=\"4\"/>");
                sb.Append("<ns1:BookingChannel Type=\"12\" />");
                sb.Append("</ns1:Source>");
                sb.Append("</ns1:POS>");
                if (newFltDsR.Length > 0)
                    sb.Append("<ns1:AirItinerary DirectionInd=\"Return\">");
                else
                    sb.Append("<ns1:AirItinerary DirectionInd=\"OneWay\">");
                sb.Append("<ns1:OriginDestinationOptions>");

                for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                {
                    string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');
                    sb.Append("<ns1:OriginDestinationOption>");
                    sb.Append("<ns1:FlightSegment ArrivalDateTime=\"" + Convert.ToDateTime(fltdt.Rows[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" DepartureDateTime=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" RPH=\"" + NewSNO[1] + "\">");
                    sb.Append("<ns1:DepartureAirport LocationCode=\"" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "\" Terminal=\"TerminalX\"/>");
                    sb.Append("<ns1:ArrivalAirport LocationCode=\"" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "\" Terminal=\"TerminalX\"/>");
                    sb.Append("</ns1:FlightSegment>");
                    sb.Append("</ns1:OriginDestinationOption>");
                }

                sb.Append("</ns1:OriginDestinationOptions>");
                sb.Append("</ns1:AirItinerary>");
                sb.Append(" <ns1:TravelerInfoSummary>");
                sb.Append("<ns1:AirTravelerAvail>");
                sb.Append("<ns1:PassengerTypeQuantity Code=\"ADT\" Quantity=\"" + fltdt.Rows[0]["Adult"].ToString() + "\"/>");
                if (Convert.ToInt32(fltdt.Rows[0]["Child"]) > 0)
                    sb.Append("<ns1:PassengerTypeQuantity Code=\"CHD\" Quantity=\"" + fltdt.Rows[0]["Child"].ToString() + "\"/>");
                if (Convert.ToInt32(fltdt.Rows[0]["Infant"]) > 0)
                    sb.Append("<ns1:PassengerTypeQuantity Code=\"INF\" Quantity=\"" + fltdt.Rows[0]["Infant"].ToString() + "\"/>");
                sb.Append("</ns1:AirTravelerAvail>");
                sb.Append("</ns1:TravelerInfoSummary>");
                sb.Append("</ns1:OTA_AirPriceRQ>");
                sb.Append("</soap:Body>");
                sb.Append("</soap:Envelope>");
            }
            catch(Exception ex)
            {
                ExecptionLogger.FileHandling("PriceQuoteRequest", "Error_001", ex, "AirArabiaFlight");
            }
            return sb.ToString();
        }
       
        public List<FlightSearchResults> GetAirArabiaAvailibility_Oldest(FlightSearch objFlt, bool RTFS, string fareType, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, float srvCharge, List<FlightCityList> CityList, CredentialList CrdList)
        {
            List<FlightSearchResults> fsrList = new List<FlightSearchResults>(); List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            try
            {
                string AvailibilityRequest = AirArabiaAvailibilityRequest(objFlt, fareType, CrdList.UserID, CrdList.Password);
                AirArabiaUtility.SaveFile(AvailibilityRequest, "SearchReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");
                string[] res = Utility.Split(AirArabiaUtility.AirArabiaPostXML(CrdList.LoginPWD, AvailibilityRequest, ""), "##");
                String JSessionId = res[0].ToString();
                string AvailibilityResponse = res[1].ToString();
                AirArabiaUtility.SaveFile(AvailibilityResponse, "SearchResp_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");

                XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; XNamespace a = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";
                // List<PriceQuoteRequest> objpricequote;
                try
                {
                    if (AvailibilityResponse.IndexOf("No availability") < 1 || AvailibilityResponse.IndexOf("Invalid") < 1 || AvailibilityResponse == "" || AvailibilityResponse == null)
                    {
                        if (AvailibilityResponse.Contains("<ns1:Success />") || AvailibilityResponse.Contains("<ns1:Success/>"))
                        {
                            XDocument AirArabisDocument = XDocument.Parse(AvailibilityResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                            XElement AvailabilityResult = AirArabisDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirAvailRS");

                            IEnumerable<XElement> OriginDestinationInfo = AvailabilityResult.Elements(ns1 + "OriginDestinationInformation");

                            string EchoToken = AvailabilityResult.Attribute("EchoToken").Value;
                            string TransactionIdentifier = AvailabilityResult.Attribute("TransactionIdentifier").Value;
                            string Version = AvailabilityResult.Attribute("Version").Value;

                            List<OriginDestinationInformation> objArrDept = new List<OriginDestinationInformation>();
                            int Sequence = 0;
                            #region Sectors
                            foreach (var OrginDepartur in OriginDestinationInfo)
                            {
                                List<OriginDestinationOption> objOrgdesc = new List<OriginDestinationOption>();

                                int Segment = 0;
                                IEnumerable<XElement> OriginDestinationOption = OrginDepartur.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption");
                                foreach (var OrginDepartures in OriginDestinationOption)
                                {
                                    objOrgdesc.Add(new OriginDestinationOption
                                    {
                                        ArrivalDateTime = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value,
                                        DepartureDateTime = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value,
                                        FlightNumber = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value,
                                        RPH = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("RPH").Value,
                                        JourneyDuration = OrginDepartures.Element(ns1 + "FlightSegment").Attribute("JourneyDuration").Value,
                                        ArrivalAirport = OrginDepartures.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value,
                                        DepartureAirport = OrginDepartures.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value,
                                        Segment = Segment++,
                                    });

                                }
                                objArrDept.Add(new OriginDestinationInformation
                                {
                                    ArrivalDateTime = OrginDepartur.Element(ns1 + "ArrivalDateTime").Value,
                                    DepartureDateTime = OrginDepartur.Element(ns1 + "DepartureDateTime").Value,
                                    OriginLocationCode = OrginDepartur.Element(ns1 + "OriginLocation").Attribute("LocationCode").Value,
                                    DestinationLocationCode = OrginDepartur.Element(ns1 + "DestinationLocation").Attribute("LocationCode").Value,
                                    DestinationLocationName = OrginDepartur.Element(ns1 + "DestinationLocation").Value,
                                    OriginLocationName = OrginDepartur.Element(ns1 + "DestinationLocation").Value,
                                    OriginDestinationOptions = objOrgdesc,
                                    LineNo = Sequence++
                                });
                            }
                            #endregion
                            try
                            {
                                XElement PricedItinerary = AvailabilityResult.Element(ns1 + "AAAirAvailRSExt").Element(ns1 + "PricedItineraries").Element(ns1 + "PricedItinerary");
                                string AirPricingreq = "", AirPricingResp = "";
                                int lineNum = 1;
                                foreach (OriginDestinationInformation OrginDepartur in objArrDept)
                                {
                                    string AirPricingRequest = PriceQuoteRequest_WithAvaibilty(objFlt, OrginDepartur, fareType, CrdList.UserID, CrdList.Password, EchoToken, TransactionIdentifier, Version);
                                    string AirPricingResponse = AirArabiaUtility.AirArabiaPostXML(CrdList.LoginPWD, AirPricingRequest, JSessionId);
                                    AirPricingreq += AirPricingRequest;
                                    AirPricingResp += AirPricingResponse;
                                    if (AirPricingResponse.IndexOf("No availability") < 1 || AirPricingResponse.IndexOf("Invalid") < 1 || AirPricingResponse == "" || AirPricingResponse == null)
                                    {
                                        if (AirPricingResponse.Contains("<ns1:Success />") || AvailibilityResponse.Contains("<ns1:Success/>"))
                                        {
                                            XDocument AirArabispricingDocument = XDocument.Parse(AirPricingResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                            XElement OTA_AirPriceRS = AirArabispricingDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirPriceRS");
                                            XElement PriceQuetResult = OTA_AirPriceRS.Element(ns1 + "PricedItineraries").Element(ns1 + "PricedItinerary");
                                            XElement AirItineraryPricingInfo = PriceQuetResult.Element(ns1 + "AirItineraryPricingInfo");
                                            XElement AirItinerary = PriceQuetResult.Element(ns1 + "AirItinerary");

                                            DataTable CommDt = new DataTable(); Hashtable STTFTDS = new Hashtable();
                                            int seg = AirItinerary.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption").Count();
                                            if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                                seg = Decimal.ToInt32(seg / 2);
                                            else
                                                seg = seg - 1;
                                            int legno = 1, flight = 1;
                                            foreach (var Sectoresdtails in AirItinerary.Element(ns1 + "OriginDestinationOptions").Elements(ns1 + "OriginDestinationOption"))
                                            {
                                                FlightSearchResults objfsr = new FlightSearchResults();
                                                objfsr.Stops = seg.ToString() + "-Stop";
                                                objfsr.LineNumber = lineNum; //Segment.Count()
                                                objfsr.Flight = flight.ToString();
                                                objfsr.Leg = legno;
                                                legno++;
                                                objfsr.Adult = objFlt.Adult;
                                                objfsr.Child = objFlt.Child;
                                                objfsr.Infant = objFlt.Infant;

                                                objfsr.depdatelcc = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("DepartureDateTime").Value.Trim();//Convert.ToDateTime(fltdtl.Element(a + "STD").Value.Trim()).ToString();
                                                objfsr.arrdatelcc = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("ArrivalDateTime").Value.Trim(); //Convert.ToDateTime(fltdtl.Element(a + "STA").Value.Trim()).ToString();
                                                objfsr.Departure_Date = Convert.ToDateTime(objfsr.depdatelcc).ToString("dd MMM");
                                                objfsr.Arrival_Date = Convert.ToDateTime(objfsr.arrdatelcc).ToString("dd MMM");
                                                objfsr.DepartureDate = Convert.ToDateTime(objfsr.depdatelcc).ToString("ddMMyy");
                                                objfsr.ArrivalDate = Convert.ToDateTime(objfsr.arrdatelcc).ToString("ddMMyy");
                                                //
                                                objfsr.FlightIdentification = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("FlightNumber").Value.Trim();
                                                objfsr.AirLineName = "Air Arabia";
                                                objfsr.ValiDatingCarrier = "G9";
                                                objfsr.OperatingCarrier = "G9";
                                                objfsr.MarketingCarrier = "G9";
                                                objfsr.DepartureLocation = Sectoresdtails.Element(ns1 + "FlightSegment").Element(ns1 + "DepartureAirport").Attribute("LocationCode").Value.Trim();
                                                objfsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objfsr.DepartureLocation);
                                                objfsr.DepartureTime = Convert.ToDateTime(objfsr.depdatelcc).ToString("HHmm");
                                                objfsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.DepartureLocation);
                                                objfsr.DepartureTerminal = "";
                                                objfsr.DepAirportCode = objfsr.DepartureLocation;

                                                objfsr.ArrivalLocation = Sectoresdtails.Element(ns1 + "FlightSegment").Element(ns1 + "ArrivalAirport").Attribute("LocationCode").Value.Trim();
                                                objfsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objfsr.ArrivalLocation);
                                                objfsr.ArrivalTime = Convert.ToDateTime(objfsr.arrdatelcc).ToString("HHmm");
                                                objfsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objfsr.ArrivalLocation);
                                                objfsr.ArrivalTerminal = "";
                                                objfsr.ArrAirportCode = objfsr.ArrivalLocation;
                                                objfsr.Trip = objFlt.Trip.ToString();
                                                DateTime STA = Convert.ToDateTime(objfsr.arrdatelcc);
                                                TimeSpan tsTimeSpan = STA.Subtract(Convert.ToDateTime(objfsr.depdatelcc));
                                                objfsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpan.TotalMinutes));
                                                objfsr.TripCnt = GetTimeInHrsAndMin(Convert.ToInt32(tsTimeSpan.TotalMinutes));
                                                objfsr.Trip = objFlt.Trip.ToString();
                                                #region Adding Fare
                                                #region Fare Calculation
                                                IEnumerable<XElement> Faresbreakups = AirItineraryPricingInfo.Element(ns1 + "PTC_FareBreakdowns").Elements(ns1 + "PTC_FareBreakdown");
                                                objfsr.fareBasis = Faresbreakups.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.EQ = "";

                                                objfsr.AvailableSeats = "";//Sectoresdtails

                                                IEnumerable<XElement> objAdt = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "ADT");
                                                IEnumerable<XElement> objChd = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "CHD");
                                                IEnumerable<XElement> objInf = Faresbreakups.Where(x => x.Element(ns1 + "PassengerTypeQuantity").Attribute("Code").Value.Trim() == "INF");

                                                #region Adult Fare calculation
                                                decimal ABfare = 0, ATax = 0, AOT = 0;
                                                foreach (var adt in objAdt)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge = 0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    ABfare += (basefare * Quantity);
                                                    ATax += (Tax * Quantity);
                                                    AOT += (othrcharge * Quantity);
                                                }
                                                // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;

                                                objfsr.AdtBfare = (float)Math.Ceiling(ABfare);
                                                objfsr.AdtFSur = 0;
                                                objfsr.AdtWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                objfsr.AdtTax = (float)Math.Ceiling(ATax + AOT) + objfsr.AdtWO + srvCharge;
                                                objfsr.AdtOT = objfsr.AdtTax;
                                                objfsr.AdtCabin = "Y";
                                                objfsr.AdtRbd = "";
                                                objfsr.AdtFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.AdtFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                objfsr.AdtFare = objfsr.AdtTax + objfsr.AdtBfare + objfsr.AdtWO + objfsr.AdtFSur; //  + SegmentSellKey

                                                objfsr.FBPaxType = "ADT";
                                                objfsr.ElectronicTicketing = "";
                                                objfsr.AdtFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                                // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;

                                                objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());
                                                objfsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());

                                                CommDt.Clear();
                                                // CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo);
                                                CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                                objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS.Clear();
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, objFlt.TDS);
                                                objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                                objfsr.AdtEduCess = 0;
                                                objfsr.AdtHighEduCess = 0;
                                                objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());

                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.AdtFare.ToString()));
                                                        objfsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion

                                                #region Child Fare calculation
                                                decimal ChdBFare = 0, CTax = 0, COT = 0;
                                                foreach (var adt in objChd)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge = 0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    ChdBFare += (basefare * Quantity);
                                                    CTax += (Tax * Quantity);
                                                    COT += (othrcharge * Quantity);
                                                }
                                                // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;

                                                objfsr.ChdBFare = (float)Math.Ceiling(ChdBFare);
                                                objfsr.ChdFSur = 0;
                                                objfsr.ChdWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                objfsr.ChdTax = (float)Math.Ceiling(CTax + COT) + objfsr.ChdWO + srvCharge;
                                                objfsr.ChdOT = objfsr.ChdTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                objfsr.ChdCabin = "Y";
                                                objfsr.ChdRbd = "";
                                                objfsr.ChdFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.ChdfareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                objfsr.ChdFare = objfsr.ChdTax + objfsr.ChdBFare + objfsr.ChdWO + objfsr.ChdFSur; //  + SegmentSellKey

                                                objfsr.ChdFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                                // objfsr.AdtFar = "Baggage: " + seg.Baggage + " Hand Baggage: " + seg.CabinBaggage + "  " + fsr.AdtFar;

                                                objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());

                                                CommDt.Clear();
                                                // CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo);
                                                CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.ChdCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", seg.ToString());
                                                objfsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                objfsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS.Clear();
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, objFlt.TDS);
                                                objfsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                objfsr.ChdDiscount = objfsr.ChdDiscount1 - objfsr.ChdSrvTax1;
                                                objfsr.ChdEduCess = 0;
                                                objfsr.ChdHighEduCess = 0;
                                                objfsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objfsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());

                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.ChdFare.ToString()));
                                                        objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion

                                                #region Infant Fare calculation
                                                decimal INFBFare = 0, INFTax = 0, INFOtherTax = 0;
                                                foreach (var adt in objInf)
                                                {
                                                    XElement PassengerFare = adt.Element(ns1 + "PassengerFare");
                                                    IEnumerable<XElement> ServiceCharges = PassengerFare.Element(ns1 + "Taxes").Elements(ns1 + "Tax");
                                                    IEnumerable<XElement> Fees = PassengerFare.Element(ns1 + "Fees").Elements(ns1 + "Fee");
                                                    decimal basefare = Convert.ToDecimal(PassengerFare.Element(ns1 + "BaseFare").Attribute("Amount").Value.Trim());
                                                    decimal Tax = 0, othrcharge = 0;//
                                                    foreach (var ChargeType in ServiceCharges)
                                                    {
                                                        Tax += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    foreach (var ChargeType in Fees)
                                                    {
                                                        othrcharge += Convert.ToDecimal(ChargeType.Attribute("Amount").Value);
                                                    }
                                                    int Quantity = Convert.ToInt32(adt.Element(ns1 + "PassengerTypeQuantity").Attribute("Quantity").Value.Trim());
                                                    INFBFare += basefare * Quantity;
                                                    INFTax += Tax * Quantity;
                                                    INFOtherTax += othrcharge * Quantity;
                                                }
                                                // Adtbasefares += ABfare; AdtTaxs += ATax; AdtOT += AOT; AdtDisc += ADisc;

                                                objfsr.InfBfare = (float)Math.Ceiling(INFBFare);
                                                objfsr.InfFSur = 0;
                                                objfsr.InfWO = 0;//objadt.tcf != null ? float.Parse(objadt.tcf.amount.ToString()) : 0;
                                                objfsr.InfTax = (float)Math.Ceiling(INFTax + INFOtherTax) + objfsr.InfWO + srvCharge;
                                                objfsr.InfOT = objfsr.InfTax;//(float)Math.Round(AdtOT * currancyinfo[0].ExchangeRate, 4);//(objfsr.AdtTax - objfsr.AdtFSur) + (float)Math.Ceiling(AdtOT);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                objfsr.InfCabin = "Y";
                                                objfsr.InfRbd = "";
                                                objfsr.InfFarebasis = objAdt.ElementAt(0).Element(ns1 + "FareBasisCodes").Element(ns1 + "FareBasisCode").Value;
                                                objfsr.InfFareType = "";//fltdtl.refundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                //objfsr.AdtDiscount = (float)Math.Round(AdtDisc * currancyinfo[0].ExchangeRate, 4);
                                                objfsr.InfFare = objfsr.InfTax + objfsr.InfBfare + objfsr.InfWO + objfsr.InfFSur;
                                                objfsr.InfFar = "NRM"; //string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.InfBfare.ToString()), decimal.Parse(objfsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.InfFare.ToString()));
                                                        objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                                #endregion

                                                #endregion
                                                XElement MealBaggageDescription = Sectoresdtails.Parent;
                                                XElement bundledService = MealBaggageDescription.Element(ns1 + "AABundledServiceExt").Element(ns1 + "bundledService");
                                                string bundleinfo = "";
                                                if (bundledService != null)
                                                {
                                                    objfsr.BagInfo = bundledService.Element(ns1 + "description").Value;
                                                    bundleinfo = bundledService.Element(ns1 + "bunldedServiceId").Value.Trim();
                                                    bundleinfo += ":" + bundledService.Element(ns1 + "bundledServiceName").Value.Trim();
                                                    bundleinfo += ":" + bundledService.Element(ns1 + "perPaxBundledFee").Value.Trim();
                                                    bundleinfo += bundledService.Element(ns1 + "bookingClasses") != null ? ":" + bundledService.Element(ns1 + "bookingClasses").Value.Trim() : ":";
                                                }

                                                else
                                                    objfsr.BagInfo = "";
                                                objfsr.RBD = objfsr.AdtRbd + ":" + objfsr.ChdRbd + ":" + objfsr.InfRbd;
                                                objfsr.FareRule = Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("RPH").Value.Trim();
                                                objfsr.sno = OTA_AirPriceRS.Attribute("TransactionIdentifier").Value + ":" + Sectoresdtails.Element(ns1 + "FlightSegment").Attribute("RPH").Value.Trim() + ":" + JSessionId + ":" + OTA_AirPriceRS.Attribute("EchoToken").Value + ":" + OTA_AirPriceRS.Attribute("Version").Value + ":" + OTA_AirPriceRS.Attribute("SequenceNmbr").Value + ":" + AirItineraryPricingInfo.Element(ns1 + "ItinTotalFare").Element(ns1 + "BaseFare").Attribute("CurrencyCode").Value + ":" + seg.ToString() + ":" + bundleinfo;
                                                objfsr.Searchvalue = JSessionId;
                                                objfsr.OriginalTF = (float)(ABfare + ATax + AOT + ChdBFare + CTax + COT + INFBFare + INFTax + INFOtherTax);
                                                objfsr.OriginalTT = srvCharge;
                                                objfsr.Provider = "G9";
                                                //Rounding the fare
                                                objfsr.AdtFare = (float)Math.Ceiling(objfsr.AdtFare);
                                                objfsr.ChdFare = (float)Math.Ceiling(objfsr.ChdFare);
                                                objfsr.InfFare = (float)Math.Ceiling(objfsr.InfFare);

                                                objfsr.STax = objfsr.AdtSrvTax + objfsr.ChdSrvTax + objfsr.InfSrvTax;
                                                objfsr.TFee = objfsr.AdtTF + objfsr.ChdTF + objfsr.InfTF;
                                                objfsr.TotDis = objfsr.AdtDiscount + objfsr.ChdDiscount;
                                                objfsr.TotCB = objfsr.AdtCB + objfsr.ChdCB;
                                                objfsr.TotTds = objfsr.AdtTds + objfsr.ChdTds + objfsr.InfTds;
                                                objfsr.TotMgtFee = objfsr.AdtMgtFee + objfsr.ChdMgtFee + objfsr.InfMgtFee;
                                                objfsr.TotalFuelSur = objfsr.AdtFSur + objfsr.ChdFSur + objfsr.InfFSur;
                                                objfsr.TotalTax = objfsr.AdtTax + objfsr.ChdTax + objfsr.InfTax;
                                                objfsr.TotBfare = objfsr.AdtBfare + objfsr.ChdBFare + objfsr.InfBfare;
                                                objfsr.TotMrkUp = objfsr.ADTAdminMrk + objfsr.ADTAgentMrk + objfsr.CHDAdminMrk + objfsr.CHDAgentMrk;
                                                objfsr.TotalFare = objfsr.AdtFare + objfsr.ChdFare + objfsr.InfFare + objfsr.TotMrkUp + objfsr.STax + objfsr.TFee + objfsr.TotMgtFee;
                                                objfsr.NetFare = (objfsr.TotalFare + objfsr.TotTds) - (objfsr.TotDis + objfsr.TotCB + objfsr.ADTAgentMrk + objfsr.CHDAgentMrk);
                                                #endregion

                                                if (OrginDepartur.DestinationLocationCode == OrginDepartur.OriginLocationCode)
                                                    objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                else
                                                    objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                                if (objfsr.ArrAirportCode == objFlt.HidTxtDepCity.Split(',')[0])
                                                {
                                                    flight++;
                                                    legno = 1;
                                                    objfsr.Leg = legno;
                                                    objfsr.Flight = flight.ToString();
                                                }
                                                if (OrginDepartur.DestinationLocationCode == OrginDepartur.OriginLocationCode && flight == 2)
                                                {
                                                    objfsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                                    objfsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                                }
                                                else
                                                {
                                                    objfsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                                    objfsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                                }
                                                if (flight == 1)
                                                    objfsr.TripType = "O";
                                                else if (flight == 2)
                                                    objfsr.TripType = "R";
                                                else objfsr.TripType = "M";
                                                fsrList.Add(objfsr);
                                                //if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                                //{
                                                //    if (flight == 1)
                                                //        fsrList.Add(objfsr);
                                                //    else
                                                //        RfsrList.Add(objfsr);
                                                //}
                                                //else
                                                //    fsrList.Add(objfsr);    
                                            }
                                            lineNum++;
                                        }
                                    }

                                }
                                AirArabiaUtility.SaveFile("<AirPricing>" + AirPricingreq + "</AirPricing>", "AirPricingReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");
                                AirArabiaUtility.SaveFile("<AirPricing>" + AirPricingResp + "</AirPricing>", "AirPricingResp_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + objFlt.UID + "_");

                            }
                            catch (Exception ex)
                            {
                                ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight");
                            }
                        }
                    }

                }
                catch (Exception ex)
                { ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight"); }

                FinalList = fsrList;
                if (RfsrList.Count != 0)
                    FinalList = RfsrList;
                if (objFlt.TripType == TripType.RoundTrip && fsrList.Count > 0 && RfsrList.Count > 0)  //RTF True
                    FinalList = RoundTripFare(fsrList, RfsrList);
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("GetAirArabiaAvailibility", "Error_001", ex, "AirArabiaFlight");
            }
            return objFltComm.AddFlightKey(FinalList, RTFS);
        }
        private string MealAvailabilityRequest_Old(string UserId, string Password, DataTable fltdt)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                string[] SNO = fltdt.Rows[0]["sno"].ToString().Split(':');
                objreqXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                objreqXml.Append("<soapenv:Header>");
                objreqXml.Append("<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                objreqXml.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-17855236\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                objreqXml.Append("<wsse:Username>" + UserId + "</wsse:Username>");
                objreqXml.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
                objreqXml.Append("</wsse:UsernameToken>");
                objreqXml.Append("</wsse:Security>");
                objreqXml.Append("</soapenv:Header>");
                objreqXml.Append("<soapenv:Body xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">");
                objreqXml.Append("<ns2:AA_OTA_AirMealDetailsRQ EchoToken=\"" + SNO[3] + "\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" TransactionIdentifier=\"" + SNO[0] + "\" Version=\"" + SNO[4] + "\">");
                objreqXml.Append("<ns2:POS>");
                objreqXml.Append("<ns2:Source TerminalID=\"TestUser/Test Runner\">");
                objreqXml.Append("<ns2:RequestorID ID=\"" + UserId + "\" Type=\"4\" />");
                objreqXml.Append("<ns2:BookingChannel Type=\"12\" />");
                objreqXml.Append("</ns2:Source>");
                objreqXml.Append("</ns2:POS>");
                objreqXml.Append("<ns2:MealDetailsRequests>");

                #region Sector info
                DataRow[] newFltDsIN = fltdt.Select("TripType='R'");
                DataRow[] newFltDsOut = fltdt.Select("TripType='O'");
                for (int m = 0; m < newFltDsOut.Length; m++)
                {
                    string[] SNONew = newFltDsOut[m]["sno"].ToString().Split(':');
                    objreqXml.Append("<ns2:MealDetailsRequest>");
                    objreqXml.Append("<ns2:FlightSegmentInfo ArrivalDateTime=\"" + Convert.ToDateTime(newFltDsOut[m]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" DepartureDateTime=\"" + Convert.ToDateTime(newFltDsOut[m]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + newFltDsOut[m]["FlightIdentification"].ToString() + "\" RPH=\"" + SNONew[1] + "\">");
                    objreqXml.Append("<ns2:DepartureAirport LocationCode=\"" + newFltDsOut[m]["DepartureLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                    objreqXml.Append("<ns2:ArrivalAirport LocationCode=\"" + newFltDsOut[m]["ArrivalLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                    objreqXml.Append("<ns2:OperatingAirline Code=\"G9\"/> ");
                    objreqXml.Append("</ns2:FlightSegmentInfo>");
                    objreqXml.Append("</ns2:MealDetailsRequest>");
                }
                if (newFltDsIN.Length > 0)
                {
                    for (int m = 0; m < newFltDsIN.Length; m++)
                    {
                        string[] SNONew = newFltDsIN[m]["sno"].ToString().Split(':');
                        objreqXml.Append("<ns2:MealDetailsRequest>");
                        objreqXml.Append("<ns2:FlightSegmentInfo ArrivalDateTime=\"" + Convert.ToDateTime(newFltDsIN[m]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" DepartureDateTime=\"" + Convert.ToDateTime(newFltDsIN[m]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + newFltDsIN[m]["FlightIdentification"].ToString() + "\" RPH=\"" + SNONew[1] + "\">");
                        objreqXml.Append("<ns2:DepartureAirport LocationCode=\"" + newFltDsIN[m]["DepartureLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                        objreqXml.Append("<ns2:ArrivalAirport LocationCode=\"" + newFltDsIN[m]["ArrivalLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                        objreqXml.Append("<ns2:OperatingAirline Code=\"G9\"/> ");
                        objreqXml.Append("</ns2:FlightSegmentInfo>");
                        objreqXml.Append("</ns2:MealDetailsRequest>");
                    }
                }
                #endregion

                objreqXml.Append("</ns2:MealDetailsRequests>");
                objreqXml.Append("</ns2:AA_OTA_AirMealDetailsRQ></soapenv:Body>");
                objreqXml.Append("</soapenv:Envelope>");
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("MealAvailabilityRequest", "Error_001", ex, "AirArabiaFlight");
            }
            return objreqXml.ToString();
        }
        public string PriceQuoteRequest_WithAvaibilty(FlightSearch objFlt, OriginDestinationInformation objArrDept, string fareType, string Username, string Password, string EchoToken, string TransactionIdentifier, string Version)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
            sb.Append("<soap:Header>");
            sb.Append("<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
            sb.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-32124385\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
            sb.Append("<wsse:Username>" + Username + "</wsse:Username>");
            sb.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
            sb.Append("</wsse:UsernameToken>");
            sb.Append("</wsse:Security>");
            sb.Append("</soap:Header>");
            sb.Append("<soap:Body xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\">");
            sb.Append("<ns1:OTA_AirPriceRQ EchoToken=\"" + EchoToken + "\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" TransactionIdentifier=\"" + TransactionIdentifier + "\" Version=\"" + Version + "\">");
            sb.Append("<ns1:POS>");
            sb.Append("<ns1:Source TerminalID=\"TestUser/Test Runner\">");
            sb.Append("<ns1:RequestorID ID=\"" + Username + "\" Type=\"4\"/>");
            sb.Append("<ns1:BookingChannel Type=\"12\" />");
            sb.Append("</ns1:Source>");
            sb.Append("</ns1:POS>");
            if (objFlt.TripType.ToString() == "RoundTrip")
                sb.Append("<ns1:AirItinerary DirectionInd=\"Return\">");
            else
                sb.Append("<ns1:AirItinerary DirectionInd=\"OneWay\">");
            sb.Append("<ns1:OriginDestinationOptions>");

            foreach (var OrigDest in objArrDept.OriginDestinationOptions)
            {
                sb.Append("<ns1:OriginDestinationOption>");
                sb.Append("<ns1:FlightSegment ArrivalDateTime=\"" + OrigDest.ArrivalDateTime + "\" DepartureDateTime=\"" + OrigDest.DepartureDateTime + "\" FlightNumber=\"" + OrigDest.FlightNumber + "\" RPH=\"" + OrigDest.RPH + "\">");
                sb.Append("<ns1:DepartureAirport LocationCode=\"" + OrigDest.DepartureAirport + "\" Terminal=\"TerminalX\"/>");
                sb.Append("<ns1:ArrivalAirport LocationCode=\"" + OrigDest.ArrivalAirport + "\" Terminal=\"TerminalX\"/>");
                sb.Append("</ns1:FlightSegment>");
                sb.Append("</ns1:OriginDestinationOption>");
            }

            sb.Append("</ns1:OriginDestinationOptions>");
            sb.Append("</ns1:AirItinerary>");
            sb.Append(" <ns1:TravelerInfoSummary>");
            sb.Append("<ns1:AirTravelerAvail>");
            sb.Append("<ns1:PassengerTypeQuantity Code=\"ADT\" Quantity=\"" + objFlt.Adult.ToString() + "\"/>");
            if (objFlt.Child > 0)
                sb.Append("<ns1:PassengerTypeQuantity Code=\"CHD\" Quantity=\"" + objFlt.Child.ToString() + "\"/>");
            if (objFlt.Infant > 0)
                sb.Append("<ns1:PassengerTypeQuantity Code=\"INF\" Quantity=\"" + objFlt.Infant.ToString() + "\"/>");
            sb.Append("</ns1:AirTravelerAvail>");
            sb.Append("</ns1:TravelerInfoSummary>");
            sb.Append("</ns1:OTA_AirPriceRQ>");
            sb.Append("</soap:Body>");
            sb.Append("</soap:Envelope>");
            return sb.ToString();
        }
        
    }
}
