﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;

namespace STD.BAL.TBO
{
    class TBOAuthProcess
    {
        public string GetTBOToken(string url,string clientId,string Username,string Pwd,string ip)
        {
            authDetails objauthDetails = new authDetails();
            objauthDetails.ClientId = clientId;//"ApiIntegration";
            objauthDetails.UserName = Username;//"cashitz";
            objauthDetails.Password = Pwd;// "cashitz@123";
            objauthDetails.EndUserIp = ip;// "125.22.194.74";

            string strJsonData = JsonConvert.SerializeObject(objauthDetails);

            string strJsonResponse = GetResponse(url, strJsonData);


            JObject objJson = JObject.Parse(strJsonResponse);
            return (string)objJson.SelectToken("TokenId");
        }
        public static string GetResponse(string url, string requestData)
        {
            string responseXML = string.Empty;
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("Accept-Encoding", "gzip");
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                if (rsp == null)
                {
                    //throw exception
                }



                if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
                {
                    using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
                    {
                        responseXML = readStream.ReadToEnd();
                    }


                }
                else
                {
                    StreamReader reader = new StreamReader(rsp, Encoding.Default);
                    responseXML = reader.ReadToEnd();
                }

            }
            catch (WebException webEx)
            {
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseXML;
        }

        //public static string GetResponse(string url, string requestData)
        //{
        //    string responseXML = string.Empty;
        //    try
        //    {
        //        byte[] data = Encoding.UTF8.GetBytes(requestData);
        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";
        //        request.Headers.Add("Accept-Encoding", "gzip");
        //        Stream dataStream = request.GetRequestStream();
        //        dataStream.Write(data, 0, data.Length);
        //        dataStream.Close();
        //        WebResponse webResponse = request.GetResponse();
        //        var rsp = webResponse.GetResponseStream();
        //        if (rsp == null)
        //        {
        //            //throw exception
        //        }
        //        using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
        //        {
        //            responseXML = readStream.ReadToEnd();
        //        }

        //    }
        //    catch (WebException webEx)
        //    {
        //        //get the response stream
        //        WebResponse response = webEx.Response;
        //        Stream stream = response.GetResponseStream();
        //        String responseMessage = new StreamReader(stream).ReadToEnd();
        //    }
        //    return responseXML;
        //}
    }
    public class authDetails
    {
        public string ClientId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EndUserIp { get; set; }
    }
    
}
